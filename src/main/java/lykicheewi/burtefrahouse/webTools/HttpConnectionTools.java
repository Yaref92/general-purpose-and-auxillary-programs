/**
 * 
 */
package lykicheewi.burtefrahouse.webTools;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

/** 
 *
 * @author Yaron Efrat
 */
public class HttpConnectionTools {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Auto-generated method stub

	}

	/**
	 * @since 0.11.0 (17.07.2020 00:40)
	 * @param url
	 * @return
	 * @throws IOException 
	 */
	public static HttpURLConnection getConnectionFromURL(URL url) throws IOException {
		// Auto-generated method stub
		return (HttpURLConnection) url.openConnection();
	}
	
	/**
	 * @since 0.11.0 (17.07.2020 00:46)
	 * @param apiUrl Basic URL of the Api for the requests
	 * @param urlParameters the relevant parameters for the GET request
	 * @param requestMap This map is optional. It is for adjusting the request.
	 * @return
	 * @throws IOException 
	 */
	public static InputStream performGetRequest(String apiUrl, String urlParameters, HashMap<String, String> requestMap) throws IOException {
		/*
		 * When going for a GET request, simply
		 * use the basic WRL + ? + parameters
		 */
		URL url = new URL(apiUrl + "?" + urlParameters);
		HttpURLConnection connection = getConnectionFromURL(url);
		adjustConnection(connection, "GET", requestMap);		
		return connection.getInputStream();
	}
	
	/**
	 * Perform a post request.
	 * @since 0.11.0 (24.07.2020 15:27)
	 * @param apiUrl Basic URL of the Api for the requests
	 * @param urlParameters the relevant parameters for the GET request
	 * @param requestMap This map is optional. It is for adjusting the request.
	 * @apiNote Due to errors, I decided to add a method so that you can return 
	 * either a string or a stream
	 * @return
	 * @throws IOException 
	 */
	public static InputStream performPostRequest(String apiUrl, String urlParameters, HashMap<String, String> requestMap) throws IOException {
		/*
		 * When going for a POST request,
		 * use the basic URL 
		 * with a request body
		 */
		//URL url = new URL(apiUrl + "?" + urlParameters);
		URL url = new URL(apiUrl);
		HttpURLConnection connection = getConnectionFromURL(url);
		adjustConnection(connection, "POST", requestMap);
		connection.setDoOutput(true); // For POST request body to be allowed
		connection.connect(); // tried adding .connect() to solve a malformed json 2.0.0 02.11.2020 10:25
		//Send request
//		/*
//		 * Trying out java tutorial's
//		 * approach
//		 */
//		OutputStreamWriter wr = new OutputStreamWriter (
//				connection.getOutputStream());
//		wr.write(urlParameters);
		DataOutputStream wr = new DataOutputStream (
		        connection.getOutputStream());
		wr.writeBytes(urlParameters.trim()); // tried adding .trim() to solve a malformed json 2.0.0 02.11.2020 10:25
		wr.flush();
		InputStream input = connection.getInputStream();
		wr.close();
		return input;
	}
	
	/**
		 * Perform a post request.
		 * @since 0.11.0 (30.07.2020 10:55)
		 * @param apiUrl Basic URL of the Api for the requests
		 * @param urlParameters the relevant parameters for the GET request
		 * @param requestMap This map is optional. It is for adjusting the request.
		 * @apiNote Due to errors, I decided to overload it so that you can return 
		 * either a string or a stream
		 * @return
		 * @throws IOException 
		 */
		public static String performPostRequestStr(String apiUrl, String urlParameters, HashMap<String, String> requestMap) throws IOException {
			
			InputStream input = performPostRequest(apiUrl, urlParameters, requestMap);
			String response = getResponseAsString(input);
			/*
			 * TESTTESTTEST
			 */
			return response;
		}

	/**
	 * Gets the imput stream from a request and returns the
	 * response as String.
	 * @since 0.11.0 (24.07.2020 15:39)
	 * @param input the input stream received from the server
	 * following a request
	 * @return the response as a String
	 * @throws IOException 
	 */
	public static String getResponseAsString(InputStream input) throws IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(input));
		StringBuffer response = new StringBuffer(); // or StringBuffer if Java version 5+
		String line;
		while ((line = rd.readLine()) != null) {
			response.append(line);
			response.append('\r');
		}
		String responseString = response.toString();
		rd.close();
		return responseString;
		

	}

	/**
	 * Sets the request method and all the request properties
	 * that are included in the map
	 * @since 0.11.0 (17.07.2020 00:52)
	 * @param connection
	 * @param requestMethod 
	 * @param requestMap
	 * @throws ProtocolException 
	 */
	private static void adjustConnection(HttpURLConnection connection, String requestMethod, HashMap<String, String> requestMap) throws ProtocolException {
		// Auto-generated method stub
		connection.setRequestMethod(requestMethod);
		for (HashMap.Entry<String, String> entry : requestMap.entrySet()) {
			connection.setRequestProperty(entry.getKey(), 
					entry.getValue());
		}
		connection.setUseCaches(false);
	}

}
