/**
 * 
 */
package lykicheewi.burtefrahouse.webTools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.HelperMethod;
import lykicheewi.burtefrahouse.annotations.methodAnnotations.OverloadingWrapperMethod;
import lykicheewi.burtefrahouse.annotations.methodAnnotations.UncheckedCastPresent;
import lykicheewi.burtefrahouse.miscTools.StringTools;

/** 
 * Added because I was messing with JSON stuff
 * @since 0.11.0 (30.07.2020 13:20)
 * @author Yaron Efrat
 */
public class JSONAuxTools {

	/**
	 * I realized that there were placed were I repeatedly
	 * did this anyways. Though I might as well make it a method
	 * @implNote I added the 4 type parameters, because I was
	 * constantly faced with type ambiguities when messing
	 * around with JSON. <br>
	 * They caused bugs and I decided
	 * I'll solve this by having a more general method where I can
	 * tell the method what to expect. <br>
	 * 0.13.0 (01.10.2020 12:32)
	 * @apiNote originally from MMMT 1.6.0 (01.10.2020 10:35:06 AM)
	 * @since 0.13.0 (01.10.2020 12:25)
	 * @param mapList
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@UncheckedCastPresent(severity = "Unknown")
	public static <K1, V1, K2, V2> HashMap<K1, V1> extractMapFromMapList(
			ArrayList<HashMap<K2, V2>> mapList) {
		return (HashMap<K1, V1>) mapList.get(0);
	}
	
	/**
	 * This method simply surrounds the String with
	 * "result":{ and }] if necessary.
	 * Other wise it sends it as is to the overloaded
	 * method.
	 * @apiNote This is a convenience method for
	 * cases where I wanted to extract from a JSON
	 * String without "result" or only a part of
	 * a JSON String. I went with this instead of
	 * changing the other method for saving time.
	 * Might change.
	 * @since 0.12.0 (06.09.2020 10:56:30)
	 * @param JSONString
	 * @param wrapWithResultString
	 * @implNote Changed stuff to LinkedHashMap
	 * to preserve insertion order
	 * (1.5.0 07.09.2020 23:19)
	 * @return
	 */
	@OverloadingWrapperMethod
	@HelperMethod
	public static ArrayList<HashMap<String, String>> extractMapListFromJSONString(String JSONString, 
			boolean wrapWithResultString) {
		if (wrapWithResultString) {
			/*
			 * the concatanations in the next part are due to the way I've made the extractMapListFromJson
			 * String.
			 */
			return extractMapListFromJSONString("\"result\":[{".concat(JSONString).concat("}]"));
		}
		else {
			return extractMapListFromJSONString(JSONString);
		}
	}
	/**
	 * Takes a JSON String and returns a list of maps of Strings
	 * representing the data.
	 * @implNote Since probably every JSON can be turned into an
	 * ArrayList<HashMap<String, String>>, I feel fine with the way
	 * it is done here.
	 * @implNote WARNING: I had to use ? extends Object and then
	 * an unchecked cast for it to work. <br>
	 * I know it is not the best practice, but I must push forward
	 * @since 0.11.0 (30.07.2020 13:21)
	 * @param JSONString
	 * @implNote Changed stuff to LinkedHashMap
	 * to preserve insertion order
	 * (1.5.0 07.09.2020 23:19)
	 * @return
	 */
	public static ArrayList<HashMap<String, String>> extractMapListFromJSONString(String JSONString) {
		/*
		 * Thanks to
		 * Bruno Grieder
		 * on
		 * https://stackoverflow.com/questions/19977979/converting-json-to-xml-in-java
		 * for suggesting the json library
		 */
		/*
		 * Because JSON object need to start with { and end with },
		 * and because I had to tke only part, I re add it here
		 */
		JSONObject json = new JSONObject("{" + JSONString + "}");
		/*
		 * The next three lines came after experimentation
		 * and thanks to 
		 * Salandur
		 * on
		 * https://stackoverflow.com/questions/933447/how-do-you-cast-a-list-of-supertypes-to-a-list-of-subtypes
		 */
		JSONArray jsonarray = json.getJSONArray("result");
		ArrayList<? extends Object> jsonList = (ArrayList<Object>) jsonarray.toList();
		@SuppressWarnings("unchecked") // :(
		ArrayList<HashMap<String, String>> mapList = (ArrayList<HashMap<String, String>>) jsonList;
		return mapList;
	}
	
	/**
	 * This method simply surrounds the String with
	 * "result":{ and }] if necessary.
	 * Other wise it sends it as is to the overloaded
	 * method.
	 * @apiNote This is a convenience method for
	 * cases where I wanted to extract from a JSON
	 * String without "result" or only a part of
	 * a JSON String. I went with this instead of
	 * changing the other method for saving time.
	 * Might change.
	 * @since 0.13.0 (07.10.2020 12:50:30 PM)
	 * @param <U>
	 * @param JSONString
	 * @param typeExample
	 * @return
	 */
	@OverloadingWrapperMethod
	public static <U> ArrayList<HashMap<String, LinkedHashMap<String, String>>> extractMapListFromJSONString(String JSONString, U typeExample, boolean wrapWithResultString) {
		if (wrapWithResultString) {
			/*
			 * the concatanations in the next part are due to the way I've made the extractMapListFromJson
			 * String.
			 */
			return extractMapListFromJSONString("\"result\":[{".concat(JSONString).concat("}]"), typeExample);
		}
		else {
			return extractMapListFromJSONString(JSONString, typeExample);
		}
	}

	/**
	 * Takes a JSON String and returns a list of maps of Strings
	 * representing the data.
	 * @implNote Since probably every JSON can be turned into an
	 * ArrayList<HashMap<String, String>>, I feel fine with the way
	 * it is done here.
	 * @implNote WARNING: I had to use ? extends Object and then
	 * an unchecked cast for it to work. <br>
	 * I know it is not the best practice, but I must push forward
	 * @apiNote My unchecked cast at {@link JSONAuxTools#extractMapListFromJSONString(String)}
	 * came back to bite me in the a$$, so I was forced to improvise We shall name that
	 * FIX202009071111
	 * @since 0.12.0 (07.09.2020 11:11)
	 * @param JSONString
	 * @implNote Changed stuff to LinkedHashMap
	 * to preserve insertion order
	 * (1.5.0 07.09.2020 23:19)
	 * @return
	 */
	public static <U> ArrayList<HashMap<String, LinkedHashMap<String, String>>> extractMapListFromJSONString(String JSONString, U typeExample) {
		/*
		 * Thanks to
		 * Bruno Grieder
		 * on
		 * https://stackoverflow.com/questions/19977979/converting-json-to-xml-in-java
		 * for suggesting the json library
		 */
		/*
		 * Because JSON object need to start with { and end with },
		 * and because I had to tke only part, I re add it here
		 */
		JSONObject json = new JSONObject("{" + JSONString + "}");
		/*
		 * The next three lines came after experimentation
		 * and thanks to 
		 * Salandur
		 * on
		 * https://stackoverflow.com/questions/933447/how-do-you-cast-a-list-of-supertypes-to-a-list-of-subtypes
		 */
		JSONArray jsonarray = json.getJSONArray("result");
		ArrayList<? extends Object> jsonList = (ArrayList<Object>) jsonarray.toList();
		@SuppressWarnings("unchecked") // :(
		ArrayList<HashMap<String, LinkedHashMap<String, String>>> mapList = (ArrayList<HashMap<String, LinkedHashMap<String, String>>>) jsonList;
		return mapList;
	}
	
	/**
	 * Like many methods, this one was born
	 * when repeated code was discovered. <br>
	 * The necessary changed to adhere
	 * to this new method will be labeled
	 * REHAUL202010011048
	 * @apiNote originally from MMMT 1.6.0 (01.10.2020 10:48:47 AM)
	 * @since 0.13.0 (01.10.2020 12:27)
	 * @apiNote After abouy 5 minutes, decided to generalize not only
	 * for title, but for the other necessities as well.
	 * @implNote This method was adapted from existing code.
	 * it was, however, made more flexible to help
	 * generalize it
	 * @param wholeString A string containing among others
	 * "keyString":"..."
	 * @param keyString TODO
	 * @param searchIndex Since searchIndex has two ways it needs to be defined:
	 * an initial one and subsequent ones depending on the search, <br>
	 * decided to add that. if equal to -1, perform initial on.
	 * else perform the other one.
	 * @param endFlag 
	 * @param unwrap Added at 1.6.0 01.10.2020 11:16 because sometimes
	 * "keyString":" and the closing " was used later
	 * @return Only the actual value associated with keyString in the wholeString
	 */
	public static String extractMapValueFromJSONStringByKey(String wholeString, String keyString, int searchIndex, String endFlag, boolean unwrap) {
		if (searchIndex == -1) {
			searchIndex = wholeString.indexOf(",\"" + keyString) + 1; 
		} else {
			searchIndex = wholeString.indexOf(",",searchIndex) + 1;
		}
		
		String tempString = StringTools.extractRelevantPart(wholeString,
				"\"" + keyString, searchIndex, endFlag, searchIndex); // 1.6.0 01.10.2020 09:33 for the split to subquestions
//		int beginIndex = keyString.length() + "\"\":\"".length(); // Could have written + 4, but I wanted to show where this 4 comes from i.e. the wrapper of the keyString
		if (unwrap) {
//			return tempString.substring(beginIndex, tempString.length() - 1);
			return StringTools.unwrap(tempString, "\"" + keyString + "\":\"", -1);
		} else {
			return tempString;
		}		
	}
	
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
