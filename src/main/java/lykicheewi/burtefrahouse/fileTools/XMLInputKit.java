/**
 * 
 */
package lykicheewi.burtefrahouse.fileTools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.DOMBuilder;
import org.xml.sax.SAXException;

import lykicheewi.burtefrahouse.miscTools.XMLAuxTools;
import lykicheewi.burtefrahouse.miscTools.XMLAuxTools.ExpandedElement;

/** 
 * @author Yaron Efrat
 * NOTE 30.03.2020, 10:10:
 * I wanted to create something flexible, for an XML file 
 * with a variable structure. The decision of which classes
 * and data structures to use has been challenging.
 * Two things caught my eye for this purpose:
 * List<Element> (Thanks https://www.tutorialspoint.com/java_xml/java_jdom_parse_document.htm)
 * and XPath (thanks https://dzone.com/articles/java-and-xml-part-2).
 * I am considering changing from List<Element> to Map<Element,Element>,
 * to have a tree like structure where the key is the parent Element and the 
 * value is the child element. Maybe even a class called TreeMap, however 
 * it depends on the types implementing Comparator interface.
 * Which it doesn't.
 * Now that I've read a little more into it, I might instead create a class 
 * of ExpandedElement, that has the list of Attribute associated with it,
 * and the Text. Now, I obviously know there are JDOM methods for getting
 * the children of a Node, the Attributes of an Element and the Text of an
 * Element, but I want to have more control and to have it my way.
 * So that I can better understand what's going on and more easily operate
 * on any level I need. Maybe instead of list of Attributes, a Map of 
 * Attribute names and values.
 * UPDATE 1.4.2020 11:32 - I've added XMLAuxTools a couple of days ago, so
 * I'll change my plans for the map to be <Element, ExpandedElement>.
 * CLARIFICATION 1.4.2020, 12:26 - While technically, everything that can get done
 * with ExpandedElement can be done with Element, it is more convenient for me
 * to use my custom made class, even at the cost of extra space usage.
 */
public class XMLInputKit extends FileInputKit {

	
    /**
	 * 
	 */
	private static final long serialVersionUID = -613968704892390810L;
	private Document doc; // A Document object for loading and manipulating XML structure and elements
    Element rootElement; //
    /**
     * @implNote On 27.07.2020 I changed the values from 
     * Expanded Element to an ArrayList of them
     * @since 30.03.2020?
     */
    HashMap<Element, ArrayList<ExpandedElement>> elementMap;
	
	/**
	 * See FileInputKit constructors
	 * @param fileIdentifier
	 * @param safeWord
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public XMLInputKit(String fileIdentifier, String safeWord) throws ParserConfigurationException, SAXException, IOException {
		super(fileIdentifier, safeWord);
		documentConstructor();
	}

	/**
	 * See FileInputKit constructors
	 * @param fileIdentifier
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public XMLInputKit(String fileIdentifier) throws ParserConfigurationException, SAXException, IOException {
		super(fileIdentifier);
		documentConstructor();
	}
	
	/**
	 * See FileInputKit constructors
	 * @since 0.11.0 (01.08.2020 00:29)
	 * @apiNote Added because I got tired of clicking yes yes all the time
	 * @param fileIdentifier
	 * @param shouldAttributes
	 * @param shouldNamespace
	 * @throws IOException
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public XMLInputKit(String fileIdentifier, boolean shouldAttributes, boolean shouldNamespace) throws IOException, ParserConfigurationException, SAXException {
		super(fileIdentifier);
		documentConstructor(shouldAttributes, shouldNamespace);
	}

	/**
	 * See FileInputKit constructors
	 * @apiNote 0.9.2 (20.4.2020 10:17) Changed to match the JPanel in FileInput kit
	 * @param differentSignature
	 * @param safeWord
	 * @throws IOException
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public XMLInputKit(JPanel parent, String safeWord) throws IOException, ParserConfigurationException, SAXException {
		super(parent, safeWord, null);
		documentConstructor();
	}
	
	/**
	 * See FileInputKit constructors
	 * @apiNote 0.9.2 (20.4.2020 10:17) Changed to match the JPanel in FileInput kit
	 * @throws IOException
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public XMLInputKit() throws IOException, ParserConfigurationException, SAXException {
		super((JPanel) null);
		documentConstructor();
	}
	
	/**
	 * See FileInputKit constructors
	 * @since 0.11.0 (01.08.2020 00:23)
	 * @apiNote Added because I got tired of clicking yes yes all the time
	 * @param shouldAttributes
	 * @param shouldNamespace
	 * @throws IOException
	 * @throws SAXException 
	 * @throws ParserConfigurationException 
	 */
	public XMLInputKit(boolean shouldAttributes, boolean shouldNamespace) throws IOException, ParserConfigurationException, SAXException {
		super((JPanel) null);
		documentConstructor(shouldAttributes, shouldNamespace);
	}

	/**
	 * 
	 * Thanks to tutorialspoint's XML Query tutorial
	 * Also to https://howtodoinjava.com/xml/jdom2-read-parse-xml-examples/
	 *
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void documentConstructor() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    org.w3c.dom.Document w3cDocument = dBuilder.parse(inputFile);
	    doc = new DOMBuilder().build(w3cDocument);
	    rootElement = doc.getRootElement();
	    //elementMapper();
	    elementMap = XMLAuxTools.elementMapper(rootElement);
	}

	/**
	 * 
	 * Thanks to tutorialspoint's XML Query tutorial
	 * Also to https://howtodoinjava.com/xml/jdom2-read-parse-xml-examples/
	 * @since 0.11.0 (01.08.2020 00:23)
	 * @apiNote Added because I got tired of clicking yes yes all the time
	 * @param shouldAttributes
	 * @param shouldNamespace
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 * @throws IOException
	 */
	private void documentConstructor(boolean shouldAttributes, boolean shouldNamespace) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    org.w3c.dom.Document w3cDocument = dBuilder.parse(inputFile);
	    doc = new DOMBuilder().build(w3cDocument);
	    rootElement = doc.getRootElement();
	    //elementMapper(shouldAttributes, shouldNamespace);
	    elementMap = XMLAuxTools.elementMapper(rootElement, shouldAttributes, shouldNamespace);
		
	}

	/**
	 * Decided I would need a queue to keep track of when I
	 * run out of elements.
	 * @implNote On 27.07.2020 I changed the values 
	 * of elementMap from 
     * Expanded Element to an ArrayList of them - CHANGE202007271219
     * @implNote Overloaded on 01.08.2020 00:26
     * @since 1.4.2020, ~11:45-50
     * @deprecated 0.12.0 (11.08.2020 13:42) once I realized I'll need for both and it's a waste to duplicate
     * code. <br>
     * Use {@link #XMLAuxTools}'s static elmentMapper method instead
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private void elementMapper() {
		/*
		 * First of all, using a JOptionPane, the user is asked whether the
		 * attribute maps should be created.
		 * Then another one for whether the namespaces be pulled.
		 * The results are recorded in two booleans using a comparison to the JOptionPane
		 * Yes option value.
		 */
		boolean mapAttributes = (JOptionPane.YES_OPTION == 
				JOptionPane.showOptionDialog(null,
						"Should the attribute maps for the XML elements be created?", "Attribute maps",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION));
		boolean getNamespace = (JOptionPane.YES_OPTION == 
				JOptionPane.showOptionDialog(null,
						"Should the namespaces for the XML elements be pulled?", "Namespaces",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION));
		//boolean mapAttributes = true, getNamespace = true;
		elementMapper(mapAttributes, getNamespace);
		
	}

	/**
	 * Decided I would need a queue to keep track of when I
	 * run out of elements.
	 * @implNote On 27.07.2020 I changed the values 
	 * of elementMap from 
     * Expanded Element to an ArrayList of them - CHANGE202007271219
     * @apiNote Changed visibiliry to protected 1.4.0 (11.08.2020 11:49)
     * so XMLOutputKit could access it
     * @since 0.11.0 (01.08.2020 00:25)
	 * @apiNote Added because I got tired of clicking yes yes all the time
	 * @deprecated 0.12.0 (11.08.2020 13:42) once I realized I'll need for both and it's a waste to duplicate
     * code. <br>
     * Use {@link #XMLAuxTools}'s static elmentMapper method instead
	 */
	@Deprecated
	protected void elementMapper(boolean shouldAttributes, boolean shouldNamespace) {
		/*
		 * A Queue to help keep track of whether there are still
		 * Elements to add to the mapping. Kinda like a BFS.
		 */
		Queue<Element> elementQueue = new LinkedList<Element>();
		/*
		 * We first add the root. We also define a currentElement for each 
		 * iteration of the While loop, and we initialize the Map
		 */		
		elementQueue.add(rootElement);
		Element currentElement;
		elementMap = new HashMap<Element, ArrayList<XMLAuxTools.ExpandedElement>>();
		/*
		 * The while continues while the queue is not empty.
		 * it polls out the Element and then foreach through the children and add them.
		 */
		while(!elementQueue.isEmpty()) {
			currentElement = elementQueue.poll();
			elementMap.put(currentElement, new ArrayList<XMLAuxTools.ExpandedElement>()); // Added for CHANGE202007271219
			ArrayList<XMLAuxTools.ExpandedElement> childrenList = elementMap.get(currentElement); // Added for CHANGE202007271219
			for (Element childElement : currentElement.getChildren()) {
				/*
				 * updated for CHANGE202007271219
				 */
				childrenList.add(new ExpandedElement(childElement, shouldAttributes, shouldNamespace));
				elementQueue.add(childElement);
			}
		}
		
	}

	/**
	 * In order to facilitate editing, this method was added.
	 * @since 0.11.0 (28.07.2020 14:15)
	 * @return the doc
	 */
	public Document getDoc() {
		return doc;
	}

	/**
	 * @implNote On 27.07.2020 I changed the values 
	 * of elementMap from 
     * Expanded Element to an ArrayList of them
     * @since 30.03.2020?
	 * @return the elementMap
	 */
	public HashMap<Element, ArrayList<ExpandedElement>> getElementMap() {
		return elementMap;
	}

	/**
	 * @return the rootElement
	 */
	public Element getRootElement() {
		return rootElement;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		XMLInputKit kitKit = null;
		double start = 0, end = 0;
		try {
			start = System.currentTimeMillis();
			kitKit = new XMLInputKit();
			end = System.currentTimeMillis();
			System.out.println("\nTime it took to create an input Kit from\n" + kitKit.inputFile + "\n(in millisecond) is: " + (end - start) );
		} catch (IOException | ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		

	}

}
