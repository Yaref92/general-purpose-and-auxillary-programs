package lykicheewi.burtefrahouse.fileTools;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

import lykicheewi.burtefrahouse.miscTools.XMLAuxTools;
import lykicheewi.burtefrahouse.miscTools.XMLAuxTools.ExpandedElement;

/**
 * 
 * @since 0.11.0 (28.07.2020 10:52)
 * @author Yaron Efrat
 */
public class XMLOutputKit extends FileOutputKit {

	/**
	 * 
	 * @since 0.11.0 (28.07.2020 10:54)
	 */
	Document doc;
	
	/**
	 * 
	 * @since 0.11.0 (28.07.2020 10:54)
	 */
	Element rootElement;
	
	/**
	 * 
	 * @since 0.11.0 (28.07.2020 10:56)
	 */
	HashMap<Element, ArrayList<ExpandedElement>> elementMap;
	
	/**
	 * To save the information whether it is
	 * empty.
	 * @since 0.11.0 (05.08.2020 18:07)
	 */
	boolean isEmptyFile;

	/**
	 * Extracted from use as a local 
	 * variable in the constructor to
	 * use the elementMapper
	 * @since 0.12.0 (11.08.2020 11:46)
	 */
	private XMLInputKit existingXML;
	
	/**
	 * @since 0.11.0 (28.07.2020 10:56)
	 * @implNote Try to implement Observer design pattern,
	 * so that if the other constructor says the file exists,
	 * this one will be notified and act accordingly
	 */
	public XMLOutputKit(String rootName) throws IOException {
		// Auto-generated constructor stub
		super(); // Calls the FileOutputKit constructor that lets the user choose the file and location
		/*
		 * Added the createNewFile section
		 * in case we are creating a new file and not
		 * editing one. It won't change an existing file
		 * or throw and exception for it, only return false
		 * instead of true.
		 */
//		isExistingFile = !outputFile.createNewFile(); // checks it in the FileOutputKit constructor
		initializeKit(rootName);
		
	}

	/**
	 *
	 * @since 0.16.0 (03.12.2020 10:02:50 AM)
	 * @apiNote Extracted from the old
	 * constructor to allow it to be used by the 
	 * new one as well.
	 * @param rootName
	 * @throws IOException
	 */
	private void initializeKit(String rootName) throws IOException {
		if (!isExistingFile) {
	        System.out.println("File created: " + fileName); //TEST
	        initialiseFields(rootName); // Added 0.11.0 05.08.2020 18:01
	        isEmptyFile = true;
	    } 
		else {
	        //System.out.println("The file "+ fileName + " already exists."); //TEST
	        existingXML = null;
	        try {
	        	/*
				 * A bug was found:
				 * an empty file would throw SAXException.
				 * Solution:
				 * Add the extraction of rootElement etc. here
				 * and on case of an exception with 
				 * "premature end of file"
				 * just nullify the existing XML
				 * and initialize the rest
				 */
	        	existingXML = new XMLInputKit(getFileName(), true, true); // Added , true, true 0.16.0 07.12.2020 09:53 for compatibility with web frontend
	        	rootElement = existingXML.getRootElement();
		        doc = existingXML.getDoc();
		        elementMap = existingXML.getElementMap();
		        isEmptyFile = false;
			} catch (ParserConfigurationException e) {
				// Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// Auto-generated catch block
				if (e.toString().contains("Premature")) {
					System.err.println("Premature end of file encountered, initialising"
							+ " new doc, rootElement and elementMap to be written into"
							+ " a new XML file");
					existingXML = null;
					initialiseFields(rootName); // Added 0.11.0 05.08.2020 18:01
					isEmptyFile = true;
				}
				else {
					e.printStackTrace();
				}
				
			}
	        
	    }
	}
	
	/**
	 *
	 * @param fileIdentifier
	 * @param rootElement
	 * @since 0.16.0 (03.12.2020 10:00:48 AM)
	 * @throws IOException
	 */
	public XMLOutputKit(String rootName, String fileIdentifier) throws IOException {
		super(fileIdentifier);
		initializeKit(rootName);
	}

	/**
	 * Adds the attributes in the list to the parent
	 * @param element The element to which attributes
	 * are to be added
	 * @param attributes The attributes to be added
	 * @since 0.12.0 (11.08.2020 ~11:52)
	 */
	public void addAttributes(Element element, ArrayList<Attribute> attributes) {
		element.setAttributes(attributes);
		/*
		 * Now it needs to remap the element because entire structure could
		 * have changed
		 */
		//existingXML.elementMapper(true, true);
		elementMap = XMLAuxTools.elementMapper(rootElement, true, true);
	}
	
	/**
	 * Adds the elements in the list as children of the parent
	 * @implNote Uses stream API's forEach to make it
	 * short and succinct
	 * @param parent The element to which children elements
	 * are to be added
	 * @param children The elements to be added
	 * @since 0.12.0 (11.08.2020 11:38:01)
	 */
	public void addChildren(Element parent, ArrayList<Element> children) {
		children.stream().forEach(child -> parent.addContent(child));
		/*
		 * Now it needs to remap the element because entire structure could
		 * have changed
		 */
		//existingXML.elementMapper(true, true);
		elementMap = XMLAuxTools.elementMapper(rootElement, true, true);
	}

	/**
	 * Adds the text to the parent
	 * @param element The element to which text
	 * is to be added
	 * @param text the text to be added
	 * @since 0.12.0 (11.08.2020 ~11:55)
	 */
	public void addText(Element element, String text) {
		element.addContent(text);
		/*
		 * Now it needs to remap the element because entire structure could
		 * have changed
		 */
		//existingXML.elementMapper(true, true);
		elementMap = XMLAuxTools.elementMapper(rootElement, true, true);
	}

	/**
	 * Writes the changes to the (empty or
	 * otherwise) output file
	 * @since 0.12.0 (11.08.2020 11:56:34)
	 */
	public void applyChanges() {
		XMLOutputter xmlOutput = new XMLOutputter();
		xmlOutput.setFormat(Format.getPrettyFormat());
		try {
			xmlOutput.output(doc, new FileOutputStream(fileName));
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @return the doc
	 */
	public Document getDoc() {
		return doc;
	}

	/**
	 * @return the rootElement
	 */
	public Element getRootElement() {
		return rootElement;
	}

	/**
	 * @return the elementMap
	 */
	public HashMap<Element, ArrayList<ExpandedElement>> getElementMap() {
		return elementMap;
	}

	/**
	 * Initialises the fields of the class
	 * @since 0.11.0 (05-08-2020 17:51:20)
	 */
	private void initialiseFields(String rootName) {
		rootElement = new Element(rootName);
		doc = new Document(rootElement);
		elementMap = new HashMap<Element, ArrayList<lykicheewi.burtefrahouse.miscTools.XMLAuxTools.ExpandedElement>>();

	}
	/**
	 *
	 * @since ?.?.? (05.08.2020 18:11:06)
	 * @return the isEmptyFile
	 */
	public boolean isEmptyFile() {
		return isEmptyFile;
	}

	/**
	 * @return the isExistingFile
	 */
	public boolean isExistingFile() {
		return isExistingFile;
	}

	/**
	 * @param args
	 * @apiNote Auto generated and redundant,
	 * but left in because of reasons
	 */
	public static void main(String[] args) {
		// TEST
		try {
			XMLOutputKit xmlok = new XMLOutputKit("hi", "ho.xml");
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
        
	}

}
