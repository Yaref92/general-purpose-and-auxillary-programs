/**
 * 
 */
package lykicheewi.burtefrahouse.fileTools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lykicheewi.burtefrahouse.questionnaireTools.DialogKit;

/**
 * 
 * A utility class providing methods to read all kinds of input from a file.
 * @author Yaron Efrat
 * @implNote Have turned it into a Serializable object since
 * 0.10.0; 20.05.2020 22:43 because it is necessary in order to 
 * serialize QuestionPack
 */
public class FileInputKit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7324864238487560841L;
	/**
	 * TODO So far, I have implemented only a few methods. Hopefully, in the future this Class will grow.
	 * For now, unfortunately, I must conserve time
	 */
	protected File inputFile;
	String pathName;
	String fileName;
	public transient Scanner inputScanner; // transient since 20.05.2020 22:44 because Scanner is not Serializable
	private transient StringTokenizer inputTokenizer;  // transient since 20.05.2020 22:45 because Scanner is not Serializable
	String safeWord; // This String will be used to stop some methods from reading all the way to EOF. The default safe word will be stop_reading
	
	/**
	 * Receives a String containing either a full pathname or file name and initializes the inputFile accordingly.
	 * Also receive a String for the safe word.
	 * @param fileIdentifier - a String containing either a full pathname or file name
	 * @param safeWord - a String containing the safe word to be used in this kit
	 * @throws FileNotFoundException 
	 */
	public FileInputKit(String fileIdentifier, String safeWord) throws FileNotFoundException {
		/*
		 * BUGFIX 0.5.0 : Added another condition (fileIdentifier.contains("\\"))
		 * because some generated pathnames used "\\"
		 */
		if ((fileIdentifier.contains("/")) ||(fileIdentifier.contains("\\"))) {
			pathName = fileIdentifier;
			inputFile = new File(pathName);
		}
		else {
			fileName = fileIdentifier;
			/*
			 * Unfortunately, I end up with this try catch block. I found new File(".").getCanonicalPath(); as a 
			 * solution on https://www.technicalkeeda.com/java-tutorials/get-current-file-path-in-java.
			 * But the IDE asked to wrap it in try catch
			 */
			try {
				String currentDirectory = new File(".").getCanonicalPath();
				inputFile = new File(currentDirectory + "/" + fileName); 
			} catch (IOException e) {
				// Auto-generated catch block
				e.printStackTrace();
				inputFile = new File(".");
			}			
		}
		inputScanner = new Scanner(inputFile);
		this.safeWord = safeWord;
	}
	
	/**
	 * Receives a String containing either a full pathname or file name and initializes the inputFile accordingly.
	 * Will call the other constructor with "stop_reading" as the safe word
	 * @param fileIdentifier - a String containing either a full pathname or file name
	 * @throws FileNotFoundException 
	 */
	public FileInputKit(String fileIdentifier) throws FileNotFoundException {
		this(fileIdentifier, "stop_reading");
	}
	
	/**
	 * Receives a String for the safe word. Uses chooseFileName for the file.
	 * @apiNote OVERLOADED 20.4.2020 14:23
	 * @param parent Added 0.9.1 15.4.2020 23:54 to allow
	 * to be incorporated into GUI apps.
	 * @param safeWord - a String containing the safe word to be used in this kit
	 * @throws IOException 
	 */
	public FileInputKit(JPanel parent, String safeWord) throws IOException {
		this(parent, safeWord, null);
	}

	/**
	 * Receives a String for the safe word. Uses chooseFileName for the file.
	 * @since 0.9.2 20.4.2020 14:23
	 * @param parent Added 0.9.1 15.4.2020 23:54 to allow
	 * to be incorporated into GUI apps.
	 * @param safeWord - a String containing the safe word to be used in this kit
	 * @param defaultFile TODO
	 * @throws IOException 
	 */
	public FileInputKit(JPanel parent, String safeWord, String defaultFile) throws IOException {
		inputFile = DialogKit.chooseFileName(parent, defaultFile);
		inputScanner = new Scanner(inputFile);
		this.safeWord = safeWord;
	}
	
	/**
	 * Uses chooseFileName for the file..
	 * Will call the other constructor with "stop_reading" as the safe word
	 * @param parent Added 0.9.1 15.4.2020 23:58 to allow
	 * to be incorporated into GUI apps
	 * @throws IOException 
	 */
	public FileInputKit(JPanel parent) throws IOException {
		this(parent, "stop_reading", null);
	}

	/**
	 * Will read integer numbers separated by whitespace from the next line in the file.
	 * COPIED AS IS FROM GraphProblemSolver AND WILL BE THE BASIS FOR OVERLOADED METHODS.
	 * @return A LinkedList of Integer
	 */
	public LinkedList<Integer> readIntsFromLine() {
		LinkedList<Integer> tempList = new LinkedList<Integer>();
		/* 
		 * Thanks to rogue_leader on stackoverflow
		 * The StringTokenizer object helps with taking a line, separating stuff there with a
		 * specific delimiter, and then take the relevant input ("tokens") one by one and
		 * do what is needed with it.
		 */
		inputTokenizer = new StringTokenizer(inputScanner.nextLine(), " "); // whitespace is the delimiter to create tokens
		while(inputTokenizer.hasMoreTokens())  // iterate until no more tokens
		{
			        tempList.add(Integer.parseInt(inputTokenizer.nextToken()));  // parse each token to integer and add to linkedlist

		}
		return tempList;
	}
	
	/**
	 * @param flushLine - If true, will jump to next line if reached a newline character
	 * @return next string up until whitespace or linebreaker
	 */
	public String readNext(boolean flushLine) {
		if (inputScanner.hasNext()) {
			String temp = inputScanner.next();
			/*
			 * Thanks to Arka Prava Basu
			 * (https://archie94.github.io/blogs/skip-newline-while-reading-from-scanner-class)
			 * who got it from Hackerrank
			 * (https://www.hackerrank.com/challenges/string-construction/problem)
			 * For the elegant solution, skipping any newline 
			 * character, in Linux mac and windows, using a regex 
			 * expression that contains \r\n for Windows
			 * \r for mac, \n for linux, and other special characters
			 * (3 of them) that are equivalent
			 */
			if (flushLine) {
				skipControlStrings(); // Skips the newline if there is one here
			}
			return temp;			
		}
		else {
			System.out.println("Nohing left to read");
			return null;
		}
		
	}
	
	/**
	 * Will call readNext(true) and return its return value
	 * @return next string up until whitespace or linebreaker
	 */
	public String readNext() {
		return readNext(true);
		
	}
	
	/**
	 * 
	 * @return next string up until linebreaker
	 */
	public String readNextLine() {
		if (inputScanner.hasNext()) {
		return inputScanner.nextLine();
		}
		else {
			System.out.println("Nohing left to read");
			return null;
		}
		
	}
	
	/**
	 * Advances the Scanner while adding to a String until reaching the safeWord String and 
	 * then returns the resulting string
	 * @return an unbroken string from the starting position of the Scanner until the safeWord String with no new line separators
	 */
	public String readUntilSafeWord() {
		String unTokenizedString = "";
		while ((!inputScanner.hasNext(safeWord)) && (inputScanner.hasNext())) {
			unTokenizedString += inputScanner.next() + " ";
		}
		skipControlStrings(); // Skips the safe word and newline if there is one after it		
		return unTokenizedString;
	}
	
	/**
	 * ADDED IN 0.8.0 10.4.2020 20:38 -
	 * to allow different delimiters
	 * Similar to readUntilSafeWord, only instead 
	 * of returning the resulting string, it sends it
	 * to inputTokenizer for further processing.
	 * @param delimiter The desired delimiter
	 */
	public void tokenizeUntilSafeWord(String delimiter) {
		String unTokenizedString = "";
		while ((!inputScanner.hasNext(safeWord)) && (inputScanner.hasNext())) {
			unTokenizedString += inputScanner.next() + " ";
		}
		skipControlStrings(); // Skips the safe word and newline if there is one after it
		inputTokenizer = new StringTokenizer(unTokenizedString, delimiter);
	}
	
	/**
	 * CHANGED IN 0.8.0, 10.4.2020 20:38 - 
	 * ONLY CALLS THE ABOVE METHOD WITH A 
	 * DEFAULT DELIMITER OF " " 
	 * Similar to readUntilSafeWord, only instead 
	 * of returning the resulting string, it sends it
	 * to inputTokenizer for further processing.
	 */
	public void tokenizeUntilSafeWord() {
		tokenizeUntilSafeWord(" ");
	}
	
	/**
	 * Simply returns whether the tokenizer
	 * has anymore tokens.
	 * It saves having to call the tokenizer. 
	 * @return inputTokenizer.hasMoreTokens()
	 */
	public boolean hasMoreTokens() {
		return inputTokenizer.hasMoreTokens();		
	}
	
	/**
	 * Simply returns the tokenizer's next token or null
	 * if not exists. It saves having to call the tokenizer,
	 * and allows us to privatize it.
	 * It also advances the token if there is a token.
	 * @return the next token in the tokenizer or null if there is none.
	 */
	public String getNextToken() {
		if (inputTokenizer.hasMoreTokens()) {
			return inputTokenizer.nextToken();
		}
		else {
			return null;
		}
		
	}
	
	/**
	 * 
	 * @param <K> - The type of the keys of the map
	 * @param <V> - The type of the values of the map
	 * @param inputMap - the map to be updated
	 * @return an updated version of inputMap
	 * @throws Exception -  if there is a mismatch between the types of input (if parseable than parsed input) and the map types or there are 
	 * too many/few elements in a line a suitable Exception will be thrown 
	 */
	@SuppressWarnings("unchecked") // Is handled by an exception instead
	public <K, V> Map<K, V> readMap(Map<K, V> inputMap) throws Exception {
		/*
		 * At the moment, will only support K as String and V as Integer.
		 * As necessary, I might try to add more. 
		 * In any case, I will keep the generic types when possible to allow
		 * future implementation
		 */
		K currentKey;
		V currentValue;		
		while ((!inputScanner.hasNext(safeWord)) && (inputScanner.hasNext())) {
			/* 
			 * Thanks to rogue_leader on stackoverflow
			 * The StringTokenizer object helps with taking a line, separating stuff there with a
			 * specific delimiter, and then take the relevant input ("tokens") one by one and
			 * do what is needed with it.
			 */
			inputTokenizer = new StringTokenizer(inputScanner.nextLine(), " "); // whitespace is the delimiter to create tokens
			if (inputTokenizer.countTokens() == 2) {
				try {
					currentKey = (K) inputTokenizer.nextToken();
					currentValue = (V) Integer.valueOf(inputTokenizer.nextToken()); // Here I have abandoned generic type partially, so TODO if we want to upgrade it
					inputMap.put(currentKey, currentValue);
				}
				catch (Exception e) {
					e.printStackTrace();
					System.err.println("The input from the file could not be cast into the key and/or value types of the map");
				}
			}
			else {
				throw new Exception("A line has contained more Tokens than a Key-Value pair");
			}
		}
		return inputMap;		
	}
	
	/**
	 * Resets the scanner to the beginning of the file.
	 * @throws FileNotFoundException
	 */
	public void resetScanner() throws FileNotFoundException {
		inputScanner.close();
		inputScanner = new Scanner(inputFile);
	}
	
	/**
	 * Skips safeWord and newline
	 */
	public void skipControlStrings() {
		if (inputScanner.hasNext(safeWord)) {
			inputScanner.next();
		}
		/*
		 * Thanks to Arka Prava Basu
		 * (https://archie94.github.io/blogs/skip-newline-while-reading-from-scanner-class)
		 * who got it from Hackerrank
		 * (https://www.hackerrank.com/challenges/string-construction/problem)
		 * For the elegant solution, skipping any newline 
		 * character, in Linux mac and windows, using a regex 
		 * expression that contains \r\n for Windows
		 * \r for mac, \n for linux, and other special characters
		 * (3 of them) that are equivalent
		 */
		inputScanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?"); // This skips any newline character.
	}
	
	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	

	/**
	 * @param safeWord the safeWord to set
	 */
	public void setSafeWord(String safeWord) {
		this.safeWord = safeWord;
	}
	
	

	/**
	 * @return the inputScanner
	 */
	public Scanner getInputScanner() {
		return inputScanner;
	}
	
	/**
	 * Using a JFileChooser object, this method opens a dialog window which allows to choose only directories,
	 * waits for the user to choose a directory, which is supposed to be a directory with two sub directories
	 * (mentees and mentors) containing users' answers .txt files and then returning it.
	 * @return Returns the directory as a File Object.
	 * @throws IOException IDE insisited upon adding new File(".").getCanonicalPath()
	 * @deprecated since 0.9.1 15.4.2020 23:51 Because it didn't have the option to define a parent
	 * component, and I preferred to migrate it to DialogKit
	 */
	@Deprecated
	protected static File chooseFileName() throws IOException {
		/*
		 * A lot of this method is from Oracle's tutorials
		 */
		// Create a file chooser
		/*
		 * The JFileChooser allows us to open up a dialog window for choosing Files and/or
		 * directories. With this, we can choose the relevant directory that houses the data we need.
		 * As an added bonus I use new File(".").getCanonicalPath() to get the current directory
		 * to act as the starting directory. 
	     * Solution previously acquired from https://www.technicalkeeda.com/java-tutorials/get-current-file-path-in-java.
		 */
		final JFileChooser fc = new JFileChooser(new File(".").getCanonicalPath());
		// Makes the FileChooser be able to select only Directories
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		/*
		 * In order to be able to allow for a user that accidentally 
		 * closed the dialog to be able to have a chance at choosing a directory, we
		 * are using another Class from the javax.swing: JOptionePane.
		 * We are using it to present a yes/no question whose input we use as a control
		 * variable in a while loop, allowing someone that repeatedly accidentally exits
		 * to get another chance.
		 */
		int userChoice = JOptionPane.YES_OPTION; // JOptionePane comes with Constant variables for its options. We're initialising to YES
		// YES continues the loop.
		while (userChoice == JOptionPane.YES_OPTION) {
			/*
			 * Now show the dialog window. The showDialog method
			 * returns an int that signifies whether the Choose Directory
			 * was chosen, or the dialog was exited another way. 
			 */
			int returnVal = fc.showDialog(null, "Choose File");
			// In case the user chose a directory, return the File object representing it
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				return fc.getSelectedFile();
			}
			// Else, using the JOptionPane, allow the user another chance (YES), or get out of the while (NO)
			else {
				userChoice = JOptionPane.showOptionDialog(null,
						"Open command cancelled. Would you like to open the dialog window again?", "Try Again?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
			} 
		}
		return null; // If didn't choose a directory, then the method returns null
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FileInputKit testKit = null;
		try {
			testKit = new FileInputKit("fileTest.txt");
		} catch (FileNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		testKit.readIntsFromLine();
		System.out.println("readNext:\n" + testKit.readNext());
		System.out.println("readNextLine:\n" + testKit.readNextLine());
		try {
			testKit.resetScanner();
		} catch (FileNotFoundException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("readUntilSafeWord:\n" + testKit.readUntilSafeWord());
		testKit.tokenizeUntilSafeWord();
		System.out.println("tokenizeUntilSafeWord:\n" + testKit.inputTokenizer.nextToken());
		try {
			testKit.readMap(new HashMap<String, Integer>());
		} catch (Exception e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		FileInputKit testKit2 = null;		
		try {
			testKit2 = new FileInputKit((JPanel) null);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		testKit2.getFileName();

	}

	/**
	 * ADDED IN 0.8.0, 10.4.2020 ~21:15 - 
	 * to solve a problem related to the tokenizing process
	 * removes a delimiter character from the beginning and the
	 * end of a string if it exists there
	 * @param stringToTrim the string to be trimmed
	 * @param delimiter the delimiter character that needs to be removed from the string
	 * @return the trimmed string
	 */
	public static String trimDelim(String stringToTrim, char delimiter) {
		String trimmedString = null;
		if (stringToTrim.charAt(stringToTrim.length() - 1) == delimiter) {
			trimmedString = stringToTrim.substring(0, stringToTrim.length());
		}
		else { // Added to avoid returning null if the first if does not execute
			trimmedString = stringToTrim; // Even if it is only a shallow copy, it should suffice for here.
		}
		if ( (stringToTrim.charAt(0) == delimiter) && (!trimmedString.isEmpty())) {
			trimmedString = trimmedString.substring(1);
		}
		return trimmedString;
	}

}
