/**
 * 
 */
package lykicheewi.burtefrahouse.fileTools;

/*
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
*/

//import org.jdom2.*;

//import lykicheewi.burtefrahouse.graphTools.TreeNode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.OverloadingWrapperConstructor;
import lykicheewi.burtefrahouse.annotations.methodAnnotations.OverloadingWrapperMethod;
import lykicheewi.burtefrahouse.questionnaireTools.DialogKit;

/** 
 *
 * @author Yaron Efrat
 */
public class FileOutputKit {

	/**
	 * ADDED IN 0.8.0 10.4.2020 11:54.
	 */
	protected File outputFile;
	String pathName;
	String fileName;
	private FileWriter writer;
	/**
	 * To save the information whether it is a file
	 * that already existed or a new one.
	 * @since 0.11.0 (28.07.2020 14:11)
	 * @apiNote I have moved it from
	 * XMLOutputKit here on 0.12.0 
	 * 12.08.2020 14:17. <br>
	 * The reason is that I already use
	 * the createNewFile method here,
	 * so it cannot be false after the call
	 * to super() in XMLOutput kit. <br>
	 * Therfore, in order to have a reliable answer,
	 * we need to do it here.
	 * {@literal FIX202008121417}
	 */
	boolean isExistingFile;
	
	/**
	 * ~~Basically the same as the constructor for FileInputKit except for minor changes~~
	 * ~~ I will add a createNewFile section though. To create the file if it does not exist~~
	 * Receives a String containing either a full pathname or file name and initializes the inputFile accordingly.
	 * Also receive a String for the safe word.
	 * @param fileIdentifier - a String containing either a full pathname or file name 
	 * @bugfix FIX202008121434 Had a bug where calling the FileWriter constructor with only
	 * one parameter erased the contents.
	 * @apiNote This has been turned to overloading wrapper constructor
	 */ 
	@OverloadingWrapperConstructor
	public FileOutputKit (String fileIdentifier) throws IOException {
		this(fileIdentifier, true);
	}

	/**
	 * ~~Basically the same as the constructor for FileInputKit except for minor changes~~
	 * ~~ I will add a createNewFile section though. To create the file if it does not exist~~
	 * Receives a String containing either a full pathname or file name and initializes the inputFile accordingly.
	 * Also receive a String for the safe word.
	 * @param fileIdentifier - a String containing either a full pathname or file name 
	 * @param shouldAppend A boolean for whether the File should append or overwrite
	 * in the case of existing file
	 * @bugfix FIX202008121434 Had a bug where calling the FileWriter constructor with only
	 * one parameter erased the contents.
	 * @apiNote Added this to allow a choice between append and overwrite. <br>
	 * @since 1.5.0 (14.08.2020 11:43)
	 */ 
	public FileOutputKit (String fileIdentifier, boolean shouldAppend) throws IOException {
		/*
		 * BUGFIX 0.5.0 (back in the input kit): Added another condition (fileIdentifier.contains("\\"))
		 * because some generated pathnames used "\\"
		 */
		if ((fileIdentifier.contains("/")) ||(fileIdentifier.contains("\\"))) {
			pathName = fileIdentifier;
			outputFile = new File(pathName);
			/*
			 * The lack of FileName in this branch caused a bug where
			 * it is used when null.
			 * FIX202008051551
			 * @since 0.11.0 05.08.2020 15:51
			 */
			fileName = outputFile.getName();
		}
		else {
			fileName = fileIdentifier;
			/*
			 * Unfortunately, I end up with this try catch block. I found new File(".").getCanonicalPath(); as a 
			 * solution on https://www.technicalkeeda.com/java-tutorials/get-current-file-path-in-java.
			 * But the IDE asked to wrap it in try catch
			 */
			try {
				String currentDirectory = new File(".").getCanonicalPath();
				outputFile = new File(currentDirectory + "/" + fileName); 
			} catch (IOException e) {
				// Auto-generated catch block
				e.printStackTrace();
				outputFile = new File(".");
			}			
		}
		/*
		 * Added because the other constructor would then always say the
		 * file did not exist
		 */
		createFileIfNecessary();		
		writer = new FileWriter(outputFile, shouldAppend); // FIX202008121434
	}
	
	/**
	 * In case you want to edit an existing file, you can just use 
	 * chooseFileName that's already implemented in FileInputKit
	 * @implNote Updated to use DialogKit.chooseFileName(JPanel)
	 * @throws IOException
	 * @bugfix FIX202008121434 Had a bug where calling the FileWriter constructor with only
	 * one parameter erased the contents. 
	 * @apiNote This has been turned to overloading wrapper constructor
	 */ 
	@OverloadingWrapperConstructor
	public FileOutputKit() throws IOException {
		this(true);
	}

	/**
	 * In case you want to edit an existing file, you can just use 
	 * chooseFileName that's already implemented in FileInputKit
	 * @implNote Updated to use DialogKit.chooseFileName(JPanel)
	 * @param shouldAppend A boolean for whether the File should append or overwrite
	 * in the case of existing file
	 * @throws IOException
	 * @bugfix FIX202008121434 Had a bug where calling the FileWriter constructor with only
	 * one parameter erased the contents. 
	 * @since 1.5.0 (14.08.2020 15:10)
	 */
	public FileOutputKit(boolean shouldAppend) throws IOException {
		// outputFile = FileInputKit.chooseFileName(); // Replaced with the updated version
		outputFile = DialogKit.chooseFileName(null);
		/*
		 * The lack of FileName in this branch caused a bug where
		 * it is used when null.
		 * FIX202008051551
		 * @since 0.11.0 05.08.2020 15:51
		 */
		fileName = outputFile.getName();
		createFileIfNecessary(); // Added to have the correct isExistingFile 0.12.0 12.08.2020 14:28
		writer = new FileWriter(outputFile, shouldAppend); // FIX202008121434
	}

	/**
	 * Added the createNewFile section
	 * in case we are creating a new file and not
	 * editing one. It won't change an existing file
	 * or throw and exception for it, only return false
	 * instead of true.
	 * @throws IOException 
	 * @apiNote Added because the second constructor
	 * would then always say the
	 * file did not exist
	 * @since 0.12.0 (12.08.2020 14:26:36)
	 */
	private void createFileIfNecessary() throws IOException {
		/*
		 * Added the createNewFile section
		 * in case we are creating a new file and not
		 * editing one. It won't change an existing file
		 * or throw and exception for it, only return false
		 * instead of true.
		 */
		if (outputFile.createNewFile()) {
	        isExistingFile = false; // FIX202008121417
			System.out.println("File created: " + fileName); //TEST
	      } else {
	    	  isExistingFile = true; // FIX202008121417
	        //System.out.println("File already exists."); //TEST
	      }		
	}

	/**
	 * Simple write operation that writes a String
	 * @param string the string to be written
	 * @throws IOException IDE insisted
	 */
	public void write(String string) throws IOException {
		
		writer.write(string);
	}

	/**
	 * Simple write operation that writes a char
	 * represented by a number
	 * @param charRepresentingNum
	 * @throws IOException IDE insisted
	 */
	public void write(int charRepresentingNum) throws IOException {
		writer.write(charRepresentingNum);		
	}
	
	/**
	 * Flushes the writer so it actually writes to file
	 * @since 0.11.0 (02.08.2020 13:48)
	 * @throws IOException
	 */
	public void flushWriter() throws IOException {
		writer.flush();
	}
	
	/**
	 * Added 0.8.0 13.4.2020 10:12
	 * Simply closes the FileWriter.
	 * Will also flush which means it will write to file.
	 * @throws IOException
	 */
	public void closeWriter() throws IOException {
		writer.close();
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}
	
	/**
	 * 
	 */
//	public void saveToXML() {
//		
//	}
	
//	public class XMLOutputKit {		
//		public Document doc;
//		//TreeNode<Element> docStructure;
//		
//		public XMLOutputKit(Element rootElement) {			
//	        try {
//	        	doc = new Document(rootElement);
//			} 
//	        catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();				
//			}
//	        
//		}
//		
//		
//	}

}
