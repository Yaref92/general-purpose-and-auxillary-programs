/**
 * Moved to encryptionTools 
 * 0.16.0 27.11.2020 12:26
 */
package lykicheewi.burtefrahouse.encryptionTools;

import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import lykicheewi.burtefrahouse.fileTools.FileInputKit;

/**
 * @author Yaron Efrat
 *
 */
public class HashHashVerifier {
	String testedHash = null;
	String testedText = null;
	int iterNum;
	boolean[] choices;

	/**
	 * 
	 */
	public HashHashVerifier(String hex, String text, int randNum, boolean[] choices) {
		testedHash = hex;
		testedText = text;
		iterNum = randNum;
		this.choices = choices;
	}
	
	/**
	 * UPDATE 03.02.2020:
	 * A constructor taking in two FileInputKits as arguments.
	 * Will extract the necessary data from there
	 * @param hshInfReader - For reading the extra bits from the hshInfo file
	 * @param hashReader - For reading the hash itself from the relevant file
	 */
	public HashHashVerifier(FileInputKit hshInfReader, FileInputKit hashReader) {
		iterNum = Integer.parseInt(hshInfReader.readNextLine());
		choices = new boolean[iterNum];
		for (int i = 0; i < iterNum; i++) {
			choices[i] = Boolean.parseBoolean(hshInfReader.readNextLine());
		}
		testedHash = hashReader.readNext();
		/*
		 * Using a method to take in the text
		 */
		testedText = readTextToBeTested();
	    
	}
	
	/**
	 * The following uses a Scanner to scan from user input directly into the 
	 * tested Text via the constructor.
	 * Preferred for some reason to go through the constructor and not directly 
	 * to the field
	 * @return
	 */
	private String readTextToBeTested() {
		// create a scanner so we can read the command-line input
	    Scanner scanner = new Scanner(System.in);
	    /*
	     * UPDATE at 3.2.2020:
	     * Changing delimiter to "\\n" worked for including everything before the newline command
	     * in the textToHash String. so I shall delete the other attempt using while
	     */
	    scanner.useDelimiter("\\n"); // Hopefully, will set newline as delimiter
	    //  prompt for the user's text
	    System.out.println("Enter your text to test against the hash: ");
	    // Starting textToHash as empty string to add to
	 	String textToTest = "";	    
	 	// get their input as a String and concatenate to end of current textToHash
	 	textToTest = textToTest.concat(scanner.next());
	 	scanner.close();
	 	return textToTest;
	}
	
	public boolean checkProvidedTextAgainstHash() throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		String tempHash = testedText.substring(0);
		String hex = null;
		for(int i = 0; i < iterNum; i++) {
	    	// Change this to UTF-16 if needed
	    	md.update(tempHash.getBytes(StandardCharsets.UTF_8));
		    byte[] digest = md.digest();
		    hex = String.format("%064x", new BigInteger(1, digest));
		    if (choices[i]) {
		    	tempHash = tempHash.concat(hex);
			    // Change this to UTF-16 if needed
			    md.update(tempHash.getBytes(StandardCharsets.UTF_8));
				byte[] digest2 = md.digest();
				hex = String.format("%064x", new BigInteger(1, digest2));
		    }
		    tempHash = hex;		    
	    }
		return hex.equals(testedHash);
	}

	/**
	 * @param args
	 * @throws FileNotFoundException 
	 * @throws NoSuchAlgorithmException 
	 */
	public static void main(String[] args) throws FileNotFoundException, NoSuchAlgorithmException {
		// 03.02.2020: Will try to use the output of HashHash (the files) to initiate this.
		// And of course, the user will have to input the text
		HashHashVerifier textToHashComparer;
		try {
			textToHashComparer = new HashHashVerifier(new FileInputKit("hshInf.txt"), new FileInputKit("hashItself.txt"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			textToHashComparer = new HashHashVerifier(new FileInputKit("bin\\hshInf.txt"), new FileInputKit("bin\\hashItself.txt"));
		}
		if (textToHashComparer.checkProvidedTextAgainstHash()) {
			System.out.println("The provided text, when HashHashed according to the provided info, matches the provided hash");
		}
		else {
			System.out.println("The provided text, when HashHashed according to the provided info, DOES NOT match the provided hash");
		}
		
		

	}

}
