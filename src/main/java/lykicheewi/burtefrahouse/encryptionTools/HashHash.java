/**
 * Moved to encryptionTools 
 * 0.16.0 27.11.2020 12:26
 */
package lykicheewi.burtefrahouse.encryptionTools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * This Class takes a text, and repeatedly hashes it according to the following scheme:
 * A random number between k1 and kn is chosen;
 * This random number will define the number of hashes taking place.
 * But there is a catch.
 * In each iteration, a choice is made randomly by the toss of a coin
 * regarding the input to the next hash:
 * Either you concatenate the new hash to the previous one, hash the concatenated String and pass the result
 * to the next iteration,
 * or you just pass it as is to the next iteration.
 * That means that each String can map to many many hashes, and in order to retrieve 
 * the original String from a hash, even if you had a magical Hash reverser, you still need to
 * know how many times to perform the magic reverser, and whether you should add some unknown hash String
 * to the beginning after each iteration or not.
 * Is it more secure than existing solutions?
 * Only trained CS people can answer that.......
 * @author Yaron Efrat
 *
 */
public class HashHash {

	
	/**
	 * 
	 */
	public HashHash() {
		// Auto-generated constructor stub
	}

	/**
	 * @param args
	 * @throws NoSuchAlgorithmException 
	 */
	public static void main(String[] args) throws NoSuchAlgorithmException {
		// Auto-generated method stub
		MessageDigest md = MessageDigest.getInstance("SHA-256");
	    // String text = "Be5*jA2Kedf45Oa";
		// Changed from 5 to args.length-1 to avoid exception that screwed it up (03.02.2020)
		// Did not work. Switched to 0 to test
		// Decided to switch to HashHashInputer being used statically from here
		String text = HashHashInputer.inputTextToHash();
	    String tempHash = text.substring(0);
	    SecureRandom random = new SecureRandom();
	    int randNum = random.nextInt(10) + 15;
	    String hex = null;
	    String[] hexes = new String[randNum];
	    boolean[] choices = new boolean[randNum];
	    for(int i = 0; i < randNum; i++) {
	    	// Change this to UTF-16 if needed
	    	md.update(tempHash.getBytes(StandardCharsets.UTF_8));
		    byte[] digest = md.digest();
		    hex = String.format("%064x", new BigInteger(1, digest));		    
		    if (random.nextInt(2) == 1) {
		        tempHash = tempHash.concat(hex);
		        // Change this to UTF-16 if needed
		    	md.update(tempHash.getBytes(StandardCharsets.UTF_8));
			    byte[] digest2 = md.digest();
			    hex = String.format("%064x", new BigInteger(1, digest2));
		        choices[i] = true;
		    }
		    hexes[i] = hex;
		    tempHash = hex;
		    
		    /*
		     * System.out.println("tempHash number " + (i + 1) + " in the series is: " + tempHash);
		     */  // TEST BLOCK
		    /*
		     * System.out.println("Hash number " + (i + 1) + " in the series is: " + hex);
		     */ // TEST BLOCK
	    }
	    /* 
	     * HashHashVerifier hashTester = new HashHashVerifier(hex, text, randNum, choices);
	     * System.out.println("Retracing the number of iterations and whether or not to concatanate to existing tempHash and comparing to hex returns " 
	     * + hashTester.checkProvidedTextAgainstHash());
	     */ // A TEST BLOCK
	    System.out.println("Hash number " + randNum + " in the series is: " + hex);
	    File hshInf = new File("hshInf.txt");
		try {
			hshInf.createNewFile();
		} catch (IOException e1) {
			// Auto-generated catch block
			e1.printStackTrace();
		}
		try {
		      // FileWriter myWriter = new FileWriter("hshInf.txt");
			  BufferedWriter myWriter = new BufferedWriter(new FileWriter("hshInf.txt", false));
		      myWriter.write(String.valueOf(randNum));
		      myWriter.newLine();
		      for (int i = 0; i < randNum; i++) {
		    	  myWriter.write(String.valueOf(choices[i]));
		    	  myWriter.newLine();
		      }
		      myWriter.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		// 03.02.2020: Decided to also output the Hash in a file. At the very least for testing
		File hashItself = new File("hashItself.txt");
		try {
			hashItself.createNewFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
		      // FileWriter myWriter = new FileWriter("hshInf.txt");
			  BufferedWriter myWriter = new BufferedWriter(new FileWriter("hashItself.txt", false));
		      myWriter.write(hex);		      
		      myWriter.close();
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
