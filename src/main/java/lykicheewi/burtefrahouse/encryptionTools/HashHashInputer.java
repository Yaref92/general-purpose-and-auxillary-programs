/**
 * Moved to encryptionTools 
 * 0.16.0 27.11.2020 12:26
 */
package lykicheewi.burtefrahouse.encryptionTools;

import java.util.Scanner;

/**
 * @author Yaron Efrat
 *
 */
public class HashHashInputer {

	
	/**
	 * 
	 */
	public HashHashInputer() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Decided to migrate from main here and use from HashHash (03.02.2020)
	 * @return the text to hash as String
	 */
	public static String inputTextToHash() {
		// create a scanner so we can read the command-line input
	    Scanner scanner = new Scanner(System.in);
	    /*
	     * UPDATE at 3.2.2020:
	     * Changing delimiter to "\\n" worked for including everything before the newline command
	     * in the textToHash String. so I shall delete the other attempt using while
	     */
	    scanner.useDelimiter("\\n"); // Hopefully, will set newline as delimiter

	    //  prompt for the user's text
	    System.out.println("Enter your text to hash: ");
	    
	    
	    // Starting textToHash as empty string to add to
		String textToHash = "";	    
	    // get their input as a String and concatenate to end of current textToHash
	    textToHash = textToHash.concat(scanner.next());
	    	    
	    scanner.close();
	    System.out.print(textToHash);
		return textToHash;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

	}

}
