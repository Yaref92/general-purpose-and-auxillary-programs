package lykicheewi.burtefrahouse.exceptions;
/**
 * 
 */

/**
 * @author Cedis
 * @since 1.2.0 (02.06.2020)
 */
public class UnacceptableValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1919771217285600978L;

	/**
	 * 
	 */
	public UnacceptableValueException() {
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public UnacceptableValueException(String message) {
		super(message);
		// Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public UnacceptableValueException(Throwable cause) {
		super(cause);
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnacceptableValueException(String message, Throwable cause) {
		super(message, cause);
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnacceptableValueException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// Auto-generated constructor stub
	}

}
