/**
 * 
 */
package lykicheewi.burtefrahouse.exceptions;

/** 
 *
 * @author Yaron Efrat
 */
public class SizeMismatchException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6092631307442837354L;

	/**
	 * 
	 */
	public SizeMismatchException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public SizeMismatchException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public SizeMismatchException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public SizeMismatchException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public SizeMismatchException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
