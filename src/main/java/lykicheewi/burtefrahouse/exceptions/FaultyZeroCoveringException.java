package lykicheewi.burtefrahouse.exceptions;
/**
 * 
 */

/**
 * @author Cedis
 *
 */
public class FaultyZeroCoveringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8547601785491228385L;

	/**
	 * 
	 */
	public FaultyZeroCoveringException() {
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public FaultyZeroCoveringException(String message) {
		super(message);
		// Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public FaultyZeroCoveringException(Throwable cause) {
		super(cause);
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public FaultyZeroCoveringException(String message, Throwable cause) {
		super(message, cause);
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public FaultyZeroCoveringException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// Auto-generated constructor stub
	}

}
