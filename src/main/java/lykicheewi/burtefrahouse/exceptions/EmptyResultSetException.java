package lykicheewi.burtefrahouse.exceptions;
/**
 * 
 */

/**
 * @author Yaron Efrat
 * @since 1.2.0 (02.06.2020)
 */
public class EmptyResultSetException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4412347982746649455L;

	/**
	 * 
	 */
	public EmptyResultSetException() {
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public EmptyResultSetException(String message) {
		super(message);
		// Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public EmptyResultSetException(Throwable cause) {
		super(cause);
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public EmptyResultSetException(String message, Throwable cause) {
		super(message, cause);
		// Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EmptyResultSetException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// Auto-generated constructor stub
	}

}
