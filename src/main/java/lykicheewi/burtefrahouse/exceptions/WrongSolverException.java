/**
 * 
 */
package lykicheewi.burtefrahouse.exceptions;

/** 
 *
 * @author Yaron Efrat
 */
public class WrongSolverException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8432151152834157393L;

	/**
	 * 
	 */
	public WrongSolverException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public WrongSolverException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public WrongSolverException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public WrongSolverException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public WrongSolverException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
