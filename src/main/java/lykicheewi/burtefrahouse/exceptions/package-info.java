/**
 * This Package contains all of the custom Exception
 * created accross my projects.
 * @apiNote Decided to put all lykicheewi.burtefrahouse.exceptions into a separate
 * package.
 * @since 0.12.0 (11.08.2020 10:52:32)
 * @author Yaron Efrat
 * @version 0.13.0
 */
package lykicheewi.burtefrahouse.exceptions;