/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.classAnnotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(CLASS)
@Target(TYPE)
/**
 *
 * @since 0.16.0 (25.11.2020 12:31:07 PM)
 * @author Yaron Efrat
 */
public @interface UtilityClass {

}
