/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.methodAnnotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(CLASS)
@Target(METHOD)
/**
 * This method is a helper method to others in
 * this class/package
 * @since 1.3.0 (10.08.2020 23:02)
 * @author Yaron Efrat
 */
public @interface HelperMethod {
	
}
