/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.methodAnnotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(CLASS)
@Target(ElementType.CONSTRUCTOR)
/**
 * This method was serves the purpose of 
 * calling a similarly named method
 * with a different signature and the 
 * actual important code.
 * @since 1.3.0 (04.08.2020 12:44)
 * @author Yaron Efrat
 */
public @interface OverloadingWrapperConstructor {
	
}
