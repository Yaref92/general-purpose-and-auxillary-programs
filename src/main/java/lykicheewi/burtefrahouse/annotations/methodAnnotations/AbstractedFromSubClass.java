/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.methodAnnotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(CLASS)
@Target(METHOD)
/** 
 * The method was pulled up as abstract
 * decleration from a subclass.
 * @since 0.16.0 (02.12.2020 11:47:38 AM)
 * @author Yaron Efrat
 */
public @interface AbstractedFromSubClass {

	/**
	 * The subclass from which it
	 * was abstracted
	 * @since ?.?.? (02.12.2020 11:49:46 AM)
	 * @return
	 */
	public String subclass() default "";
}
