/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.methodAnnotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(CLASS)
@Target(METHOD)
/**
 *
 * @since 0.16.0 (03.12.2020 10:52:55 AM)
 * @author Yaron Efrat
 */
public @interface ExtractedFromAnotherClass {

	/**
	 * The class from which it
	 * was extracted
	 * @since ?.?.? (03.12.2020 10:53:30 AM)
	 * @return
	 */
	public String originClass() default "";
}
