/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.methodAnnotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(CLASS)
@Target(METHOD)
/**
 * This method was serves the purpose of 
 * calling a similarly named method
 * with a different signature and the 
 * actual important code.
 * @since 1.5.0 (14.08.2020 10:59)
 * @author Yaron Efrat
 */
public @interface WrapperMethod {
	
}
