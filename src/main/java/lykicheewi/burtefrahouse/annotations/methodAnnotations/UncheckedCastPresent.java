/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.methodAnnotations;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(METHOD)
/** 
 * A Method annotation that informs readers
 * that an unchecked cast is present.
 * It has a single String to indicate 
 * severity.
 * Either Safe, Unsure, or Unsafe.
 * @author Yaron Efrat
 */
public @interface UncheckedCastPresent {
	/**
	 * @apiNote Please use Safe, Unsure, or Unsafe
	 */
	public String severity() default "Unsure";
}
