/**
 * 
 */
package lykicheewi.burtefrahouse.annotations;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(CLASS)
@Target({ FIELD, METHOD, CONSTRUCTOR })
/**
 *
 * @since 0.16.0 (17.11.2020 2:35:38 PM)
 * @author Yaron Efrat
 */
public @interface PulledUpFromSubClass {
	public String subClass() default "?";	
}
