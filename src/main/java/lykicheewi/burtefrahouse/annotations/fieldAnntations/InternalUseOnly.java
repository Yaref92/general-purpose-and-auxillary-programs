/**
 * 
 */
package lykicheewi.burtefrahouse.annotations.fieldAnntations;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(FIELD)
/** 
 * The associated field is for internal use only
 * and does not need getters, setters or public access
 * @author Yaron Efrat
 */
public @interface InternalUseOnly {

}
