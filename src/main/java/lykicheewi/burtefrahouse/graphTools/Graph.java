/**
 * 
 */
package lykicheewi.burtefrahouse.graphTools;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lykicheewi.burtefrahouse.exceptions.SizeMismatchException;
import lykicheewi.burtefrahouse.miscTools.CollectionsTools;

import java.util.Collection;
import java.util.LinkedHashMap;

/** 
 * The isDirected boolean implies whether it is a directed or undirected graph and is final since it is forbidden to switch in the middle 
 * @author Yaron Efrat
 */
public class Graph {
	protected final boolean isDirected;
	protected int numberOfNodes;
	/**
	 * @implNote We switched to using Hashmap instead 
	 * of ArrayList to store the edges in the graph
	 * @implNote IMPORTANT: Because of An access order 
	 * problem in the createPartition method, 
	 * I shall impose a linkedHashMap and not a
	 * regular one. See initialization for more details
	 * @apiNote I reworked existing comments  into
	 * a javaDocable comment block on
	 * 0.16.0 11.11.2020 14:33
	 */
	LinkedHashMap<Integer, List<Edge> > adjacencyList;

	/**
	 * Creates an instance of a graph. 
	 * This constructor receives a ready adjacency list, which is probably rare.
	 * Use the {@link #addEdge(int, int, long)}
	 * method to add edges to the graph.  
	 * @param numberOfNodes The number of nodes in the graph.
	 * @param adjacencyList A mapping of nodes and edge lists representing the edges of all the nodes in a directed manner. In undirected graphs
	 * the inverse edges is simply automatically created.
	 * @param isDirected A boolean indicating whether the graph is directed or not
	 * @throws SizeMismatchException if the supplied Adjacency list is of the wrong size
	 */
	public Graph(int numberOfNodes, Map<Integer, List<Edge> > adjacencyList, boolean isDirected) throws SizeMismatchException {
		this.isDirected = isDirected;
		if(adjacencyList.keySet().size() != numberOfNodes) {
			throw new SizeMismatchException("The supplied adjacency list is of the wrong size!");
		}
		this.numberOfNodes = numberOfNodes;
		/*
		 * This creates a LinkedHashMap with the same  mappings as adjacencyList
		 * It was updated from HashMap to LinkedHashMap, because I needed the iterator
		 * to have a defined order, specifically access order, so that if only the first 
		 * edge exists when creating partition it will start there to avoid NullPointerException
		 * Another point: In order to get it to be access order, I had to use a constructor for the size
		 * load factor and a boolean, which forced me to use forEach with a key,value tuple.
		 * This might come back to bite me in the a$$.
		 */
		this.adjacencyList = new LinkedHashMap<Integer, List<Edge>>(numberOfNodes, (float) 0.75, true); 
		adjacencyList.forEach((key,value) -> {
			this.adjacencyList.put(key, value);
		});
	}
	
	
	
	/**
	 * Creates an instance of a graph. 
	 * It also calls {@link #initializeAdjacencyList()} to initialize all the nodes in a Hashmap to empty instances of LinkedList&lt;{@link lykicheewi.burtefrahouse.graphTools.Graph.Edge}&gt;.
	 * Use the {@link #addEdge(int, int, long)}
	 * method to add edges to the graph.
	 * @param numberOfNodes The number of nodes in the graph.
	 * @param isDirected A boolean indicating whether the graph is directed or not.
	 */
	public Graph(int numberOfNodes, boolean isDirected) {
		this.isDirected = isDirected;
		this.numberOfNodes = numberOfNodes;
		initializeAdjacencyList();
	}
	
	/**
	 * Creates an instance of an undirected graph. Use the {@link #addEdge(int, int, long)}
	 * method to add edges to the graph.
	 * @param numberOfNodes The number of nodes in the graph.
	 */
	public Graph(int numberOfNodes) {
		this(numberOfNodes, false);
	}
     
	/**
	 * Initializes each of the lists in the array which represents the adjacency list
	 *
	 * @apiNote I reworked existing comments  into
	 * a javaDocable comment block on
	 * 0.16.0 11.11.2020 14:35
	 */
	private void initializeAdjacencyList() {
		/*
		 * This creates a LinkedHashMap with the same  mappings as adjacencyList
		 * It was updated from HashMap to LinkedHashMap, because I needed the iterator
		 * to have a defined order, specifically access order, so that if only the first 
		 * edge exists when creating partition it will start there to avoid NullPointerException
		 */
		this.adjacencyList = new LinkedHashMap<Integer, List<Edge> >(numberOfNodes, (float) 0.75, true);
		for (int i = 0; i < numberOfNodes; i++) {
			adjacencyList.put(i, new LinkedList<Edge>());
		}		
	}
	
	/**
	 * @apiNote ADDED IN 0.6.0 (4.4.2020 11:28) TO ADDRESS A PROBLEM IN THE HUNGARIAN
	 * ALGORITHM. NULLIFYS AND REINITIALIZES THE ADJACENCY LIST.
	 * @since 0.6.0 (4.4.2020 11:28)
	 */
	public void restartAdjacenctList() {
		adjacencyList = null;
		initializeAdjacencyList();
	}



	/** 
	 * The Edge class will be used to represent an Edge between
	 * two vertices in the graph. 
	 * It will handle both the undirected and directed cases. 
	 * @author Yaron Efrat
	 */
	static class Edge {
		public final int from, to; // Changed it to final while creating FlowGraph to protect existing edges from being changed in their head of tail
		public long weight;
		boolean isDirected;
		/*
		 * For simplicity reasons, I'm adding here a variable that's only relevant for FlowGraph.
		 * It is too much work to create a nested subclass
		 */
		public long currentFlow = 0;
		public Edge residual = null; // Ended up giving up and having an actual residual Edge and not a boolean for added simplicity
		public long capacity = -1; // Added that after realizing that I need to have weight to act as the cost, I can't user it for capacity
		
		/**
		 * Creates an instance of an Edge.
		 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
		 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
		 * @param weight The weight of the edge, which is set to a default value of 1 in case it is an unweighted graph
		 * @param isDirected A boolean for whether this is an edge of a directed graph
		 */
		public Edge(int from, int to, long weight, boolean isDirected) {
			this.from = from;
			this.to = to;
			this.weight = weight;
			this.isDirected = isDirected;
		}
		
		/**
		 * Creates an instance of an Edge with a default weight of 1. Also relevant for unweighted graphs where all weights are 1 by default.
		 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
		 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
		 * @param isDirected A boolean for whether this is an edge of a directed graph
		 */
		public Edge(int from, int to, boolean isDirected) {
			this(from, to, 1, isDirected);
		}
		
		/**
		 * Creates an instance of an Edge Which is undirected by default.
		 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
		 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
		 * @param weight The weight of the edge
		 */
		public Edge(int from, int to, long weight) {
			this(from, to, weight, false);
		}
		
		/**
		 * Creates an undirected instance of an Edge with a default weight of 1.
		 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
		 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
		 */
		public Edge(int from, int to) {
			this(from, to, 1, false);
		}
		/*
		 * For simplicity reasons, I'm adding here methods that are only relevant for FlowGraph.
		 * It is too much work to create a nested subclass
		 */
		
		/**
		 * Simply gets max capacity - current flow and returns it
		 * @return capacity currentFlow which is the remaining capacity
		 */
		public long remainingCapacity() {
			return (capacity - currentFlow);
		}
		
		/**
		 * Changes currentFlow by adjustBy if the resulting flow is legal (0 to capacity inclusive if residual == false
		 * or up to capacity if residual == true).
		 * Otherwise prints a message and doesn't add anything.
		 * @param adjustBy The amount to adjust by: Add this much flow to the Edge. Can be negative.
		 */
		public void adjustFlow(long adjustBy) {
			if ((isResidual() == false) && (currentFlow + adjustBy < 0)) {
				System.out.print("Adjusting the flow not performed for the following reason:"
							+ "For non residual edges it is illegal to have negative flow");
				return;
			}
			if (currentFlow + adjustBy > capacity) {
				System.out.print("Adjusting the flow not performed for the following reason:"
						+ "An edge can't exceed its capacity");
			return;
			}
			currentFlow += adjustBy;
			residual.currentFlow -= adjustBy; // DON'T FORGET to update the residual as well
		}

		/**
		 * if the capacity is 0 it means it is a residual edge.
		 * In the construct edge method it will be forced to have capacity 0.
		 * @return is this edge a residual edge
		 */
		protected boolean isResidual() {
			return capacity == 0;
		}

		/**
		 * Generated by the IDE along with equals
		 */
		@Override
		public int hashCode() {
			return Objects.hash(from, to, weight);
		}

		/**
		 * I have asked the IDE to generate this equals override to be able to get specific 
		 * edges for problems like max flow.
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (!(obj instanceof Edge)) {
				return false;
			}
			Edge other = (Edge) obj;
			return from == other.from && to == other.to; // && weight == other.weight; // For now really complicates matters. Only relevant for multigraphs anyways
		}

		/**
		 * This custom toString prints:<br>
		 * {@link #from}(<)--(~~{@link #currentFlow}/{@link #capacity}~~){@link #weight}-->{@link #to}<br>
		 * Where the things in parantheses are added only if:<br>
		 * for the first one only for undirected edges<br>
		 * for the second one for edges with capacity of -1 (not flow graph)<br>
		 * @apiNote Decided to finally add a proper toString method for the
		 * edges while in a long debug session after a faulty zero covering
		 * exception in the matching tool
		 * @since 0.14.0 (28.10.2020 11:27:48 AM)
		 * @return The custom string depicting the edge
		 */
		@Override
		public String toString() {
			String edgeString = String.valueOf(from);
			if (!isDirected) {
				edgeString = edgeString.concat("<");
			}
			edgeString = edgeString.concat("--");
			if (capacity != -1) {
				edgeString = edgeString.concat(
						String.format("~~%s/%s~~", currentFlow, capacity));
			} 
			edgeString = edgeString.concat(
					String.format("%s-->%s", weight, to));
			return edgeString;
		}

		/**
		 *
		 * @since 0.16.0 (12.11.2020 9:42:20 AM)
		 * @return the from
		 */
		int getFrom() {
			return from;
		}

		/**
		 *
		 * @since 0.16.0 (12.11.2020 9:42:20 AM)
		 * @return the to
		 */
		int getTo() {
			return to;
		}
		
	}
	
	/**
	 * This method adds an edge to the adjacency list. If the graph is undirected, it will also automatically
	 * add the "inverse" edge.
	 * NEW IN 0.3.0 -&gt; Changed so it returns the edge added. Useful for creating capacity for flow problems!
	 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
	 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
	 * @param weight The weight of the edge, which is set to a default value of 1 in case it is an unweighted graph
	 * @return The created edge
	 */
	public Edge addEdge(int from, int to, long weight) {
		Edge edge1 = new Edge(from, to, weight, isDirected);
		adjacencyList.get(from).add(edge1);
		// If an undirected graph, automatically adds the inverse edge with the same weight
		if (!isDirected) {
			Edge edge2 = new Edge(to, from, weight, isDirected);
			adjacencyList.get(to).add(edge2);
		}
		return edge1;
		
	}
	
	/**
	 * This method adds an edge to the adjacency list. If the graph is undirected, it will also automatically
	 * add the "inverse" edge. The edge has a default weight of 1. Calls {@link #addEdge(int, int, long)}
	 * NEW IN 0.3.0 -&gt; Changed so it returns the edge added. Useful for creating capacity for flow problems!
	 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
	 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
	 * @return The created edge
	 */
	public Edge addEdge(int from, int to) {
		return addEdge(from, to, 1);
	}
	
	/**
	 * Will retrieve the edge that goes from -&gt; to.
	 * WARNING: This implementation assumes this is NOT
	 * a multigraph.
	 * NOTE: This is a bad implementation probably, since it is O(n).
	 * possible TODO would be to find a better way to achieve that goal.
	 * @param from the tail of the requested edge
	 * @param to the head of the requested edge
	 * @return The requested edge or null if the edge doesn't exist
	 */
	public Edge getEdge(int from, int to) {
		for (Edge edge : adjacencyList.get(from)) {
			if (edge.equals(new Edge(from,to))) {
				return edge;
			}
			
		}
		return null;
	}
	
	/**
	 * This method takes the from and to of an
	 * edge, and a boolean called shouldInverse.
	 * It then removes the edge from the adjacencyList,
	 * and if shouldInverse is true, it also removes
	 * the inverse if it exists.
	 * @apiNote A serious bug in the matching tool
	 * made me come back here. I actually need
	 * a method to remove a node from the graph,
	 * but I think I should add this, both for
	 * completion and because it might make the 
	 * removeNode method more succint and 
	 * readable.
	 * @since 0.16.0 (11.11.2020 2:47:05 PM)
	 * @param from
	 * @param to
	 * @param shouldInverse
	 * @return the removed edge, or null if it
	 * didn't exist
	 */
	public Edge removeEdge(int from, int to, boolean shouldInverse) {
		Edge edge1 = CollectionsTools.findObjectInListByField(this.adjacencyList.get(from), 
				Edge::getTo, to);
		adjacencyList.get(from).removeIf((Edge edge) -> edge.equals(edge1));
		// If an undirected graph, automatically adds the inverse edge with the same weight
		if (shouldInverse) {
			Edge edge2 = CollectionsTools.findObjectInListByField(this.adjacencyList.get(to), 
					Edge::getTo, from);
			adjacencyList.get(to).removeIf((Edge edge) -> edge.equals(edge2));
		}
		return edge1;
		
	}
	
	/**
	 * Currently prints lines containing the node, and then an arrow to all the nodes it points to.
	 * So basically the adjacency list.
	 * NEW IN 0.3.0: The edge will now shoe their weights as well.
	 * @return A String representing the adjacency list
	 */
	@Override
	public String toString() {
		// TODO Make it a bit more sophisticated maybe
		String graphString = new String();
		for (int i =0; i < numberOfNodes; i++) {
			graphString += Integer.toString(i) + " -> [ ";
			for ( Edge edge : adjacencyList.get(i)) {
				graphString += "(" + Integer.toString(edge.to) + "; W:" + Long.toString(edge.weight) + ") ";
			}
			graphString += "]\n";
		}
		return graphString;
		
	}
		
}

/**
 * A FlowGraph is a directed graph that has a "source" node that has only outgoing edges and is considered the
 * "start" of the flow, and a "sink" node, that has only ingoing edges and is considered the
 * "end" of the flow.
 * FlowGraph is directed, and so it always sends true to isDirected when calling the super() constructor
 * @author Yaron Efrat
 */
class FlowGraph extends Graph {

	int source, sink;
	
	/**
	 * Creates a FlowGraph with numberOfNodes nodes.
	 * @param numberOfNodes The number of nodes in the graph
	 * @param source The number of the node chosen to be the source
	 * @param sink The number of the node chosen to be the sink
	 * @throws UnsupportedOperationException - if the created graph is illegal
	 */
	public FlowGraph(int numberOfNodes, int source, int sink) throws UnsupportedOperationException {
		super(numberOfNodes, true);
		/* 
		 * The FlowGraph will be created with source and sink as the source and sink 
		 * unless it creates an illegal flow graph,
		 * in which case an exception will occur. 
		 */
		this.source = source;
		this.sink = sink;
		if (!isFlowGraph()) {
			throw new UnsupportedOperationException("The constructed graph is not a legal flow graph! Please specify legal source and sink.");
		}
	}
	
	/**
	 * Creates a FlowGraph with numberOfNodes nodes and sets the source and sink to default nodes size-2 and size-1
	 * @param numberOfNodes The number of nodes in the graph
	 */
	public FlowGraph(int numberOfNodes) {
		/* 
		 * The FlowGraph will be created with size-2 and size-1 as the source and sink 
		 * unless it creates an illegal flow graph,
		 * in which case an exception will occur. 
		 */
		this(numberOfNodes, numberOfNodes - 2, numberOfNodes - 1);
	}
	
	

	/**
	 * Tries to Creates an instance of a flow graph.
	 * This constructor receives a ready adjacency list, which is probably rare.
	 * It constructs it using the Graph constructor, and then immediately sets 
	 * the source and sink 
	 * and checks if that is a legal flow graph.
	 * Should it fail, it will throw an UnsupportedOperationException
	 * @param numberOfNodes The number of nodes in the graph
	 * @param adjacencyList A mapping of nodes and edge lists representing the edges of all the nodes in a directed manner.
	 * @param source The number of the node chosen to be the source
	 * @param sink The number of the node chosen to be the sink
	 * @throws UnsupportedOperationException if the created graph is illegal
	 * @throws Exception
	 */
	public FlowGraph(int numberOfNodes, Map<Integer, List<Edge>> adjacencyList, int source, int sink) throws Exception {
		super(numberOfNodes, adjacencyList, true);
		/* 
		 * The FlowGraph will be created with source and sink as the source and sink 
		 * unless it creates an illegal flow graph,
		 * in which case an exception will occur. 
		 */
		this.source = source;
		this.sink = sink;
		if (!isFlowGraph()) {
			throw new UnsupportedOperationException("The constructed graph is not a legal flow graph! Please specify legal source and sink.");
		}
	}
	
	/**
	 * Tries to Creates an instance of a flow graph.
	 * This constructor receives a ready adjacency list, which is probably rare.
	 * It constructs it using the Graph constructor, and then immediately sets 
	 * the source and sink to default nodes size-2 and size-1 
	 * and checks if that is a legal flow graph.
	 * Should it fail, it will throw an UnsupportedOperationException
	 * @param numberOfNodes The number of nodes in the graph
	 * @param adjacencyList A mapping of nodes and edge lists representing the edges of all the nodes in a directed manner. In undirected graphs
	 * the inverse edges is simply automatically created.
	 * @throws UnsupportedOperationException if the created graph is illegal
	 * @throws Exception
	 */
	public FlowGraph(int numberOfNodes, Map<Integer, List<Edge>> adjacencyList) throws Exception {
		this(numberOfNodes, adjacencyList, numberOfNodes - 2, numberOfNodes - 1);
	}
	
	/**
	 * This constructor is actually different from the others, in that it's used to convert an existing 
	 * BipartiteGraph into a FlowGraph by "attaching" a source to the leftNodes and "attaching"
	 * the "rightNodes" to a sink.
	 * @param oldGraph A legal BipartiteGraph
	 * @throws Exception
	 */
	public FlowGraph(BipartiteGraph oldGraph) throws Exception {
		/*
		 * First, we need to make all edges be directed from left to right. 
		 * We will use a specialized method in BipartiteGraph called leftToRightDirectedness.
		 * By using this constructor with a number of nodes that is greater than oldGraph's
		 * by two, we know that the source and sink will be added as the two additional
		 * nodes, i.e. at the end of the numbering, which serves two functions:
		 * First, the old adjacency list is not affected.
		 * Second, we know their keys which makes them easy to access.
		 */
		/*
		 * It's funny, I got my own exception 
		 * "The supplied adjacency list is of the wrong size!"
		 * because I didn't to extra key,value pairs to the Map for the source and the 
		 * sink. I will fix it by creating a local map variable,
		 * getting the leftToRight... return value, and adding the pairs...
		 * but we can't do it here, so we need to add something to BipartiteGraph to adjust for this.	 
		 */
		this(oldGraph.numberOfNodes, oldGraph.leftToRightDirectedness()); // Removed the +2 because of the new solution involving an auxiliary graph that has the source and sink already
		/*
		 * Now we need to connect the source to all of the nodes in leftNodes.
		 * AT THE MOMENT ONLY SUPPORTS EDGES OF CAPACITY 1 FROM SOURCE
		 * TODO possibly add support for variable capacity from the source node
		 */
		for (Integer node : oldGraph.leftNodes) {
			this.addEdge(numberOfNodes - 2, node, 0).capacity = 1;
		}
		/*
		 * Now we need to connect all of the nodes in rightNodes to the sink.
		 * AT THE MOMENT ONLY SUPPORTS EDGES OF CAPACITY 1 TO SINK
		 * TODO possibly add support for variable capacity to the source node
		 */
		for (Integer node : oldGraph.rightNodes) {
			this.addEdge(node, numberOfNodes - 1, 0).capacity = 1;
		}
	}
	
	/**
	 * Goes through all edges of the graph, creates residual edges with capacity 0 for them, and appends them to the adjacency list.
	 */
	public void generateResidualEdges() {
		Collection<List<Edge>> edgesLists = new LinkedList<List<Edge>>();
		/*
		 * Got a ConccurrentModificationException, which I assume is 
		 * because I edited the adjacency list while iterating through 
		 * values related to it. 
		 * So I now copy the values elsewhere.
		 */
		edgesLists.addAll(adjacencyList.values());
		for (List <Edge> edges : edgesLists) {
			for (Edge edge : edges) {
				/*
				 * Added the if because without it it added residuals
				 * to the residuals which is completely unnecessary
				 */
				if (!(edge.capacity == 0)) {
					edge.residual = new Edge(edge.to, edge.from, edge.weight, true);
					/* BECAUSE OF THE CHANGE IN 0.3.0, HAD TO CHANGE THE 0 to edge.weight AND
					 * ADDRESS THIS SEPARATELY
					 */ 
					edge.residual.capacity = 0;
					adjacencyList.get(edge.to).add(edge.residual);
				}
		    }
		}
	}

	/**
	 * Kind of a clumsy method for now. Might improve at a later stage.
	 * Checks whether the Graph truly is a flow graph by taking the collection of 
	 * List<Edge> defined by adjacencyList.values(), and running a nested foreach for
	 * loops to access the individual edge and check all of them to make sure
	 * the sink has no outgoing edges and the source no incoming edges.
	 * @return true of the above condition is met and false otherwise.
	 */
	private boolean isFlowGraph() {
		// TODO Auto-generated method stub
		Collection<List<Edge>> edgesLists = adjacencyList.values();
		for (List <Edge> edges : edgesLists) {
			for (Edge edge : edges) {
				if ((edge.to == source) || (edge.from == sink)) {
					return false;
			    }
		    }
		}
		return true;
		
	}

	/**
	 * Again thanks to WilliamFiset for the inspiration.
	 * Will show the edges in a format similar to the one he suggested:
	 * Edge ?->? | Flow/capacity ?/? | is residual: ?
	 */
	@Override
	public String toString() {
		// TODO Make it a bit more sophisticated maybe
		String graphString = new String();
		for (int i =0; i < numberOfNodes; i++) {
			for ( Edge edge : adjacencyList.get(i)) {
				// First the from, with overly complicated condition
				graphString += "Edge " 
			+ ((edge.from == numberOfNodes - 2) ? "source" 
					: ((edge.from == numberOfNodes - 1) 
							? "sink  " : (Integer.toString(edge.from) + "     ")));
				// then the to, with overly complicated condition
				graphString += "->" + ((edge.to == numberOfNodes - 2) ? "source" 
						: ((edge.to == numberOfNodes - 1) 
								? "sink  " : (Integer.toString(edge.to) + "     ")));
				// Now separator and flow / capacity
				graphString += " | Flow/Capacity " + edge.currentFlow + "/" + edge.capacity + " | ";
				// And is it residual
				graphString += "Is it residual?: " + edge.isResidual() + "\n";
			}			
		}
		return graphString;
	}
	
	
}
