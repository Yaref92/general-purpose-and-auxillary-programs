/**
 * 
 */
package lykicheewi.burtefrahouse.graphTools;

import java.util.LinkedList;
import java.util.Queue;

import lykicheewi.burtefrahouse.graphTools.Graph.Edge;

/**
 * @author Yaron Efrat
 *
 */
public class GraphSearchToolKit {

	/**
	 * 
	 */
	public GraphSearchToolKit() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
    /**
     * 
     * @author Yaron Efrat
     * An abstract Searcher class to allow different searching techniques (mostly DFS and BFS)
     * to be implemented.
     * post 0.2.0: changed from private to protected to access from
     * GraphProblemSolver
     */
	protected static abstract class Searcher {
		Graph targetGraph; // The graph to be searched over
		boolean[] visited; // An array representing which of the nodes of a graph were visited
		int[] previousNode; // An array to represent for each node which node comes before it in the search. Useful for path reconstruction.
		int currentNode; // The node we are currently in
		int start; // The node we want to start from
		int end; // The node we want to reach (if necessary for the application we're using the searcher for)
		
		/**
		 * Creates an instance of a searcher that is based on the graph and searches paths from start to end
		 * @param graph - The graph on which the searcher searches
		 * @param start - The node where the search starts
		 * @param end - The node where the search should end
		 */
		public Searcher(Graph graph, int start, int end) {
			targetGraph = graph;
			this.start = start;
			currentNode = start; // Because we start the search at the start node
			this.end = end;
			/*
			 * Moved these initialization here because it threw lykicheewi.burtefrahouse.exceptions
			 */
			visited = new boolean[targetGraph.numberOfNodes];
			previousNode = new int[targetGraph.numberOfNodes];
			visited[start] = true;
		}
		
		/**
		 * Creates an instance of a searcher that is based on the graph and searches paths from node 0 to node 
		 * graph.numberOfNodes - 1 (default values).
		 * @param graph - The graph on which the searcher searches
		 */
		public Searcher(Graph graph) {
			this(graph,	0, graph.numberOfNodes - 1);			
		}
		
		public abstract void performSearch();
		
		public abstract void advanceSearch();
		
		/**
		 * Uses the previous array to retrace the search's steps
		 * to find the path from start to end.
		 * @return the reconstructed path as a LinkedList<Integer>
		 */
		public LinkedList<Integer> reconstructPath() {
			LinkedList<Integer> pathFromStartToEnd = new LinkedList<Integer>();
			/*
			 *  We will reuse currentNode, since we only use reconstructPath
			 *  after reaching the end, and currentNode will be at the end.
			 *  With the constructed classes reconstructing the path 
			 *  becomes surprisingly easy.
			 *  Add the current node to path, and go to the previous node 
			 *  array to find out what came before it.
			 *  once you reach the start, finish the loop...
			 */
			while (currentNode != start) {
				pathFromStartToEnd.add(currentNode);
				currentNode = previousNode[currentNode];
			}
			/*
			 *  ... but don't forget to add the start node (which we will always converge to) to the start of the path.
			 */
			pathFromStartToEnd.add(currentNode);
			return pathFromStartToEnd;
		}
		
		/**
		 * Uses the previous array to retrace the search's steps
		 * to find the path from start to end, represented
		 * as a list of traversed edges.
		 * Better for some problems, such as max flow.
		 * @return the reconstructed path as a LinkedList<Edge>
		 */
		public LinkedList<Edge> reconstructEdgePath() {
			LinkedList<Edge> pathFromStartToEnd = new LinkedList<Edge>();
			/*
			 *  We will reuse currentNode, since we only use reconstructPath
			 *  after reaching the end, and currentNode will be at the end.
			 *  With the constructed classes reconstructing the path 
			 *  becomes surprisingly easy.
			 *  Get the edge from previousNode[currentNode] to currentNode
			 *  with the relevant method, add it to path, 
			 *  and go to the previous node array
			 *  to find out what is the previous node.
			 *  once you reach the start, finish the loop...
			 */
			Edge tempEdge;
			while (currentNode != start) {				
				tempEdge = targetGraph.getEdge(previousNode[currentNode], currentNode);
				pathFromStartToEnd.add(tempEdge);
				currentNode = previousNode[currentNode];
			}
			/*
			 *  ... Unlike the regular reconstructPath, here there is nothing to add after the loop.
			 */
			return pathFromStartToEnd;
		}
		
	}
	
	/**
	 * TODO Implement when necessary
	 * @author Yaron Efrat
	 *
	 */
	class DepthFirstSearcher extends Searcher {

		/**
		 * @param graph
		 * @param start
		 * @param end
		 */
		public DepthFirstSearcher(Graph graph, int start, int end) {
			super(graph, start, end);
			// TODO Auto-generated constructor stub
		}

		/**
		 * @param graph
		 */
		public DepthFirstSearcher(Graph graph) {
			super(graph);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void performSearch() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void advanceSearch() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	class BreadthFirstSearcher extends Searcher {
		/*
		 *  Moved the initialization of all variables to constructor
		 *  so that hopefully it can easily inherit from 
		 *  Searcher. 
		 */
		Queue<Integer> searchQueue;
		boolean reachedEnd;
		int moveCount, nodesLeftInLayer, nodesInNextLayer; // nodesLeftInLayer is initialized to 1 because we start at the starting node.
		
		/**
		 * @param graph
		 * @param start
		 * @param end
		 */
		public BreadthFirstSearcher(Graph graph, int start, int end) {
			super(graph, start, end);
			/*
			 *  Moved the initialization of all variables to here
			 *  so that hopefully it can easily inherit from 
			 *  Searcher. 
			 */
			searchQueue = new LinkedList<Integer>();
			moveCount = 0; nodesLeftInLayer = 1; nodesInNextLayer = 0;
			reachedEnd = false;
			// Now we will add the first node to the queue
			searchQueue.add(start);
		}

		/**
		 * @param graph
		 */
		public BreadthFirstSearcher(Graph graph) {
			this(graph,	0, graph.numberOfNodes - 1);
		}

		@Override
		public void performSearch() {
			// TODO Auto-generated method stub
			while (!searchQueue.isEmpty()) {
				currentNode = searchQueue.poll(); // retrieves and removes the head of the queue to be current node
				nodesLeftInLayer--; // Since we've moved to current node by removing it, the layer has one less node
				// If we're at the end node, then we can say we've reached the end and stop searching 
				if (currentNode == end) {
					reachedEnd = true;
					break;
				}
				advanceSearch();
				/*
				 * If we have depleted the layer, then it's time to move on to 
				 * the next layer (if there is one) by switiching the nodes in 
				 * the next layer to be in this layer.
				 * We also advance the move count since we've made
				 * one more step
				 */
				if (nodesLeftInLayer == 0) {
					nodesLeftInLayer = nodesInNextLayer;
					nodesInNextLayer = 0;
					moveCount++;
				}
			}
			// One way or another, the search will have ended.			
		}

		/**
		 * Looks at the edges that go from the current node. 
		 * For every node that an edge is pointing to and wasn't visited
		 * yet, it adds it to the queue, marks it as visited,
		 * increments nodesInNextLayer, and makes sure to update its
		 * entry in the previousNode array.
		 */
		@Override
		public void advanceSearch() {
			// TODO Auto-generated method stub
			for (Edge edge : targetGraph.adjacencyList.get(currentNode)) {
				if (visited[edge.to]) {
					continue;
				}
				searchQueue.add(edge.to);
				visited[edge.to] = true;
				nodesInNextLayer++;
				previousNode[edge.to] = currentNode;
			}
			
		}
		
	}
	
	/** 
	 * @author Yaron Efrat
	 * Created for weighted graph when I realized I'll need
	 * to account for capacity in the search of flow graphs.
	 * Will be used for shortest path and max flow problems,
	 * so a char variable will handle knowing which problem type to solve
	 * TODO ADD COMMENTS WHERE NECESSARY
	 */
	class WeightedBreadthFirstSearcher extends BreadthFirstSearcher {

		char problemType;
		/**
		 * @param graph
		 * @param start
		 * @param end
		 * @param problem - 'f' for flow problems and 'p' for path finding problems
		 */
		public WeightedBreadthFirstSearcher(Graph graph, int start, int end, char problem) {
			super(graph, start, end);
			problemType = problem;
		}

		/**
		 * @param graph
		 * @param problem - 'f' for flow problems and 'p' for path finding problems		 
		 */
		public WeightedBreadthFirstSearcher(Graph graph, char problem) {
			super(graph);
			// TODO Auto-generated constructor stub
			problemType = problem;
		}
		
		/**
		 * OVERRIDES the method from the regular breadth first
		 * searcher to account for flow.
		 * Looks at the edges that go from the current node. 
		 * For every node that an edge is pointing to and wasn't visited
		 * yet, it adds it to the queue, marks it as visited,
		 * increments nodesInNextLayer, and makes sure to update its
		 * entry in the previousNode array.
		 */
		@Override
		public void advanceSearch() {
			if (problemType == 'p') {
				/* 
				 * TODO At the moment I just call the superclass
				 * method. If necessary I shall change that.
				 */
				super.advanceSearch();
			}
			/*
			 * For flow problem also avoid traversing edges whose remaining capacity is 0
			 */
			else if (problemType == 'f') {
				// TODO Auto-generated method stub
				for (Edge edge : targetGraph.adjacencyList.get(currentNode)) {
					if ((visited[edge.to]) || (edge.remainingCapacity() <= 0) ) {
						continue;
					}
					searchQueue.add(edge.to);
					visited[edge.to] = true;
					nodesInNextLayer++;
					previousNode[edge.to] = currentNode;
				}
			}
			
		}
		
	}
}

