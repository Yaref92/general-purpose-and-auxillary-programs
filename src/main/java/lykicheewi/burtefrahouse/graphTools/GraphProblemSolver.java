/**
 * 
 */
package lykicheewi.burtefrahouse.graphTools;

import static java.lang.Math.min;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.stream.Collectors;

import lykicheewi.burtefrahouse.exceptions.FaultyZeroCoveringException;
import lykicheewi.burtefrahouse.exceptions.WrongSolverException;
import lykicheewi.burtefrahouse.graphTools.Graph.Edge;
import lykicheewi.burtefrahouse.graphTools.GraphSearchToolKit.*;
import lykicheewi.burtefrahouse.miscTools.ArrayTools;
import lykicheewi.burtefrahouse.miscTools.Matrix;

import java.util.StringTokenizer;

/**
 * @author Yaron Efrat
 *
 */
public class GraphProblemSolver {	

	/** 
	 * @author Yaron Efrat
	 * It might not be necessary to have this parent class to all solvers,
	 * but if I ever a ton of different problems that do share a lot of
	 * things, I prefer to have it ready for implementation
	 */	
	private static abstract class GraphGeneralSolver {
		// TODO Implement if necessary

	}

	/** 
	 * @author Yaron Efrat
	 * Thanks for WilliamFiset for the instructional videos.
	 */
	@SuppressWarnings("unused")
	private static abstract class NetworkFlowGeneralSolver extends GraphGeneralSolver {
		// To avoid overflow, we define a fake infinity value that is big but not too big
		static final long INF = Long.MAX_VALUE / 2;
		// There is no need to solve more than once. That's were this boolean helps
		protected boolean solved;
		// The maximum flow pushed through the graph
		protected long maxFlow;
		// The flow graph to solve over
		protected FlowGraph graphToFlow;
		// It is needed for creating instances of Searcher and its subclasses
		GraphSearchToolKit toolKit = new GraphSearchToolKit();
		/*
		 *  A general Searcher to allow more than one implementation.
		 *  Might be risky, so I might have to change it to being 
		 *  declared in the subclasses
		 */
		// Searcher networkSearcher; // Too problematic

		/**
		 * 
		 * @param problemInputFile
		 * @throws FileNotFoundException
		 */
		public NetworkFlowGeneralSolver(File problemInputFile) throws FileNotFoundException {
			// TODO implement this constructor when relevant and add comments
			/*
			 * 
			 * 
			 */
			Scanner input = new Scanner(problemInputFile);
			/*
			 * 
			 */
			input.close(); // Apparently it's a thing. eclipse warned me so I listened and put it in.
		}

		/**
		 * This constructor receives a Bipartite graph and uses it to 
		 * create a flow graph
		 * @param graphToMatch - the bipartite graph to convert
		 * @throws Exception
		 */
		public NetworkFlowGeneralSolver(BipartiteGraph graphToMatch) throws Exception {
			/*
			 * Using the special constructor of FlowGraph
			 * that takes a bipartite graph and converts
			 * it to a flow graph.
			 */
			graphToFlow = new FlowGraph(graphToMatch.attachSourceAndSink());
		}

		/**
		 *  
		 * @return the maximum flow from the source to the sink
		 */
		public long getMaxFlow() {
			executeSolverIfNecessary();
			return maxFlow;
		}

		/**
		 * Wrapper method that ensures we only call solveForMaxFlow
		 * once. Again, thanks to WilliamFiset for the idea.
		 */
		private void executeSolverIfNecessary() {
			if (solved) {
				return;
			}
			else {
				solveForMaxFlow();
				solved = true;
			}

		}

		protected abstract void solveForMaxFlow();		

	}

	@SuppressWarnings("unused")
	private static class EdmondsKarpNetworkFlowSolver extends NetworkFlowGeneralSolver {

		// A Searcher object from a subclass for weighted BFS
		WeightedBreadthFirstSearcher networkSearcher;

		/**
		 * @param problemInputFile
		 * @throws FileNotFoundException
		 */
		public EdmondsKarpNetworkFlowSolver(File problemInputFile) throws FileNotFoundException {
			super(problemInputFile);			
			maxFlow = 0; // Setting the counter.
		}

		/**
		 * This constructor receives a Bipartite graph and uses it to 
		 * create a flow graph
		 * @param graphToMatch - the bipartite graph to convert
		 * @throws Exception
		 */
		public EdmondsKarpNetworkFlowSolver(BipartiteGraph graphToMatch) throws Exception {
			super(graphToMatch);
			networkSearcher = toolKit.new WeightedBreadthFirstSearcher(graphToFlow, graphToFlow.source, graphToFlow.sink, 'f');
			maxFlow = 0; // Setting the counter.
		}

		@Override
		protected void solveForMaxFlow() {
			// A variable for the current flow for each iteration
			long currentFlow = -1;
			graphToFlow.generateResidualEdges(); // No choice really... I need the residual graph
			while (currentFlow != 0) {
				/* 
				 * Try to find the shortest path 
				 * from the source to the sink
				 */
				networkSearcher.performSearch();
				/* 
				 * If we didn't reach the end then we failed to push new flow
				 * and so we're done.
				 */ 
				if (networkSearcher.reachedEnd == false) {
					break;
				}
				/*
				 * Reconstruct the path of traversed edges
				 */
				LinkedList<Edge> currentPath = networkSearcher.reconstructEdgePath();
				// Find the minimal remaining capacity along the path and that would be the flow
				currentFlow = findBottleNeckValue(currentPath);
				// Push the flow through the network
				augmentPath(currentFlow, currentPath);
				// Clear the searcher for the next round
				networkSearcher = toolKit.new WeightedBreadthFirstSearcher(graphToFlow, graphToFlow.source, graphToFlow.sink, 'f');
			}


		}

		/**
		 * Goes through each Edge in the list, creates a residual Edge if one doesn't exist,
		 * and then adjusts flow by +currentFlow for regular edge, and -currentFlow 
		 * for residualEdge
		 * @param currentFlow - the bottle neck value found for this path
		 * @param currentPath - the current path represented by traversed edges.
		 */
		private void augmentPath(long currentFlow, LinkedList<Edge> currentPath) {
			// Now iterating over all edges in the path and pushing flow through them. the order doesn't matter.
			for (Edge edge : currentPath) {
				edge.adjustFlow(currentFlow);
			}
			maxFlow += currentFlow; // Updating the maximum flow by the flow we pushed.
		}

		/**
		 * Using the reconstructed edge path, it looks for the minimal remaining capacity of any edge 
		 * in the path and returns it.
		 * @param currentPath - the path from source to sink represented by the traversed edges
		 * @return The bottleneck value - The minimal remaining capacity of any edge on the currentPath
		 */
		private long findBottleNeckValue(LinkedList<Edge> currentPath) {
			long bottleNeck = Long.MAX_VALUE; // First of all initializing the bottleneck to maximal value to compare to remaining capacities
			// Now iterating over all edges in the path and making comparisons. the order doesn't matter.
			for (Edge edge : currentPath) {
				bottleNeck = min(bottleNeck, edge.remainingCapacity());
			}
			return bottleNeck;
		}

	}

	/** 
	 * @author Yaron Efrat
	 * It might not be necessary to have this intermediate class, but if I ever solve a radically
	 * different problem on bipartite graphs than matching, I prefer to have it ready for
	 * implementation
	 */
	private static abstract class bipartiteGraphGeneralSolver extends GraphGeneralSolver {
		// TODO Implement if necessary

	}

	/**
	 * Changed to public in 0.5.0 so it can be used from the MMMTPrototype
	 * @author Yaron Efrat
	 *
	 */
	public static class bipartiteGraphMatchingProblemSolver extends bipartiteGraphGeneralSolver {
		// Add Stuff
		/*
		 * Will declare the graph here for more visibility of its existence
		 */
		public BipartiteGraph graphToMatch;
		// In the solution we actually solve for max flow to find the matching, so I'm using an EdmondsKarp solver
		private EdmondsKarpNetworkFlowSolver flowSolver = null;
		// ADDED IN 0.6.0: Now there is the option to use hungarianSolver for mincost instead of flowSolver
		private HungarianMatchingSolver hungarianSolver = null;

		/**
		 * NEW IN 0.5.0: and not really clear why I didn't 
		 * add this option before.
		 * It was added for the MMMTPrototype to make use of.
		 * NEW IN 0.6.0: From research into min cost max match algorithms,
		 * I came accross the Hungarian algorithm, which DOES NOT use network 
		 * flow. From now on, you can either solve ignoring weights with network
		 * flow, or, I will add a Hungarian solver, and IT will be used for the
		 * min cost solution.
		 * ANOTHER THING RELEVANT TO 0.6.0 (2.4.2020 14:40) - I somehow missed the 
		 * HUGE bug: I didn't actually copy the graph. I missed it because I actually
		 * sent the original graph to the flow solver. 
		 * I actually don't necessarily need to fix it because it is actually obsolete in that 
		 * case, for now I'll leave it like this. I have a more important bug to deal with.
		 * @param graphToMatch - The Bipartite graph to be copied and worked on.
		 * This allows for programs that create graphs and not files to still use this solver
		 * @param shouldHungarian - If true use Hungarian algorithm for min cost. Otherwise
		 * ignore cost and go with max flow.
		 * @throws Exception 
		 */
		public bipartiteGraphMatchingProblemSolver(BipartiteGraph graphToMatch, boolean shouldHungarian) throws Exception {
			/*
			 * I forgot at first only to use this constructor with the manualPartition
			 * I have all the information I need in the received graph anyways.
			 */
			this.graphToMatch = new BipartiteGraph(graphToMatch.numberOfNodes, graphToMatch.isDirected);
			this.graphToMatch.manualPartition(graphToMatch.leftNodes, graphToMatch.rightNodes);
			if (shouldHungarian) {
				// NEW IN 0.6.0: Now we just initialise a Hungarian solver by sending it the bipartite graph
				hungarianSolver = new HungarianMatchingSolver(graphToMatch);
			}
			else {
				// Now we just initialise a network flow solver by sending it the bipartite graph
				flowSolver = new EdmondsKarpNetworkFlowSolver(graphToMatch);
			}
		}

		public bipartiteGraphMatchingProblemSolver(File problemInputFile) throws Exception {
			/*
			 * In the case of Maximum Cardnality (B)ipartite (M)atching, a file is expected which will contain three things:
			 * A line with integers separated by a whitespace representing the node numbers for the leftNodes
			 * A line with integers separated by a whitespace representing the node numbers for the rightNodes
			 * Lines with three integers separated by a whitespace representing the from, to, and weight of the edges.
			 * Since it is a MCBM problem, it will create a Bipartite graph and then immediately create from it a flow 
			 * graph, so it is better to have it be a directed map with only edge from left to right.
			 * For now there is no check for it, and so any edges from right to left WILL BE IGNORED!
			 * The safest approach is to have a good way of differentiating the numbers for each side 
			 * to have an easier time.
			 * FOR NOW, I WILL NOT IMPROVE IT SINCE I HAVE A DEADLINE.
			 * TODO Improve it
			 */
			Scanner input = new Scanner(problemInputFile);
			generatePartitionedGraph(input);
			// TODO Possibly take the rest of the code and migrate to a method
			/*
			 * Now all that's left is to get the triplets, check if the edge will leave the graph bipartite, and if so add it
			 */
			LinkedList<Integer> tempList = new LinkedList<Integer>();
			int from, to, weight;
			while (input.hasNextLine()) {					
				tempList = readIntsFromLine(input);
				/*
				 * While probably unnecessary to poll into 3 middle variables, I prefer to write three more lines to 
				 * avoid bugs and have a more readable code
				 */
				from = tempList.poll();
				to = tempList.poll();
				weight = tempList.poll();
				/* 
				 * The addEdge method will check if the edge is legal (doesn't break bipartition)
				 * If so it will be added.
				 * Otherwise, it won't and a message will be displayed
				 */
				graphToMatch.addEdge(from, to, weight);					
			}				
			input.close(); // Apparently it's a thing. eclipse warned me so I listened and put it in.
			// Now we just initialize a network flow solver by sending it the bipartite graph
			flowSolver = new EdmondsKarpNetworkFlowSolver(graphToMatch);
			/*
			 * FOR TESTING PURPOSES
			 */
			flowSolver.solveForMaxFlow();
			System.out.println(flowSolver.graphToFlow.toString());
			System.out.println("The max flow " + flowSolver.getMaxFlow());
			/*
			 * FOR TESTING PURPOSES
			 */
		}

		/**
		 * This method receives two lists of integers from a file, construct a 
		 * BipartiteGraph of size (left.size() + right.size()), and manually
		 * creates a partition using the left and right lists.
		 * @param input - a File object pointing to the file with the information of which
		 * nodes go to which list.
		 */
		private void generatePartitionedGraph(Scanner input) {			
			List<Integer> left = new LinkedList<Integer>();
			List<Integer> right = new LinkedList<Integer>();				
			// Reading the first two lines as the two parts of the graph is easy enough using a custom method
			left = readIntsFromLine(input);
			right = readIntsFromLine(input);
			/*
			 * Now we need to have a list of Integer that we will use each iteration to read the from to and weight
			 * of each edge and then promptly adding them.
			 * That means it is time to create our bipartite map and already partition it.
			 * Since we have first accepted all nodes, into the above two lists, we can use their sizes
			 * for the numberOfNodes in the constructor. In addition, I will remind again that I am
			 * assuming a directed graph for now, so I'm sending true.
			 * It will also consume less space since half as many edges will be created.
			 */
			graphToMatch = new BipartiteGraph(left.size() + right.size(), true);
			graphToMatch.manualPartition(left, right); // Using the manualPartition, we simply send the two lists			
		}

		/**
		 * ADDED IN 0.5.0.
		 * A pretty simple method. It calls flowsolver's getMasFlow,
		 * which also solves the flow problem before retrieving the max flow.
		 * It then uses a nested loop to iterate over all edges in the (solved) flow graph
		 * If there's flow through that edge, and it is not a residual edge,
		 * it is added to the List of Edge that represents the matching.
		 * It then prints the number of matches and returns the matching.
		 * @return A List of Edges that are the Maximum Matching.
		 */
		public List<Edge> getSimpleMatching() throws WrongSolverException {
			if (flowSolver == null) {
				throw new WrongSolverException("The wrong solver was defined for a simple matching");
			}
			long numOfMatches = flowSolver.getMaxFlow();
			List<Edge> matching = new LinkedList<Graph.Edge>();
			/*
			 * A nested foreach is required because the values list is in fact a collection
			 * of List<Edge>. So the outside foreach iterates over all the Lists of 
			 * edges, and the inner loop iterates over the edges in each list.
			 */
			for (List<Edge> edges : flowSolver.graphToFlow.adjacencyList.values()) {
				for (Edge edge : edges) {
					/*
					 * At first I forgot to handle the sink and the source
					 * which should not be included in the matching
					 * which caused some confusing result (a lot of nulls in 
					 * the final printing).
					 * I will now handle it with an if:
					 * It will only check the flow if the source is not the
					 * from node and also the sink is not the to node.
					 */
					if ((edge.from != flowSolver.graphToFlow.source) && (edge.to != flowSolver.graphToFlow.sink)) {
						/*
						 * If the above condition is met, we check that there is
						 * a flow through the edge and that it is not residual.
						 */
						if ((edge.currentFlow > 0) && (!edge.isResidual())) {
							matching.add(edge);
						}
					}
				}
			}
			System.out.println("The number of matches found is: " + numOfMatches);
			return matching;
		}

		/**
		 * ADDED IN 0.6.0
		 * 
		 * @return the matching List&lt;Graph.Edge&gt;
		 */
		public List<Edge> getWeightedMatching() throws WrongSolverException {
			if (hungarianSolver == null) {
				throw new WrongSolverException("The wrong solver was defined for a weighted matching");
			}
			List<Edge> matching = new LinkedList<Graph.Edge>();
			hungarianSolver.tryToMatch(matching); // The minimalCost here is a dud - I need to calculate it again
			long minimalCost = hungarianSolver.getMinCost(matching);
			System.out.println(" for a matching cost of " + minimalCost);
			return matching;
		}

		/**
		 * ADDED IN 0.5.0.
		 * Another straightforward method. 
		 * create HashMap, foreach through the matching,
		 * put(from,to).		  
		 * @param matching - The List<Edge> you get from getSimpleMatching
		 * @return - A HashMap that maps from all the Edge.from to their Edge.to
		 */
		public HashMap<Integer, Integer> convertMatchingListToMap (List<Edge> matching) {
			HashMap<Integer, Integer> convertedMatching = new HashMap<Integer, Integer>();
			for (Edge edge : matching) {
				convertedMatching.put(edge.from, edge.to);
			}
			return convertedMatching;
		}


	}

	/** 
	 *
	 * @author Yaron Efrat
	 * ADDED IN 0.6.0 to solve for min cost max matching for 
	 * the MMMT
	 */
	private static class HungarianMatchingSolver extends GraphGeneralSolver {
		// There is no need to solve more than once. That's were this boolean helps
		protected boolean solved;
		// The minimum cost. Unused for now
		// protected long minCost;
		// The flow graph to solve over
		// protected BipartiteGraph graphToMatch; //Since we really just need a matrix, might not be needed
		/*
		 * Finally, 02.04.2020, 10:41, many days after I've started,
		 * I've implemented enough of the Matrix aux class to use it for
		 * the Hungarian algorithm, so let's go with it!
		 * The titles will be String for the names, the content
		 * will be Integer for the weight of the edge between any two 
		 * names. LET'S GO ALREADY!!
		 */
		protected Matrix<Integer, Integer> hungarianMatrix;
		// The minimal matching cost initialized to 0 since it is a summation variable
		protected long minCost = 0;
		// Decided to also add a numOfMatches
		protected int numOfMatches;

		/**
		 * A constructor that takes a BipartiteGraph with weighted edges
		 * and translates it to a matrix.
		 * For now (0.6.0; 02.04.2020) if an edge between two nodes does
		 * not exist it will be assigned Integer.MAX_VALUE.
		 * The matrix will be initialized using {@linlykicheewi.burtefrahousese.miscTools.Matrix#Matrix(T[], T[])},
		 * Using the node numbers of the matched entities for the titles.
		 * @param graphToMatch The original weighted Bipartite graph that needs to be matched.
		 */
		public HungarianMatchingSolver(BipartiteGraph graphToMatch) {
			/*
			 *  What I need to do, is go over the left side and check connections
			 *  to ALL nodes on right side. 
			 *  If an edge exists, put its weight in the relevant cell of the Matrix.
			 *  Otherwise, put Integer.MAX_VALUE there.
			 *  It will first create a Matrix Using the node numbers of the matched 
			 *  entities for the titles.
			 */
			Integer[] tempLeftNodes = new Integer[graphToMatch.leftNodes.size()]; 
			Integer[] tempRightNodes = new Integer[graphToMatch.rightNodes.size()];
			graphToMatch.leftNodes.toArray(tempLeftNodes);
			graphToMatch.rightNodes.toArray(tempRightNodes);
			/*
			 * The preparation I did is creating temporary Integer arrays and populating them with
			 * the leftNodes and rightNodes Lists to use when constructing the new empty Matrix
			 * with the Integers as row and column titles
			 */
			hungarianMatrix = new Matrix<Integer, Integer>(tempLeftNodes, tempRightNodes);
			/*
			 * I need to handle the case in which the number of rows and columns is not equal.
			 * I will add a row or column with Integer.MAX_VALUE 
			 * (2.4.2020 22:49 update - seen a video with 0 instead and discarding fake  matches. think it will be easier)
			 * So if tempLeftNodes.size() != tempRightNodes.size()
			 * but first, let's populate the Matrix to avoid index out of bound or null pointer
			 * Exceptions.
			 */
			/*
			 * We shall foreach through the leftNodes. better yet, a regular for, and we will use the
			 * temp arrays. we will then for each through the edges. We'll send the titles and weight. 
			 */
			/*
			 * (2.4.2020, 15:16) I've thought of a way to know which edges do not exist:
			 * HOWEVER I THOUGHT OF SOMETHING BETTER!
			 * (2.4.2020, 15:17) I WILL POPULATE THE ENTIRE MATRIX WITH Integer.MAX_VALUE
			 * in advance. That way, the existing edges will change to the correct weights
			 * and the others WILL BE THE VALUE I NEED THEM TO BE!
			 */
			/*
			 * (2.4.2020 18:20) I realized that thanks to my new method, It will actually be easier to
			 * add the extra rows or columns earlier!
			 * 23:19 - Adding extra rows or columns should be done without changing the rowNum colNum 
			 * variables, and so we can remember how many we had originally and ignore the dummy ones
			 */
			/*
			 * (4.4.2020, 9:32) Yesterday night I realized that we can add the extra rows after the row
			 * and column reduction.
			 */
			hungarianMatrix.fillMatrix(Integer.MAX_VALUE);
			for (int i = 0; i < tempLeftNodes.length; i++) {
				for (Edge edge : graphToMatch.adjacencyList.get(tempLeftNodes[i])) {
					hungarianMatrix.populateElement((Integer) tempLeftNodes[i], (Integer) edge.to, (Integer) (int) edge.weight);
				}
			}
			// TEST
			//			System.out.println();
			//			hungarianMatrix.printMat(true);
			//			System.out.println();
			/*
			 * According to a comment by ? in
			 * https://www.youtube.com/watch?v=KiHLpX0vLGI
			 * to find the minimum number of lines you need to
			 * use the 0 entries as edges and find a maximum matching.
			 */
		}

		public long getMinCost(List<Edge> matching) {
			/*
			 * Had to add 0.6.1 8.4.2020 11:16
			 * because row and column titles
			 */
			int rowIndex, colIndex;
			Integer[] rowTitles = hungarianMatrix.getRowTitles();
			Integer[] colTitles = hungarianMatrix.getColTitles();
			for (Edge match : matching) {
				rowIndex = ArrayTools.findInArray(match.from, rowTitles);
				colIndex = ArrayTools.findInArray(match.to, colTitles);
				minCost += hungarianMatrix.getElement(rowIndex, colIndex);
			}
			return minCost;
		}

		/**
		 *  
		 * @param matching HAD TO ADD 0.6.1 7.4.2020 14:48 TO FACILITATE RETURNING IT FROM THE SOLVER.
		 * Would have been a nightmare otherwise
		 * @return the minimum cost to maximally match
		 */
		public void tryToMatch(List<Edge> matching) {
			executeSolverIfNecessary(matching);
		}

		/**
		 * Wrapper method that ensures we only call solveForMaxFlow
		 * once. Again, thanks to WilliamFiset for the idea.
		 * @param matching HAD TO ADD 0.6.1 7.4.2020 14:48 TO FACILITATE RETURNING IT FROM THE SOLVER.
		 * Would have been a nightmare otherwise
		 */
		private void executeSolverIfNecessary(List<Edge> matching) {
			if (solved) {
				return;
			}
			else {
				matchForMinCost(matching);
				solved = true;
			}

		}

		/**
		 * In this method we shall execute the actual Hungarian algorithm
		 * Starting with row reductions according to minimal value,
		 * then column reductions, then the "full Hungarian"
		 * IMPORTANT NOTE 0.6.1, 7.4.2020, 12:19 - It would seem I made a 
		 * critical error - I've so far waited with adding the dummy rows/columns
		 * until after all the reductions and covering.
		 * THAT WAS A MISTAKE! 
		 * THEY HAVE TO BE ADDED AT THE BEGINNING!!
		 * I should add methods to Matrix to handle adding rows or columns
		 * Maybe also to fill rows and columns..
		 * @param matching HAD TO ADD 0.6.1 7.4.2020 14:48 TO FACILITATE RETURNING IT FROM THE SOLVER.
		 * Would have been a nightmare otherwise
		 */
		private void matchForMinCost(List<Edge> matching) {
			// I prefer to leave the original untarnished
			Matrix<Integer, Integer> hungarianClone = null;			
			try {
				hungarianClone = hungarianMatrix.clone();
			} catch (CloneNotSupportedException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
			// A variable to save the current minimum in row, column or whatever
			int currentMin;
			/*
			 * 0.16.0 12.11.2020 10:35 - 
			 * As part of an attempt to fix the bug
			 * where a faulty zero covering is produced,
			 * I have decided to both migrate the adding of extra
			 * rows or columns to a method
			 * and also streamline it:
			 * instead of calling a getXNum *8* times,
			 * I'll reduce it to 2, and instead of having 
			 * a separate excessRows and excessColumns,
			 * I'll have a single int representing both
			 * of them in the same time:
			 * columnsMinusRows, which will be equal to
			 * hungarianMatrix.getColNum() - hungarianMatrix.getRowNum().
			 * That way, a positive answer will be equivalent to
			 * excessRows = answer && excessColumns = 0, 
			 * a negative answer will be equivalent to
			 * excessRows = 0 && excessColumns = answer,
			 * and a positive answer will be equivalent to
			 * excessRows = 0 && excessColumns = 0.
			 * This new solution will save space and time and will
			 * also be more readable.
			 * IMPROVE202011121040 
			 */
			/*
			 * We will use this to avoid wasting time on trying to 
			 * reduce dummy rows/columns in addition to deciding
			 * if any dummy rows/columns are needed
			 */
			int columnsMinusRows = hungarianMatrix.getColNum() - hungarianMatrix.getRowNum();
			int numToFillDummiesWith = hungarianMatrix.maxInMatrix(); // Also added as part of IMPROVE202011121040
			//TEST
//			numToFillDummiesWith = 0;
			//TEST
			if (columnsMinusRows != 0) {
				addExcessRowsOrColumnsWhenNecessary(hungarianClone, columnsMinusRows, numToFillDummiesWith);
			}
			// OLD CODE (before 0.16.0 12.11.2020 10:55)
//			/*
//			 * Time to add any necessary fake rows/columns.
//			 * They will get the titles of -1
//			 * Word of advice : avoid titles of -1 to your origial matrix
//			 * Will create two ints for excess rows and columns. one of them will
//			 * obviously be 0, but we can't know which one.
//			 * We will use them to avoid wasting time on trying to reduce 
//			 * dummy rows/columns.
//			 */
//			int excessRows = 0, excessColumns = 0;
//			if (hungarianMatrix.getRowNum() < hungarianMatrix.getColNum()) {
//				excessRows = hungarianMatrix.getColNum() - hungarianMatrix.getRowNum();
//				Integer[] newRowTitles = new Integer[excessRows];
//				Arrays.fill(newRowTitles, -1);
//				hungarianClone.addRows(excessRows, newRowTitles, 0);
//			}
//			else if (hungarianMatrix.getRowNum() > hungarianMatrix.getColNum()){
//				excessColumns = hungarianMatrix.getRowNum() - hungarianMatrix.getColNum();
//				Integer[] newColTitles = new Integer[excessColumns];
//				Arrays.fill(newColTitles, -1);
//				hungarianClone.addColumns(excessColumns, newColTitles, 0);
//			}
			/*
			 * Since the number of rows and the number of columns are used
			 * more then once... (0.6.1 4.4.2020 16:06)
			 */
			int rowNum = hungarianClone.getRowNum();
			int colNum = hungarianClone.getColNum();
			/*
			 * Important NOTE 0.6.1 7.4.2020 13:21 - 
			 * From now on, all reductions will be performed up until ?Num - excess?
			 */
			// TEST
//						System.out.println();
//						hungarianClone.printMat(true);
//						System.out.println();
			/*
			 * IMPORTANT NOTE 0.6.1 7.4.2020 13:42 -
			 * I just realized, that if there are excess rows filled with 0,
			 * column reduction will do nothing and is therefore unnecessary,
			 * and similarly for excess columns and row reductions.
			 * So to improve performance and speed,
			 * I shall add conditions around the loops.
			 * Might decide to delegate to a method later, we will see.
			 */
			/*
			 * As part of IMPROVE202011121040,
			 * I needed to only do small changed to the ifs here.
			 * instead of two checks of ==0 on different ints,
			 * I will do >=0 on their replacement.
			 * The reason it works, is that if it is 0 it is the
			 * equivalent of both of the originals being 0, which
			 * would apply to both >= and <=, which is why it works.
			 * A positive one is equivalent to only the first
			 * original if being true, and negative to only the
			 * second one being true.
			 */
//			if (excessColumns == 0) {
			if (columnsMinusRows >= 0) {

				/*
				 * A for loop that goes over the rows, finds the minimum
				 * and uses it to perform row reduction
				 */
//				for (int i = 0; i < rowNum - excessRows; i++) {
				for (int i = 0; i < rowNum - columnsMinusRows; i++) {
					/*
					 * Since we don't want to perform reduction if the minimum
					 * is zero, we will wrap it with an if which will also
					 * include the assignment to currentMin
					 */
					if ((currentMin = hungarianClone.minInRow(i)) > 0) {
						// minCost += currentMin; // I'll just assign it in the end.
						hungarianClone.rowAddition(i, (Integer) (-currentMin));
					}
				} 
				// TEST
				//				System.out.println();
				//				hungarianClone.printMat(true);
				//				System.out.println();
			}
//			if (excessRows == 0) {
			if (columnsMinusRows <= 0) {
				/*
				 * A for loop that goes over the columns, finds the minimum
				 * and uses it to perform column reduction
				 * very similar to the row one.
				 * Thanks Abstraction!
				 */
//				for (int i = 0; i < colNum - excessColumns; i++) {
				for (int i = 0; i < colNum - (-columnsMinusRows); i++) { // Minus is faster than Math.abs(), and since it is in <= 0 it is NONE-NEGATIVE
					/*
					 * Since we don't want to perform reduction if the minimum
					 * is zero, we will wrap it with an if which will also
					 * include the assignment to currentMin
					 */
					if ((currentMin = hungarianClone.minInColumn(i)) > 0) {
						// minCost += currentMin; // I'll just assign it in the end.
						hungarianClone.columnAddition(i, (Integer) (-currentMin));
					}
				}
				// TEST
//								System.out.println();
//								hungarianClone.printMat(true);
//								System.out.println();
			}
			// TEST
//			System.out.println();
//			hungarianClone.printMat(true);
//			System.out.println();
			/* 
			 * I will have a while loop.
			 * As long as the minimum cover is not equal
			 * min(rowNum,colNum) (accounting for non square
			 * matrix) it will keep running.
			 * minimumCover will be initialized first to zero.
			 * In every iteration of the loop, I will find the minimal
			 * covering, and if it is not good, I will find
			 * the minimal non zero value, substract it from
			 * the non zero values and add it to the doubly covered
			 * elements.
			 * I can use populateElement for the last part 
			 */
			/*
			 * (4.4.2020. 11:50)
			 * I have thought about how to create the covering lines.
			 * I saw a comment that doing the line or column with the
			 * most 0s first won't work, HOWEVER that was referring to
			 * a random process. Here we will ONLY use 0s as pivots for 
			 * covering lines.
			 * Therfore, for each uncovered zero, we will see whether its row or column
			 * contains more zeros. We will mark the one with more 0s (or any of them if
			 * they are equal).
			 * If you reach a covered zero and the non covered part has more zeros,
			 * cover it. Otherwise don't change anything.
			 * We go over the entire zeroKeeper like this, aaaaand....
			 * (11:51) I just realized I found a way to cover all zeros without
			 * Bipartite Matching.
			 * I will back this up and then I shall change it. Time to Git it up
			 * so I can then change it..
			 */
			coverUntilItWorks(hungarianClone);	
			// TEST
//			System.out.println();
//			hungarianClone.printMat(true);
//			System.out.println();
			/*
			 * 0.6.1 7.4.2020 14:38
			 * Now, we will create here variables like in coverUntilItWorks, and
			 * call zeroCounter one last time.
			 * Should I have done it otherwise? maybe.
			 * If necessary I can always change it.
			 */
			// Records where the zeros are
			HashMap<Integer, ArrayList<Integer>> zeroKeeper = new HashMap<Integer, ArrayList<Integer>>();	
			/*
			 * two int arrays to count the number of zeros in each row/column
			 */
			int[] rowZeros = new int[rowNum];
			int[] colZeros = new int[colNum];
			zeroCounter(hungarianClone, zeroKeeper, rowZeros, colZeros);
			// TEST
			//			System.out.println();
			//			hungarianClone.printMat(true);
			//			System.out.println();
			try { // Added in 0.8.0 13.4.2020 11:48
				assignMatching(hungarianClone, zeroKeeper, rowZeros, colZeros, matching);
			} catch (FaultyZeroCoveringException e) {
				// Auto-generated catch block
				e.printStackTrace();
				System.out.println("\n~~~No maximal match made. Any match presented is incomplete.~~~\n");
			}
			System.out.print(numOfMatches + " matches were made");
		}

		/**
		 * Time to add any necessary fake rows/columns.
		 * They will get the titles of -1
		 * Word of advice : avoid titles of -1 to your origial matrix
		 * @apiNote This method was delegated and improved from
		 * an existing code block in {@link #matchForMinCost(List)}<br>
		 * IMPROVE202011121040
		 * @apiNote IMPORTANT! THIS METHOD SHOULD BE CALLED
		 * ONLY WITH columnsMinusRows that is none
		 * zero. This assumption is used to save time
		 * on an if
		 * @implNote As part of the attempt to fix the
		 * faulty zero covering bug, I will try to put in a different number
		 * then 0 in those dummy rows/columns.<br>
		 * If it doesn't work I'll go back to 0, however,
		 * I will still have a third parameter here for telling
		 * the method which number to put in the dummy rows/columns
		 * @since 0.16.0 (12.11.2020 10:42:41 AM)
		 * @param hungarianClone
		 * @param columnsMinusRows An int representing whether there should
		 * be dummy rows (positive), columns (negative), or none (0). 
		 * @param numToFillDummiesWith a third parameter here for telling
		 * the method which number to put in the dummy rows/columns
		 */
		private void addExcessRowsOrColumnsWhenNecessary(Matrix<Integer, Integer> hungarianClone, int columnsMinusRows,
				int numToFillDummiesWith) {
			int columnsMinusRowsAbs = Math.abs(columnsMinusRows); // To call it only once
			Integer[] newTitles = new Integer[columnsMinusRowsAbs];
			Arrays.fill(newTitles, -1);
			if (columnsMinusRows > 0) {				
				hungarianClone.addRows(columnsMinusRows, newTitles, numToFillDummiesWith);
			}
			else {				
				hungarianClone.addColumns(columnsMinusRowsAbs, newTitles, numToFillDummiesWith);
			}

		}

		/**
		 * UPDATE 0.6.1 4.4.2020 22:02 - I have migrated all of the 
		 * covering procedure to this method and a method for the reductions
		 * and additions.
		 * @param hungarianClone The clone of the original Matrix
		 * that I can mess around with.
		 */
		private void coverUntilItWorks(Matrix<Integer, Integer> hungarianClone) {
			// A variable to save the current minimum in row, column or whatever
			int currentMin;
			/*
			 * Since the number of rowsand the number of columns are used
			 * more then once... (0.6.1 4.4.2020 16:06)
			 */
			int rowNum = hungarianClone.getRowNum();
			int colNum = hungarianClone.getColNum();
			int minimalCovering = 0;
			int expectedMatches = min(rowNum,colNum); // To avoid calling the min again and again
			/*
			 * We need a BipartiteGraph and a Map
			 */
			//BipartiteGraph coverGraph = new BipartiteGraph(rowNum + colNum, true);
			//coverGraph.manualPartition(hungarianClone.getRowTitles(), hungarianClone.getColTitles());
			//HashMap<Integer, Integer> zeroKeeper = new HashMap<Integer, Integer>();
			/*
			 * BUGFIX 0.6.1 6.4.2020, 22:48 - HASHMAP<Integer, Integer> IS THE WRONG DATA STRUCTURE FOR zeroKeeper
			 * because it allows only one value per key.
			 * We need HashMap<Integer, ArrayList<Integer>>
			 */
			HashMap<Integer, ArrayList<Integer>> zeroKeeper = new HashMap<Integer, ArrayList<Integer>>();			
			//bipartiteGraphMatchingProblemSolver coverSolver = null;
			/*
			 * 0.6.1, 4.4.2020 16:02 - Now we need either 4 arrays or maybe
			 * it would be better to have two lists containing pairs of value?
			 * nah.
			 * two int arrays to count the number of zeros in each row/column
			 * and two boolean arrays to see if a row or column is covered
			 */
			int[] rowZeros = new int[rowNum];
			int[] colZeros = new int[colNum];
			boolean[] rowCovered = new boolean[rowNum];
			boolean[] colCovered = new boolean[colNum];
			/*
			 * And finally the while.
			 * At its end, we shall nullify the adjacency list and renewify it 
			 * and the same for zeroKeeper to avoid dragging stuff from previous iterations.
			 * Also for the solver
			 */
			while (minimalCovering != expectedMatches) {
				/*
				 * PREP BLOCK
				 * zeroing the minimal covering and maximaizing the current min
				 */
				minimalCovering = 0;
				currentMin = Integer.MAX_VALUE;	// ACtually necessary 
				/*
				 * PREP BLOCK
				 */
				zeroCounter(hungarianClone, zeroKeeper, rowZeros, colZeros); // Migrated the zero counting nested loop
				// to a method because it shall be used again.

				/*
				 * IMPORTANT NOTE 0.6.1 14:08-
				 * Added minCost += currentMin; 
				 * Because the costs associated with those reductions 
				 * were missing
				 */
				//minCost += currentMin; // Didn't solve it. I'll just assign it in the end.
				/*
				 * After the zero locating,
				 * we can start the covering
				 */
				/*
				 * try { coverSolver = new bipartiteGraphMatchingProblemSolver(coverGraph,
				 * false); } catch (Exception e) { // Auto-generated catch block
				 * e.printStackTrace(); }
				 */
				/*
				 * ADDED IN 0.8.0 13.4.2020 11:57
				 */
				minimalCovering = produceMinimalCovering(zeroKeeper, rowCovered, colCovered, rowZeros, colZeros);


				if (minimalCovering >= expectedMatches) { // 0.7.1 10.4.2020 10:00, Changed from == to >= because saw a case during a debug, but I think it will still work fine
					numOfMatches = min(hungarianMatrix.getColNum(), hungarianMatrix.getRowNum()); // ADDED 0.6.1 14:22
					break; // If the minimal covering equals the number of matches we need we can stop checking covers.
				}
				currentMin = minimumComputer(hungarianClone, zeroKeeper, rowZeros, colZeros, rowCovered, colCovered); // Added in 0.7.1, see method desc. for why
				coveredReduction(hungarianClone, currentMin, rowCovered, colCovered);
				// TEST
//								System.out.println();
//								hungarianClone.printMat(true);
//								System.out.println();
				/*
				 * 
				 * Phoenix block
				 * might not be necessary, but I prefer not to take risks.
				 */
				//coverGraph.restartAdjacenctList();
				zeroKeeper = null;
				zeroKeeper = new HashMap<Integer, ArrayList<Integer>>();	
				//coverSolver = null;
				rowZeros = null;
				colZeros = null;
				rowCovered = null;
				colCovered = null;
				rowZeros = new int[rowNum];
				colZeros = new int[colNum];
				rowCovered = new boolean[rowNum];
				colCovered = new boolean[colNum];
				/*
				 * End Phoenix block
				 * 
				 */
			}
		}

		/**
		 * This method uses the map of zeros,
		 * the boolean arrays that track which
		 * row and column are covered,
		 * and the arrays for the number of zeros
		 * in each row/column. <br>
		 * It then takes the maximal number of zeros
		 * in any row or column, and looks at row/columns
		 * with such numbers, covering them first, and then
		 * takes a look at rows/column with less and less
		 * zeros until the tracked number reaches 0. <br>
		 * It then uses a (delegated) method to cover any
		 * checked row and column IF NECESSARY
		 * (if it is uncovered and if the other entity of
		 * the current zero (column/row) contains less zeros
		 * (in case of ties and in case the zero is not
		 * covered at all defaults to covering the column).
		 * <br> If covered, minimalCovering is incremented. 
		 * @since 0.8.0 (13.4.2020 11:57).
		 * @apiNote Had a bug in the actual covering step,
		 * decided it is a good oppurtunity to migrate to a method
		 * to try an improved (i.e. not buggy) algorithm.<br>
		 * I will keep the description of the old one for scholarly reasons.
		 * @implNote It is recommended to take a look at the
		 * notes at the end of the method to anyone reading the
		 * source code. <br> They explain the lambda functions. 
		 * @implNote As of 0.8.0 (13.4.2020 17:22) It is supposed to
		 * copy the original rowZeros and colZeros arrays,
		 * and work with the copies to produce the correct covering. <br>
		 * Every time you cover a row/column, you need to ignore all
		 * zeros in that row/column in the count. <br>
		 * You do that by editing the copies of the array so that
		 * the newly covered zeros are not counted there.
		 * We shall call it FIX202004131722
		 * @implNote As of 0.8.0 (13.4.2020 19:30)
		 * The previous fix wasn't good.
		 * In order for it to work, it needs to update the used
		 * arrays ONLY AFTER ALL ZEROS WERE CHECKED!
		 * OTHERWISE, EXTRA COVERS WILL BE APPLIED.
		 * I have a fix, though it isn't pretty:
		 * Have a third set of arrays which will be updated in real time and
		 * transferred to the second afterwards.
		 * FIX202004131930
		 * @implNote It wasn't enough.
		 * I need a temp zeroKeeper as well now...
		 * FIX202004131950
		 * @param zeroKeeper A map for where are all the zeros.
		 * @param rowCovered bool array for which rows are covered
		 * @param colCovered bool array for which columns are covered
		 * @param rowZeros How many zeros in each row (array) (will be used in improved algorithm)
		 * @param colZeros How many zeros in each column (array) (will be used in improved algorithm)
		 * @return the minimalCovering
		 */
		private int produceMinimalCovering(HashMap<Integer, ArrayList<Integer>> zeroKeeper, boolean[] rowCovered,
				boolean[] colCovered, int[] rowZeros, int[] colZeros) {
			/*
			 * IMPORTANT NOTE
			 * 0.8.0 (13.4.2020 17:22) - I know how to fix the bug!
			 * I need to clone the rowZeros AND colZeros arrays,
			 * and remove zeros from them when they are covered.
			 * THEN It will solve it.
			 * Will do it at the beginning of the method.
			 */
			int[] refactoredRowZeros = Arrays.copyOf(rowZeros, rowZeros.length);
			int[] refactoredColZeros = Arrays.copyOf(colZeros, colZeros.length);	
			// FIX202004131950
			HashMap<Integer, ArrayList<Integer>> refactoredZeroKeeper = new HashMap<Integer, ArrayList<Integer>>();
			/*
			 * Thanks to
			 * Teocci
			 * From the answer to question
			 * https://stackoverflow.com/questions/10079266/copying-a-hashmap-in-java
			 * for providing the inspiration to
			 * this solution for deep copying the map
			 */
			refactoredZeroKeeper = deepCopyMap(zeroKeeper);
			int minimalCovering = 0;
			/*
			 * As discussed here is the algorithm:
			 * start going through the tempZeroKeeper,
			 * but do it with the current maximal
			 * number of zeros in any row/column in mind:
			 * start in rows an columns with maximal
			 * number of zeros. Start going through the zeros.
			 * if a zero is uncovered, check whether its
			 * row or column has more zeros. the winner
			 * gets covered (in case of a tie it doesn't matter
			 * which). If it is covered as a row, check if the
			 * column has more 0s and if so cover it.
			 * Otherwise do nothing. 
			 * Similar to covered column.
			 * When you went through all rows and columns with the current
			 * tracked number of zeros, decrease the number by one and repeat until you reach
			 * 0. Might end up with similar tracking to assignMatching method.
			 */
			int currentMaxRowZeros = ArrayTools.maxInArray(refactoredRowZeros); // I can track the max in tempRowZeros. and I can update it when the overall track changes
			int currentMaxColZeros = ArrayTools.maxInArray(refactoredColZeros); // I can track the max in tempRowZeros. and I can update it when the overall track changes
			int zeroNumberTracker = Math.max(currentMaxRowZeros, currentMaxColZeros); // created a method in ArrayTools to facilitate finding max
			while (zeroNumberTracker > 0) {
				/*
				 * Because I had to make zeroKeeper values ArrayList<Integer>>,
				 * I had to nest another for loop.
				 * I took the opportunity to 
				 * also change the outer on to be just a regular one
				 */	
				final int zeroTrackerDuplicate = zeroNumberTracker; 
				// Added zeroTrackerDuplicate because of: "Local variable zeroNumberTracker defined in an enclosing scope must be final or effectively final" in lambda functions
				/*
				 * SEE FIX202004131930
				 * They have been moved and finalized for the lambda functions,
				 * which forced a couple of small changes (19:39).
				 * They will be used each iteration of the while as immutable copies
				 * of the results of the previous iteration
				 */
				final int[] tempRowZeros = Arrays.copyOf(refactoredRowZeros, refactoredRowZeros.length);
				final int[] tempColZeros = Arrays.copyOf(refactoredColZeros, refactoredColZeros.length);
				// FIX202004131950
				final HashMap<Integer, ArrayList<Integer>> tempZeroKeeper = deepCopyMap(refactoredZeroKeeper); 
				/*
				 * I will attempt a Stream Lambda Filter to only 
				 * take rows with the zeroNumberTracker
				 */
				for (Integer zeroRow : tempZeroKeeper.keySet()
						.stream()
						.filter(key -> tempRowZeros[key] == zeroTrackerDuplicate)
						.collect(Collectors.toList())) { // The Collectors.toList is to make it Iterable so the foreach will be allowed. see note *
					for (Integer zeroColumn : tempZeroKeeper.get(zeroRow)
							.stream()
							.collect(Collectors.toList())) { // I added the .stream().collect(...) to try and avoid ConcurrentModificationException (13.4.2020 20:10)
						minimalCovering += coverIfNecessary(rowCovered, colCovered, tempRowZeros, tempColZeros, zeroRow, zeroColumn);
						// Took the oppurtunity to take all the annoying ifs and shove them to a method (0.8.0 13.4.2020, 12:31)
						/*
						 * Instead of reusing code,
						 * migrated the important ifs from the FIX...
						 * to a wrapper method
						 * @since 0.8.0 13.4.2020 18:01
						 */
						refactorZerosWrapper(refactoredZeroKeeper, rowCovered, colCovered, refactoredRowZeros, refactoredColZeros, zeroRow, zeroColumn);
					}
				}
				/*
				 * We will now (0.8.0 13.4.220 12:38)
				 * add a similar filtered for loop
				 * for the columns. 
				 * In some cases it will be skipped but
				 * it is crucial to include it to avoid another bug 
				 */
				for (Integer zeroColumn : tempZeroKeeper.values()
						.stream()
						.flatMap(colList -> colList.stream())
						.distinct()
						.filter(column -> tempColZeros[column] == zeroTrackerDuplicate)
						.collect(Collectors.toList())) { // This lambda function is a bit complicated. see note ** at the end of method
					for (Integer zeroRow : tempZeroKeeper.keySet()
							.stream()
							.filter(row -> tempZeroKeeper.get(row).contains(zeroColumn))
							.collect(Collectors.toList())) { // See note *** for this Lambda
						minimalCovering += coverIfNecessary(rowCovered, colCovered, tempRowZeros, tempColZeros, zeroRow, zeroColumn);
						// This line is the same though.
						/*
						 * Instead of reusing code,
						 * migrated the important ifs from the FIX...
						 * to a wrapper method
						 * @since 0.8.0 13.4.2020 18:01
						 */
						refactorZerosWrapper(refactoredZeroKeeper, rowCovered, colCovered, refactoredRowZeros, refactoredColZeros, zeroRow, zeroColumn);
					}

				}
				zeroNumberTracker--; // After we checked the rows AND the columns, reduce this tracker
			}
			return minimalCovering;

			/*
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * As discussed here is the OLD algorithm:
			 * start going through the zeroKeeper,
			 * if a zero is uncovered, check whether its
			 * row or column has more zeros. the winner
			 * gets covered (in case of a tie it doesn't matter
			 * which). If it is covered as a row, check if the
			 * column has more 0s and if so cover it.
			 * Otherwise do nothing. 
			 * Similar to covered column.
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 */
			/*
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * NOTE *:
			 * We create a keySet for the tempZeroKeeper
			 * map which contains all the row numbers.
			 * We turn it to stream to allow the 
			 * sophisticated stream operations.
			 * We then filter it the stream to only
			 * contain keys (i.e. rows) such that
			 * the number of zeros in the row (captured
			 * in tempRowZeros[key]) is the current tracked
			 * number (zeroTrackerDuplicate is for the
			 * purpose of having an effectively final
			 * variable. Don't ask me why it's
			 * necessary).
			 * Then, we collect the elements of this
			 * trimmed stream to a list using the
			 * toList method of Collectors, so that
			 * we can iterate on it. We then iterate
			 * with for (zeroRow : collected list)
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 */
			/*
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * NOTE **:
			 * We use the values method for the
			 * tempZeroKeeper HashMap.
			 * Unfortunately, it returns a Collection
			 * of ArrayList<Integer> because the values
			 * ARE array lists. In order to be able to
			 * properly work with the values to avoid
			 * duplicates which will just make it
			 * harder, we use the flatMap stream
			 * method (after we used the stream method
			 * of course). What it does, is it takes
			 * this collection of lists and flattens
			 * it, i.e. it takes all elements of all
			 * lists and put them in a big
			 * Stream<Integer>.
			 * Then, we use the distinct method.
			 * The reason it is necessary, is because
			 * the same column number can appear in
			 * more then one list, because more than
			 * one row can have a zero in the same
			 * column. Since it is unnecessary to look
			 * at the same column more than once, we
			 * trim duplicates.
			 * We then filter the stream so that only
			 * columns that contain the tracked number
			 * of zeros remain.
			 * We then collect to a list to iterate 
			 * over.
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 */
			/*
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * NOTE ***:
			 * We create a keySet for the tempZeroKeeper
			 * map which contains all the row numbers.
			 * We turn it to stream to allow the 
			 * sophisticated stream operations.
			 * We then filter the stream to only
			 * contain keys (i.e. rows) such that
			 * there is a zero in the column from the
			 * outer for loop.
			 * Then, we collect the elements of this
			 * trimmed stream to a list using the
			 * toList method of Collectors, so that
			 * we can iterate on it. We then iterate
			 * with for (zeroRow : collected list)
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 */
		}

		private static HashMap<Integer, ArrayList<Integer>> deepCopyMap(HashMap<Integer, ArrayList<Integer>> zeroKeeper) {
			HashMap<Integer, ArrayList<Integer>> copy = new HashMap<Integer, ArrayList<Integer>>();
			for (Map.Entry<Integer, ArrayList<Integer>> entry : zeroKeeper.entrySet()) {
				copy.put(entry.getKey(), new ArrayList<Integer>());
				for (Integer listEntry : entry.getValue()) {
					copy.get(entry.getKey()).add(listEntry);
				}
			}
			return copy;
		}

		/**
		 * A wrapper method for the refactor methods to decides which (if any) 
		 * to call.
		 * @since 0.8.0 (13.4.2020 18:02)
		 * @apiNote FIX202004131722
		 * @param tempZeroKeeper
		 * @param rowCovered
		 * @param colCovered
		 * @param tempRowZeros
		 * @param tempColZeros
		 * @param zeroRow
		 * @param zeroColumn
		 */
		private void refactorZerosWrapper(HashMap<Integer, ArrayList<Integer>> tempZeroKeeper, boolean[] rowCovered, boolean[] colCovered, int[] tempRowZeros,
				int[] tempColZeros, Integer zeroRow, Integer zeroColumn) {
			/*
			 * See FIX202004131722 for the ifs and the content
			 * The first if:
			 * if this row is covered, but tempRowZeros still
			 * thinks it contains zeros:
			 */
			if (rowCovered[zeroRow] && tempRowZeros[zeroRow] > 0) {
				refactorRowZeros(tempZeroKeeper, tempRowZeros, tempColZeros, zeroRow);
			}
			else if (colCovered[zeroColumn] && tempColZeros[zeroColumn] > 0) { // It assumes that only one of the conditions can be true at any one time
				/*
				 * If the column is covered, but tempColZeros still
				 * thinks it contains zeros:
				 */
				refactorColZeros(tempZeroKeeper, tempRowZeros, tempColZeros, zeroColumn);
			}

		}

		/**
		 * Sets tempColZeros[zeroColumn] to 0 and reduces
		 * all the relevant (0 containing) rows for this column
		 * in tempRowZeros by 1.
		 * @since 0.8.0 (13.4.2020 17:56)
		 * @apiNote Created as part of FIX202004131722
		 * @implNote As of FIX202004131950, the tempZeroKeeper
		 * needs to have the zeros removed as well.
		 * @param tempZeroKeeper  A map for where are all the zeros.
		 * @param tempRowZeros How many zeros in each row (array)
		 * @param tempColZeros How many zeros in each column (array)
		 * @param zeroColumn The specific column to be refactored
		 */
		private void refactorColZeros(HashMap<Integer, ArrayList<Integer>> tempZeroKeeper, int[] tempRowZeros,
				int[] tempColZeros, Integer zeroColumn) {
			// First of all, we ignore ALL zeros in this row
			tempColZeros[zeroColumn] = 0;			
			/*
			 * Now we go through the rows containing zeros
			 * and reduce the number of zeros by 1
			 */
			for (Integer zeroRow : tempZeroKeeper.keySet()
					.stream()
					.filter(row -> tempZeroKeeper.get(row).contains(zeroColumn))
					.collect(Collectors.toList())) { // See note *** in produceMinimalCovering for this Lambda
				tempRowZeros[zeroRow]--;
				tempZeroKeeper.get(zeroRow).remove(zeroColumn); // FIX202004131950
			}
		}

		/**
		 * Sets tempRowZeros[zeroRow] to 0 and reduces
		 * all the relevant (0 containing) columns for this row
		 * in tempColZeros by 1.
		 * @since 0.8.0 (13.4.2020 17:45)
		 * @apiNote Created as part of FIX202004131722
		 * @implNote As of FIX202004131950, the tempZeroKeeper
		 * needs to have the zeros removed as well.
		 * @param tempZeroKeeper  A map for where are all the zeros.
		 * @param tempRowZeros How many zeros in each row (array)
		 * @param tempColZeros How many zeros in each column (array)
		 * @param zeroRow The specific row to be refactored
		 */
		private void refactorRowZeros(HashMap<Integer, ArrayList<Integer>> tempZeroKeeper, int[] tempRowZeros, int[] tempColZeros, Integer zeroRow) {
			// First of all, we ignore ALL zeros in this row
			tempRowZeros[zeroRow] = 0;
			/*
			 * Now we go through the columns containing zeros
			 * and reduce the number of zeros by 1
			 */
			for (Integer zeroColumn : tempZeroKeeper.get(zeroRow)
					.stream()
					.collect(Collectors.toList())) { // I added the .stream().collect(...) to try and avoid ConcurrentModificationException (13.4.2020 20:10) 
				tempColZeros[zeroColumn]--;
				tempZeroKeeper.get(zeroRow).remove(zeroColumn); // FIX202004131950
			}

		}

		/**
		 * Checks if the current row OR the current column
		 * are covered or not. <br>
		 * If both are uncovered - it covers the 
		 * one with more 0s (or the column if tie - does not matter). <br>
		 * If one of them is uncovered - it only covers it if it has more
		 * 0s than the other one.
		 * @apiNote Another migration. <br>
		 * Makes it more modular, and hopefully 
		 * easier to catch bugs. <br>
		 * Also more concise and readable.
		 * @implNote As of 0.8.0 (13.4.2020 17:30) It is supposed to get
		 * a (possibly modified) copy of the original rowZeros and colZeros arrays,
		 * to produce the correct covering.
		 * @since 0.8.0 (13.4.2020 12:34)
		 * @param rowCovered bool array for which rows are covered
		 * @param colCovered bool array for which columns are covered
		 * @param rowZeros How many zeros in each row (array)
		 * @param colZeros How many zeros in each column (array)
		 * @param zeroRow the row to check for
		 * @param zeroColumn the column to check for
		 * @return 1 if a row or column were covered. 0 otherwise.
		 */
		private int coverIfNecessary(boolean[] rowCovered, boolean[] colCovered, int[] rowZeros, int[] colZeros, Integer zeroRow, Integer zeroColumn) {
			/*
			 *  Now it will only return 1 or 0.
			 *  If it covered returns 1 otherwise 0
			 *  That way minimalCovering gets incremented by 1 
			 *  if a cover was made and by 0 otherwise
			 */						
			if (!rowCovered[zeroRow] && !colCovered[zeroColumn]) {
				if (rowZeros[zeroRow] > colZeros[zeroColumn]) {
					rowCovered[zeroRow] = true;
				}
				else {
					colCovered[zeroColumn] = true;
				}
				return 1;
			}
			else if (rowCovered[zeroRow] && !colCovered[zeroColumn]) {
				if (rowZeros[zeroRow] < colZeros[zeroColumn]) {
					colCovered[zeroColumn] = true;
					return 1;
				}
			}
			else if (!rowCovered[zeroRow] && colCovered[zeroColumn]) {
				if (rowZeros[zeroRow] > colZeros[zeroColumn]) {
					rowCovered[zeroRow] = true;
					return 1;
				}
			}
			return 0;
		}

		/**
		 * Finds and returns the current non covered minimum
		 * which is non zero.
		 * @apiNote I was forced to separate it from the zeroCounter
		 * because it then acted on the previous cover, which
		 * meant it could accidentally take the wrong minimum. <br>
		 * Also had to include the rowCovered colCovered
		 * @since 0.7.1 (9.4.2020 14:03)
		 * @param hungarianClone Clone of the matrix
		 * @param zeroKeeper A map for where are all the zeros.
		 * @param rowZeros How many zeros in each row (array)
		 * @param colZeros How many zeros in each column (array)
		 * @param rowCovered bool array for which rows are covered
		 * @param colCovered bool array for which columns are covered
		 * @return the current non covered minimum (excluding 0s)
		 */
		private int minimumComputer(Matrix<Integer, Integer> hungarianClone,
				HashMap<Integer, ArrayList<Integer>> zeroKeeper, int[] rowZeros, int[] colZeros, boolean[] rowCovered,
				boolean[] colCovered) {
			int currentMin = Integer.MAX_VALUE;	// ACtually necessary
			int rowNum = hungarianClone.getRowNum();
			int colNum = hungarianClone.getColNum();
			/*
			 * We are forced to go for a nested for loop 
			 * unfortunately to find all zeros.
			 * ADDED 0.6.1, 4.4.20 16:00 - ACtually, if we are already
			 * going through the entire array, might as well check for the minimal non zero 
			 * simultaneously.
			 */
			for (int i = 0; i < rowNum; i++) {
				for (int j = 0; j < colNum; j++) {
					if ((hungarianClone.getElement(i, j) != 0)
							&& (hungarianClone.getElement(i, j) < currentMin)
							&& (!rowCovered[i]) && (!colCovered[j])) {
						currentMin = hungarianClone.getElement(i, j);
					}
				}
			}
			return currentMin;
		}

		/**
		 * MIGRATED FROM INSIDE coverUntilItWorks 0.6.1 7.4.2020 14:30
		 * CHANGED TO VOID IN 0.7.1 9.4.2020 14:05 - 
		 * Because I moved the minimum computation to a separate method.
		 * @param hungarianClone Clone of the matrix
		 * @param zeroKeeper A map for where are all the zeros.
		 * @param rowZeros How many zeros in each row (array)
		 * @param colZeros How many zeros in each column (array)
		 */
		private void zeroCounter(Matrix<Integer, Integer> hungarianClone,
				HashMap<Integer, ArrayList<Integer>> zeroKeeper, int[] rowZeros, int[] colZeros) {
			int rowNum = hungarianClone.getRowNum();
			int colNum = hungarianClone.getColNum();
			/*
			 * We are forced to go for a nested for loop 
			 * unfortunately to find all zeros.
			 * ADDED 0.6.1, 4.4.20 16:00 - ACtually, if we are already
			 * going through the entire array, might as well check for the minimal non zero 
			 * simultaneously.
			 */
			for (int i = 0; i < rowNum; i++) {
				for (int j = 0; j < colNum; j++) {
					if (hungarianClone.getElement(i, j) == 0) {							
						/*
						 * In order not to saturate the zeroKeeper with unnecessary Entries, changed the initialization
						 * to here. Only relevant rows get initialized. and only once thanks to the if
						 */
						if (!zeroKeeper.containsKey(i)) {
							zeroKeeper.put(i, new ArrayList<Integer>());
						}
						zeroKeeper.get(i).add(j); // Record the location of the zero
						//coverGraph.addEdge(i, j); // add an edge representing the zero
						// Also increment the zero counters
						rowZeros[i]++;
						colZeros[j]++;
					}
				}
			}
		}

		/**
		 * This method handles reducing uncovered elements after finding a minimal covering
		 * which is not sufficient and increasing doubly covered elements.
		 * It uses the minimum found during the covering.
		 * @param hungarianClone The cloned matrix that was covered
		 * @param currentMin The minimum non zero element in the current covering attempt
		 * @param colCovered A boolean array representing which rows are covered
		 * @param rowCovered A boolean array representing which columnss are covered
		 */
		private void coveredReduction(Matrix<Integer, Integer> hungarianClone, int currentMin, boolean[] rowCovered, boolean[] colCovered) {

			int rowNum = hungarianClone.getRowNum();
			int colNum = hungarianClone.getColNum();
			// We are forced to do another nested loop
			for (int i = 0; i < rowNum; i++) {
				for (int j = 0; j < colNum; j++) {
					if ( !rowCovered[i] && !colCovered[j]) { // If the element is not covered reduce it by currentMin
						hungarianClone.populateElement(i, j, (Integer) (hungarianClone.getElement(i, j) - currentMin)); 
					}
					else if ( rowCovered[i] && colCovered[j]) { // If the element is not covered add to it currentMin
						hungarianClone.populateElement(i, j, (Integer) (hungarianClone.getElement(i, j) + currentMin)); 
					}					
				}				
			}			
		}

		/**
		 * Given the reduced (possibly expanded) Matrix, the locations of the zeros there, 
		 * the counters of zeros per row and column, and an empty List to be filled with the matching,
		 * assign the row and column of the appropriate zero cells to the Edge List
		 * @param hungarianClone the reduced (possibly expanded) Matrix
		 * @param zeroKeeper A Map with the the locations of the zeros
		 * @param rowZeros an array with the number of zeros per row
		 * @param colZeros an array with the number of zeros per column
		 * @param matching HAD TO ADD 0.6.1 7.4.2020 14:48 TO FACILITATE RETURNING IT FROM THE SOLVER.
		 * Would have been a nightmare otherwise
		 * @throws FaultyZeroCoveringException if the coverUntilItWorks method produced the wrong covering,
		 */
		private void assignMatching(Matrix<Integer, Integer> hungarianClone, HashMap<Integer, ArrayList<Integer>> zeroKeeper, 
				int[] rowZeros, int[] colZeros, List<Edge> matching) throws FaultyZeroCoveringException {
			/*
			 * two ints count the number of rows/columns. SHOULD BE THE SAME AT THIS STAGE
			 * but couldn't bother to delete one of them.
			 * and two boolean arrays to see if a row or column is covered
			 */
			int rowNum = hungarianClone.getRowNum();
			int colNum = hungarianClone.getColNum();
			boolean[] rowCovered = new boolean[rowNum];
			boolean[] colCovered = new boolean[colNum];
			/*
			 * Now comes a tricky part:
			 * We don't want to record matches related to
			 * dummy rows/columns, but we must cover them when a relevant match is found.
			 * Here is what we're going to do. 
			 * We will create ints for the REAL rownum and colnum 
			 * using hungrianMatrix (the original)
			 * and anything outside of them WILL NOT
			 * BE RECORDED in the matching
			 */
			int realRowNum = hungarianMatrix.getRowNum();
			int realColNum = hungarianMatrix.getColNum();
			/*
			 * Now comes a really tricky part - 
			 * which zero is assigned to whom?
			 * Let's begin by searching for rows/columns
			 * with only 1 zero and that aren't covered already
			 * We will use a while loop with a counter
			 * being compared to numOfMatches
			 * and dummy matches will not increment it
			 */
			int matchCounter = 0;
			/*
			 * Another thing, we should have another kind of counter
			 * for the current number of zeros being looked at.
			 * It will hopefully make sense inside the loop
			 */
			int zeroNumberTracker = 1; // Initialized to the first value to be searched
			int zeroIndex;
			// See 0.6.1 8.4.2020 10:33 -  below
			/*
			 * 0.7.1 10.4.2020 10:07 - Had to split to zeroTrackerRow and zeroTrackerCol
			 * because it skipped a zero in a column.
			 */
			int zeroTrackerRow = 1;
			int zeroTrackerCol = 1;
			/*
			 * CRITICAL BUG! (13.4.2020 11:31)
			 * Because of the way I did the assignment,
			 * I reached a situation in which the only uncovered element
			 * left is NOT a zero, and therefore it keeps looking
			 * for bigger and bigger number of zeros in any row/column
			 * without success.
			 * So first of all, I shall throw an Exception if
			 * zeroNumberTracker, zeroTrackerRow, or zeroTrackerCol 
			 * go over the number of rows/columns...
			 * Because it is better than infinite run 
			 * and also it is good to know.
			 * Then, I shall fix the bug.
			 */
			while (matchCounter != numOfMatches) { 
				/*
				 * As mentioned above, I shall throw an exception
				 * I shall create a new one for that
				 * I shall throw an Exception if
				 * zeroNumberTracker, zeroTrackerRow, or zeroTrackerCol 
				 * go over the number of rows/columns...
				 */
				// As a side note, it seems the problem is with my covering routine. I think
				if(zeroNumberTracker > rowNum || zeroTrackerCol > colNum || zeroTrackerRow > rowNum) {
					throw new FaultyZeroCoveringException("\nDue to faulty zero covering, one of the zero"
							+ " trackers exceeded the number of rows/columns in the clone Matrix.\n"
							+ "This means we've reached a dead end in the matching assignment attempt"
							+ "(no uncovered zeros remaining)\n"
							+ "The actual number of matches made: " + matchCounter); // The last line was added on 0.14.0 28.10.2020 12:09
				}
				/*
				 * 0.6.1 8.4.2020 10:33 - 
				 * Because ArrayTools.findInArray finds only the first occurence,
				 * and we want to cycle through all of them,
				 * I created ArrayTools.findNthInArray (for generic type for now.
				 * I will therfore have a counter now which increments
				 * when we look through a zero, and resets 
				 * when zeroNumberTracker increments.
				 */
				// First check the uncovered rows that have the number of zeros
				if ((zeroIndex = ArrayTools.findNthInArray(zeroNumberTracker, rowZeros, zeroTrackerRow)) > -1) {
					zeroTrackerRow++;
					// Got it out of the other if so that zeroTracker keeps going even if something IS covered
					if (!rowCovered[zeroIndex]) {
						// Go through the zeros in the row until you find one that is uncovered and use it
						for (Integer zeroCol : zeroKeeper.get(zeroIndex)) {
							//It's messy, I know.
							if (!colCovered[zeroCol]) {
								rowCovered[zeroIndex] = true;
								colCovered[zeroCol] = true;
								if ((zeroIndex < realRowNum) && (zeroCol < realColNum)) {
									// matching.add(new Edge(zeroIndex, zeroCol)); // 8.4.2020 11:11 - bug - needs to have the titles not the indices
									// 0.6.1 - 8.4.2020 11:13 - fixed to add the titles of the rows/columns
									matching.add(new Edge(hungarianClone.getRowTitles()[zeroIndex], hungarianClone.getColTitles()[zeroCol]));
									matchCounter++;
								}
								break;
							}
						} 
					}

				} // Then the columns
				else if ((zeroIndex = ArrayTools.findNthInArray(zeroNumberTracker, colZeros, zeroTrackerCol)) > -1) {
					zeroTrackerCol++;
					// Got it out of the other if so that zeroTracker keeps going even if something IS covered					
					if (!colCovered[zeroIndex]) {
						// Go through the zeros in the row until you find one that is uncovered and use it
						/*
						 * Using a Lambda expression to only go over
						 * entries whose lists have the specified column.
						 * The filter() makes sure of that
						 * and then the foreach iterates over the relevant entries					
						 */
						//					/*
						//					 * //It's messy, I know. matchCounter += zeroKeeper.entrySet().stream()
						//					 * .filter(entry -> entry.getValue().contains(zeroIndex)) .forEachOrdered(entry
						//					 * -> { Integer zeroRow = entry.getKey();
						//					 * 
						//					 * if (!rowCovered[zeroRow]) { rowCovered[zeroRow] = true; colCovered[zeroIndex]
						//					 * = true; if ( (zeroRow <= realRowNum) && (zeroIndex <= realColNum)) {
						//					 * matching.add(new Edge(zeroRow, zeroIndex)); // Fix it } } });
						//					 */
						/*
						 * 0.6.1, 8.4.2020 9:47 -
						 * I've tried the Lambda expression to save iterations, but
						 * it was too complicated to get it to work.
						 * Decided to just for loop over all rows.
						 * More important the it works..
						 */
						// It is ugly, and messy, and that's how it is..
						for (Entry<Integer, ArrayList<Integer>> zero : zeroKeeper.entrySet()) {
							// Moved it outside of the inner loop because it is better and more useful
							Integer zeroRow = zero.getKey();
							// To nip unnecessary iterations in the bud
							if ((rowCovered[zeroRow]) || (colCovered[zeroIndex])) {
								continue; // Fixed 0.7.1 10.4.2020 10:12 from break
							}
							for (Integer col : zero.getValue()) {
								if (col == zeroIndex) {

									if (!rowCovered[zeroRow]) {
										rowCovered[zeroRow] = true;
										colCovered[zeroIndex] = true;
										if ((zeroRow < realRowNum) && (zeroIndex < realColNum)) {
											// matching.add(new Edge(zeroRow, zeroIndex)); // 8.4.2020 11:11 - bug - needs to have the titles not the indices
											// 0.6.1 - 8.4.2020 11:14 - fixed to add the titles of the rows/columns
											matching.add(new Edge(hungarianClone.getRowTitles()[zeroRow], hungarianClone.getColTitles()[zeroIndex]));
											matchCounter++;
										}
										break;
									}
								}
							}
						} 
					}
				} // If fails - increment the number and reset zeroTracker to 1
				else {
					zeroNumberTracker++;
					zeroTrackerRow = 1;
					zeroTrackerCol = 1;
				}
			}
		}

	}

	/**
	 * The main method for this class expects to either receive in
	 * args[0] the abbreviation of the desired problem and in
	 * args[1] the name of a relevant file if needed. 
	 * Otherwise there should be nothing, in which case it will invoke askUser.
	 * @param args Either contains the problemAbbreviation or is empty
	 */
	public static void main(String[] args) {
		// TODO Each time a new type of problem becomes available, add it here
		String problemAbbreviation;
		File problemInputFile;
		if (args.length == 0) {
			problemAbbreviation = askUser();
			problemInputFile = getGraphInputFile("default");
		}
		else {
			problemAbbreviation = args[0];
			problemInputFile = getGraphInputFile(args[1]);
		}
		/*
		 * Preparing a switch case even though at the moment it is unnecessary.
		 * Preparing for when there is more than one problem
		 */
		switch (problemAbbreviation.toLowerCase()) {
		case "bm": {
			bipartiteGraphMatchingProblemSolver bipartiteSolver;
			try {
				bipartiteSolver = new bipartiteGraphMatchingProblemSolver(problemInputFile);
				System.out.println(bipartiteSolver.graphToMatch.toString());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}			
			break;
		}

		default:
			break;
		}		

	}



	/**
	 * Will read integer numbers separated by whitespace from the next line in the file
	 * @param input  A Scanner object the will help me traverse the file
	 * @return A LinkedList of Integer
	 */
	@Deprecated
	private static LinkedList<Integer> readIntsFromLine(Scanner input) {
		LinkedList<Integer> tempList = new LinkedList<Integer>();
		/* 
		 * Thanks to rogue_leader on stackoverflow
		 * The StringTokenizer object helps with taking a line, separating stuff there with a
		 * specific delimiter, and then take the relevant input ("tokens") one by one and
		 * do what is needed with it.
		 */
		StringTokenizer st = new StringTokenizer(input.nextLine(), " "); // whitespace is the delimiter to create tokens
		while(st.hasMoreTokens())  // iterate until no more tokens
		{
			tempList.add(Integer.parseInt(st.nextToken()));  // parse each token to integer and add to linkedlist

		}
		return tempList;
	}

	/**
	 * Asks user which graph problem to solve
	 * @return an abbreviation that symbolizes the problem
	 */
	private static String askUser() {
		/*
		 * Using a prompt to the user to let them decide what problem to solve.
		 * It doesn't have to be a human.
		 */
		System.out.println("Choose from the following list which problem to solve");
		System.out.println("(Choose by entering a string of the characters in parantheses)");
		System.out.println("Maximum Cardnality (B)ipartite (M)atching");
		System.out.println("At the moment that is all. Stay tuned for more problems to solve");
		Scanner input = new Scanner(System.in);
		String answer = input.next();
		input.close(); // Apparently it's a thing. eclipse warned me so I listened and put it in.
		return answer;
	}

	/**
	 * For code readability and robustness, decided to start modularizing the code.
	 * This is simply for handling the File object creation
	 * @return The input File object
	 */
	@Deprecated
	private static File getGraphInputFile(String pathName) {
		File problemInputFile;
		if (pathName.equalsIgnoreCase("default")) {
			/*
			 * Unfortunately, I end up with this try catch block. I found new File(".").getCanonicalPath(); as a 
			 * solution on https://www.technicalkeeda.com/java-tutorials/get-current-file-path-in-java.
			 * But the IDE asked to wrap it in try catch
			 */
			try {
				String currentDirectory = new File(".").getCanonicalPath();
				problemInputFile = new File(currentDirectory + "/input.txt"); // At the moment using a default "input.txt". TODO Change that to something flexible. 
				// TODO This is a source for errors, since if there is no input.txt it will raise an exception. I'm ignoring it for now on purpose to save time
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				problemInputFile = new File(".");
			}			
		}
		else {
			problemInputFile = new File("pathName");
		}
		return problemInputFile;
	}

}
