/**
 * 
 */
package lykicheewi.burtefrahouse.graphTools;

import java.util.ArrayList;
import java.util.List;

/** 
 *
 * @author Yaron Efrat
 */
public class TreeNode<T> {

	T content; // The content of the TreeNode
	private TreeNode<T> parent = null; // The parent of this node. If null then this node is the root of the tree.
	String treeName = null; // A string that represents the whole tree for quick access from any node. If it is null, then the TreeNode was created using a constructor and without being connected to a parent. Allows to quickly check if the node is part of a specific tree.
	private String nodeName = null; // An optional name for the specific node. Can be useful
	ArrayList<TreeNode<T>> children; // A list containing all TreeNodes that are direct children of this one

	/**
	 * A regular constructor for a TreeNode setting its content but nothing else
	 * @param content
	 */
	public TreeNode(T content) {
		this.content = content;
		children = new ArrayList<TreeNode<T>>();
	}

	/**
	 * If you want To start a tree from what will be its root,
	 * you can use this method if you want the root to have content.
	 * @param content - the content of the Tree
	 * @param treeName - The name associated with the tree
	 * @param rootName - TBA
	 * @return this TreeNode, so the root
	 */
	public TreeNode<T> plantTree(T content, String treeName, String rootName) {
		this.treeName = treeName;
		this.nodeName = rootName;
		this.content = content;
		children = new ArrayList<TreeNode<T>>();
		return this;
	}
	
	/**
	 * If you want To start a tree from what will be its root,
	 * you can use this method if you want the root to have content.
	 * @param content - the content of the Tree
	 * @param treeName - The name associated with the tree
	 * @return this TreeNode, so the root
	 */
	public TreeNode<T> plantTree(T content, String treeName) {
		this.treeName = treeName;
		this.nodeName = treeName.concat("Root"); // Because why not? It's easy enough. Even if one decides not to give names to other nodes.
		this.content = content;
		children = new ArrayList<TreeNode<T>>();
		return this;
	}

	/**
	 * If you want To start a tree from what will be its root,
	 * you can use this method if you don't want the root to have content.
	 * @param treeName
	 * @return this TreeNode, so the root
	 */
	public TreeNode<T> plantTree(String treeName) {
		this.treeName = treeName;
		children = new ArrayList<TreeNode<T>>();
		return this;
	}

	/**
	 *Creates a root with a default treeName
	 *"tree".
	 *For the lazy programmer who just wants
	 *to know they did start with a defined
	 *root.
	 */
	public TreeNode<T> plantTree() {
		return this.plantTree("tree");
	}

	//�
	/**
	 * Takes a content of type T, uses it to create a new TreeNode, making sure this is
	 * its parent and that it had the same treeName. Then it adds it to the children 
	 * list and returns it.
	 * @param content - The desired content to add of type T
	 * @return A TreeNode of the new child
	 */
	public TreeNode<T> addChild(T content) {
		TreeNode<T> child = new TreeNode<T>(content);
		child.parent = this;
		child.treeName = treeName;
		children.add(child);
		return child;
	}
	
	/**
	 * Takes a list of contents and loops through them while using the addChild method on the content
	 * and adding the resulting TreeNode to the list that is eventually returned
	 * @param contents A list of contents of type T
	 * @return A list of all new children TreeNodes
	 */
	public List<TreeNode<T>> addChildren (List<T> contents) {
		List<TreeNode<T>> newChildren = new ArrayList<TreeNode<T>>();
		for (T content : contents) {
			newChildren.add(addChild(content));
		}
		return newChildren;
	}

	//�

	public void setName (String name)
	{
		nodeName = name;
	}

	public String getName() {
		return nodeName;
	}

	/**
	 *Obviously don't use if you decide
	 * to make treeName final
	 */
	public void setTreeName (String name)
	{
		treeName = name;
	}

	public String getTreeName() {
		return treeName;
	}
	
	/**
	 * This method checks if this TreeNode is a root
	 * i.e. if it is parent-less
	 * @return true if there is no parent and false otherwise
	 */
	public boolean isRoot() {
		return (parent == null);
	}
	
	/**
	 * This method checks if this TreeNode is a leaf
	 * i.e. if it is child-less
	 * @return true if there are no children and false otherwise
	 */
	public boolean isLeaf() {
		return (children.size() == 0);
	}
	
	/**
	 * get the number of children
	 * @return the number of children as int
	 */
	public int getChildCount() {
		return children.size();
	}
	
	/**
	 * Gets the parent of this TreeNode or null if it is the root
	 * @return The parent TreeNode
	 */
	public TreeNode<T> getParent() {
		return parent;
	}
	
	/**
	 * Gets the root of this Tree by recursively going up the tree and asking if that's the root
	 * @return The root TreeNode
	 */
	public TreeNode<T> getRoot() {
		if (this.isRoot()) {
			return this;
		}
		else {
			return parent.getRoot();
		}
	}

}
