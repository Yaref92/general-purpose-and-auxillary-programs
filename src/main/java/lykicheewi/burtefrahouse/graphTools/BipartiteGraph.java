package lykicheewi.burtefrahouse.graphTools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lykicheewi.burtefrahouse.exceptions.SizeMismatchException;

/**
 * UPDATE 0.5.0: IN ORDER TO BE ABLE TO CREATE A BIPARTITE GRAPH OUTSIDE OF THE GENERAL-PURPOSE... 
 * PROJECT, I WAS FORCED TO TAKE EVERYTHING I CREATED UNDER BIPARTITEGRAPH 
 * AND PUT IT IN A SEPARATE .JAVA FILE
 * <br> <br>
 * In order for a graph to be bipartite, it needs to be have two parts and have only edges between the two parts.
 * I have decided to implement it so that I have two Integer Lists that contain Node IDs. 
 * If an adjacency list is provided when constructed, then the lists are possibly not needed, but a method to check whether the graph is bipartite is needed.
 * Technically, if such a method exists, then these lists are not needed for the construction, HOWEVER, since the two parts have meaning, I would like 
 * to use these Lists
 * TODO There is a bug related to adding edges where no node is in the partition yet, because it's not added and it's
 * TODO not clear how to put them on the right list. But I need to ship out a prototype soon so I'll 
 * TODO circumvent it for now. I will add the option to populate the leftNodes rightNodes lists for 
 * TODO cases like ours where you know you want to have a specific group matching another one.
 * AT THE MOMENT, It is advised to use manualPartition.
 * @author Yaron Efrat
 */
public class BipartiteGraph extends Graph {

	List<Integer> leftNodes, rightNodes;
	/**
	 * Was forced to add standbyList for case a partition exists and
	 * you try to add an edge whose incident nodes are not
	 * in it.
	 * TODO Possibly better solution
	 * @apiNote I reworked existing comments  into
	 * a javaDocable comment block on
	 * 0.16.0 11.11.2020 14:41
	 */
	List<Edge> standbyList = new ArrayList<Graph.Edge>();
	boolean partitioned = false;
	
	/**
	 * 
	 * @param numberOfNodes The number of nodes in the graph.
	 * @param isDirected A boolean indicating whether the graph is directed or not
	 */
	public BipartiteGraph(int numberOfNodes, boolean isDirected) {
		super(numberOfNodes, isDirected);
		leftNodes = new LinkedList<Integer>();
		rightNodes = new LinkedList<Integer>();
	}
	
	/**
	 * Tries to Creates an instance of a bipartite graph.
	 * This constructor receives a ready adjacency list, which is probably rare.
	 * It constructs it using the Graph constructor, and then immediately tries to divide it into two parts.
	 * If it fails, it throws an Exception.
	 * Use the {@link #addEdge(int, int, long)}
	 * method to add edges to the graph.	 *  
	 * @param numberOfNodes The number of nodes in the graph.
	 * @param adjacencyList A mapping of nodes and edge lists representing the edges of all the nodes in a directed manner. In undirected graphs
	 * the inverse edges is simply automatically created.
	 * @param isDirected A boolean indicating whether the graph is directed or not
	 * @throws SizeMismatchException if the supplied Adjacency list is of the wrong size
	 */
	public BipartiteGraph(int numberOfNodes, Map<Integer, List<Edge> > adjacencyList, boolean isDirected) throws SizeMismatchException {		
		super(numberOfNodes, adjacencyList, isDirected);
		createPartition(adjacencyList);
		leftNodes = new LinkedList<Integer>();
		rightNodes = new LinkedList<Integer>();
	}

    /**
     * This method receives an adjacencyList mapping and tries to divide the nodes(keys) into separate parts that only contain edges between the parts
     * @param adjacencyList A mapping of nodes and edge lists
     * @throws UnsupportedOperationException I actually don't remember why at the moment
     */
	private void createPartition(Map<Integer, List<Edge>> adjacencyList) throws UnsupportedOperationException{
		// leftNodes.add(0); // Adds the 0th node to the left list (arbitrary)// BAD IDEA: can't know what will be pulled first from map
		/*
		 * The fix to the above error, since it really doesn't matter which node we add first ad long as we do, is to 
		 * Extract the keySet, and from it an Iterator, and just go over it in whatever order it wants with the caveat that
		 * the first one will be explicitly put in leftNodes
		 */
		// Iterator<Integer> iterate = adjacencyList.keySet().iterator();
		/*
		 * After some reading, decided to go with new ArrayList<Entry<Integer, List<Edge> > >(adjacencyList.entrySet());
		 * Not the most elegant solution, but it will do for now
		 * TODO FIND A BETTER SOLUTION MAYBE
		 * It's quite convoluted, but I start at entryList.size() - 1, and while going backwards in i, 
		 * take the key of the ith Entry in the list.
		 * Hopefully it works as desired.
		 */
		List<Entry<Integer, List<Edge> > > entryList = new ArrayList<Entry<Integer, List<Edge> > >(adjacencyList.entrySet());
		int i = entryList.size() - 1;
		leftNodes.add(entryList.get(i).getKey()); // TODO FIX THIS BULLSHIT
		// I had to change it to a do while to properly handle the iterator without losing anything
		// Now it goes through all nodes and updates the partition with the edges inside
		do {
			
			/*
			 * I migrated the contents of the for loop to a method, updatePartition, which serves as a general partition updating method which
			 * serves other purposes. Although at the moment it's not efficient for the other thing I use it for
			 */
			updatePartition(entryList.get(i).getKey()); 
			i--;	
		} while (i >= 0);
		partitioned = true;
	}
	
	/**
	 * Takes a node number, and checks for all edges that stem from it (if
	 * in left partition list) or that lead to it (if in right partition
	 * list) two things:
	 * if the other node on the edge is on the same side throw an exception.
	 * if it is not yet on the other side, add it to the other side 
	 * @param from The node number to be checked for in the left or right list
	 * @throws UnsupportedOperationException if there is an edge whose two nodes
	 * are on the same side, then it stops being a bipartite graph and therefore
	 * it is not supported.
	 */
	private void updatePartition(int from) throws UnsupportedOperationException {
		// TODO At the moment it is also used when adding an edge which is very foolish. I need to fix this soon. I decided to try to complete some things first
		if (leftNodes.contains(from)) {
			adjacencyList.get(from).forEach(edge -> {
				if (leftNodes.contains(edge.to)) {
					throw new UnsupportedOperationException("The graph is not bipartite!");
				}
				else {
					if (!rightNodes.contains(edge.to)) {
						rightNodes.add(edge.to);
					}
				}

			});
		}
		else if (rightNodes.contains(from)) {
			adjacencyList.get(from).forEach(edge -> {
				if (rightNodes.contains(edge.to)) {
					throw new UnsupportedOperationException("The graph is not bipartite!");
				}
				else {
					if (!leftNodes.contains(edge.to)) {
						leftNodes.add(edge.to);
					}
				}

			});
		}
	}

	/**
	 * The method isBipartite checks if the graph is truly bipartite. If the instance variable partitioned is false, it calls createPartition which will throw an
	 * exception if the graph is none bipartite. Otherwise it will iterate over the leftNodes list to check that no node there exist in rightNodes as well
	 * @return false if it found a node which is in both lists. true if it's not the case or it survived createPartition with no exception thrown
	 */
	//@Override
	public boolean isBipartite() {
		if (!partitioned) {
			createPartition(adjacencyList);
		}
		else {			
			for(Integer node: leftNodes) {
				if (rightNodes.contains(node)) {
					return false;
					
				}
			}
		}
		return true;
	}
	
	/**
	 * The method isBipartite(Edge) checks if the edge would be legal in the existing graph.
	 * It will check if from and to are in the same list or not
	 * @param edge The specific edge to be checked for.
	 * @return false if it found a node which is in both lists. true if it's not the case 
	 * or it survived createPartition with no exception thrown
	 */
	//@Override
	public boolean isBipartite(Edge edge) {
		if (leftNodes.contains(edge.from) && leftNodes.contains(edge.to)) {
			return false;
		}
		else if (rightNodes.contains(edge.from) && rightNodes.contains(edge.to)){			
			return false;				
		}
		else {
		    return true;
		}
	}
	
	/**
	 * This method adds an edge to the adjacency list. If the graph is undirected, it will also automatically
	 * add the "inverse" edge.
	 * It will also try to update the partition if it exists or it will try to create one otherwise.
	 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
	 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
	 * @param weight The weight of the edge, which is set to a default value of 1 in case it is an unweighted graph
	 * @return The created edge
	 */
	@Override
	public Edge addEdge(int from, int to, long weight) {
		Edge edge1 = new Edge(from, to, weight, isDirected);
		if (leftNodes.size() + rightNodes.size() != numberOfNodes) {
			if (partitioned) {
				/*
				 * In order to handle cases where only the to node or none of them are in the partition, added 
				 * if statements and standby list.
				 * Will change updatePartition as well to compensate.
				 */
				if ((leftNodes.contains(from)) || (rightNodes.contains(from))) {
					updatePartition(from);
				} else if ((leftNodes.contains(to)) || (rightNodes.contains(to))) {
					updatePartition(to);
				} else {
					standbyList.add(edge1);
				}

			} else {
				createPartition(adjacencyList);
			} 
		}
		else {
			if (!isBipartite(edge1)) {
				System.out.println("This edge (" + from + " -> " + to + ") is illegal and will therefore not be added");
				return edge1;
			}
		}
		/*
		 * After the introduction of manualPartition, I switched to using update/createPArtition 
		 * only if leftNodes.size() + rightNodes.size() != numberOfNodes.
		 * That meant that else, I didn't have an exception thrown. Instead, I realized I could move the actual 
		 * adding to after the if ... else block and then opt out if the Edge is illegal
		 */		
		adjacencyList.get(from).add(edge1);
		// If an undirected graph, automatically adds the inverse edge with the same weight
		if (!isDirected) {
			Edge edge2 = new Edge(to, from, weight, isDirected);
			adjacencyList.get(to).add(edge2);
		}
		return edge1;
	}
	
	/**
	 * This method adds an edge to the adjacency list. If the graph is undirected, it will also automatically
	 * add the "inverse" edge. The edge has a default weight of 1. Calls {@link #addEdge(int, int, long)}
	 * @param from In the case of a directed graph this is the tail, otherwise it is one of the incident nodes
	 * @param to In the case of a directed graph this is the head, otherwise it is one of the incident nodes
	 * @return The created edge
	 */
	@Override
	public Edge addEdge(int from, int to) {
		return addEdge(from, to, 1);
	}
	
	/**
	 * I was forced to create this method as a patchwork solution for "Orphan" edges.
	 */
	public void clearStandby() {
		Edge edge1;
		while(!standbyList.isEmpty()) {
			edge1 = standbyList.remove(standbyList.size() - 1);
			if ( (!leftNodes.contains(edge1.from)) && (!rightNodes.contains(edge1.from))) {
			    if (!leftNodes.contains(edge1.to)) {
				    leftNodes.add(edge1.from);
			    }
			    else {
			    	rightNodes.add(edge1.from);
			    }
			}
			updatePartition(edge1.from);			
		}
	}
	
	/**
	 * Takes two arrays of int that represent a partition of the nodes and puts it into the relevant lists
	 * @param left array of nodes to be inserted into the leftNodes list
	 * @param right array of nodes to be inserted into the rightNodes list
	 */
	public void manualPartition(int[] left, int[] right) {
		// Thanks to Sheepy on stackoverflow for their answer
		Collections.addAll(leftNodes, Arrays.stream( left ).boxed().toArray( Integer[]::new ));
		Collections.addAll(rightNodes, Arrays.stream( right ).boxed().toArray( Integer[]::new ));
		partitioned = true;
	}
	
	/**
	 * Takes two lists of Integer that represent a partition of the nodes and puts it into the relevant lists
	 * @param left list of nodes to be inserted into the leftNodes list
	 * @param right list of nodes to be inserted into the rightNodes list
	 */
	public void manualPartition(List<Integer> left, List<Integer> right) {
		leftNodes.addAll(left);
		rightNodes.addAll(right);
		partitioned = true;
	}

	/**
	 * ADDED AT 0.5.0 BECAUSE IT MADE LIFE EASIER FOR MMMTPrototype
	 * Takes two arrays of Integers that represent a partition of the nodes and puts it into the relevant lists
	 * @param left array of nodes to be inserted into the leftNodes list
	 * @param right array of nodes to be inserted into the rightNodes list
	 */
	public void manualPartition(Integer[] left, Integer[] right) {
		Collections.addAll(leftNodes, left);
		Collections.addAll(rightNodes, right);
		partitioned = true;
		
	}
	
	/**
	 * Removes any edges whose from -&gt; to are right to left, and makes 
	 * sure that isDirected is set to true.
	 * @return An adjacency list containing only left to right directed edges 
	 */
	public Map<Integer, List<Edge>> leftToRightDirectedness() {
		LinkedHashMap<Integer, List<Edge> > leftToRightList = new LinkedHashMap<Integer, List<Edge>>(numberOfNodes, (float) 0.75, true);
		/*
		 * I actually got a Null pointer exception because I didn't put any keys 
		 * and then tried to access them. So now I will just initialize
		 * all keys to avoid lykicheewi.burtefrahouse.exceptions.
		 */
		for (int i = 0; i < numberOfNodes; i++) {
			leftToRightList.put(i, new LinkedList<Edge>());
		}
		/*
		 * The method is pretty straightforward: A nested foreach for loop to access the edges themselves in the adjacency list
		 * and then if this edge is from a node in the leftNodes list (We assume it is a legal bipartite map, meaning it's
		 * pointing to the rightNodes node) make sure it will be directed and then add it to the map to be returned to 
		 * the caller. 
		 * Note: This handles undirected graphs as well, because all they do is add an edge in the opposite direction,
		 * so of the edge pairs only the right facing one is taken.
		 */
		for (List<Edge> edgeList : adjacencyList.values()) {
			for (Edge edge : edgeList) {
				if (leftNodes.contains(edge.from)) {
					edge.isDirected = true;
					
					leftToRightList.get(edge.from).add(edge);
				}
			}
		}
		return leftToRightList;
	}
	
	/**
	 * Had to add this method to help handle the conversion to a flow graph.
	 * @return an auxiliary graph that has a source and sink attached to help in the transition.
	 * @throws Exception Don't remember why :(
	 */
	public BipartiteGraph attachSourceAndSink() throws Exception {		
		BipartiteGraph tempGraph = new BipartiteGraph(numberOfNodes + 2, isDirected); // Creating a bigger graph with sink and source
		tempGraph.adjacencyList.putAll(adjacencyList); // Copying the original graph's map
		/*
		 * Adding isolated source and sink
		 */
		tempGraph.adjacencyList.put(numberOfNodes, new LinkedList<Edge>());
		tempGraph.adjacencyList.put(numberOfNodes + 1, new LinkedList<Edge>());
		/*
		 * copying partition from original map
		 */
		tempGraph.leftNodes = leftNodes;
		tempGraph.rightNodes = rightNodes;
		/*
		 * After the weight_capacity debacle, I had to find a way to change all capacities to 1 for the new flow graph. This is the perfect spot
		 */
		for (List<Edge> edgeList : adjacencyList.values()) {
			for (Edge edge : edgeList) {
				edge.capacity = 1;
			}
		}
		return tempGraph;
	}
	
}
