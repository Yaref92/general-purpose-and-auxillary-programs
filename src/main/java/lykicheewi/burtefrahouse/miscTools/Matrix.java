/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import lykicheewi.burtefrahouse.exceptions.SizeMismatchException;
import lykicheewi.burtefrahouse.graphTools.GraphProblemSolver;

/**
 * NOTE 0.6.1 6.4.2020 11:23 -
 * In order to allow toString to display the matrix itself,
 * I wanted to allow it access to printMat but it is void...
 * I feel like it kind of forces me to change it to String...
 * 
 */

/** 
 * NOTE 02.04.2020, 10:38 -  have implemented the following methods so far:
 * constructors, resolve operator, populateElement and Matrix, getElement,
 * row and column addition, max and min in row and column, printMat.
 * While there are more useful methods to be realized, I shall leave
 * that to the future because I am under time pressure.
 * <br> <br>
 * This class defines an implementation of a Matrix using ArrayList.
 * It has two generic types: one for the actual content,
 * and one for the titles.
 * The titles can be used to access rows and columns directly
 * using them instead of the indices of the row or column.
 * @param <T> The type for the titles (String or int mostly)
 * @param <C> The type for the content (int or double mostly)
 * @author Yaron Efrat
 */
@SuppressWarnings("unused")
public class Matrix<C, T> {

	private ArrayList<ArrayList<C>> matrixa;
	private int rowNum, colNum; // The number of rows and number of columns
	private T[] rowTitles; // The titles for the rows
	private T[] colTitles; // The titles for the columns

	// part of an effort to solve addition of Generic types (25.03.2020, 11:52)
	private final GenericOperable<C> operationMaster;

	/**
	 * Constructor already using a List in a List.
	 * @param twoDArray A List in a List representing a matrix
	 */
	public Matrix(ArrayList<ArrayList<C>> twoDArray) {
		matrixa = twoDArray;
		rowNum = twoDArray.size(); // That's how you get the number of rows: the size of a 2d ArrayList
		colNum = twoDArray.get(0).size(); // For the number of columns you access one of the row ArrayList and take its size
		rowTitles = null;
		colTitles = null;
		operationMaster = resolveOperable(); // part of an effort to solve addition of Generic types (25.03.2020, 11:53)
	}


	/**
	 * Creates an 0s Matrix with the titles being provided in arr1 and arr2
	 * <br> I am aware of the unchecked cast
	 * @param rowTitles an array representing the different rows. The values are the row titles.
	 * @param colTitles an array representing the different columns. The values are the columns titles.
	 * @apiNote Changed the parameter names to make it
	 * more clear 0.16.0 09.12.2020 20:38
	 */
	@SuppressWarnings("unchecked")
	public Matrix(T[] rowTitles, T[] colTitles) {
		// Creates a list of lists of size arr1.length and then 
		// in a loop assigns each sub list a new list object size arr2.length
		matrixa = new ArrayList<ArrayList<C>>(rowTitles.length);
		for (int i = 0; i<rowTitles.length; i++) {
			matrixa.add(new ArrayList<C>(colTitles.length));
			/* In order to avoid problems later on 
			 * when wanting to populate elements using 
			 * set directly on a specific index
			 * it is better to put a default 0
			 * in there. Converted to type C of course.
			 */ 
			for (int j = 0; j < colTitles.length; j++) {
				matrixa.get(i).add((C) (Integer) 0); // Using double cast because int to Integer is supported, but int to generic type isn't.
			}
		}
		rowNum = rowTitles.length; // That's the number of rows
		colNum = colTitles.length; // 
		this.rowTitles = Arrays.copyOf(rowTitles, rowNum); // Added this to prepare for parameter clarification 0.16.0 09.12.2020 20:37
		this.colTitles = Arrays.copyOf(colTitles, colNum); // Added this to prepare for parameter clarification 0.16.0 09.12.2020 20:37
		operationMaster = resolveOperable(); // part of an effort to solve addition of Generic types (25.03.2020, 11:53)
	}

	/**
	 * Creates a Matrix from twoDArray with the titles being provided in arr1 and arr2
	 * @param rowTitles an array representing the different rows. The values are the row titles.
	 * @param colTitles an array representing the different columns. The values are the columns titles.
	 * @param twoDArray A List in a List representing a matrix
	 * @apiNote Changed the parameter names to make it
	 * more clear 0.16.0 09.12.2020 20:39
	 * @throws SizeMismatchException - if the sizes of the twoDArray and the Titles array don't match
	 */
	public Matrix(T[] rowTitles, T[] colTitles, ArrayList<ArrayList<C>> twoDArray) throws SizeMismatchException{
		if ((twoDArray.size() != rowTitles.length) || (twoDArray.get(0).size() != colTitles.length) ) {
			throw new SizeMismatchException("The twoDArray sizes are not compatible with the sizes of arr1 and arr2");
		}
		matrixa = twoDArray;
		rowNum = rowTitles.length; // That's the number of rows
		colNum = colTitles.length; // 
		this.rowTitles = rowTitles; // Added this to prepare for parameter clarification 0.16.0 09.12.2020 20:39
		this.colTitles = colTitles; // Added this to prepare for parameter clarification 0.16.0 09.12.2020 20:39
		operationMaster = resolveOperable(); // part of an effort to solve addition of Generic types (25.03.2020, 11:53)
	}

	/**
	 * part of an effort to solve addition of Generic types (25.03.2020, 11:52).
	 * Will only support Integer for now for time reasons and extend as other types are necessary.
	 * It's pretty easy to expand anyways, so it's not that bad.
	 * @return an Object from the Class implementing {@link misctools.GenericOperable}
	 * for the relevant content class of the Matrix.
	 */
	@SuppressWarnings("unchecked")
	private GenericOperable<C> resolveOperable() {
		if (matrixa.get(0).get(0) instanceof Integer) {
			return (GenericOperable<C>) runtimeInteger.OPERATOR;
		}
		return null;
	}

	/**
	 * This method takes indices for a row and a column
	 * and injects the desired element there
	 * @param rowIndex - index of the row
	 * @param colIndex - index of the column
	 * @param desiredElement - the desired element to put in (row,column)
	 */
	public void populateElement(int rowIndex, int colIndex, C desiredElement) {
		matrixa.get(rowIndex).set(colIndex, desiredElement);
	}

	/**
	 * This method takes titles for a row and a column,
	 * checks their index in the rowTitles and colTitles,
	 * and injects the desired element in the cell corresponding
	 * to the row and column represented by the index.
	 * @param rowTitle - The title of the row. Should correspond to an entry of rowTitles
	 * @param colTitle - The title of the column. Should correspond to an entry of columnTitles
	 * @param desiredElement - the desired element to put in (row,column)
	 */
	public void populateElement(T rowTitle, T colTitle, C desiredElement) {
		/*
		 * To find the relevant indices, the static method
		 * findInArray from the custom made ArrayTools class
		 * is used
		 */
		int rowIndex = ArrayTools.findInArray(rowTitle, rowTitles);
		int colIndex = ArrayTools.findInArray(colTitle, colTitles);
		populateElement(rowIndex, colIndex, desiredElement);
	}

	/**
	 * Takes desiredMatrix, loops through it, and populates each element
	 * with populateElement.
	 * @param desiredMatrix - the desired Matrix to put in the Matrix&lt;T,C&gt;
	 */
	public void populateMatrix(C[][] desiredMatrix) {
		for (int i = 0; i < desiredMatrix.length; i++) {
			for (int j = 0; j < desiredMatrix[0].length; j++) {
				populateElement(i, j, desiredMatrix[i][j]);
			}
		}
	}

	/**
	 * ADDED IN (4.4.2020 10:37) TO SUPPORT DEEP CLONING
	 * Takes desiredMatrix, loops through it, and populates each element
	 * with populateElement.
	 * @param desiredMatrix - the desired Matrix to put in the Matrix&lt;T,C&gt;
	 */
	public void populateMatrix(ArrayList<ArrayList<C>> desiredMatrix) {
		for (int i = 0; i < desiredMatrix.size(); i++) {
			for (int j = 0; j < desiredMatrix.get(0).size(); j++) {
				populateElement(i, j, desiredMatrix.get(i).get(j));
			}
		}
	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:29) because of need.
	 * Adds more rows to the matrixa
	 * @param numAddedRows the number of rows to add
	 * @param newRowTitles the titles for the new rows. can be empty
	 * @param valueToFill the value to be filled in new row.
	 * if none is specified then the default is null
	 */
	public void addRows(int numAddedRows, T[] newRowTitles, C valueToFill) {
		// We will also need to update the row titles, and will use an ArrayList to facilitate the change
		ArrayList<T> tempRowTitles = new ArrayList<T>();
		Collections.addAll(tempRowTitles, rowTitles);
		for (int i = 0; i < numAddedRows; i++) {
			matrixa.add(new ArrayList<C>()); // We add an empty ArrayList, which we will need to fill with something
			// Didn't initialize all individual elements at first - a mistake. Will replace fillRow with direct ssignment.
			for (int j = 0; j < colNum; j++) {
				matrixa.get(rowNum + i).add(valueToFill);
			}// Fills the row so we don't have unintended jagged matrix
			tempRowTitles.add(newRowTitles[i]); // Adding the new titles.
		}		
		rowTitles = tempRowTitles.toArray(rowTitles);	
		rowNum += numAddedRows; // Do not forget to update this!
	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:59) because of need.
	 * Adds more rows to the matrixa. Fills them with 
	 * nulls.
	 * @param numAddedRows the number of rows to add
	 * @param newRowTitles the titles for the new rows. can be empty
	 */
	public void addRows(int numAddedRows, T[] newRowTitles) {
		addRows(numAddedRows, newRowTitles, null);
	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:29) because of need.
	 * Adds more columns to the matrixa
	 * @implNote BUG DETECTED 0.16.0 12.11.2020 12:09 -
	 * THE TITLE IS ADDED TOO MANY TIMES! 
	 * I think the best solution in terms
	 * of efficiency is a boolean called
	 * addedTitles to only add them once and not
	 * every time we go to a new row...
	 * BUGFIX202011121211
	 * @param numAddedColumns the number of columns to add
	 * @param newColTitles the titles for the new columns. can be empty
	 * @param valueToFill the value to be filled in new row.
	 * if none is specified then the default is null
	 */
	public void addColumns(int numAddedColumns, T[] newColTitles, C valueToFill) {
		// We will also need to update the row titles, and will use an ArrayList to facilitate the change
		List<T> tempColTitles = new ArrayList<T>();
		Collections.addAll(tempColTitles, colTitles);
		// BUGFIX202011121211
		boolean addedTitles = false;
		for (ArrayList<C> row : matrixa) {			
			for (int i = 0; i < numAddedColumns; i++) {
				row.add(valueToFill); // We add an empty ArrayList, which we will need to fill with something
				if (!addedTitles) { // BUGFIX202011121211
					// Also fills the column so we don't have unintended jagged matrix
					tempColTitles.add(newColTitles[i]); // Adding the new titles.
				}
			}
			addedTitles = true; // BUGFIX202011121211
		}
		colTitles = tempColTitles.toArray(colTitles);		
		colNum += numAddedColumns; // Do not forget to update this!
	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:59) because of need.
	 * Adds more columns to the matrixa. Fills them with 
	 * nulls.
	 * @param numAddedColumns the number of columns to add
	 * @param newColTitles the titles for the new columns. can be empty
	 */
	public void addColumns(int numAddedColumns, T[] newColTitles) {
		addColumns(numAddedColumns, newColTitles, null);
	}

	/**
	 * ADDED 0.6.0 (2.4.2020 15:26) because of need.
	 * It fills the whole Matrix with the same value.
	 * @param valueToFill The value to fill the matrix with.
	 */
	public void fillMatrix(C valueToFill) {
		for (ArrayList<C> row : matrixa) {
			Collections.fill(row, valueToFill);
		}

	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:29) because of need.
	 * It fills the specified row with the same value. 
	 * @param rowIndex the index of the row to be filled
	 * @param valueToFill The value to fill the matrix with.
	 */
	public void fillRow (int rowIndex, C valueToFill) {
		Collections.fill(matrixa.get(rowIndex), valueToFill);
	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:29) because of need.
	 * It fills the specified row with the same value. 
	 * @param rowTitle the title of the row to be filled
	 * @param valueToFill The value to fill the matrix with.
	 */
	public void fillRow (T rowTitle, C valueToFill) {
		int rowIndex = ArrayTools.findInArray(rowTitle, rowTitles);
		fillRow(rowIndex, valueToFill);
	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:29) because of need.
	 * It fills the specified column with the same value. 
	 * @param columnIndex the index of the column to be filled
	 * @param valueToFill The value to fill the matrix with.
	 */
	public void fillColumn (int colIndex, C valueToFill) {
		for (ArrayList<C> row : matrixa) {
			row.set(colIndex, valueToFill);
		}
	}

	/**
	 * ADDED 0.6.1 (7.4.2020 12:29) because of need.
	 * It fills the specified column with the same value. 
	 * @param colTitle the title of the column to be filled
	 * @param valueToFill The value to fill the matrix with.
	 */
	public void fillColumn (T colTitle, C valueToFill) {
		int colIndex = ArrayTools.findInArray(colTitle, colTitles);
		fillColumn(colIndex, valueToFill);
	}

	/**
	 * Simply accesses the element at (i,j)
	 * and returns it
	 * @param rowIndex - index of the row
	 * @param colIndex - index of the column
	 * @return the element at (row,column)
	 */
	public C getElement(int rowIndex, int colIndex) {
		return matrixa.get(rowIndex).get(colIndex);
	}

	/**
	 * Simply accesses the element at (i,j)
	 * and returns it
	 * @param rowTitle - The title of the row. Should correspond to an entry of rowTitles
	 * @param colTitle - The title of the column. Should correspond to an entry of columnTitles
	 * @return the element at (row,column)
	 */
	public C getElement(T rowTitle, T colTitle) {
		int rowIndex = ArrayTools.findInArray(rowTitle, rowTitles);
		int colIndex = ArrayTools.findInArray(colTitle, colTitles);
		return getElement(rowIndex, colIndex);
	}

	/**
	 * Goes to the specified row and adds to all elements the valueToAdd<br> 
	 * WARNING: be sure to be aware of the way your type handles
	 * the '+' operator. If it does so in unexpected ways or doesn't
	 * support it don't say I didn't warn you.
	 * @param rowIndex - The index of the row to which the value should
	 * be add
	 * @param valueToAdd - The value to be added to the row
	 */
	public void rowAddition(int rowIndex, C valueToAdd) {
		/*
		 * There was a bug (4.4.2020, ~10:45), that caused
		 * the same element to be reduxexd twice.
		 * At first, I suspected the Iterator was to blame
		 * The Iterator was not the culprit! It was indexOf!
		 * Who found the same element twice because after reducing from 32 to 16
		 * it found it instead of the 16 next to it.
		 * HOWEVER! a regular for (instead of a foreach) 
		 * will still help solve this problem,
		 * since it will circumvent the use of indexOf, 
		 * using instead the index from the loop
		 * Thanks ArrayList! 
		 * (10:59) - Bug eliminated
		 */
		//for (C element : matrixa.get(rowIndex)) {
		for (int i = 0; i < matrixa.get(rowIndex).size(); i++) {
			C element = matrixa.get(rowIndex).get(i);
			// Was a hassle. I had to create an interface and a class handling Integer
			// because of generic type. And will need to add any type I want to the interface
			// from now on. operationMaster is of the interface and handles addition etc for the
			// generic type (at the moment can only be Integer)
			matrixa.get(rowIndex).set(i, operationMaster.add(element, valueToAdd));
		}
	}

	/**
	 * Goes to the specified row and adds to all elements the valueToAdd<br> 
	 * WARNING: be sure to be aware of the way your type handles
	 * the '+' operator. If it does so in unexpected ways or doesn't
	 * support it don't say I didn't warn you.
	 * @param rowTitle - The title of the row to which the value should
	 * be added
	 * @param valueToAdd - The value to be added to the row
	 */
	public void rowAddition(T rowTitle, C valueToAdd) {
		int rowIndex = ArrayTools.findInArray(rowTitle, rowTitles);
		rowAddition(rowIndex,  valueToAdd);
	}

	/**
	 * See {@link #rowAddition(int, Object)}, it's the same purpose
	 * but for columns.
	 * @param colIndex - index of the column to which the value should
	 * be added
	 * @param valueToAdd - The value to be added to the column
	 */
	public void columnAddition(int colIndex, C valueToAdd) {
		for (int i = 0; i < matrixa.size(); i++) {
			// see above for operationMaster comment
			matrixa.get(i).set(colIndex, operationMaster.add(matrixa.get(i).get(colIndex), valueToAdd));
		}
	}

	/**
	 * see {@link #rowAddition(Object, Object)}. But for columns.
	 * @param colTitle - title of the column to which the value should
	 * be added
	 * @param valueToAdd - The value to be added to the column
	 */
	public void columnAddition(T colTitle, C valueToAdd) {
		int colIndex = ArrayTools.findInArray(colTitle, colTitles);
		columnAddition(colIndex,  valueToAdd);
	}

	/**
	 * Finds and returns maximal value in row.<br>
	 * WARNING: The types need to be comparable.
	 * @param rowIndex - the index of the row to be checked
	 * @return the maximal value in the row
	 */
	public C maxInRow(int rowIndex) {
		C max = operationMaster.MIN_VALUE(); // Initializing max to minimal value possible to account for every case
		for (C element : matrixa.get(rowIndex)) {
			if (operationMaster.compare(element, max) > 0) {
				max = element;
			}				
		}
		return max;
	}

	/**
	 * Finds and returns maximal value in row.<br>
	 * WARNING: The types need to be comparable.
	 * @param rowTitle - the title of the row to be checked
	 * @return the maximal value in the row
	 */
	public C maxInRow(T rowTitle) {
		int rowIndex = ArrayTools.findInArray(rowTitle, rowTitles);
		return maxInRow(rowIndex);
	}

	/**
	 * Finds and returns minimal value in row.<br>
	 * WARNING: The types need to be comparable.
	 * @param rowIndex - the index of the row to be checked
	 * @return the minimal value in the row
	 */
	public C minInRow(int rowIndex) {
		C min = operationMaster.MAX_VALUE(); // Initializing max to minimal value possible to account for every case
		for (C element : matrixa.get(rowIndex)) {
			if (operationMaster.compare(element, min) < 0) {
				min = element;
			}				
		}
		return min;
	}

	/**
	 * Finds and returns minimal value in row.<br>
	 * WARNING: The types need to be comparable.
	 * @param rowTitle - the title of the row to be checked
	 * @return the minimal value in the row
	 */
	public C minInRow(T rowTitle) {
		int rowIndex = ArrayTools.findInArray(rowTitle, rowTitles);
		return minInRow(rowIndex);
	}

	/**
	 * Finds and returns maximal value in column.<br>
	 * WARNING: The types need to be comparable.
	 * @implNote 0.16.0 12.11.2020 11:18
	 * Noticed that I stupidly called
	 * matrixa.get(i).get(colIndex)
	 * twice (once in the if in the compare, <br>
	 * and a second time in the assignment)
	 * instead of Putting it inside of a variable.<br>
	 * Inefficient.<br>
	 * Fixed it.<br>
	 * It now also resembles maxInRow more closely.
	 * @param colIndex - the index of the column to be checked
	 * @return the maximal value in the column
	 */
	public C maxInColumn(int colIndex) {
		C max = operationMaster.MIN_VALUE(); // Initializing max to minimal value possible to account for every case
		for (int i = 0; i < matrixa.size(); i++) {
			/*
			 * See the implNote.
			 */
			C element = matrixa.get(i).get(colIndex);
			if (operationMaster.compare(element, max) > 0) {
				max = element;
			}				
		}
		return max;
	}

	/**
	 * Finds and returns maximal value in column.<br>
	 * WARNING: The types need to be comparable.
	 * @param colTitle - the title of the column to be checked
	 * @return the maximal value in the column
	 */
	public C maxInColumn(T colTitle) {
		int colIndex = ArrayTools.findInArray(colTitle, colTitles);
		return maxInColumn(colIndex);
	}

	/**
	 * Finds and returns minimal value in column.<br>
	 * WARNING: The types need to be comparable.
	 * @implNote 0.16.0 12.11.2020 11:22
	 * Noticed that I stupidly called
	 * matrixa.get(i).get(colIndex)
	 * twice (once in the if in the compare, <br>
	 * and a second time in the assignment)
	 * instead of Putting it inside of a variable.<br>
	 * Inefficient.<br>
	 * Fixed it.<br>
	 * @param colIndex - the index of the column to be checked
	 * @return the minimal value in the column
	 */
	public C minInColumn(int colIndex) {
		C min = operationMaster.MAX_VALUE(); // Initializing max to minimal value possible to account for every case
		for (int i = 0; i < matrixa.size(); i++) {
			/*
			 * See the implNote.
			 */
			C element = matrixa.get(i).get(colIndex);
			if (operationMaster.compare(element, min) < 0) {
				min = element;
			}				
		}
		return min;
	}

	/**
	 * Finds and returns minimal value in column.<br>
	 * WARNING: The types need to be comparable.
	 * @param colTitle - the title of the column to be checked
	 * @return the minimal value in the column
	 */
	public C minInColumn(T colTitle) {
		int colIndex = ArrayTools.findInArray(colTitle, colTitles);
		return minInColumn(colIndex);
	}

	/**
	 * @return the rowNum
	 */
	public int getRowNum() {
		return rowNum;
	}


	/**
	 * @return the colNum
	 */
	public int getColNum() {
		return colNum;
	}

	/**
	 * @return the rowTitles
	 */
	public T[] getRowTitles() {
		return rowTitles;
	}


	/**
	 * @return the colTitles
	 */
	public T[] getColTitles() {
		return colTitles;
	}


	/**
	 * CHANGED TO STRING AS OF 0.6.1
	 * Changed on 25.03.2020 to call the new printMat(boolean withTitles)
	 * with false, i.e. it will call the other printMat but without
	 * asking for titles.
	 * After creating the printMat with the formatting option, decided to let it do
	 * ALL the heavy lifting, and just call for it with default lengths of 5 and 5.
	 * @return The String of the Matrix
	 */
	public String printMat() {
		return printMat(false, 5, 5, true);		
	}

	/**
	 * CHANGED TO STRING AS OF 0.6.1
	 * Added on 25.03.2020, to facilitate the possibility of
	 * printing a Matrix with its Row and Column titles (if
	 * relevant).
	 * After creating the printMat with the formatting option, decided to let it do
	 * ALL the heavy lifting, and just call for it with default lengths of 5 and 5.
	 * @param withTitles  A boolean for whether the titles of the rows and columns
	 * should be printed
	 * @return The String of the Matrix
	 */
	public String printMat(boolean withTitles) {
		return printMat(withTitles, 5, 5, true);		
	}

	/**
	 * Added in 0.6.1
	 * @param withTitles A boolean for whether the titles of the rows and columns
	 * @param shouldPrint A boolean for whether the String should be printed to stdout
	 * @return The String of the Matrix
	 */
	public String printMat(boolean withTitles, boolean shouldPrint) {
		return printMat(withTitles, 5, 5, shouldPrint);		
	}

	/**
	 * CHANGED TO STRING AS OF 0.6.1
	 * Added on 25.03.2020, 10:06 to facilitate the possibility of
	 * formatting the printing of a Matirx in such a way that
	 * the maximal allowed lengths of titles and contents are 
	 * limited and with those limits in mind a printf is usef
	 * to pad the excesses with whitespace and truncate longer
	 * strings.
	 * Still allows the choice of printing a Matrix with its 
	 * Row and Column titles (if relevant) or not.
	 * UPDATE 25.03.2020, 10:44 - DECIDED TO SWITCH [ and ] with | to accomodate title
	 * separation. Now it leads me to the decision to add a row of ----------
	 * between each row. Let's see if it works properly.
	 * For now it's on hold (11:10)
	 * @param withTitles - A boolean for whether the titles of the rows and columns
	 * should be printed
	 * @param maxTitleLength - The maximum length allowed for the titles to occupy.
	 * Used as part of the format of the String in printf
	 * @param maxContentLength - The maximum length allowed for the contents to occupy.
	 * Used as part of the format of the String in printf
	 * @param shouldPrint A boolean for whether the String should be printed to stdout. Defaults to true
	 * @return The String of the Matrix
	 */
	public String printMat(boolean withTitles, int maxTitleLength, int maxContentLength, boolean shouldPrint) {
		String matString = "";
		/*
		 * The way the formatting will work will be maxTitle.maxTitle
		 * for the titles and maxTitle.maxContent for the content.
		 * However, if the user put a maxContent that is larger than the 
		 * maxTitle, it makes sense to use it for both
		 */
		if (maxContentLength > maxTitleLength) {
			maxTitleLength = maxContentLength;
		}
		if (withTitles) {
			/*
			 * First, you need to pad the upperleft corner with maxTitleLength
			 * plus 1 for the following [ in the other rows
			 * The next line deals with it
			 */
			System.out.printf("%"+maxTitleLength+"."+maxTitleLength+"s ", " ");
			//String temp = String.format("%"+maxTitleLength+"."+maxTitleLength+"s ", " ");
			//matString.concat(String.format("%"+maxTitleLength+"."+maxTitleLength+"s ", " "));			
			for (int i = 0; i < colNum; i++) {
				System.out.printf("%"+maxTitleLength+"."+maxTitleLength+"s|", colTitles[i]);
				matString.concat(String.format("%"+maxTitleLength+"."+maxTitleLength+"s|", colTitles[i]));
			}	
			/*
			 * System.out.println();
			 * // SEE UPDATE 25.03.2020, 10:44
			 * System.out.printf("%"+maxTitleLength+"."+maxTitleLength+"s ", " ");				
			 * // Thanks to Caner for their answer on https://stackoverflow.com/questions/1235179/simple-way-to-repeat-a-string-in-java
			 * String.join(" ", Collections.nCopies(maxTitleLength*colNum, "-"));
			 */
			System.out.println();
			matString.concat(String.format("\n"));
		}	
		//for (ArrayList<C> row : matrixa) {
		for (int i = 0; i < rowNum; i++) { // Changed from the commented out for to this one.
			if (withTitles) {
				/*
				 *  matrixa.indexOf(row) to know the current row num to pull the title
				 *  IMPORTANT NOTE: indexOf does not behave in the way I thought. It checks
				 *  with equals, not ==. I need to do it differently.
				 *  Possibly, because it is an ArrayList I will do a regular for instead of foreach.
				 *  CHANGED ACCORDING TO THIS. LOOK AT CHANGED ROWs.
				 */
				System.out.printf("%"+maxTitleLength+"."+maxTitleLength+"s", rowTitles[i]);	// Changed from matrixa.indexOf to i
				matString.concat(String.format("%"+maxTitleLength+"."+maxTitleLength+"s", rowTitles[i]));
			}
			System.out.print("|");
			matString.concat(String.format("|"));
			for (C element : matrixa.get(i)) { // Changed from row to matrixa.get(i)
				System.out.printf("%"+maxTitleLength+"."+maxContentLength+"s|",element.toString());
				matString.concat(String.format("%"+maxTitleLength+"."+maxContentLength+"s|",element.toString()));
			}
			System.out.print("\n");
			matString.concat(String.format("\n"));
		}
		if (shouldPrint) {
			System.out.print(matString);
		}
		return matString;		
	}

	@Override
	public Matrix<C, T> clone() throws CloneNotSupportedException {
		Matrix<C, T> clone =  new Matrix<C, T>(rowTitles, colTitles);
		clone.populateMatrix(matrixa);
		return clone;
	}


	/*
	 * @Override public String toString() { //return
	 * String.format("Matrix [matrixa=%s]", matrixa); return printMat(true, false);
	 * }
	 */


	/**
	 * @param args - default main parameter. unused.
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<ArrayList<Integer>> mati = new ArrayList<ArrayList<Integer>>(3);
		for (int i = 0; i < 3; i++) {
			mati.add(new ArrayList<Integer>(3));
			mati.get(i).add(3 * i + 1);
			mati.get(i).add(3 * i + 2);
			mati.get(i).add(3 * i + 3);
		}
		Matrix<Integer, Integer> mat = new Matrix<Integer, Integer>(mati);
		Integer[] arr1 = {123456, 234567, 3};
		Integer[] arr2 = {456789, 567890, 6};
		Matrix<Integer, Integer> mat2 = new Matrix<Integer, Integer>(arr1, arr2);
		mat.printMat();
		System.out.println();
		/*
		 * Without the cast to Integer, you get
		 * The method populateElement(int, int, Integer) is ambiguous for the type Matrix<Integer,Integer>
		 * The cast is necessary, because it helps resolve the ambiguity.
		 * Apparantely, casts to int are not necessary.
		 */
		mat.populateElement(1, 1, (Integer) 50);
		mat.printMat();
		System.out.println();
		Integer[][] testo = {{2, 4, 6}, {1, 3, 5}, {7, 7, 7}};
		mat2.populateMatrix(testo);
		mat2.printMat();
		System.out.println();
		/*
		 * Again, Without all 3 casts to Integer, you get
		 * The method populateElement(int, int, Integer) is ambiguous for the type Matrix<Integer,Integer>
		 * This time, I wanted to invoke the method with the T type
		 */
		mat2.populateElement(arr1[0], arr2[0], (Integer) 50);
		mat2.printMat(true);
		System.out.println();
		//System.out.printf("|%5."+arr2[0]+"s|%5."+arr2[0]+"s|", "Hello World", 123456);
		mat2.rowAddition(1, (Integer) 10);
		mat2.printMat();
		System.out.println();
		mat2.columnAddition(1, (Integer) 100);
		mat2.printMat();
		System.out.println();
		mat2.columnAddition((Integer) arr2[2], (Integer) 1000);
		mat2.printMat();
		System.out.println();
		System.out.println("The max in first row is: " + mat2.maxInRow(0));
		System.out.println("The max in second row is: " + mat2.maxInRow((Integer) 234567));
		System.out.println("The min in first row is: " + mat2.minInRow(0));
		System.out.println("The min in second row is: " + mat2.minInRow((Integer) 234567));
		System.out.println("The max in first column is: " + mat2.maxInColumn(0));
		System.out.println("The max in second column is: " + mat2.maxInColumn((Integer) 567890));
		System.out.println("The min in first column is: " + mat2.minInColumn(0));
		System.out.println("The min in second column is: " + mat2.minInColumn((Integer) 567890));

	}

	/**
	 * Finds and returns maximal value in matrix,
	 * by iterating over the rows and calling 
	 * maxInRow on them and then using that as the
	 * element to compare to.<br>
	 * WARNING: The types need to be comparable.
	 * @apiNote Added as part of IMPROVE202011121040<br>
	 * See related notes in {@link GraphProblemSolver}
	 * @since 0.16.0 (12.11.2020 11:06:28 AM)
	 * @return the minimal element in the matrix
	 */
	public C maxInMatrix() {
		C max = operationMaster.MIN_VALUE(); // Initializing max to minimal value possible to account for every case
		for (int i = 0; i < rowNum; i++) {
			C element = maxInRow(i);
			if (operationMaster.compare(element, max) > 0) {
				max = element;
			}
		}
		return max;
	}
	
	/**
	 * Finds and returns minimal value in matrix,
	 * by iterating over the rows and calling 
	 * minInRow on them and then using that as the
	 * element to compare to.<br>
	 * WARNING: The types need to be comparable.
	 * @apiNote After creating maxInMatrix,
	 * I figured I'll do the min as well
	 * @since 0.16.0 (12.11.2020 11:28:11 AM)
	 * @return the minimal element in the matrix
	 */
	public C minInMatrix() {
		C min = operationMaster.MAX_VALUE(); // Initializing max to minimal value possible to account for every case
		for (int i = 0; i < rowNum; i++) {
			C element = minInRow(i);
			if (operationMaster.compare(element, min) < 0) {
				min = element;
			}
		}
		return min;
	}


}
