/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import lykicheewi.burtefrahouse.exceptions.SizeMismatchException;

/** 
 *
 * @since 0.11.0 (02.08.2020 ~22:10)
 * @author Yaron Efrat
 */
public class CollectionsTools {

	/**
	 * Creates a map between the elements of two lists.
	 * @since 0.11.0 (02.08.2020 ~22:10)
	 * @param <K> The type of the keys of the resulting map
	 * @param <V> The type of the values of the resulting map
	 * @param keyList A list of the keys of the resulting map
	 * @param valueList A list of the values of the resulting map
	 * @return A map mapping from the values of keyList to those
	 * valueList
	 * @throws SizeMismatchException if the lists are of different sizes
	 */
	public static <K,V> HashMap<K,V> combineListsToMap(List<K> keyList,
			List<V> valueList) throws SizeMismatchException {
		if (keyList.size() != valueList.size()) {
			throw new SizeMismatchException("The key list and value list"
					+ "have different sizes");
		}
		HashMap<K,V> returnMap = new HashMap<K, V>(keyList.size());
		for (int i = 0; i < keyList.size(); i++) {
			returnMap.put(keyList.get(i), valueList.get(i));
		}
		return returnMap;
	}

	/**
	 * This method allows to search for a specific
	 * object in a list by using one of
	 * its fields' getter to get the field
	 * and then compare it to a given value.
	 * @apiNote Created for the Great Ugly 
	 * Hack in the matching tool, but it is
	 * actually a useful tool to have!
	 * @apiNote It returns the first object
	 * whose field matches the test field. <br>
	 * If ever a find nth object is necessary,
	 * we shall make it.
	 * @since 0.14.0 (09.11.2020 11:36:00 AM)
	 * @param <R> The field's type
	 * @param <T> The list contents' type
	 * @param list The list to search in
	 * @param fieldGetter The getter from the list. <br>
	 * Use form listContentTypeName::getterFunctionName
	 * @param fieldToTestAgainst
	 * @return The first entry from the list whose field
	 * matches the test field or null if entry satisfies
	 * the requirement. 
	 */
	public static <R, T> T findObjectInListByField(List<T> list, Function<T,R> fieldGetter, R fieldToTestAgainst) {
		for (T t : list) {
			if (fieldGetter.apply(t).equals(fieldToTestAgainst)) {
				return t;
			}
		}
		
		return null;
	}

}
