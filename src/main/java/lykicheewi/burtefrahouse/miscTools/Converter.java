/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.UncheckedCastPresent;

/**
 * This class is intended to convert from old (<O>) types
 * to new (<N>) types, and some other conversions (like decoding
 * base 64 strings)
 * @author Yaron Efrat
 * @implNote On 0.13.0 07.10.2020 11:11 made a decision to make a change
 * in order to support a method in ArrayTools that has a type parameter.<br>
 * This will have type parameters as well,
 * and subclasses that implement specific types in order to convert. <br>
 * With time, different conversions will be added as necessary, making my
 * library more and more flexible.
 * I have used my GenerivOperable interface for inspiration, which itself
 * was made thanks to:
 * Thanks to missingfaktor on https://stackoverflow.com/questions/8203197/java-generic-addition
 * and to Paulo Ebermann on https://stackoverflow.com/questions/6481083/generic-sparse-matrix-addition
 * for the idea.
 * CHANGE202010071111
 * @apiNote This class had two methods before the big change CHANGE202010071111.
 * One of them will remain the same no matter what (the static method for decoding
 * a base 64 string), while the other we will see
 * @apiNote was changed to abstract as part of CHANGE202010071111 to allow the 
 * abstract {@link #convert(Object)}.
 * @param <O> The type of the variable to convert
 * @param <N> The type to convert to
 */
public class Converter<O,N> {

	/**
	 * @apiNote THANKS TO https://rgomes.info/using-typetokens-to-retrieve-generic-parameters/
	 * @since 0.13.0 (07.10.2020 14:31)
	 */
	private final Class<?> typeO;

	/**
	 * @apiNote THANKS TO https://rgomes.info/using-typetokens-to-retrieve-generic-parameters/
	 * @since 0.13.0 (07.10.2020 14:31)
	 */
	private final Class<?> typeN;

	/**
	 * The simple name of the <O> type
	 * @apiNote Added those because I wanted to have
	 * the simple name available without
	 * additional code cluttering any of the
	 * other classes using this one.
	 * @since 0.13.0 (08.10.2020 11:00)
	 */
	private final String typeOName;

	/**
	 * The simple name of the <N> type
	 * @apiNote Added those because I wanted to have
	 * the simple name available without
	 * additional code cluttering any of the
	 * other classes using this one.
	 * @since 0.13.0 (08.10.2020 11:00)
	 */
	private final String typeNName;

	/**
	 * This actualConverter gets resolved
	 * during the construction of the converter,
	 * and it is made from the relevant subclass
	 * using the types as guides.<br>
	 * This is handled by {@link #resolveConverter()}
	 * @since 0.13.0 08.10.2020 10:18
	 * @deprecated is irrelevant
	 * as of 0.13.0 08.10.2020 11:59
	 */
	@Deprecated
	private Converter<?, ?> actualConverter;

	/**
	 * @apiNote This constructor was changed to represent the "classical
	 * solution" from https://rgomes.info/using-typetokens-to-retrieve-generic-parameters/,
	 * where you tell the class its types. 0.13.0 (07.10.2020 14:33)
	 * @implNote It was changed to protected as part of CHANGE202010071111
	 * when it was needed for the subclass {@link IntegerToStringConverter}
	 * so since 0.13.0  07.10.2020 11:23
	 */
	public Converter(final Class<?> typeO, final Class<?> typeN) {
		// No more hidden because we are screwed
		this.typeO = typeO;
		this.typeOName = this.typeO.getSimpleName();
		this.typeN = typeN;
		this.typeNName = this.typeN.getSimpleName();
		//		actualConverter = resolveConverter();
	}

	/**
	 * @apiNote This constructor represents the "sorcerer
	 * solution" from https://rgomes.info/using-typetokens-to-retrieve-generic-parameters/,
	 * where you use an anonymous class and the magical
	 * getType method which is taken from the great
	 * https://rgomes.info/using-typetokens-to-retrieve-generic-parameters/,
	 *  to find out the types. 0.13.0 (07.10.2020 14:33)
	 *  @apiNote Must be used with {} after in order to make an anonymous class
	 *  which is the only way it will work. <br>
	 *  See aforementioned link for details
	 * @implNote It was changed to protected as part of CHANGE202010071111
	 * when it was needed for the subclass {@link IntegerToStringConverter}
	 * so since 0.13.0  07.10.2020 11:23
	 * @since 0.13.0 (07.10.2020 2:33:55 PM)
	 */
	public Converter() {
		Type[] types = getTypes(this.getClass());
		this.typeO = (Class<?>) types[0];
		this.typeOName = this.typeO.getSimpleName();
		this.typeN = (Class<?>) types[1];
		this.typeNName = this.typeN.getSimpleName();
		//		actualConverter = resolveConverter();
	}

	/**
	 * @implNote So apparently, Boolean.hashCode returns
	 * 1231 for true and 1237 for false for whatever reason..
	 * However, if we just divide the hashcode of the input
	 * bool by that of false. <br>
	 * If bool is true, we get 1231/1237, which in int
	 * is 0.
	 * If bool is false, we get 1237/1237, which in int
	 * is 1. <br>
	 * doing 1 - ?? will give what we want
	 * @apiNote FINALLY! Something in here
	 * @since 0.11.0 (31.07.2020 15:00)
	 * @param bool
	 * @return 1 for a true bool and 0 for a false bool
	 */
	public static int boolToInt(boolean bool) {
		return 1 - (Boolean.hashCode(bool) / Boolean.hashCode(Boolean.FALSE));
	}

	/**
	 * Takes a String written in base64 and uses Java's
	 * Base64 utilities to decode it into Byte[] and from
	 * there, with the encoding into a regular String.
	 * @apiNote Inspired by the LimeSurveyCommunicator from
	 * the MMMT, where I had to decode a base64 string
	 * @since 0.12.0 (06.09.2020 10:39:41)
	 * @param base64String
	 * @param encoding
	 * @return
	 */
	public static String decodeBase64String(String base64String, String encoding) {
		/*
		 * Thanks
		 * Andrea and Ivar
		 * on
		 * https://stackoverflow.com/questions/469695/decode-base64-data-in-java
		 */
		byte[] decoded = Base64.getDecoder().decode(base64String);
		/*
		 * Thanks
		 * omerKudat and CoasterChris
		 * on
		 * https://stackoverflow.com/questions/1536054/how-to-convert-byte-array-to-string-and-vice-versa
		 */
		String decodedString = null;
		try {
			decodedString =  new String(decoded, encoding);
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} 
		return decodedString;
	}

	/**
	 * A method the takes an int and returns
	 * a byte array containing the int's digits,
	 * where digits[0] is the rightmost digit etc..
	 * @implNote I am using byte[] to save
	 * space, since the digits can only be
	 * 0 - 9, and byte is the smallest numerical
	 * primitive datatype (-128 - 127)<br>
	 * 
	 * @since 0.16.0 (30.11.2020 23:22:20)
	 * @param number
	 * @return
	 */
	public static byte[] getDigits(int number) {
		/*
		 * We'll take the absolute value,
		 * since we just want the digits.
		 * (0.16.0 01.12.2020 22:22)
		 */
		number = Math.abs(number);
		byte[] digits = new byte[String.valueOf(number).length()];
		int index = 0;
		while (number > 0) {
			digits[index] = (byte) (number % 10);
			index++;
			number /= 10;
		}
		return digits;
	}

	/**
	 * 
	 * @apiNote Created for the metabolomics
	 * thing
	 * 
	 * @since ?.?.? (13.12.2020 01:13:52)
	 * @param mappingsList
	 */
	public static HashMap<String, String> listOfMappingsToMap(List<String> mappingsList) {
		return listOfMappingsToMap(mappingsList, "=");
	}

	/**
	 * 
	 * @apiNote Created for the metabolomics
	 * thing
	 * @implNote Added the separator to allow
	 * more flexibility in the syntax of the
	 * list
	 * @since ?.?.? (14.12.2020 14:54:00)
	 * @param mappingsList
	 * @param separator
	 * @return
	 */
	public static HashMap<String, String> listOfMappingsToMap(List<String> mappingsList, String separator) {
		HashMap<String, String> map = new HashMap<String, String>();
		String[] separatedMapping = new String[2];
		for (String mapping : mappingsList) {
			/**
			 * Using the split method with the separator
			 * and a limit to enforce only 2 parts to 
			 * the mapping - key and value.
			 * If there are more than one separator only
			 * the first one is taken into account.
			 * 0.16.0 14.12.2020 15:00
			 */
			separatedMapping = mapping.split(separator, 2);
			map.put(separatedMapping[0], separatedMapping[1]);
		}
		return map;
	}

	/**
	 * @apiNote Decided to add it since it obviously
	 * will be implemented by all subclasses.<br>
	 * Also forced me to change Converter to Abstract.<br>
	 * Part of CHANGE202010071111<br>
	 * I was later (08.10.2020 after 10) forced
	 * to remove the abstract modifier in order not
	 * to be forced to implement convert when
	 * declaring the converter.<br>
	 * So I had to just put something silly or remove it.
	 * I shall put something silly, and the reason for it
	 * is the principle of it!
	 * I want all subclasses to show that they are
	 * overriding the superclass for a deep understanding
	 * that the conver method is CRUCIAL to the whole
	 * raison d'�tre of this.
	 * @since 0.13.0 (07.10.2020 11:26:15 AM)
	 * @param oldVariable The variable to convert
	 * @return A variable in the new type
	 * @implNote it looked practically impossible to get
	 * to work if this was as I intended it.
	 * I had to instead basically use the structure
	 * of the now deprecated reolveConverter
	 */
	@SuppressWarnings("unchecked")
	@UncheckedCastPresent(severity = "Unsafe")
	public N convert(O oldVariable) {
		//		return (N) oldVariable; // This is silly and dangerous on principle, and is supposed to be overridden
		//		return actualConverter.convert(typeO.cast(oldVariable));
		//		return ((IntegerToStringConverter) IntegerToStringConverter.CONVERTER).convert(typeO.cast(oldVariable));
		//		this.actualConverter.convert(oldVariable);
		//		return null;
		switch (typeOName) {
		case "Integer":
			switch (typeNName) {
			case "String":
				//				return IntegerToStringConverter.CONVERTER;
				return (N) new IntegerToStringConverter().convert((Integer) oldVariable);

			default:
				return null;
			}

		default:
			return null;
		}		
	}



	/**
	 * 
	 * Thanks to
	 * Yann-Ga�l Gu�h�neuc
	 * in
	 * https://stackoverflow.com/questions/1901164/get-type-of-a-generic-parameter-in-java-with-reflection
	 * for his answer and for providing the following link:
	 * https://rgomes.info/using-typetokens-to-retrieve-generic-parameters/.
	 * And thanks to the author of the article in the link,
	 * @implNote Will use nested switches because that's life... 
	 * @implSpec The return type is Converter<?,?> to allow ANY subclass to be returned
	 * @return A sub class of Converter that will have the relevant implementation
	 * @since 0.13.0 (07.10.2020 11:43:51 AM)
	 * @deprecated It was not feasible to go down that road 0.13.0 08.10.2020 11:59
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private Converter<?,?> resolveConverter() {
		switch (typeOName) {
		case "Integer":
			switch (typeNName) {
			case "String":
				//				return IntegerToStringConverter.CONVERTER;
				return new IntegerToStringConverter();

			default:
				return null;
			}

		default:
			return null;
		}		
	}

	/**
	 * 
	 * @apiNote ONLY WORKS FOR ANONYMOUS CLASSES
	 * @apiNote This is taken from
	 * https://rgomes.info/using-typetokens-to-retrieve-generic-parameters/,
	 * and shortened.
	 * THANK YOU THANK YOU THANK YOU~!
	 * @since 0.13.0 (07.10.2020 2:39:51 PM)
	 * @param klass
	 * @param pos
	 * @return
	 */
	private Type[] getTypes(final Class<?> klass) {
		// obtain anonymous, if any, class for 'this' instance
		final Type superclass = klass.getGenericSuperclass();

		// test if an anonymous class was employed during the call
		// Changed from !(superclass instanceof Class) because did not work 0.13.0 08.10.2020 09:58
		if ( (superclass == null) || (superclass instanceof Class) ) {
			throw new RuntimeException("This instance should belong to an anonymous class");
		}
		ParameterizedType paramClass = ((ParameterizedType) superclass);

		// obtain RTTI of all generic parameters
		return paramClass.getActualTypeArguments();
	}

	/**
	 *
	 * @since 0.13.0 (08.10.2020 10:15:45 AM)
	 * @return the typeO
	 */
	public Class<?> getTypeO() {
		return typeO;
	}

	/**
	 *
	 * @since 0.13.0 (08.10.2020 10:15:45 AM)
	 * @return the typeN
	 */
	public Class<?> getTypeN() {
		return typeN;
	}

	/**
	 *
	 * @since 0.13.0 (08.10.2020 11:03:33 AM)
	 * @return the typeOName - the simple name of the <O> type
	 */
	public String getTypeOName() {
		return typeOName;
	}

	/**
	 *
	 * @since 0.13.0 (08.10.2020 11:03:33 AM)
	 * @return the typeNName - the simple name of the <O> type
	 */
	public String getTypeNName() {
		return typeNName;
	}

	/**
	 * Kind of needed a way to access it without
	 * allowing it to be changed
	 * @since 0.13.0 (08.10.2020 11:46:36 AM)
	 * @return the actualConverter
	 * @deprecated is irrelevant
	 * as of 0.13.0 08.10.2020 11:59
	 */
	@Deprecated
	public Converter<?, ?> getActualConverter() {
		return actualConverter;
	}

}

/**
 * A subclass for converting from Integers (or ints which will automatically
 * be boxed to Integer) to Strings
 * @since 0.13.0 (07.10.2020 11:19:58 AM)
 * @author Yaron Efrat
 * @implNote The extends Converter<Integer, String> is irrelevant
 * as of 0.13.0 08.10.2020 11:59 (but I'll keep it for now anyways)
 */
class IntegerToStringConverter extends Converter<Integer, String> {
	//	public static final Converter<Integer, String> CONVERTER = new IntegerToStringConverter(); // Again, thanks to missingfaktor and Paulo Ebermann for this trick

	IntegerToStringConverter() {

	}

	/**
	 * A simple implementation of
	 * the convert method since String is
	 * easy to convert to.
	 * @since 0.13.0 (07.10.2020 11:30:08 AM)
	 * @param oldVariable
	 * @return
	 */
	//	@Override
	public String convert(Integer oldVariable) {
		return String.valueOf(oldVariable);
	}


}
