/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

/** 
 * @author Yaron Efrat
 * Thanks to missingfaktor on https://stackoverflow.com/questions/8203197/java-generic-addition
 * and to Paulo Ebermann on https://stackoverflow.com/questions/6481083/generic-sparse-matrix-addition
 * for the idea.
 * It's part of an effort to solve addition of Generic types
 */
public interface GenericOperable<T> {
	
	public T add(T generic1, T generic2);
	public T subtract(T generic1, T generic2);
	public T multiply(T generic1, T generic2);
	public T MIN_VALUE();
	public T MAX_VALUE();
	public int compare(T generic1, T generic2);
}

class runtimeInteger implements GenericOperable<Integer> {
	public static final GenericOperable<Integer> OPERATOR = new runtimeInteger();	
	
	private runtimeInteger() {
		
	}

	@Override
	public Integer add(Integer generic1, Integer generic2) {
		// TODO Auto-generated method stub
		return generic1 + generic2;
	}

	@Override
	public Integer subtract(Integer generic1, Integer generic2) {
		// TODO Auto-generated method stub
		return generic1 - generic2;
	}

	@Override
	public Integer multiply(Integer generic1, Integer generic2) {
		// TODO Auto-generated method stub
		return generic1 * generic2;
	}

	@Override
	public Integer MIN_VALUE() {
		// TODO Auto-generated method stub
		return Integer.MIN_VALUE;
	}

	@Override
	public Integer MAX_VALUE() {
		// TODO Auto-generated method stub
		return Integer.MAX_VALUE;
	}

	/**
	 * @return the value 0 if generic1 is equal to generic2 
	 * a value less than 0 if generic1 is numerically less
	 * than the generic2 and a value greater than 0 if 
	 * generic1 is numerically greater than generic2 (signedcomparison).
	 */
	@Override
	public int compare(Integer generic1, Integer generic2) {
		// TODO Auto-generated method stub
		return generic1.compareTo(generic2);
	}
	
}