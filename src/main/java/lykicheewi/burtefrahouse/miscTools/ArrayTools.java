/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import java.util.Arrays;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.OverloadingWrapperMethod;
import lykicheewi.burtefrahouse.annotations.methodAnnotations.UncheckedCastPresent;
import lykicheewi.burtefrahouse.exceptions.SizeMismatchException;

/** 
 *
 * @author Yaron Efrat
 */
public class ArrayTools {
	
	/**
	 * CREDIT FOR THE PREPEND AND APPEND METHODS
	 * GOES TO:
	 * polygenelubricants
	 * WHOSE ANSWER IN
	 * https://stackoverflow.com/questions/2925153/can-i-pass-an-array-as-arguments-to-a-method-with-variable-arguments-in-java
	 * CONTAINED THEM
	 */
	
	/**
	 * ADDED IN 0.8.0 10.4.2020 22:54
	 * all credit to polygenelubricants
	 * @param <T> The generic type of the array
	 * @param arr An array of type T
	 * @param lastElement
	 * @return
	 */
	public static <T> T[] append(T[] arr, T lastElement) {
	    final int N = arr.length;
	    arr = java.util.Arrays.copyOf(arr, N+1);
	    arr[N] = lastElement;
	    return arr;
	}

	/**
	 * This method searches for the first occurence of element
	 * in array and returns the index if it exists or -1 otherwise.
	 * @param <T> The generic type of the array
	 * @param array An array of type T
	 * @param element - An object of type T
	 * @return the index of the element in the array or -1
	 */
	public static <T> int findInArray(T element, T[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(element)) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * This method searches for the first occurence of element
	 * in array and returns the index if it exists or -1 otherwise.
	 * @param array - an int array
	 * @param element - an int
	 * @return the index of the element in the array or -1
	 */
	public static int findInArray(int element, int[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == element) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * This method searches for the first occurence of element
	 * in array and returns the index if it exists or -1 otherwise.
	 * @param array - an array of String
	 * @param element - A String to look for
	 * @return the index of the element in the array or -1
	 */
	public static int findInArray(String element, String[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(element)) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * This method searches for the first occurence of element
	 * in array and returns the index if it exists or -1 otherwise.
	 * @since 0.16.0 (09.12.2020 02:10:23)
	 * @param element
	 * @param array an array of char
	 * @return the index of the element in the array or -1
	 */
	public static int findInArray(char element, char[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i] == element) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * ADDED IN 8.4.2020, 10:29 to address a problem I had
	 * with assigning matches in the Hungarian Algorithm
	 * This method searches for the nth occurence of element
	 * in array and returns the index if it exists or -1 otherwise.
	 * @param <T> The generic type of the array
	 * @param array An array of type T
	 * @param element An object of type T
	 * @param n the number of the desired occurence
	 * @return the index of the element in the array or -1
	 */
	public static <T> int findNthInArray(T element, T[] array, int n) {
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(element)) {
				count++;
			}
			if (count >= n) {
				return i;
			}
		}
		return -1;		
	}

	/**
	 * ADDED IN 8.4.2020, 10:37 to address a problem I had
	 * with assigning matches in the Hungarian Algorithm
	 * This method searches for the nth occurence of element
	 * in array and returns the index if it exists or -1 otherwise. 
	 * @param array - an int array
	 * @param element - an int
	 * @param n the number of the desired occurence
	 * @return the index of the element in the array or -1
	 */
	public static int findNthInArray(int element, int[] array, int n) {
		int count = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == element) {
				count++;
			}
			if (count >= n) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * ADDED IN 0.8.0 13.4.2020 12:11 - 
	 * A simple method to run through the array to find the maximal value.
	 * I could (in the future) improve upon it by sorting the array first. 
	 * @param array the int array to be searched.
	 * @return the maximum value in the array
	 */
	public static int maxInArray(int[] array) {
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < array.length; i++) {
			if (array[i] > max) {
				max = array[i];
			}
		}
		return max;
	}
	
	/**
	 * CREDIT FOR THE PREPEND AND APPEND METHODS
	 * GOES TO:
	 * polygenelubricants
	 * WHOSE ANSWER IN
	 * https://stackoverflow.com/questions/2925153/can-i-pass-an-array-as-arguments-to-a-method-with-variable-arguments-in-java
	 * CONTAINED THEM
	 */
	
	/**
	 * 
	 * A simple method to run through the array to find the minimal value.
	 * I could (in the future) improve upon it by sorting the array first.
	 * @since 0.16.0 18.11.2020 10:59
	 * @apiNote Added to assist SelectCandidates 
	 * @param array the int array to be searched.
	 * @return the maximum value in the array
	 */
	public static int minInArray(int[] array) {
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}
		return min;
	}

	/**
	 * ADDED IN 0.8.0 10.4.2020 22:54
	 * all credit to polygenelubricants
	 * @param <T> The generic type of the array
	 * @param arr An array of type T
	 * @param firstElement
	 * @return
	 */
	public static <T> T[] prepend(T[] arr, T firstElement) {
	    final int N = arr.length;
	    arr = java.util.Arrays.copyOf(arr, N+1);
	    System.arraycopy(arr, 0, arr, 1, N);
	    arr[0] = firstElement;
	    return arr;
	}
	
	/**
	 * Prints the elements of an array separated
	 * by a delimiter
	 * @implNote Probably not the most efficient
	 * way to do it. Might want to improve it
	 * when I have time. <br>
	 * Anyone is welcome to improve upon it.
	 * @since 0.11.0 (10.08.2020 23:07:04)
	 * @param <T> The generic type of the array
	 * @param array An array of type T
	 * @param delimiter A delimiter to separate different elements in the array
	 * @param limitPerLine the number of elements before a line break or 0 for
	 * no line breaks. If delimiter is a newline character, then this parameter
	 * is ignored (set to -1)
	 */
	public static <T> void printArrayElements(T[] array, String delimiter, int limitPerLine) {
		String[] newLineStrings = {"\n", "\r", "\r\n"}; // Creating an array with new line strings to check against
		/*
		 * If the delimiter is found in the new line strings
		 * array (the result of findInArray is NOT -1)
		 * then set limitPerLine to -1
		 */
		if (findInArray(delimiter, newLineStrings) != -1) {
			limitPerLine = -1;
		}
		/*
		 * ON 0.16.0 22.11.2020 11:25,
		 * changed both count = 0 to
		 * count = 1 because the count
		 * is not correct otherwise
		 */
		int count = 1; // To count if we should break line
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i].toString() + delimiter);
			if ((limitPerLine > 0)) {
				if ((count < limitPerLine)) {
					count++;
				}
				else {
					System.out.println();
					count = 1;
				}
			}			
		}
	}
	
	/**
	 * Prints the elements of an array separated
	 * by a delimiter by calling {@link #printArrayElements(Object[], String, int)}
	 * @implNote Calls {@link #printArrayElements(Object[], String, int)} with
	 * limit Per Line based on a random width of 80 for the console (there is
	 * no simple way to get the width unfortunately), and then takes
	 * the size of 80/array[0].toString().length()
	 * @since 0.12.0 (11.08.2020 09:16)
	 * @param <T> The generic type of the array
	 * @param array An array of type T
	 * @param delimiter A delimiter to separate different elements in the array
	 */
	@OverloadingWrapperMethod
	public static <T> void printArrayElements(T[] array, String delimiter) {
		printArrayElements(array, delimiter, 80/array[0].toString().length());
	}
	
	/**
	 * Prints the elements of an array separated
	 * by a  default delimiter ; by calling {@link #printArrayElements(Object[], String, int)}
	 * @since 0.12.0 (11.08.2020 09:16)
	 * @param <T> The generic type of the array
	 * @param array An array of type T
	 * @param limitPerLine the number of elements before a line break or 0 for
	 * no line breaks. If delimiter is a newline character, then this parameter
	 * is ignored (set to -1)
	 */
	@OverloadingWrapperMethod
	public static <T> void printArrayElements(T[] array, int limitPerLine) {
		printArrayElements(array, ";", limitPerLine);
	}
	
	/**
	 * Prints the elements of an array separated
	 * by a default delimiter ; by calling {@link #printArrayElements(Object[], String, int)}
	 * @implNote Calls {@link #printArrayElements(Object[], String, int)} with
	 * limit Per Line based on a random width of 80 for the console (there is
	 * no simple way to get the width unfortunately), and then takes
	 * the size of 80/array[0].toString().length()
	 * @since 0.12.0 (11.08.2020 09:24)
	 * @param <T> The generic type of the array
	 * @param array An array of type T
	 */
	@OverloadingWrapperMethod
	public static <T> void printArrayElements(T[] array) {
		printArrayElements(array, ";", 80/array[0].toString().length());
	}

	/**
	 * The first in the convertArrayType methods.
	 * This one converts from String array to outputArray
	 * @implNote The beauty of streams allows me to make this complex
	 * action in one line of code
	 * @since 0.12.0 (17.08.2020 11:37:00)
	 * @param sourceArray
	 * @param outputArray The output array is being given to check it is of the
	 * same size as the source array. it is not actually used
	 * @return sourceArray converted to a new array of the target type
	 * @throws SizeMismatchException 
	 */
	public static int[] convertArrayType(String[] sourceArray, int[] outputArray) throws SizeMismatchException {
		if (sourceArray.length != outputArray.length) {
			throw new SizeMismatchException("The source array and output array are not the same size");
		}
		return Arrays.asList(sourceArray).stream().mapToInt(element -> Integer.parseInt(element)).toArray();
	}
	
	/**
	 * The second in the convertArrayType methods.
	 * This one is general, receiving type parameters
	 * and trying to cast the elements from
	 * old type to new if possible
	 * @implNote The beauty of streams allows me to make this complex
	 * action in one line of code
	 * @apiNote This method contains an unsafe conversion. Please
	 * try to use it when knowing that the cast is possible
	 * @since 0.12.0 (17.08.2020 12:24:14)
	 * @param <O> The type of the source array
	 * @param <N> The target type for the array to be returned
	 * @param sourceArray
	 * @param outputArray The output array is being given to check it is of the
	 * same size as the source array. it is not actually used
	 * @return
	 * @throws SizeMismatchException
	 */
	@SuppressWarnings("unchecked")
	@UncheckedCastPresent(severity = "Unsafe")
	public static <O, N> N[] convertArrayType(O[] sourceArray, int[] outputArray) throws SizeMismatchException {
		if (sourceArray.length != outputArray.length) {
			throw new SizeMismatchException("The source array and output array are not the same size");
		}
		return (N[]) Arrays.asList(sourceArray).stream().map(element -> (N)(element)).toArray();
	}

	/**
	 * Receives an array of type A, and fills it with
	 * A's representation of number from startingNumber
	 * until the end of the array
	 * @apiNote Created to help with the default scores for
	 * the MMMT AdminGUI
	 * @since 0.13.0 (07.10.2020 11:00:01 AM)
	 * @param arrayToFill
	 * @param startingNumber
	 * @param <A> The array objects' type
	 * @implNote On 08.10.2020 12:10 I discovered that if I try to do
	 * new Converter<Integer, A> it just
	 * @implNote 0.13.0 08.10.2020 12:12 I decided to have a Converter
	 * outside of the loop instead of new ones all the time inside it
	 * (why the hell did I do it in the first place?)
	 * @apiNote In order to stop wasting time, I am forced to face defeat and
	 * ask as a parameter for the class.... I am sorry...
	 * Maybe in the future I will be able to solve this, but unfortunately
	 * at the moment I will introduce Class<?> arrayClass... as a parameter
	 * @param arrayClass Was forced to add it because I couldn't solve it
	 * after trying for a long time
	 */
	public static <A> void fillWithAscendingNumbers(A[] arrayToFill, int startingNumber, Class<A> arrayClass) {
		// arrayToFill.getClass().getComponentType().getSimpleName()
//		Annotation arrayType = arrayToFill.getClass().getComponentType().getAnnotations()[0];
//		Class<?> arrayClass = arrayToFill.getClass().getComponentType();
//		TypeVariable<?> tv = arrayClass.getTypeParameters()[0];
//		Converter<Integer, lykicheewi.burtefrahouse.miscTools.arrayClass> converter = new Converter<Integer, arrayClass>(){};
//		arrayClass.cast(null);
		
//		Converter<Integer, ?> converter = new Converter<Integer, arrayClass>(){};
		/*
		 * And so, 0.13.0 08.10.2020 13:17,
		 * I have been defeated, and forced to to
		 * the terrible thing:
		 * Actually sending the class<?> objects
		 * for the Integer and for the arrayclass.
		 * UPDATE 13:21:
		 * It is working as expected.
		 * So why do I feel defeated?
		 * SHAMEOFDEFEAT202010081321
		 */
		Converter<Integer, A> converter = new Converter<Integer, A>(Integer.class, arrayClass){};
		for (int i = 0; i < arrayToFill.length; i++) {
			arrayToFill[i] = converter.convert(startingNumber + i); // See the Converter class to see the magic in action
		}
		
	}

	/**
	 * 
	 * @apiNote Created to assist with Lychee's
	 * thing. I had a byte array and wanted
	 * to use stream.<br>
	 * Apparently, converting to list with
	 * Arrays.tolist does not work properly
	 * on primitive types (it does a list of arrays).<br>
	 * So I needed to box them up.
	 * @apiNote Will probably overload with other types
	 * if necessary.
	 * @since 0.16.0 (01.12.2020 22:05:45)
	 * @param array
	 * @return A boxed version of the input array
	 */
	public static Byte[] boxArray(byte[] array) {
		Byte[] boxedArray = new Byte[array.length];
		for (int i = 0; i < array.length; i++) {
			boxedArray[i] = Byte.valueOf(array[i]);
		}
		return boxedArray;
	}

//	/**
//	 * NOT IMPLEMENTED FOR NOW
//	 * This method actually does binary search
//	 * @param <T>
//	 * @param element
//	 * @param array
//	 * @param low
//	 * @param high
//	 * @return
//	 */
//	private static <T> int searchArray(T element, T[] array, int low, int high) {
//		return -1;
//		
//	}

}
