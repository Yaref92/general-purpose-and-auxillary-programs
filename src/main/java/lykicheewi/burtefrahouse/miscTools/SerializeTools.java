/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.JPanel;

import lykicheewi.burtefrahouse.fileTools.FileInputKit;
import lykicheewi.burtefrahouse.fileTools.FileOutputKit;

/** 
 * Finally found out what IS serialization (basically saving an object),
 * and realized it can be VERY useful for things (e.g. QuestionPack from QuestionnaireTools) 
 * @since 0.10.0 (20.05.2020 ~21:50)
 * @author Yaron Efrat
 */
public class SerializeTools {

	/**
	 * Takes a Serializable Object, serializes it and puts the data in a file
	 * @since 0.10.0 (20.05.2020 ~22:07)
	 * @param serializableObj
	 * @param fileName
	 * @throws IOException
	 */
	public static void serializeToFile(Serializable serializableObj, String fileName) throws IOException {
		// Serialization   
		//Saving of object in a file 
		FileOutputStream fileOutStream = new FileOutputStream(fileName); 
		ObjectOutputStream objOutStream = new ObjectOutputStream(fileOutStream); 

		// Method for serialization of object 
		objOutStream.writeObject(serializableObj); 

		objOutStream.close(); 
		fileOutStream.close();   
	}

	/**
	 * Takes a Serializable Object, serializes it and puts the data in a file
	 * @since 0.10.0 (20.05.2020 ~22:18)
	 * @param serializableObj
	 * @throws IOException
	 */
	public static void serializeToFile(Serializable serializableObj) throws IOException {
		// Serialization   
		//Saving of object in a file 
		FileOutputKit outputKit = new FileOutputKit();
		serializeToFile(serializableObj, outputKit.getFileName());
	}

	/**
	 * Deserializes a Serializable Object from a file containing serialization data
	 * @since 0.10.0 (20.05.2020 22:31)
	 * @param fileName
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static Serializable deserializeFromFile(String fileName) throws ClassNotFoundException, IOException {
		// Serialization   
		//Saving of object in a file 
		FileInputStream fileInStream = new FileInputStream(fileName);
		ObjectInputStream objInStream = new ObjectInputStream(fileInStream); 

		// Method for serialization of object 
		Serializable deserializedObj = (Serializable) objInStream.readObject(); 

		objInStream.close(); 
		fileInStream.close();

		return deserializedObj;
	}

	/**
	 * Deserializes a Serializable Object from a file containing serialization data
	 * @since 0.10.0 (20.05.2020 22:33)
	 * @return
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static Serializable deserializeFromFile() throws ClassNotFoundException, IOException {
		// Serialization   
		FileInputKit inputKit = new FileInputKit((JPanel) null);

		return deserializeFromFile(inputKit.getFileName());
	}

}
