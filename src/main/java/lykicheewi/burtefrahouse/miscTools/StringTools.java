/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created because it was time to unbloat LimeSurveyCommunicator
 * and it makes way more sense to put it here anyways
 * @since 0.13.0 (01.10.2020 12:16:46 PM)
 * @author Yaron Efrat
 */
public class StringTools {
	
	
	/**
	 * Takes a substring of wholeString
	 * that starts at the index of the first occurrence
	 * of beginString AFTER beginSearchIndex
	 * and ends at the index of the first occurrence
	 * of endString AFTER endSearchIndex
	 * @apiNote originally from MMMT 1.3.0 (31.07.2020 14:20)
	 * @since 0.13.0 (01.10.2020 12:16)
	 * @implNote Added a try catch fpr the return clause
	 * @param wholeString The string from which the substring
	 * should be extracted
	 * @param beginString
	 * @param beginSearchIndex
	 * @param endString
	 * @param endSearchIndex
	 * @return the substring
	 */
	public static String extractRelevantPart(String wholeString, 
			String beginString, int beginSearchIndex, 
			String endString, int endSearchIndex) {
		/*
		 *  Added because it has again thrown a StringIndexOutOfBoundsException
		 *  @since 1.5.2 22.09.2020 10:18
		 */
		try {
			return wholeString.substring(
					wholeString.indexOf(beginString, beginSearchIndex), 
					wholeString.indexOf(endString, endSearchIndex)); // Was a pain in the ass
		} catch (StringIndexOutOfBoundsException e) {
			System.err.println("The extraction from \"" + wholeString
					+ "\" has failed.\n"
					+ "The used parameters:\n"
					+ "beginString: " + beginString + "\n"
					+ "beginSearchIndex: " + beginSearchIndex + "\n"
					+ "endString: " + endString + "\n"
					+ "endSearchIndex: " + endSearchIndex + "\n");
			e.printStackTrace();
			return "";
		}
	}
	
	/**
	 * Removes the stringToRemoveFromBeginning from the left of
	 * stringToUnwrap and trims by abs(relativePositionFromEnd)
	 * from the right
	 * @apiNote Another method to facilitate REHAUL202010011048... <br>
	 * @apiNote originally from MMMT 1.6.0 (01.10.2020 11:19:15 AM)
	 * @since 0.13.0 (01.10.2020 12:22)
	 * @param stringToUnwrap
	 * @param stringToRemoveFromBeginning
	 * @param relativePositionFromEnd
	 * @return
	 */
	public static String unwrap(String stringToUnwrap, String stringToRemoveFromBeginning, int relativePositionFromEnd) {
		return stringToUnwrap.substring(stringToRemoveFromBeginning.length(), stringToUnwrap.length() + relativePositionFromEnd);
	}
	
	/**
	 * Just wrap stringToWrap with leftWrapper from
	 * the left and rightWrapper from the right
	 * @apiNote It is getting crazy here.
	 * @apiNote originally from MMMT 1.6.0 (01.10.2020 11:59:10 AM)
	 * @since 0.13.0 (01.10.2020 12:20)
	 * @param stringToWrap
	 * @param leftWrapper
	 * @param rightWrapper
	 * @return
	 */
	public static String wrap(String stringToWrap, String leftWrapper, String rightWrapper) {
		return leftWrapper.concat(stringToWrap).concat(rightWrapper);
	}
	
	/**
	 * Takes a String of substrings separated by a delimiter,
	 * and puts those substrings into an ArrayList.
	 * @apiNote Added to help finish the implementation of
	 * the matching tool's multiple choice 
	 * questions.
	 * @since 0.14.0 (10.11.2020 10:35:54 AM)
	 * @param stringToBreak
	 * @param delimiter
	 * @return
	 */
	public static ArrayList<String> breakStringIntoListOfTokens(String stringToBreak, String delimiter) {
		ArrayList<String> tokenList = new ArrayList<String>();
		StringTokenizer tokeny = new StringTokenizer(stringToBreak, delimiter, false);
		while(tokeny.hasMoreTokens()) {
			tokenList.add(tokeny.nextToken());
		}
		return tokenList;
	}

	/**
	 * Returns the reversed string
	 * @since 0.16.0 (24.11.2020 9:42:16 AM)
	 * @param string
	 * @return
	 */
	public static String reverse(String string) {
		String reversedString = "";
		char[] chars = string.toCharArray();
		for (int i = chars.length - 1; i >= 0 ; i--) {
			reversedString = reversedString.concat(String.valueOf(chars[i]));
		}
		return reversedString;
	}

}
