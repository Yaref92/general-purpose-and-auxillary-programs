/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.InputSource;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.HelperMethod;
import lykicheewi.burtefrahouse.annotations.methodAnnotations.OverloadingWrapperMethod;
import lykicheewi.burtefrahouse.fileTools.XMLInputKit;
import lykicheewi.burtefrahouse.miscTools.XMLAuxTools.ExpandedElement;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jdom2.Attribute;
import org.jdom2.Document;

/** 
 *
 * @author Yaron Efrat
 */
public class XMLAuxTools {

	/**
	 * 
	 */
	public XMLAuxTools() {
		// TODO Auto-generated constructor stub
	}
	
	public static class ExpandedElement {
		
		private Element theElement;
		private String name;
		private HashMap<String, String> attributeMap;
		private String data;
		private Namespace namespace;
		
		
		/**
		 * 
		 * @param element - The element we want to expand upon
		 * @param mapAttributes - A bool representing whether or not we should construct the Attribute map at this stage
		 * @param getNamespace - A bool representing whether we want to get the Namespace
		 */
		public ExpandedElement(Element element, boolean mapAttributes, boolean getNamespace) {
			theElement = element;
			name = element.getName();
			data = element.getText();
			if (getNamespace) {
				namespace = element.getNamespace();
			}
			else {
				namespace = null;
			}
			if (mapAttributes) {
				mapAttributes();
			}
			else {
				attributeMap = null;
			}
			
		}
		
		/**
		 * Overloaded constructor of the first one.
		 * Sends false to getNamespace.
		 * @param element
		 * @param mapAttributes - whether to call the main constructor with true of false here.
		 */
		public ExpandedElement(Element element, boolean mapAttributes) {
			this(element, mapAttributes, false);
		}
		
		/**
		 * Overloaded constructor of the first one.
		 * Sends true to mapAttributes
		 * Sends false to getNamespace.
		 */
		public ExpandedElement(Element element) {
			this(element, true, false);
		}


		private void mapAttributes() {
			attributeMap = new HashMap<String, String>();
			for (Attribute attr : theElement.getAttributes()) {
				attributeMap.put(attr.getName(), attr.getValue());
			}
			
		}

		/**
		 * @return the theElement
		 */
		public Element getTheElement() {
			return theElement;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the attributeMap
		 */
		public HashMap<String, String> getAttributeMap() {
			return attributeMap;
		}

		/**
		 * @return the data
		 */
		public String getData() {
			return data;
		}

		/**
		 * @return the namespace
		 */
		public Namespace getNamespace() {
			return namespace;
		}
	}
	
	/**
	 * Adds the attributes in the list to the parent
	 * @param element The element to which attributes
	 * are to be added
	 * @param attributes The attributes to be added
	 * @apiNote Adapted from the non static version in XMLOutputKit
	 * @since 0.12.0 (11.08.2020 13:13)
	 */
	public static void addAttributes(Element element, ArrayList<Attribute> attributes) {
		element.setAttributes(attributes);
	}
	/**
	 * Adds the elements in the list as children of the parent
	 * @implNote Uses stream API's forEach to make it
	 * short and succinct
	 * @param parent The element to which children elements
	 * are to be added
	 * @param children The elements to be added
	 * @apiNote Adapted from the non static version in XMLOutputKit
	 * @since 0.12.0 (11.08.2020 13:13)
	 */
	public static void addChildren(Element parent, ArrayList<Element> children) {
		children.stream().forEach(child -> parent.addContent(child));
	}
	/**
	 * Adds the text to the parent
	 * @param element The element to which text
	 * is to be added
	 * @param text the text to be added
	 * @apiNote Adapted from the non static version in XMLOutputKit
	 * @since 0.12.0 (11.08.2020 13:13)
	 */
	public static void addText(Element element, String text) {
		element.addContent(text);
	}
	/**
	 * 
	 * @since 0.11.0 (30.07.2020 12:00)
	 * @param xmlString
	 * @return
	 */
	public static Document convertStringToXML(String xmlString) {
		/*
	     * Thanks to
	     * 
	     * on
	     * 
	     * for the answer regarding
	     * string to XML conversion
	     */
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
	    DocumentBuilder builder;  
	    Document document = null;
	    try {  
	        builder = factory.newDocumentBuilder();  
	        document = (Document) builder.parse(new InputSource(new StringReader(xmlString)));  
	    } catch (Exception e) {  
	        e.printStackTrace();  
	    } 
	    return document;
	}
	/**
	 * A simple helper method to return the number of children of an element
	 * @since 0.11.0 (15.07.2020 23:23)
	 * @param parent
	 * @return
	 */
	public static int countChildren(Element parent) {
		return parent.getChildren().size();
	}
	/**
	 * @since 0.11.0 (15.07.2020 23:19)
	 * @param XMLKit
	 * @param name
	 * @return
	 */
	public static int countElementsByName(XMLInputKit XMLKit, String name) {
		HashMap<Element, ArrayList<ExpandedElement>> elementMap = XMLKit.getElementMap();
		// System.out.print(elementMap);
		int count = 0;
		for (Element e : elementMap.keySet()) {
			if (e.getName().equals(name)) {
				count++;
			}
		}
		
		return count;
	}
	
	/**
	 * 
	 * @implNote Uses stream functions. Took a while to get it working
	 * @since 0.11.0 (01.08.2020 23:03)
	 * @param XMLKit
	 * @param string
	 * @return
	 */
	public static int countOcurrencesByString(XMLInputKit XMLKit, String string) {
		HashMap<Element, ArrayList<ExpandedElement>> elementMap = XMLKit.getElementMap();
		// System.out.print(elementMap);
		int count = 0;
		for (Element e : elementMap.keySet()) {
			if ((e.getAttributes().stream().map(attribute -> isExisting(attribute, string)).anyMatch(bool -> (bool == true))) ||
					(e.getText().contains(string))) {
				count++;
			}
		}
		
		return count;
	}
	
	/**
	 * Receives a list of strings representing the names of elements and returns a list of Elements
	 * with those names and no content
	 * @since 0.12.0 (11.08.2020 12:00:53)
	 * @param elementNames A list of Strings representing the element names
	 * @return A list of Element objects with the names extracted from elementNames
	 */
	@HelperMethod
	public static ArrayList<Element> generateElementListFromStringList(ArrayList<String> elementNames) {
		ArrayList<Element> elementList = new ArrayList<Element>(elementNames.size());
		for(int i = 0; i < elementNames.size(); i++) {
			elementList.add(new Element(elementNames.get(i)));
		}
		return elementList;
	}
	/**
	 * 
	 * @param attribute
	 * @param string
	 * @return
	 * @since 0.11.0 (01/02-08-2020)
	 */
	private static boolean isExisting(Attribute attribute, String string) {
		return attribute.getValue().contains(string);
		
	}
	/**
	 * @since 0.11.0 (17.07.2020 00:17)
	 * @param httpResponseStream The InputStream of the response from the server
	 * @return
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public static Document parseHttpRequestToXML(InputStream httpResponseStream) throws JDOMException, IOException {
		return new SAXBuilder().build(httpResponseStream);
	}
	
	/**
	 * Decided I would need a queue to keep track of when I
	 * run out of elements.
	 * @implNote On 27.07.2020 I changed the values 
	 * of elementMap from 
	 * Expanded Element to an ArrayList of them - CHANGE202007271219
	 * @apiNote Adapted from the non static version in XMLInputKit
	 * once I realized I'll need for both and it's a waste to duplicate
	 * code. So I will deprecate the one in XMLInputKit and use this one
	 * in both the input and output kit
	 * @since 0.12.0 (11.08.2020 13:38)
	 */
	@OverloadingWrapperMethod
	public static HashMap<Element, ArrayList<ExpandedElement>> elementMapper(Element rootElement) {
		/*
		 * First of all, using a JOptionPane, the user is asked whether the
		 * attribute maps should be created.
		 * Then another one for whether the namespaces be pulled.
		 * The results are recorded in two booleans using a comparison to the JOptionPane
		 * Yes option value.
		 */
		boolean mapAttributes = (JOptionPane.YES_OPTION == 
				JOptionPane.showOptionDialog(null,
						"Should the attribute maps for the XML elements be created?", "Attribute maps",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION));
		boolean getNamespace = (JOptionPane.YES_OPTION == 
				JOptionPane.showOptionDialog(null,
						"Should the namespaces for the XML elements be pulled?", "Namespaces",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION));
		return elementMapper(rootElement, mapAttributes, getNamespace);
	}
	/**
	 * Decided I would need a queue to keep track of when I
	 * run out of elements.
	 * @implNote On 27.07.2020 I changed the values 
	 * of elementMap from 
     * Expanded Element to an ArrayList of them - CHANGE202007271219
     * @apiNote Adapted from the non static version in XMLInputKit
     * once I realized I'll need for both and it's a waste to duplicate
     * code. So I will deprecate the one in XMLInputKit and use this one
     * in both the input and output kit
	 * @since 0.12.0 (11.08.2020 13:44)
	 * @apiNote Added because I got tired of clicking yes yes all the time
	 */
	public static HashMap<Element, ArrayList<ExpandedElement>> elementMapper(Element rootElement,
			boolean shouldAttributes, boolean shouldNamespace) {
		/*
		 * A Queue to help keep track of whether there are still
		 * Elements to add to the mapping. Kinda like a BFS.
		 */
		Queue<Element> elementQueue = new LinkedList<Element>();
		/*
		 * We first add the root. We also define a currentElement for each 
		 * iteration of the While loop, and we initialize the Map
		 */		
		elementQueue.add(rootElement);
		Element currentElement;
		HashMap<Element, ArrayList<ExpandedElement>> elementMap = 
				new HashMap<Element, ArrayList<XMLAuxTools.ExpandedElement>>();
		/*
		 * The while continues while the queue is not empty.
		 * it polls out the Element and then foreach through the children and add them.
		 */
		while(!elementQueue.isEmpty()) {
			currentElement = elementQueue.poll();
			elementMap.put(currentElement, new ArrayList<XMLAuxTools.ExpandedElement>()); // Added for CHANGE202007271219
			ArrayList<XMLAuxTools.ExpandedElement> childrenList = elementMap.get(currentElement); // Added for CHANGE202007271219
			for (Element childElement : currentElement.getChildren()) {
				/*
				 * updated for CHANGE202007271219
				 */
				childrenList.add(new ExpandedElement(childElement, shouldAttributes, shouldNamespace));
				elementQueue.add(childElement);
			}
		}
		return elementMap;
	}

}
