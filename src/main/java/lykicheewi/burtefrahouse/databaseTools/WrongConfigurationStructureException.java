/**
 * 
 */
package lykicheewi.burtefrahouse.databaseTools;

/** 
 *
 * @author Yaron Efrat
 */
public class WrongConfigurationStructureException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4189323411135073884L;

	/**
	 * 
	 */
	public WrongConfigurationStructureException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public WrongConfigurationStructureException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public WrongConfigurationStructureException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public WrongConfigurationStructureException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public WrongConfigurationStructureException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
