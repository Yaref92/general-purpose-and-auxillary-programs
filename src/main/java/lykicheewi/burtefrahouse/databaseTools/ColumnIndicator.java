/**
 * 
 */
package lykicheewi.burtefrahouse.databaseTools;

/** 
 * AN enumaration for the creation of columns
 * in mysql tables. helps to only use legal indicators.
 * First come (some common) datatypes (At the moment varchar, single char and int)
 * Then some indicators (at the moment not null, auto_increment and primary key)
 * @since 0.10.0 (18.05.2020 21:32)
 * @author Yaron Efrat
 */
public enum ColumnIndicator {
	/**
	 * For non binary strings. max length of 511 - datatype
	 * Added to accommodate MMMT
	 * @since 0.13.0 (30.09.2020 13:51)
	 */
	LONGVARCHAR("VARCHAR(511)"),
	/**
	 * For non binary strings. max length of 255 - datatype
	 */
	VARCHAR("VARCHAR(255)"),
    /**
     * A single non binary character for the MMMT question answers - datatype
     */
	CHAR1("CHAR(1)"), 
    /**
     *   A standard int - datatype
     */
	INT("INT"),
	/**
	 * A non binary string containing up to 3 characters. Meant for Yes/No input - datatype
	 */
	YESNO("VARCHAR(3)"),
	/**
	 * A non binary string specifically for use in the MMMT for the user type (mentor or mentee) - datatype
	 */
	MENT("CHAR(6)"),
	/**
	 * Tells SQL not to accept null in this column - indicator
	 */
	NONULL("NOT NULL"),
	/**
	 * Tells SQL to automatically increment - indicator
	 */
	AUTOINC("AUTO_INCREMENT"),
	/**
	 * Tells SQL to include in the table's primary key - indicator
	 */
	PRIMKEY("PRIMARY KEY")
	;
	
	private final String indicatorString;
	
	private ColumnIndicator(String indicator) {
		this.indicatorString = indicator;
	}
	
	/**
	 * @return the indicator string
	 */
	public String getIndicatorString() {
		return indicatorString;
	}

}
