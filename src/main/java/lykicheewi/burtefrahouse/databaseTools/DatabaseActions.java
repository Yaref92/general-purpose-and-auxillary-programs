/**
 * 
 */
package lykicheewi.burtefrahouse.databaseTools;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.HelperMethod;
import lykicheewi.burtefrahouse.annotations.methodAnnotations.OverloadingWrapperMethod;
import lykicheewi.burtefrahouse.exceptions.SizeMismatchException;
import lykicheewi.burtefrahouse.fileTools.FileOutputKit;
import lykicheewi.burtefrahouse.miscTools.SerializeTools;
import lykicheewi.burtefrahouse.questionnaireTools.DialogKit;

/** 
 * @since 0.10.0
 * @author Yaron Efrat
 */
public class DatabaseActions {

	/**
	 * 
	 */
	public DatabaseActions() {
		// Auto-generated constructor stub
	}

	/**
	 * Adds the columns with the names from the
	 * columns array.
	 * @since 0.10.0 (6.5.2020 18:23)
	 * @param conn Connection object to communicate with the database
	 * @param table The table's name (String)
	 * @param columns The columns' names (String array)
	 * @throws SQLException
	 */
	public static void addColumns(Connection conn, String table, String[] columns) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		for (int i = 0; i < columns.length; i++) {
			SQLStatement.executeUpdate("ALTER TABLE " + table + 
					"ADD COLUMN"
					+ columns[i] + ";");
		}		
	}

	/**
	 * Adds the columns with the names from the
	 * columns array. <br>
	 * Specifies the types and
	 * the modifiers from the columnsTypes
	 * array.
	 * @since 0.10.0 (18.5.2020 ~21:15)
	 * @apiNote Allows more flexibility - you can choose the types of the columns
	 * and which of them is the primary key
	 * @param conn Connection object to communicate with the database
	 * @param table The table's name (String)
	 * @param columns The columns' names (String array)
	 * @param columnsTypes the desired data types for the columns or null for default.
	 * Extra parameters (AUTO_INCREMENT; NOT NULL; PRIMARY KEY if you want this to be in
	 * the primary key) can be added delimited by whitespace. <br>
	 * IT IS RECOMMENDED TO USE THE ColumIndicator ENUM for this array. Remember you can use
	 * several in succession as long as you separate by whitespace. <br>
	 * FIRST USE TYPE THOUGH
	 * @throws SQLException
	 */
	public static void addColumns(Connection conn, String table, String[] columns, String[] columnsTypes) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		for (int i = 0; i < columns.length; i++) {
			/*
			 * First we create a String for the query to allow for flexibility.
			 * That way, if columnsTypes[i] is not null, we also include it
			 */
			String query = "ALTER TABLE " + table + 
					"\nADD COLUMN "
					+ columns[i];
			if (columnsTypes[i] != null) {
				query = query.concat(" " + columnsTypes[i]);
			}
			query = query.concat(";");
			SQLStatement.executeUpdate(query);
		}		

	}

	/**
	 * Adds a single row to the table indicated by the table
	 * paramaeter such that the values in the values array
	 * go into the columns in the column array. <br>
	 * The handling of what happens if ot all columns are
	 * included is made by MySQL. 
	 * @since 0.10.0 (6.5.2020 9:48)
	 * @param conn Connection object to communicate with the database
	 * @param table The table's name (String)
	 * @param columns The columns' names (String array)
	 * @param values The values to be inserted into the columns
	 * @throws SQLException 
	 * @throws SizeMismatchException If the columns and values arrays are not the same size
	 */
	public static void addRow(Connection conn, String table, String[] columns, String[] values) throws SQLException, SizeMismatchException {
		if (columns.length == values.length) {
			Statement SQLStatement = conn.createStatement();
			String query = null;
			if (columns.length > 1) { // The if else added on 29.5.2020 21:08 because didn't handle single String arrays
				query = "INSERT INTO "
						+ table + "("
						+ String.join(",", columns) + ")\n"
						+ "VALUES ('"
						+ String.join("','", values) + "');";
			}
			else {
				query = "INSERT INTO "
						+ table + "("
						+ columns[0] + ")\n"
						+ "VALUES ('"
						+ values[0] + "');";
			}
			SQLStatement.executeUpdate(query);
		}
		else {
			throw new SizeMismatchException("The number of values does not match the number of columns");
		}

	}
	// TODO Clean up the three following methods so that only one of them is long(for 0.11.0)
	/**
	 * This method creates a new database.<br>
	 * It first asks for the details of a user which can create a
	 * database.<br> It will then ask for the name of the new database.<br>
	 * Then, it will offer to give privileges on the database
	 * for a user.
	 * @return The name of the file created by writeDbConfig
	 * which is called by this method. database_connection_config.txt
	 * @since 0.10.0 (24.4.2020 11:53)
	 * @implNote returns String since 4.5.2020 17:29
	 * @apiNote Migrated to DatabaseActions on 0.10.0 (27.4.2020 10:05)
	 * @throws SQLException
	 */
	@SuppressWarnings("unused")
	public static String createDatabase() throws SQLException {
		DialogKit.showPlainMessage(null, "Privileged user", "First, provide the details of a user that can create a database");
		SQLUser currentUser = new SQLUser();
		String user = currentUser.getUserName();
		String pass = currentUser.getPassword();
		String database = null;

		database = DialogKit.executeFreeTextDialog(null, "database name", "Enter the name of the database you want to create");
		/*
		 * Thanks Jeremy Stanley in https://stackoverflow.com/questions/717436/create-mysql-database-from-java
		 */
		Connection conn = DriverManager.getConnection
				("jdbc:mysql://127.0.0.1", user, pass); // Changed from localhost because it might be translated tonip6
		Statement SQLStatement = conn.createStatement();
		int Result=SQLStatement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + database + ";"); // Added  [IF NOT EXISTS] so that if it exists it does not throw you out.
		if (DialogKit.executeYesNoQuestion(null, "Database privileges", "Would you like to give all privileges for the"
				+ " created database for a specific user?", JOptionPane.YES_OPTION) == JOptionPane.YES_OPTION) {
			currentUser = DatabaseActions.giveDatabasePrivileges(currentUser, database, conn);
		}
		/*
		 * THE next section was added 0.10.0, 04.05.2020 17:17
		 * to create an SQL config file to be used by the connector to connect
		 * to this db in the future
		 */		 
		try {
			writeDbConfig(database, currentUser.getUserName(), currentUser.getPassword());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return database + "_connection_config.txt";
	}

	/**
	 * This method uses a previously serialized SQLUser object for
	 * the privileged user and receives the database name as a
	 * parameter. <br> it will offer to give privileges on the database
	 * for a user.
	 * @since 0.10.0 (24.05.2020 11:26
	 * @param database The database's name (String)
	 * @return The name of the file created by writeDbConfig
	 * which is called by this method. database_connection_config.txt
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static String createDatabase(String database) throws SQLException, ClassNotFoundException, IOException {
		SQLUser currentUser = (SQLUser) SerializeTools.deserializeFromFile("privilegedSQLUser.ser");
		String user = currentUser.getUserName();
		String pass = currentUser.getPassword();
		/*
		 * Thanks Jeremy Stanley in https://stackoverflow.com/questions/717436/create-mysql-database-from-java
		 */
		Connection conn = DriverManager.getConnection
				("jdbc:mysql://127.0.0.1", user, pass); // Changed from localhost because it might be translated tonip6
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + database + ";"); // Added  [IF NOT EXISTS] so that if it exists it does not throw you out.
		if (DialogKit.executeYesNoQuestion(null, "Database privileges", "Would you like to give all privileges for the"
				+ " created database for a specific user?", JOptionPane.YES_OPTION) == JOptionPane.YES_OPTION) {
			currentUser = DatabaseActions.giveDatabasePrivileges(currentUser, database, conn);
			currentUser.serializeUser();
		}
		/*
		 * THE next section was added 0.10.0, 04.05.2020 17:17
		 * to create an SQL config file to be used by the connector to connect
		 * to this db in the future
		 */		 
		try {
			writeDbConfig(database, currentUser.getUserName(), currentUser.getPassword());
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		return database + "_connection_config.txt";
	}

	/**
	 * This method uses a previously serialized SQLUser object for
	 * the privileged user and receives the database name as a
	 * parameter. <br> it will offer to give privileges on the database
	 * for a user.
	 * @since 0.12.0 (11.09.2020 12:38)
	 * @param database The database's name (String)
	 * @apiNote Updated to allow for custom url
	 * @return The name of the file created by writeDbConfig
	 * which is called by this method. database_connection_config.txt
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static String createDatabase(String connectionURL, String database) throws SQLException, ClassNotFoundException, IOException {
		SQLUser currentUser = (SQLUser) SerializeTools.deserializeFromFile("privilegedSQLUser.ser");
		String user = currentUser.getUserName();
		String pass = currentUser.getPassword();
		/*
		 * Thanks Jeremy Stanley in https://stackoverflow.com/questions/717436/create-mysql-database-from-java
		 */
		Connection conn = DriverManager.getConnection
				("jdbc:mysql://" + connectionURL, user, pass); // Changed from localhost because it might be translated tonip6
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + database + ";"); // Added  [IF NOT EXISTS] so that if it exists it does not throw you out.
		if (DialogKit.executeYesNoQuestion(null, "Database privileges", "Would you like to give all privileges for the"
				+ " created database for a specific user?", JOptionPane.YES_OPTION) == JOptionPane.YES_OPTION) {
			currentUser = DatabaseActions.giveDatabasePrivileges(currentUser, database, conn);
			currentUser.serializeUser();
		}
		/*
		 * THE next section was added 0.10.0, 04.05.2020 17:17
		 * to create an SQL config file to be used by the connector to connect
		 * to this db in the future
		 */		 
		try {
			writeDbConfig(database, currentUser.getUserName(), currentUser.getPassword());
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		return database + "_connection_config.txt";
	}

	/**
	 * This method uses a previously serialized SQLUser object for
	 * the privileged user and receives the database name as a
	 * parameter. <br> it will offer to give privileges on the database
	 * for a user only if the parameter givePrivileges is true.
	 * @since 0.10.0 (24.05.2020 11:26)
	 * @param database The database's name (String)
	 * @param givePrivileges A boolean for whether or not it should offer
	 * to give all privileges on the database to a user
	 * @return The name of the file created by writeDbConfig
	 * which is called by this method. database_connection_config.txt
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static String createDatabase(String database, boolean givePrivileges) throws SQLException, ClassNotFoundException, IOException {
		SQLUser currentUser = (SQLUser) SerializeTools.deserializeFromFile(SQLUser.defaultMainSQLUserFileName);
		String user = currentUser.getUserName();
		String pass = currentUser.getPassword();
		/*
		 * Thanks Jeremy Stanley in https://stackoverflow.com/questions/717436/create-mysql-database-from-java
		 */
		Connection conn = DriverManager.getConnection
				("jdbc:mysql://127.0.0.1", user, pass); // Changed from localhost because it might be translated tonip6
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + database + ";"); // Added  [IF NOT EXISTS] so that if it exists it does not throw you out.
		if (givePrivileges) {
			DialogKit.showPlainMessage(null, "Database privileges", "Please provide a user to give privileges to on the database");
			currentUser = DatabaseActions.giveDatabasePrivileges(null, database, conn);
			currentUser.serializeUser();
		}
		/*
		 * THE next section was added 0.10.0, 04.05.2020 17:17
		 * to create an SQL config file to be used by the connector to connect
		 * to this db in the future
		 */		 
		try {
			writeDbConfig(database, currentUser.getUserName(), currentUser.getPassword());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return database + "_connection_config.txt";
	}

	/**
	 * This method uses a previously serialized SQLUser object for
	 * the privileged user and receives the database name as a
	 * parameter. <br> it will offer to give privileges on the database
	 * for a user only if the parameter givePrivileges is true.
	 * @since 0.12.0 (11.09.2020 12:40)
	 * @param database The database's name (String)
	 * @param givePrivileges A boolean for whether or not it should offer
	 * to give all privileges on the database to a user
	 * @apiNote Updated to allow for custom url
	 * @return The name of the file created by writeDbConfig
	 * which is called by this method. database_connection_config.txt
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static String createDatabase(String connectionURL, String database, boolean givePrivileges) throws SQLException, ClassNotFoundException, IOException {
		SQLUser currentUser = (SQLUser) SerializeTools.deserializeFromFile(SQLUser.defaultMainSQLUserFileName);
		String user = currentUser.getUserName();
		String pass = currentUser.getPassword();
		/*
		 * Thanks Jeremy Stanley in https://stackoverflow.com/questions/717436/create-mysql-database-from-java
		 */
		Connection conn = DriverManager.getConnection
				("jdbc:mysql://" + connectionURL, user, pass); // Changed from localhost because it might be translated tonip6
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate("CREATE DATABASE IF NOT EXISTS " + database + ";"); // Added  [IF NOT EXISTS] so that if it exists it does not throw you out.
		if (givePrivileges) {
			DialogKit.showPlainMessage(null, "Database privileges", "Please provide a user to give privileges to on the database");
			currentUser = DatabaseActions.giveDatabasePrivileges(null, database, conn);
			currentUser.serializeUser();
		}
		/*
		 * THE next section was added 0.10.0, 04.05.2020 17:17
		 * to create an SQL config file to be used by the connector to connect
		 * to this db in the future
		 */		 
		try {
			writeDbConfig(database, currentUser.getUserName(), currentUser.getPassword());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return database + "_connection_config.txt";
	}

	/**
	 * This method creates a new table. <br>
	 * It asks the user for the table name
	 * and the column names.
	 * @implNote It actually calls the overloaded method for the actual creation
	 * @param conn Connection object to communicate with the database
	 * @throws SQLException 
	 * @since 0.10.0 (5.5.2020 18:19)
	 */
	public static void createTable(Connection conn) throws SQLException {
		String tableName = DialogKit.executeFreeTextDialog(null, "table name", "Enter the name of the table you want to create");
		String[] columns = DialogKit.executeFreeTextDialog(null, 
				"column names", 
				"Enter the names of the columns you want to create"
						+ "separated by ,").split(",");
		createTable(conn, tableName, columns);
	}

	/**
	 * This method creates a new table. <br>
	 * It receives the table name
	 * and the column names as parameters. <br>
	 * <b>IMPORTANT: IS PROBABLY NOT WORKING AT THE MOMENT
	 * BEACUSE THE OTHER ONE HAD A BUG WHEN TRIED TO CREATE
	 * TABLE WITHOUT COLUMNS. FIX THIS FOR 0.11.0 </b>
	 * @since 0.10.0 (5.5.2020 19:45)
	 * @param conn Connection object to communicate with the database
	 * @param table The table's name (String)
	 * @param columns The columns' names (String array)
	 * @throws SQLException 
	 */
	public static void createTable(Connection conn, String table, String[] columns) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate("CREATE TABLE IF NOT EXISTS " + table + ";");
		addColumns(conn, table, columns);
		try {
			writeTableConfig(table, columns);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * This method creates a new table. <br>
	 * It receives the table name,
	 * the column names and the column
	 * types and modifiers as parameters.
	 * @since 0.10.0 (18.5.2020 21:07)
	 * @apiNote Allows more flexibility - you can choose the types of the columns
	 * and which of them is the primary key
	 * @implNote I have decided to add a check to see if the columns and 
	 * columnsTypes arrays are the same size. (1.2.0; 19.05.2020 19:41). CHK202005191941
	 * @param conn Connection object to communicate with the database
	 * @param table The table's name (String)
	 * @param columns The columns' names (String array)
	 * @param columnsTypes the desired data types for the columns or null for default.
	 * Extra parameters (AUTO_INCREMENT; NOT NULL; PRIMARY KEY if you want this to be in
	 * the primary key) can be added delimited by whitespace.
	 * @throws SQLException
	 */
	public static void createTable(Connection conn, String table, String[] columns, 
			String[] columnsTypes) throws SQLException, SizeMismatchException {
		/*
		 * see CHK202005191941
		 */
		if (columns.length != columnsTypes.length) {
			throw new SizeMismatchException("The array for the types of the columns is not the same size as the array for the names of the columns");
		}
		Statement SQLStatement = conn.createStatement();
		//		SQLStatement.executeUpdate("CREATE TABLE IF NOT EXISTS " + table + ";");
		//		addColumns(conn, table, columns, columnsTypes); // Because of exception replaced
		/*
		 * Had to change tactics - Apparently you can't
		 * create a table with no columns.
		 * So I had to add the columns here
		 */
		String query = "CREATE TABLE IF NOT EXISTS " + table + " (\n";
		for (int i = 0; i < columns.length; i++) {
			query = query.concat(columns[i] + " " + columnsTypes[i]);
			/*
			 * Had to do the if to not add an additional comma
			 */
			if(i < columns.length - 1) {
				query = query.concat(",\n");
			}
		}
		query = query.concat("\n);");
		SQLStatement.executeUpdate(query);
		try {
			writeTableConfig(table, columns);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Drops the table from the database
	 * @since 0.10.0 (20.5.2020 22:52)
	 * @param conn Connection object to communicate with the database
	 * @param table The name (String) of the table to be dropped
	 * @throws SQLException 
	 */
	public static void dropTable(Connection conn, String table) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate("DROP TABLE IF EXISTS " + table + ";");
	}

	/**
	 * Returns the name of the currently used database
	 * @since 0.10.0 (20.05.2020 20:17)
	 * @param conn Connection object to communicate with the database
	 * @return A string with the name of the currently used database
	 * @throws SQLException 
	 */
	public static String getCurrentDatabaseName(Connection conn) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		ResultSet rs = SQLStatement.executeQuery("SELECT DATABASE();");
		rs.next();
		return rs.getString(1);
	}

	/**
	 * Give all privileges on a database to a user.
	 * It offers to give the privileges to an exisiting user,
	 * to a new user, and if currentUser is not null then
	 * to the current user.
	 * @since 0.10.0 (25.4.2020 ~11:50)
	 * @param currentUser Possibly the user the privileges will be given to. It is done this way because
	 * it allows to record a user being used in another method in case the client wants to give 
	 * that user the privileges. <br> as an SQLUser object, allows to check if exists already
	 * @param database The database name (String) to which the privileges should be given
	 * @param conn Connection object to communicate with the database In order to be able to do stuff
	 * @return An SQLUser object with the data of the user who ended up getting the privileges
	 * @throws SQLException 
	 */
	@SuppressWarnings("unused")
	public static SQLUser giveDatabasePrivileges(SQLUser currentUser, String database, Connection conn) throws SQLException {
		String user = null;
		if (currentUser != null) {
			user = currentUser.getUserName();
		}
		ArrayList<String> options = new ArrayList<String>();
		options.add("An existing user");
		options.add("A new user");
		if (user != null && !user.isEmpty()) {
			options.add("The same user you used");
		}
		String userChoice = DialogKit.executeRadioSelector(null, "Which user", "Which user should receive " + 
				"all privileges for the database?", options.toArray());
		SQLUser updatedUser = null;
		switch (userChoice) {
		case "The same user you used":
			if (!currentUser.isAnExistingUser(conn)) {
				SQLUser.offerToCreateUser(currentUser, conn);
			}
			updatedUser = currentUser;
			break;
		case "An existing user":
			DialogKit.showPlainMessage(null, "existing user", "Please enter existing user details");
			updatedUser = new SQLUser();
			if (!updatedUser.isAnExistingUser(conn)) {
				SQLUser.offerToCreateUser(updatedUser, conn);
			}
			break;
		case "A new user":
			updatedUser = SQLUser.createUser(conn);
			break;
		default:
			System.err.print("Illegal String during giveDatabasePrivileges");
		}
		/*
		 * Make sure updatedUser is not null and exists in MySQL
		 */
		if (updatedUser != null && updatedUser.isAnExistingUser(conn)) {
			Statement SQLStatement = conn.createStatement();
			int Result = SQLStatement.executeUpdate("GRANT ALL PRIVILEGES ON " + 
					database + ".* TO '" + 
					updatedUser.getUserName() + 
					"'@'" + updatedUser.getHost() + "';");
			// TO FIX AN ACCESS DENIED BUG (6.5.2020 20:44)
			//int Result = SQLStatement.executeUpdate("GRANT ALL PRIVILEGES ON " + 
			//         database + ".* TO '" + 
			//         updatedUser.getUserName() + 
			//	     "'@'%';");
			/*
			 * WARNING:
			 * If the user was specified to be localhost only, then it won't work properly
			 * with wildcard.
			 * Which will cause problems later on when trying to connect.....
			 */
		}
		else {
			System.err.print("Illegal SQLUser to give privileges to");
		}
		Statement SQLStatement = conn.createStatement();
		ResultSet rs = SQLStatement.executeQuery("FLUSH PRIVILEGES;");
		return updatedUser;
	}

	/**
	 * @apiNote In this method it is assumed the current database is used
	 * @implNote Migrated the part that gets the current database name to a separate method
	 * so it can be used in other places as well
	 * @since 0.10.0 (17.5.2020 19:52)
	 * @param conn Connection object to communicate with the database
	 * @param table The table's name (String)
	 * @param column The column's name (String)
	 * @return {@link #isExistingColumn(Connection, String, String, String)}
	 * @throws SQLException
	 */
	public static boolean isExistingColumn(Connection conn, String table, String column) throws SQLException {
		try {			
			return isExistingColumn(conn, getCurrentDatabaseName(conn), table, column);
		}
		catch (SQLException e) {
			e.printStackTrace();
			System.err.println("Could not establish existence");
		}
		return false;
	}

	/**
	 * @apiNote This method is to make sure the right database is used,
	 * i.e. - you give the database name
	 * @since 0.10.0 (17.5.2020 19:52)
	 * @param conn Connection object to communicate with the database
	 * @param database The database's name (String)
	 * @param table The table's name (String)
	 * @param column The column's name (String)
	 * @return true if the column exists false otherwise
	 * @throws SQLException
	 */
	public static boolean isExistingColumn(Connection conn, String database, String table, String column) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		try {
			/*
			 * First we create a String for the query to allow for flexibility.
			 * That way, if databaseName is not null, we also include it
			 */
			/*
			 * https://www.mysqltutorial.org/mysql-add-column/ suggestion
			 * "SELECT 
			 * IF(count(*) = 1, 'Exist','Not Exist') AS result
			 * FROM
			 *     information_schema.columns
			 * WHERE
			 *     table_schema = 'database'
			 *     AND table_name = 'table'
			 *     AND column_name = 'column';
			 */
			String query = "SELECT\n"
					+ "    IF(count(*) = 1, 'Exist','Not Exist') AS result\n"
					+ "FROM\n"
					+ "    information_schema.columns\n"
					+ "WHERE\n"
					+ "    table_schema = '" + database + "'\n"
					+ "        AND table_name = '" + table + "'\n"
					+ "        AND column_name = '" + column +"';";
			ResultSet rs = SQLStatement.executeQuery(query); 
			/*
			 * It checks whether columnName exists in the table
			 * in the database. If so it returns Exist else Not Exist
			 */
			rs.next();
			return rs.getString(1).equals("Exist"); // This will return false for an empty ResultSet
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println("Could not establish existence");
		}
		return false;
	}

	/**
	 * Returns true if the database exists and false otherwise<br>
	 * WARNING!: IF MISSING PRIVILEGES,
	 * AN EXISTING db WILL NOT BE VISIBLE TO THE USER
	 * TODO : Might want to handle it, maybe a method for is the db accessible and one if it exists
	 * @since 0.10.0 (6.5.2020 20:11)
	 * @param conn Connection object to communicate with the database
	 * @param database The database's name (String)
	 * @return
	 * @throws SQLException
	 */
	public static boolean isExistingDatabase(Connection conn, String database) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		try {
			ResultSet rs = SQLStatement.executeQuery("SHOW DATABASES;");
			rs.next();
			while (!rs.isAfterLast()) {
				if (rs.getString("Database").equals(database)) {
					return true;
				}
				rs.next();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println("Could not establish existence");
		}
		return false;
	}

	/**
	 * @since 0.10.0 (6.5.2020 21:32)
	 * @param conn Connection object to communicate with the database
	 * @param database The database's name (String)
	 * @param table The table's name (String)
	 * @return
	 * @throws SQLException
	 */
	public static boolean isExistingTable(Connection conn, String database, String table) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		try {
			ResultSet rs = SQLStatement.executeQuery("SHOW TABLES;");
			if (rs.first()) { // If not empty then moves to first. otherwise returns false and we return false as well
				// rs.next(); // Replaced with the if due to exception on empty result set
				while (!rs.isAfterLast()) {
					if (rs.getString("Tables_in_" + database).equals(table)) {
						return true;
					}
					rs.next();
				} 
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println("Could not establish existence");
		}
		return false;
	}

	/**
	 * Delete all rows from a table
	 * @param conn Connection object to communicate with the database 
	 * @since 0.10.0 (28.05.2020, 21:30)
	 * @param table The name (String) of the table to be purged
	 * @throws SQLException 
	 */
	public static void purgeTable(Connection conn, String table) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate("DELETE FROM " + table + ";");

	}

	/**
	 * @since 0.10.0 (02.06.2020 15:37)
	 * @param conn Connection object to communicate with the database
	 * @param table The name (String) of the table to be updated
	 * @param updates The updates to be carried out in the form of
	 * Strings "column = new_value"
	 * @param conditions The conditions to be obeyed in the form of
	 * Strings "column = conditional" (to only update selected rows)
	 * @throws SQLException 
	 */
	public static void updateColumns(Connection conn, String table, String[] updates, String[] conditions) throws SQLException {
		String query = "UPDATE " + table
				+ "\nSET\n";
		/*
		 * Now concat the updates
		 */
		for (int i = 0; i < updates.length; i++) {
			query = query.concat(updates[i]);
			if (i != updates.length - 1) {
				query = query.concat(",\n");
			}
		}
		/*
		 * Now concat WHERE and
		 * then the conditions
		 */
		query = query.concat("\nWHERE\n");
		for (int i = 0; i < conditions.length; i++) {
			query = query.concat(conditions[i]);
			if (i != conditions.length - 1) {
				query = query.concat(",\n");
			}
		}
		query = query.concat(";");
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeUpdate(query);		
	}

	/**
	 * Use the database 'database'
	 * @since 0.10.0 (6.5.2020 21:32)
	 * @param conn Connection object to communicate with the database
	 * @param database The database's name (String)
	 * @throws SQLException
	 */
	public static void useDatabase(Connection conn, String database) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		SQLStatement.executeQuery("USE " + database);
	}

	/**
	 * Writes a connection config for the database
	 * @since 0.10.0 (4.5.2020, 17:20)
	 * @param database the database to create a connection
	 * config for
	 * @param user an authorized user
	 * @param pass their password
	 * @throws IOException
	 */
	private static void writeDbConfig(String database, String user, String pass) throws IOException {
		FileOutputKit dbConfig = new FileOutputKit(database + "_connection_config.txt");
		dbConfig.write("Database name:\n" + database +
				"\nUsername:\n" + user +
				"\nPassword:\n" + pass);
		dbConfig.closeWriter();
	}

	/**
	 * @since 0.10.0 (6.5.2020 10:00)
	 * @param table The table's name (String)
	 * @param columns The columns' names (String array)
	 * @throws IOException 
	 */
	private static void writeTableConfig(String table, String[] columns) throws IOException {
		FileOutputKit dbConfig = new FileOutputKit(table + "_table_config.txt");
		dbConfig.write("Table name:\n" + table +
				"\nColumns:\n" + String.join(",", columns));
		dbConfig.closeWriter();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TEST
		String[] columns = {"c1","c2","c3"};
		System.out.print(String.join(",", columns));
		try {
			giveDatabasePrivileges(null, "anothertest", new MySQLConnector("root", "fE^4lvER0hj094").getConn());
		} catch (SQLException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * This method counts the rows
	 * in a result set by going to the last
	 * row and get its number.
	 * It then takes the resultSet back to
	 * before first row.
	 * @since 0.13.0 (28.09.2020 1:01:01 PM)
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	public static int countResultSetRows(ResultSet rs) throws SQLException {
		/*
		 * In order to extract the number of rows,
		 * I was forced to go to the last row and ask
		 * for the row number.
		 * And since I had to mess with the current row 
		 * anyways, I decided to afterwards take it
		 * to the first row instead of the before first
		 * row, to avoid annoying problems and bugs.
		 */
		rs.last();	
		int count = rs.getRow(); // Should be the correct number
		rs.beforeFirst();
		return count;
	}

	/**
	 * Returns a String that looks like:
	 * WHERE
	 * columns1 = values1
	 * ...
	 * columnsn = valuesn
	 * @apiNote Created to allow it to be used
	 * with a method to delete rows.
	 * And from now on it can be used whenever a
	 * WHERE x=y is needed.
	 * @apiNote Overloaded 0.16.0 (26.11.2020 14:04
	 * when I realized I'll need something very similar
	 * but with SET instead of WHERE
	 * @apiNote First, there was constructConditionString.
	 * Then I extracted this wrapper from it.
	 * @since 0.16.0 (26.11.2020 1:28:59 PM)
	 * @param columns
	 * @param values
	 * @return
	 * @throws SizeMismatchException 
	 */
	@HelperMethod
	@OverloadingWrapperMethod
	public static String constructWhereString(String[] columns, String[] values) throws SizeMismatchException {
		return constructConditionString("WHERE", columns, values);
	}

	/**
	 * Returns a String that looks like:
	 * SET
	 * columns1 = values1
	 * ...
	 * columnsn = valuesn
	 * @apiNote Created to allow it to be used
	 * with a method to update rows.
	 * And from now on it can be used whenever a
	 * SET x=y is needed.
	 * @param columns
	 * @param values
	 * @since 0.16.0 (26.11.2020 2:11:50 PM)
	 * @return
	 * @throws SizeMismatchException
	 */
	@HelperMethod
	public static String constructSetString(String[] columns, String[] values) throws SizeMismatchException {
		return constructConditionString("SET", columns, values);
	}

	/**
	 * Returns a String that looks like:
	 * WHERE
	 * columns1 = values1
	 * ...
	 * columnsn = valuesn
	 * @param conditionType 
	 * @apiNote Created to allow it to be used
	 * with a method to delete rows.
	 * And from now on it can be used whenever a
	 * WHERE x=y is needed.
	 * @since 0.16.0 (26.11.2020 1:28:59 PM)
	 * @param columns
	 * @param values
	 * @return
	 * @throws SizeMismatchException 
	 */
	@HelperMethod
	public static String constructConditionString(String conditionType, String[] columns, String[] values) throws SizeMismatchException {
		if (columns.length != values.length) {
			throw new SizeMismatchException("The number of columns and the number"
					+ "of assiciated values must be the same");
		}
		String conditionString = conditionType;
		for (int i = 0; i < values.length; i++) {
			conditionString = conditionString.concat(
					"\n`"	+ columns[i] + "` = '" + values[i] + "'");
			/*
			 * Added 0.16.0 26.11.2020 14:06
			 */
			if (i < values.length - 1) {
				conditionString = conditionString.concat(",");
			}
		}
		return conditionString;
	}

	/**
	 * Delete rows that answer the condition.
	 * @apiNote Actually created to delete
	 * duplicate row in the group matching
	 * scenario, but can be used in general
	 * from now on.
	 * @since 0.16.0 (26.11.2020 1:38:26 PM)
	 * @param conn
	 * @param database
	 * @param table
	 * @param conditionString A String having WHERE and a
	 * list of conditions. Recommended to use 
	 * {@link #constructWhereString(String[], String[])}
	 * @throws SQLException 
	 */
	public static void deleteRows(Connection conn, String database, String table, String conditionString) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		String query = "DELETE FROM "
				+ "`" + database + "`.`" + table + "`\n"
						+ conditionString + ";";
		SQLStatement.executeUpdate(query);		
	}

	/**
	 * Updates rows that answer the conditions
	 * according to the updatesString.
	 * @apiNote Actually created to update
	 * matched_to values in the group matching
	 * scenario, but can be used in general
	 * from now on.
	 * @since 0.16.0 (26.11.2020 1:59:51 PM)
	 * @param conn
	 * @param database
	 * @param table
	 * @param updatesString
	 * @param conditionString A String having WHERE and a
	 * list of conditions. Recommended to use 
	 * {@link #constructSetString(String[], String[])}
	 * @throws SQLException 
	 */
	public static void updateRows(Connection conn, String database, String table, String updatesString,
			String conditionString) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		String query = "UPDATE "
				+ "`" + database + "`.`" + table + "`\n"
						+ updatesString + "\n"
						+ conditionString + ";";
		SQLStatement.executeUpdate(query);		
		
	}

}
