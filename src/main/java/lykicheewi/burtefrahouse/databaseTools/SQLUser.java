package lykicheewi.burtefrahouse.databaseTools;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import lykicheewi.burtefrahouse.miscTools.SerializeTools;
import lykicheewi.burtefrahouse.questionnaireTools.DialogKit;

/** 
 * A convenience class to encapsulate username,
 * password, and possibly the host
 * @since 0.10.0 (25.4.2020 ~12:00)
 * @author Yaron Efrat
 */
public class SQLUser implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7527694634326631953L;
	private String userName;
	private String password;
	private String host;
	
	/**
	 * Added this to avoid making spelling mistakes anywhere in this class or elsewhere.
	 * this and the replacement of individual literal Strings will be given the name
	 * DEFSTRSWP202005242204
	 * @since 0.10.0 (24.05.2020 22:04)
	 */
	public static final String defaultSQLUserFileName = "SQLUser.ser";
	/**
	 * @since 0.10.0 (24.05.2020 22:11)
	 */
	public static final String defaultMainSQLUserFileName = "privilegedSQLUser.ser";
	
	/**
	 * Calls {@link #SQLUser(boolean isLocalhost)} with the result of the method determineHost 
	 * {false (a wild card host), allowing access with the user from anywhere, true
	 * only the host machine}
	 */
	public SQLUser() {			
		this(determineHost());
	}
	
	/**
	 * @since 0.10.0 (6.5.2020 20:57)
	 */
	@Override
	public String toString() {
		return String.format("SQLUser [userName=%s, password=%s, host=%s]", userName, password, host);
	}

	/**
	 * Asks the user whether the SQLUser should be accessed only 
	 * through local host
	 * @since 0.10.0 (25.4.2020 17:54)
	 * @return a boolean of the answer
	 */
	public static boolean determineHost() {
		return (DialogKit.executeYesNoQuestion(null, "host", "Can/Should the user "
				+ "only be accessed through localhost?" , JOptionPane.YES_OPTION) == JOptionPane.YES_OPTION);
	}

	/**
	 * Asks the user for the userName and password, 
	 * and sets the host to localhost or wildcard depending
	 * on the boolean
	 * @param isLocalhost Whether the host should be localhost
	 * or any host.
	 
	 */
	public SQLUser(boolean isLocalhost) {
		/*
		 * JFrame GUI = DialogKit.generateSimpleGUI("User", 200, 150, 1, null, null);
		 * JPanel contentPane = (JPanel) GUI.getContentPane(); contentPane.add(new
		 * JLabel("Enter username ")); JTextField user = new JTextField();
		 * contentPane.add(user); contentPane.add(new JLabel("Enter password "));
		 * JPasswordField pass = new JPasswordField(20); contentPane.add(pass); Thread
		 * GUIThread = new Thread((Runnable) GUI); DialogKit.showFrame(GUI, true); try {
		 * GUIThread.wait(); } catch (InterruptedException e) { // Auto-generated
		 * catch block e.printStackTrace(); }
		 * * @implNote Changed to a more streamlined GUI with two lines of input instead of
	 * two dialogs. (5.5.2020 18:53)
		 */
		userName = DialogKit.executeFreeTextDialog(null, "username", "Enter username");
		password = DialogKit.executePasswordDialog(null, "password", "Enter password");			
		assignHost(isLocalhost);
	}		

	/**
	 * Calls {@link #SQLUser(String userName, String password, boolean isLocalhost)} 
	 * with the default option for the boolean that 
	 * is false (a wild card host), allowing access with the user from anywhere, not
	 * only the host machine
	 * @param userName the username (String)
	 * @param password the password (String)
	 */
	public SQLUser(String userName, String password) {
		this(userName, password, false);
	}
	
	/**
	 * Sets the userName and password according to parametes
	 * and sets the host to localhost or wildcard depending
	 * on the boolean
	 * @param userName the username (String)
	 * @param password the password (String)
	 * @param isLocalhost Whether the host should be localhost
	 * or any host.
	 */
	public SQLUser(String userName, String password, boolean isLocalhost) {
		this.userName = userName;
		this.password = password;
		assignHost(isLocalhost);
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}
	
	private void assignHost(boolean isLocalhost) {
		if (isLocalhost) {
			//host = "localhost";
			host = "127.0.0.1"; // Since apparently localhost can be interpreted as ip6 which is not supported
		}
		else {
			host = "%";
		}			
	}
	
	/**
	 * Thanks to Ed Harper
	 * on https://stackoverflow.com/questions/356000/how-do-you-test-for-the-existence-of-a-user-in-sql-server/356030#356030
	 * @param conn the connection on which to execute the query
	 * @since 0.10.0 (25.4.2020 ~12:20)
	 * @implNote (0.10.0 30.4.2020 20:34) After trying for days,
	 * I finally got this to work:
	 * Check mysql.user for a User with the username,
	 * and return resultSet.first()
	 * @return true if the user exists, false otherwise
	 * @throws SQLException SQL Exception obviously (kidding, the IDE asked for it)
	 */
	public boolean isAnExistingUser(Connection conn) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		String queryString = "SELECT TRUE \nWHERE EXISTS(SELECT 1 \nFROM mysql.user \nWHERE "
				+ "User = N'" + userName + "')";
		//return SQLStatement.execute(queryString);
		ResultSet resultSet = SQLStatement.executeQuery(queryString);
		
		/*
		 * Thanks Chris Aldrich and CodeZombie
		 * in https://dba.stackexchange.com/questions/125886/check-if-a-user-exists-in-a-sql-server-database
		 */
		return resultSet.first();
		
	}

	/**
	 * If the user you specified does not exist in MySQL,
	 * offers to create it.
	 * @apiNote delegated from MySQLConnector
	 * @since 0.10.0 (4.5.2020 17:48)
	 * @param currentUser An SQLUser Object with a username, password and host
	 * representing a user that doesn't exist yet in MySQL
	 * @param conn An SQL Connection
	 * @throws SQLException 
	 */
	static void offerToCreateUser(SQLUser currentUser, Connection conn) throws SQLException {
		if (DialogKit.executeYesNoQuestion(null, "create user?", "The selected user does not exist. "
				+ "Would you like to create it?" , JOptionPane.YES_OPTION) == JOptionPane.YES_OPTION) {
			createUser(currentUser, conn);
		}	
	}

	/**
	 * Uses the SQLUser currentUser to create a new User 
	 * in MySQL
	 * @apiNote delegated from MySQLConnector
	 * @since 0.10.0 (4.5.2020 17:48)
	 * @param currentUser AnSQLUser object with the new user details.
	 * @param conn An SQL Connection
	 * @return currentUser
	 * @throws SQLException 
	 */
	@SuppressWarnings("unused")
	public static SQLUser createUser(SQLUser currentUser, Connection conn) throws SQLException {
		Statement SQLStatement = conn.createStatement();
		/*
		 * Added IF NOT EXISTS in 0.16.0 24.11.2020 13:04
		 */
		int Result = SQLStatement.executeUpdate("CREATE USER IF NOT EXISTS '" + currentUser.getUserName() + 
				"'@'" + currentUser.getHost() + 
				"' IDENTIFIED BY '" + currentUser.getPassword() + "';"); 
		return currentUser;
	}

	/**
	 * lets the user create a new SQLUser and also creates it in MySQL
	 * @apiNote delegated from MySQLConnector
	 * @since 0.10.0 (4.5.2020 17:48)
	 * @param conn An SQL Connection
	 * @return
	 * @throws SQLException 
	 */
	public static SQLUser createUser(Connection conn) throws SQLException {
		SQLUser currentUser = new SQLUser();
		return createUser(currentUser, conn);		
	}
	
	/**
	 * @since 0.10.0 (24.05.2020 11:17)
	 * @throws IOException
	 */
	public void serializeUser() throws IOException {
		SerializeTools.serializeToFile(this, defaultSQLUserFileName); // DEFSTRSWP202005242204 - swapped from literal String to avoid spelling mistakes and to be more clear
	}
	
	/**
	 * @since 0.10.0 (24.05.2020 11:21)
	 * @throws IOException
	 */
	public void serializeUser(String fileName) throws IOException {
		if (fileName.contains(".ser")) {
			SerializeTools.serializeToFile(this, fileName);
		}
		else {
			SerializeTools.serializeToFile(this, fileName + ".ser");
		}
	}
	
	/**
	 * To make a main user
	 * @param args
	 */
	public static void main (String[] args) {
		SQLUser mainUser = new SQLUser();
		try {
			mainUser.serializeUser(defaultMainSQLUserFileName);
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}