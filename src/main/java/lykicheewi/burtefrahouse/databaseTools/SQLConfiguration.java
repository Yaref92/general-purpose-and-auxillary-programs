/**
 * 
 */
package lykicheewi.burtefrahouse.databaseTools;

import java.io.Serializable;

/** 
 *
 * @author Yaron Efrat
 */
public class SQLConfiguration implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5130334288117796924L;
	private String databaseName;
	private String userName;
	private String password;
	
	/**
	 * @apiNote Since We finally had access to the database on the cluster,
	 * it was time to make it more adjustable...
	 * @since 0.12.0 11.09.2020 12:06
	 */
	private String connectionURL;
	
	/**
	 * @apiNote Updated on 11.09.2020 12:09 to
	 * have "jdbc:mysql://127.0.0.1/" as a default
	 * connectionurl
	 */
	public SQLConfiguration(String database, String user, String pass) {
		connectionURL = "jdbc:mysql://127.0.0.1/";
		databaseName = database;
		userName = user;
		password = pass;
	}
	
	/**
	 * 
	 * @apiNote Since We finally had access to the database on the cluster,
	 * it was time to make it more adjustable...
	 * @since 0.12.0 (11.09.2020 12:07:48 PM)
	 * @param connectionURL
	 * @param database
	 * @param user
	 * @param pass
	 */
	public SQLConfiguration(String connectionURL, String database, String user, String pass) {
		this.connectionURL = connectionURL;
		databaseName = database;
		userName = user;
		password = pass;
	}
	/**
	 *
	 * @since 0.12.0 (11.09.2020 12:10:13 PM)
	 * @return the connectionURL
	 */
	public String getConnectionURL() {
		return connectionURL;
	}

	/**
	 * @return the databaseName
	 */
	public String getDatabaseName() {
		return databaseName;
	}
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

}
