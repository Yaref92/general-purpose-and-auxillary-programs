/**
 * 
 */
package lykicheewi.burtefrahouse.databaseTools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.OverloadingWrapperMethod;
import lykicheewi.burtefrahouse.fileTools.FileInputKit;
import lykicheewi.burtefrahouse.questionnaireTools.DialogKit;

// Notice, do not import com.mysql.cj.jdbc.*
// or you will have problems!

/** 
 * @implNote Decided to use as non static class 0.10.0 (27.4.2020 9:48)
 * @since 0.10.0 (21.4.2020 13:47)
 * @author Yaron Efrat
 */
public class MySQLConnector {

	Connection conn;
	Statement currentQuery;
	
	
	/**
	 * Copied from the main that I made before.
	 * Will edit it.
	 * @since 0.10.0 (27.4.2020 9:49)
	 * 
	 */
	public MySQLConnector() {
		initializeDriver();
		/*
		 * Will add a method that searches for a configuration file.
		 * Otherwise, prompts user to create data base or provide details on existing one
		 */
		SQLConfiguration dbConfig = null;
		try {
			dbConfig = tryToLoadSQLConfiguration("SQLConfig.txt");
		} catch (WrongConfigurationStructureException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		conn = establishConnection(dbConfig);
	}
	
	/**
	 * Calls a connection on localhost with no database with the user
	 * username and the password password
	 * @since 0.10.0 (27.4.2020 9:56)
	 * @param username
	 * @param password
	 * @throws SQLException 
	 */
	public MySQLConnector (String username, String password) throws SQLException {
		initializeDriver();
		conn =
	    		DriverManager.getConnection("jdbc:mysql://127.0.0.1", username, password);
        
	}
	
	/**
	 * Calls a connection on localhost with database databaseName with the user
	 * username and the password password
	 * @since 0.10.0 (27.4.2020 9:56)
	 * @param username
	 * @param password
	 * @param databaseName
	 * @throws SQLException 
	 */
	public MySQLConnector (String username, String password, String databaseName) throws SQLException {
		initializeDriver();
		conn =
	    		DriverManager.getConnection("jdbc:mysql://127.0.0.1/" + databaseName, username, password);
	}
	
	/**
	 * Calls a connection on URL connectionURL with database databaseName with the user
	 * username and the password password
	 * @since 0.12.0 (11.09.2020 12:12)
	 * @param username
	 * @param password
	 * @param databaseName
	 * @throws SQLException 
	 */
	public MySQLConnector (String username, String password, 
			String databaseName, String connectionURL) throws SQLException {
		initializeDriver();
		conn =
	    		DriverManager.getConnection("jdbc:mysql://" + connectionURL + "/" + 
	    				databaseName, username, password);
	}

	/**
	 * Calls a connection on localhost with no database with the user
	 * username and the password password
	 * @since 0.10.0 (27.4.2020 9:56)
	 * @param username
	 * @param password
	 * @throws SQLException 
	 */
	public MySQLConnector (SQLUser currentUser) throws SQLException {
		initializeDriver();
		conn =
	    		DriverManager.getConnection("jdbc:mysql://127.0.0.1", currentUser.getUserName(), currentUser.getPassword());
        
	}
	
	/**
	 * Calls a connection on localhost with database databaseName with the user
	 * username and the password password
	 * @since 0.10.0 (27.4.2020 9:56)
	 * @param username
	 * @param password
	 * @param databaseName
	 * @throws SQLException 
	 */
	public MySQLConnector (SQLUser currentUser, String databaseName) throws SQLException {
		initializeDriver();
		conn =
	    		DriverManager.getConnection("jdbc:mysql://127.0.0.1/" + databaseName, currentUser.getUserName(), currentUser.getPassword());
	}

	/**
	 * Calls a connection on URL connectionURL with database databaseName with the user
	 * username and the password password
	 * @since 0.12.0 (11.09.2020 12:13)
	 * @param username
	 * @param password
	 * @param databaseName
	 * @throws SQLException 
	 */
	public MySQLConnector (SQLUser currentUser, String databaseName, 
			String connectionURL) throws SQLException {
		initializeDriver();
		conn =
	    		DriverManager.getConnection("jdbc:mysql://" + connectionURL + "/" + 
	    				databaseName, 
	    				currentUser.getUserName(), currentUser.getPassword());
	}

	/**
	 * Simple aux method for readability
	 * @since 0.10.0 (27.4.2020 9:59)
	 */
	private void initializeDriver() {
		try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
        }
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        } catch (Exception ex) {
            // handle the error
        }
		/*
		 * Will add a method that searches for a configuration file.
		 * Otherwise, prompts user to create data base or provide details on existing one
		 */
		SQLConfiguration dbConfig = null;
		try {
			dbConfig = tryToLoadSQLConfiguration();
		} catch (WrongConfigurationStructureException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		Connection conn = establishConnection(dbConfig);
		try {
			System.out.println("The answer to whether database testdb exists: " + DatabaseActions.isExistingDatabase(conn, "testdb"));
			System.out.println("The answer to whether database fakedb exists: " + DatabaseActions.isExistingDatabase(conn, "fakedb"));
			DatabaseActions.useDatabase(conn, "sys");
			System.out.println("The answer to whether table sys_config in database sys exists: " + DatabaseActions.isExistingTable(conn, "sys", "sys_config"));
		} catch (SQLException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @since 0.10.0 (5.5.2020 18:25)
	 * @apiNote Decided {@link #tryToLoadSQLConfiguration(String)} was too clunky
	 * and so I use this instead
	 * @apiNote BECAUSE OF MMMT I changed it so an overloaded method, {@link #tryToLoadSQLConfiguration(String, Boolean)}, is the main
	 * method and this just calls it with null and null
	 * @return SQLConfiguration object with the database, username and password
	 * @throws Exception 
	 */
	private static SQLConfiguration tryToLoadSQLConfiguration() throws Exception {
		// Auto-generated method stub
		//
		return tryToLoadSQLConfiguration(null, null);
	}

	/**
	 * Will first try to see if there is a file named defaultFileName.
	 * If not,
	 * @since 0.10.0 (24.4.2020 ~10:40)
	 * @param string
	 * @return SQLConfiguration object with the database, username and password
	 * @throws Exception 
	 */
	@Deprecated
	private static SQLConfiguration tryToLoadSQLConfiguration(String configFileName) throws Exception {
		
		FileInputKit configFile = null;		
		try {
			configFile = new FileInputKit(configFileName);
		} catch (IOException e) {
			 if (DialogKit.executeYesNoQuestion(null, "Database exists?", "Does the database exist yet?", 
						JOptionPane.NO_OPTION, true, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				// Auto-generated catch block
				try {
					configFile = new FileInputKit(new JPanel());
				} catch (IOException e1) {
					// Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e2) {
					System.err.print("File Opening Operation cancelled by user\n");
				}
			}
			else {
				if (DialogKit.executeYesNoQuestion(null, "Create Database?", "Would you like to create a database??", 
						JOptionPane.NO_OPTION, true, JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					String fileName = DatabaseActions.createDatabase();
					return loadSQLConfiguration(new FileInputKit(fileName));
				}
			}
		} 
		if (configFile != null) {
			return loadSQLConfiguration(configFile);
		}
		else {
			throw new Exception("No file chosen");
		}
	}

	/**
	 * @since 0.10.0 (24.05.2020 11:44)
	 * @apiNote Became the new main method to try and load SQL Configuration to 
	 * allow more flexibility and compatability with other classes who need to 
	 * do this
	 * @implNote Added to the old method  the ability to have it create 
	 * a database with a pre defined name and the ability to ask the user to
	 * give privileges to another user
	 * @return SQLConfiguration object with the database, username and password
	 * @throws Exception 
	 */
	public static SQLConfiguration tryToLoadSQLConfiguration(String database, Boolean givePrivileges) throws Exception{
		FileInputKit configFile = null;		
		try {
			configFile = new FileInputKit("SQLConfig.txt");
		} catch (FileNotFoundException e) {
		    String[] options = new String[] {"Load database", "Create database", "Cancel"};
			int userChoice = JOptionPane.showOptionDialog(null, 
		    		"Should we load or create a database?", 
		    		"Database", 
		    		JOptionPane.DEFAULT_OPTION, 
		    		JOptionPane.PLAIN_MESSAGE, 
		    		null, 
		    		options, 
		    		options[0]);
			if (userChoice == 0) {
				try {
					configFile = new FileInputKit(new JPanel());
				} catch (IOException e1) {
					// Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e2) {
					System.err.print("File Opening Operation cancelled by user\n");
				}
			}
			else if (userChoice == 1) {
				String fileName = null;
				/*
				 * In the following there are 4 cases:
				 * A name was provided with a boolean
				 * "  "    "     "     without a boolean
				 * "  "    " not provided and a boolean was
				 * "  "    "  "     "      "  "    "    wasn't either
				 */
				if (database != null) { // Added this nested if as of the new method, 0.10.0; 24.05.2020 11:51
					if (givePrivileges != null) {
						fileName = DatabaseActions.createDatabase(database, givePrivileges);
					}
					else {
						fileName = DatabaseActions.createDatabase(database);
					}
				}
				else {
					if (givePrivileges != null) {
						database = DialogKit.executeFreeTextDialog(null, "Database name", "Please give the database a name");
						fileName = DatabaseActions.createDatabase(database, givePrivileges);
					}
					else {
						fileName = DatabaseActions.createDatabase();
					}				 
				}
				configFile = new FileInputKit(fileName);
			}
			else {
				throw new Exception("No database chosen");
			}
		}
		if (configFile != null) {
			return loadSQLConfiguration(configFile);
		}
		else {
			throw new Exception("No configuration file available");
		}
	}

	/**
	 * @since 0.12.0 (11.09.2020 12:41)
	 * @apiNote Became the new main method to try and load SQL Configuration to 
	 * allow more flexibility and compatability with other classes who need to 
	 * do this
	 * @apiNote Updated to allow for custom url
	 * @implNote Added to the old method  the ability to have it create 
	 * a database with a pre defined name and the ability to ask the user to
	 * give privileges to another user
	 * @return SQLConfiguration object with the database, username and password
	 * @throws Exception 
	 */
	public static SQLConfiguration tryToLoadSQLConfiguration(String connectionURL, 
			String database, Boolean givePrivileges) throws Exception{
		FileInputKit configFile = null;		
		try {
			configFile = new FileInputKit("SQLConfig.txt");
		} catch (FileNotFoundException e) {
		    String[] options = new String[] {"Load database", "Create database", "Cancel"};
			int userChoice = JOptionPane.showOptionDialog(null, 
		    		"Should we load or create a database?", 
		    		"Database", 
		    		JOptionPane.DEFAULT_OPTION, 
		    		JOptionPane.PLAIN_MESSAGE, 
		    		null, 
		    		options, 
		    		options[0]);
			if (userChoice == 0) {
				try {
					configFile = new FileInputKit(new JPanel());
				} catch (IOException e1) {
					// Auto-generated catch block
					e1.printStackTrace();
				} catch (NullPointerException e2) {
					System.err.print("File Opening Operation cancelled by user\n");
				}
			}
			else if (userChoice == 1) {
				String fileName = null;
				/*
				 * In the following there are 4 cases:
				 * A name was provided with a boolean
				 * "  "    "     "     without a boolean
				 * "  "    " not provided and a boolean was
				 * "  "    "  "     "      "  "    "    wasn't either
				 */
				if (database != null) { // Added this nested if as of the new method, 0.10.0; 24.05.2020 11:51
					if (givePrivileges != null) {
						fileName = DatabaseActions.createDatabase(connectionURL, database, givePrivileges);
					}
					else {
						fileName = DatabaseActions.createDatabase(connectionURL, database);
					}
				}
				else {
					if (givePrivileges != null) {
						database = DialogKit.executeFreeTextDialog(null, "Database name", "Please give the database a name");
						fileName = DatabaseActions.createDatabase(database, givePrivileges);
					}
					else {
						fileName = DatabaseActions.createDatabase();
					}				 
				}
				configFile = new FileInputKit(fileName);
			}
			else {
				throw new Exception("No database chosen");
			}
		}
		if (configFile != null) {
			return loadSQLConfiguration(configFile);
		}
		else {
			throw new Exception("No configuration file available");
		}
	}

	/**
	 * Establishes a connection using the SQLConfiguration dbConfig
	 * @since 0.10.0 (4.5.2020 17:43)
	 * @apiNote Changed to public to allow MMMT to use it at 0.10.0 (24.05.2020 11:06)
	 * @param dbConfig
	 * @return A connection object
	 */
	public static Connection establishConnection(SQLConfiguration dbConfig) {
		Connection conn = null;
		try {
		    //conn =
		      // DriverManager.getConnection("jdbc:mysql://160.45.170.75:3306/wordpress?" +
		        //                           "user=wpuser&password=lceXuotP9djGzevdO0w4");
//		    conn =
//		    		DriverManager.getConnection("jdbc:mysql://127.0.0.1/" + dbConfig.getDatabaseName(), 
//		    				dbConfig.getUserName(), dbConfig.getPassword());
			conn =
		    		DriverManager.getConnection(
		    				"jdbc:mysql://" + 
		    		dbConfig.getConnectionURL() + "/" + dbConfig.getDatabaseName(), 
		    				dbConfig.getUserName(), dbConfig.getPassword());

		    // Do something with the Connection
		    System.out.println("Connected");

		} catch (SQLException ex) {
		    // handle any errors
		    System.out.println("SQLException: " + ex.getMessage()); // Shows the password.
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		}
		return conn;
	}

	/**
	 * @since 0.10.0 (24.4.2020 11:53)
	 * @throws SQLException
	 * @deprecated Use {@link DatabaseActions#createDatabase()} instead (0.10.0 27.4.2020 10:04)
	 */
	@SuppressWarnings("unused")
	@Deprecated
	private static void createDatabase() throws SQLException {
		DatabaseActions.createDatabase();
	}

	/**
	 * Give all privileges on a database to a user
	 * @since 0.10.0 (25.4.2020 ~11:50)
	 * @param currentUser Possibly the user the privileges will be given to. It is done this way because
	 * it allows to record a user being used in another method in case the client wants to give 
	 * that user the privileges. <br> as an SQLUser object, allows to check if exists already
	 * @param database The database name (String) to which the privileges should be given
	 * @param conn In order to be able to do stuff
	 * @throws SQLException 
	 * @deprecated Use {@link DatabaseActions#giveDatabasePrivileges(SQLUser,String,Connection)} instead
	 */
	public static void giveDatabasePrivileges(SQLUser currentUser, String database, Connection conn) throws SQLException {
		DatabaseActions.giveDatabasePrivileges(currentUser, database, conn);
	}

	/**
	 * If the user you specified does not exist in MySQL,
	 * offers to create it.
	 * @since 0.10.0 (25.4.2020 12:37)
	 * @param currentUser An SQLUser Object with a username, password and host
	 * representing a user that doesn't exist yet in MySQL
	 * @param conn An SQL Connection
	 * @throws SQLException 
	 * @deprecated Use {@link SQLUser#offerToCreateUser(SQLUser,Connection)} instead
	 */
	static void offerToCreateUser(SQLUser currentUser, Connection conn) throws SQLException {
		SQLUser.offerToCreateUser(currentUser, conn);
	}

	/**
	 * Uses the SQLUser currentUser to create a new User 
	 * in MySQL
	 * @since 0.10.0 (~17:35)
	 * @param currentUser AnSQLUser object with the new user details.
	 * @param conn An SQL Connection
	 * @return currentUser
	 * @throws SQLException 
	 * @deprecated Use {@link SQLUser#createUser(SQLUser,Connection)} instead
	 */
	public static SQLUser createUser(SQLUser currentUser, Connection conn) throws SQLException {
		return SQLUser.createUser(currentUser, conn);
	}

	/**
	 * lets the user create a new SQLUser and also creates it in MySQL
	 * @since 0.10.0 (~17:30)
	 * @param conn An SQL Connection
	 * @return
	 * @throws SQLException 
	 * @deprecated Use {@link SQLUser#createUser(Connection)} instead
	 */
	public static SQLUser createUser(Connection conn) throws SQLException {
		return SQLUser.createUser(conn);
	}

	/**
	 * @apiNote Changed to public to allow MMMT to use it at 0.10.0 (24.05.2020 12:31)
	 * @implNote UPDATE 0.12.0 (11.09.2020 12:24) Since we now allow
	 * a custom connectionURL, we need an option that accounts for that.
	 * The best thing, is to make it optional, since in that case it
	 * defaults to localhost. To do that, we add a 4th if at the end,
	 * and if sanitychek is 4 we create an SQLConfiguration with
	 * connectionURL
	 * @param configFile
	 * @return
	 * @throws WrongConfigurationStructureException
	 */
	public static SQLConfiguration loadSQLConfiguration(FileInputKit configFile) throws WrongConfigurationStructureException {
		int sanityCheck = 0;
		String database = null, user = null, pass = null;
		String connectionURL = null; // UPDATE 0.12.0 (11.09.2020 12:24)
		if (configFile.readNextLine().equals("Database name:")) {
			database = configFile.readNextLine();
			sanityCheck++;
		}
		if (configFile.readNextLine().equals("Username:")) {
			user = configFile.readNextLine();
			sanityCheck++;
		}
		if (configFile.readNextLine().equals("Password:")) {
			pass = configFile.readNextLine();
			sanityCheck++;
		}
		if (configFile.readNextLine().equals("Connection URL:")) {
			connectionURL = configFile.readNextLine();
			sanityCheck++;
		}
		if (sanityCheck == 3) {
		    return new SQLConfiguration(database, user, pass);
		}
		else if (sanityCheck == 4) {
			return new SQLConfiguration(connectionURL, database, user, pass);
		}
		else {
			throw new WrongConfigurationStructureException();
		}
	}

	/**
	 * @return the conn
	 */
	public Connection getConn() {
		return conn;
	}

}
