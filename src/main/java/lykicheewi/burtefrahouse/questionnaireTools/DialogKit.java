/**
 * 
 */
package lykicheewi.burtefrahouse.questionnaireTools;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.io.File;
import java.io.IOException;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import lykicheewi.burtefrahouse.annotations.methodAnnotations.*;
import lykicheewi.burtefrahouse.miscTools.ArrayTools;

/** 
 * ADDED IN 0.8.0, 10.4.2020 16:48
 * I was starting to create these methods for another
 * class (UserMaker for the MMMT), when I decided
 * that since they would be highly reusable I should give them the honor of a separate class.
 * @author Yaron Efrat
 */
public class DialogKit {

	/**
	 * 
	 */
	public DialogKit() {
		// Auto-generated constructor stub
		// Maybe I'll do everything static
	}

	/**
	 * Generates a new JFrame Object and sets its title
	 * width and height. <br>
	 * Also sets it to exit the application when the
	 * close button is pressed and show in the center.
	 * <br> It is NOT resizeable. <br>
	 * Didn't make it visible, otherwise it will show 
	 * an empty frame before we put stuff there. <br>
	 * The method {@link #showFrame(JFrame)} will
	 * make it visible.
	 * @since 0.9.0 (14.4.2020, ~11:50)
	 * @param title the title of the new frame
	 * @param width the width of the new frame
	 * @param height the height of the new frame
	 * @return the JFrame object
	 */
	public static JFrame generateFrame(String title, int width, int height) {
		/*
		 * Thanks you Seun Matt for the tutorial at
		 * https://medium.com/prodsters/how-to-build-a-desktop-application-with-java-a34ee9c18ee3
		 */
		JFrame frame = new JFrame(title);
		// Set the layout manager
		frame.setLayout(new BorderLayout());
		// Set the size
		frame.setSize(new Dimension(width, height));
		//this terminate the app when the close button is clicked
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this will make the app to always display at the center
		frame.setLocationRelativeTo(null);
		//well, I don't want you to resize the window
		frame.setResizable(false);		


		return frame;

	}

	/**
	 * Generates a new JDialog Object and sets its title
	 * width and height. <br>
	 * Also sets it to exit the application when the
	 * close button is pressed and show in the center.
	 * <br> It is NOT resizeable. <br>
	 * Didn't make it visible, otherwise it will show 
	 * an empty frame before we put stuff there. <br>
	 * The method {@link} will
	 * make it visible.
	 * @apiNote JDialog is modal (blocks other actions)
	 * whereas JFrame is not
	 * @since 0.9.0 (14.4.2020, ~17:33)
	 * @param title the title of the new frame
	 * @param width the width of the new frame
	 * @param height the height of the new frame
	 * @return the JDialog object
	 */
	public static JDialog generateDialog(String title, int width, int height) {
		JDialog dialog = new JDialog(generateFrame(null, 800, 650));
		dialog.setModalityType(Dialog.ModalityType.DOCUMENT_MODAL);
		dialog.setTitle(title);
		// Set the layout manager
		dialog.setLayout(new BorderLayout());
		// Set the size
		dialog.setSize(new Dimension(width, height));
		//this terminate the app when the close button is clicked
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		//this will make the app to always display at the center
		dialog.setLocationRelativeTo(null);
		//well, I don't want you to resize the window
		dialog.setResizable(false);		
		return dialog;
	}

	/**
	 * @since 0.9.0 (14.4.2020 21:12)
	 * @param parent Either a parent frame, Dialog, Panel or null
	 * @param layout A LayoutManager to manage the panel's layout
	 * @return A panel who is set to be parent's contentPane if parent is not null. <br>
	 * It also has layout as its LayoutManager.
	 */
	public static JPanel generatePanel(Container parent, LayoutManagerType layout, String...constraints ) {
		JPanel panel = new JPanel();

		layoutAssigner(panel, layout);
		setPanelParent(panel, parent, constraints);

		return panel;
	}

	/**
	 * decided to add an option to generate
	 * a scrollable panel.
	 * was deemed necessary during CHANGE202010021021
	 * @implNote The only difference between generatePanel
	 * and this one is the part related to the Scrollpane
	 * and the return type being JScrollPane 
	 * @since 0.13.0 (02.10.2020 10:53)
	 * @param parent Either a parent frame, Dialog, Panel or null
	 * @param layout A LayoutManager to manage the panel's layout
	 * @param constraints
	 * @return A panel in a scrollPane who is set to be parent's contentPane if parent is not null. <br>
	 * It also has layout as its LayoutManager.
	 */
	public static JScrollPane generateScrollablePanel(Container parent, LayoutManagerType layout, String...constraints ) {
		JPanel panel = new JPanel();
	
		layoutAssigner(panel, layout);
		setPanelParent(panel, parent, constraints);
		JScrollPane scrollPane = new JScrollPane(panel);
	
		return scrollPane;
	}

	/**
	 * An auxillary method, the takes panel, and some parent
	 * which is one of the extensions of Container, and possibly
	 * a layout constraint. <br>
	 * Either sets panel to be the contentPane of the parent
	 * (if JFrame or JDialog) or adds it to the parent
	 * with possible constraints (if JPanel).
	 * @since 0.9.0 (14.4.2020 21:46)
	 * @apiNote Another delegation to make the generatePanel
	 * method more concise.
	 * @param <T> Should be JFrame, JDialog or JPanel
	 * @param panel the panel to be adopted
	 * @param parent the expecting parent (JFrame, JDialog or JPanel)
	 * @param constraints if there are layout constraints in the case of
	 * panel inside a panel
	 */
	private static <T extends Container> void setPanelParent(JPanel panel, T parent, String...constraints) {
		if (parent != null) {
			if (parent instanceof JFrame) {
				( (JFrame) parent).setContentPane(panel);
			}
			else if (parent instanceof JDialog) {
				( (JDialog) parent).setContentPane(panel);
			}
			else if (parent instanceof JPanel) {
				( (JPanel) parent).add(panel, constraints[0]);
			}
		}
	}

	/**
	 * @since 0.9.0 (14.4.2020 21:43)
	 * @apiNote A delegation of the switch statement to make the generatePanel
	 * method more concise.
	 * @apiNote GRIDX and GRIDY Added in the process of CHANGE202010021021
     * in order to assist with a scrollable JPanel of JPanels <br>
     * at 0.13.0 02.10.2020 11:21
	 * @param panel
	 * @param layout
	 */
	private static void layoutAssigner(JPanel panel, LayoutManagerType layout) {
		switch (layout) {
		case BORDER:
			panel.setLayout(new BorderLayout());
			break;
		case FLOW:
			panel.setLayout(new FlowLayout());
			break;
		case BOXX:
			panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
			break;
		case BOXY:
			panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
			break;
		case GRIDX:
			panel.setLayout(new GridLayout(1, 0));
			break;
		case GRIDY:
			panel.setLayout(new GridLayout(0, 1));
			break;
		}

	}

	/**
	 * Makes the frame visible.
	 * @since 0.9.0
	 * @param frame the frame to be made visible.
	 * @param focusStealer A boolean whether the frame
	 * should disallow focus loss through mouse clicks
	 * outside of it.
	 */
	public static void showFrame(JFrame frame, boolean focusStealer) {
		if (focusStealer) {
			frame.setAlwaysOnTop(true);
			frame.addWindowFocusListener(new WindowFocusListener() {

				@Override
				public void windowLostFocus(WindowEvent e) {
					frame.toFront();
					frame.setAutoRequestFocus(true);
				}

				@Override
				public void windowGainedFocus(WindowEvent e) {
					// Auto-generated method stub

				}
			});
		}
		//and yes this will show the visible frame/app
		frame.setVisible(true);
	}	

	/**
	 * Makes the dialog visible.
	 * @since 0.9.0
	 * @param dialog the dialog to be made visible.
	 * @param focusStealer A boolean whether the dialog
	 * should disallow focus loss through mouse clicks
	 * outside of it.
	 */
	public static void showDialog(JDialog dialog, boolean focusStealer) {
		if (focusStealer) {
			dialog.setAlwaysOnTop(true);
			dialog.addWindowFocusListener(new WindowFocusListener() {

				@Override
				public void windowLostFocus(WindowEvent e) {
					dialog.toFront();
					dialog.setAutoRequestFocus(true);
				}

				@Override
				public void windowGainedFocus(WindowEvent e) {
					// Auto-generated method stub

				}
			});
		}
		//and yes this will show the visible dialog/app
		dialog.setVisible(true);
		dialog.getContentPane().setVisible(true);
	}

	/**
	 * Shows a message and accepts free text from the user.
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box.
	 * @param isPopup whether or not the dialog should be a popup or internal dialog. 
	 * @since 0.9.0 14.4.2020 23:42
	 * @apiNote Added the JPanel in 0.9.0, 14.4.2020 17:01
	 * @return the free text entered by the user.
	 */
	public static String executeFreeTextDialog(JPanel parent, String title, String message, boolean isPopup) {
		/*
		 * Thanks Java tutorial
		 * https://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html#featuress
		 */
		if (isPopup) {
			return (String)JOptionPane.showInputDialog(
					parent,
					message,
					title,
					JOptionPane.PLAIN_MESSAGE,
					(Icon) null,
					null, "");
		}
		else {
			return (String)JOptionPane.showInternalInputDialog(
					parent,
					message,
					title,
					JOptionPane.PLAIN_MESSAGE,
					(Icon) null,
					null, "");
		}
	}

	/**
	 * Shows a message and accepts free text from the user.
	 * @apiNote separated to overloaded methods 0.9.0 14.4.2020 23:42
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box.
	 * @since 0.8.0
	 * @apiNote Added the JPanel in 0.9.0, 14.4.2020 17:01
	 * @return the free text entered by the user.
	 */
	@OverloadingWrapperMethod
	public static String executeFreeTextDialog(JPanel parent, String title, String message) {
		/*
		 * Thanks Java tutorial
		 * https://docs.oracle.com/javase/tutorial/uiswing/components/dialog.html#featuress
		 */
		return executeFreeTextDialog(parent, title, message, true);
	}
	
	/**
	 * 
	 *
	 * @since 0.12.0 (11.08.2020 14:19:45)
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the main message of the dialog box.
	 * @param inputPropmts the messages to the left of
	 * each free text area
	 * @return An array of the free texts entered
	 * by the user
	 */
	@OverloadingWrapperMethod
	public static String[] executeGuidedFreeInputsDialog(JPanel parent, String title, 
			String message, String[] inputPropmts) {
		return executeGuidedFreeInputsDialog(parent, title, message, inputPropmts, true, null);
	}

	/**
	 * 
	 * @apiNote Was overloaded to include default
	 * inputs and delegated from here and the method above
	 * on 0.13.0 (02.10.2020 12:45)
	 * @since 0.12.0 (11.08.2020 14:21:21)
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the main message of the dialog box.
	 * @param inputPropmts the messages to the left of
	 * each free text area
	 * @param isPopup whether or not the dialog should be a popup or internal dialog. 
	 * @return An array of the free texts entered
	 * by the user
	 */
	@OverloadingWrapperMethod
	public static String[] executeGuidedFreeInputsDialog(JPanel parent, String title, String message,
			String[] inputPropmts, boolean isPopup) {
				return executeGuidedFreeInputsDialog(parent, title, message, inputPropmts, isPopup, null);
			}

	/**
	 * 
	 * @apiNote Was overloaded from {@link #executeGuidedFreeInputsDialog(JPanel, String, String, String[], boolean)}
	 * while that one was turned into a delegate method.
	 * @since 0.13.0 (02.10.2020 12:45)
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the main message of the dialog box.
	 * @param inputPropmts the messages to the left of
	 * each free text area
	 * @param isPopup whether or not the dialog should be a popup or internal dialog. 
	 * @param defaultInputs The default texts in the free text area or null if
	 * no defaults
	 * @return An array of the free texts entered
	 * by the user
	 */
	public static String[] executeGuidedFreeInputsDialog(JPanel parent, String title, String message,
			String[] inputPropmts, boolean isPopup, String[] defaultInputs) {
		/*
		 * Thanks whiskeyspider
		 * https://stackoverflow.com/questions/30265720/java-joptionpane-radio-buttons
		 */
		int inputCount = inputPropmts.length;
//		final JPanel panel = generatePanel(parent, LayoutManagerType.BOXY, BorderLayout.CENTER); // 10.4.2020 22:31 - the BoxLayout (BOXY) attempts to display options vertically
		final JPanel panel = generatePanel(parent, LayoutManagerType.GRIDY, BorderLayout.CENTER); // 0.13.0 02.10.2020 11:26 - part of CHANGE202010021021 - changed to GridLayout (GRIDY) to have the options vertically while allowing scrolling
//		final JScrollPane panel = generateScrollablePanel(parent, LayoutManagerType.BOXY, BorderLayout.CENTER); // part of CHANGE202010021021 - making the whole thing scrollable
		/*
		 * Create a label to contain the message and then put it in the panel
		 */
		final JLabel label = new JLabel();
		label.setText(message);
		panel.add(label);
		/*
		 * An array of panels for the inputs'
		 * presentation
		 * and an array of text fields for 
		 * the actual input strings
		 */
		JPanel[] inputPanels = new JPanel[inputCount];
		JTextField[] inputFields = new JTextField[inputCount];
		for (int i = 0; i < inputCount; i++) {
			inputPanels[i] = generatePanel(panel, LayoutManagerType.BOXY, BorderLayout.CENTER);
			/*
			 * Create a label to contain the message and then put it in the panel
			 */
			final JLabel label1 = new JLabel();
			label1.setText(inputPropmts[i]);
			inputPanels[i].add(label1);
			/*
			 * initialize a textfield and put 
			 * it in the panel
			 */
//			inputFields[i] = new JTextField();
			inputFields[i] = new JTextField(defaultInputs[i]); // added 0.13.0 (02.10.2020 12:49) and hopefully will behave even if default inputs is null. Otherwise an if will be introduced
			inputPanels[i].add(inputFields[i]);
			/*
			 * Now add to the main panel
			 */
			panel.add(inputPanels[i]);
			
		}
		/*
		 * 0.13.0 02.10.2020 11:08 - part of CHANGE202010021021 - 
		 * part of making the whole thing scrollable
		 */
//		panel.setPreferredSize(new Dimension(640, 480)); // Not good, instead make the scrollPane size this one
		JScrollPane scrollPane = new JScrollPane(panel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); // 0.13.0 02.10.2020 11:08 - part of CHANGE202010021021 - making the whole thing scrollable
		scrollPane.setPreferredSize(new Dimension(640, 480));
		/*
		 * The do while handles the presentation for the user
		 * and allows cancel
		 */
		do {			
			if (isPopup) {
				
					if (JOptionPane.showOptionDialog(parent, scrollPane, title, JOptionPane.OK_CANCEL_OPTION, 
							JOptionPane.PLAIN_MESSAGE, null, null, 0) == JOptionPane.CANCEL_OPTION) { // 0.13.0 02.10.2020 11:08 - changed from panel to scrollPane - part of CHANGE202010021021 - making the whole thing scrollable
						return null;
					}
				
			} else {
				
				
					if (JOptionPane.showInternalOptionDialog(parent, scrollPane, title, JOptionPane.OK_CANCEL_OPTION, 
							JOptionPane.PLAIN_MESSAGE, null, null, 0) == JOptionPane.CANCEL_OPTION) { // 0.13.0 02.10.2020 11:08 - changed from panel to scrollPane - part of CHANGE202010021021 - making the whole thing scrollable
						return null;
					}
				
			} 
		} while (anyInputIsNull(inputFields));
		String[] responses = new String[inputCount];
		/*
		 * for loops over the textfields and recorde the responses
		 */
		for (int i = 0; i < responses.length; i++) {
			responses[i] = inputFields[i].getText();			
		}
		return responses;
	}

	/**
	 * A helper method for {@link #executeGuidedFreeInputsDialog(JPanel, String, String, String[], boolean, String[])}
	 * returns null if any of the input texts is empty or null
	 * @since 0.12.0 (11.08.2020 14:40:33)
	 * @param inputFields
	 * @return true if any of the text is null or empty. false otherwise.
	 */
	@HelperMethod
	private static boolean anyInputIsNull(JTextField[] inputFields) {
		/* 
		 * a simple for loop that goes over the fields and if any
		 * has empty or null text return true.
		 * if the for loop goes through then returns false
		 */
		for (int i = 0; i < inputFields.length; i++) {
			if(inputFields[i].getText() == null) {
				return true;
			}			
		}
		return false;
	}

	/**
	 * Shows a message and radio buttons
	 * and accepts a choice from the user.
	 * @implNote 0.8.0 10.4.2020 21:45 - changed options from String[] 
	 * to Object[] to avoid a classCast Exception
	 * @apiNote Added the JPanel parameter in 0.9.0, 14.4.2020 22:20
	 * @implNote 0.9.0 14.4.2020 22:22 - Changed the creation of JPanel panel
	 * to use the new generatePanel method, with parent as the parent. <br>
	 * @implNote Added the do while at 0.9.0 15.4.2020 00:20.
	 * Such a simple solution to the trouble I've had when the user
	 * didn't choose anything (bad) -
	 * a do while where the condition is that the selected
	 * button is null.
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box.
	 * @param options an array containing the Strings for the options (as Objects)
	 * @param isPopup whether or not the dialog should be a popup or internal dialog. 
	 * @since 0.9.0 14.4.2020 23:36
	 * @return the String associated with the chosen radio button
	 */
	public static String executeRadioSelector(JPanel parent, String title, String message, Object[] options, boolean isPopup) {
		return executeRadioSelector(parent, title, message, options, isPopup, true);
	}

	/**
	 * Shows a message and radio buttons
	 * and accepts a choice from the user.
	 * @implNote 0.8.0 10.4.2020 21:45 - changed options from String[] 
	 * to Object[] to avoid a classCast Exception
	 * @apiNote Added the JPanel parameter in 0.9.0, 14.4.2020 22:20
	 * @implNote 0.9.0 14.4.2020 22:22 - Changed the creation of JPanel panel
	 * to use the new generatePanel method, with parent as the parent. <br>
	 * @implNote Added the do while at 0.9.0 15.4.2020 00:20.
	 * Such a simple solution to the trouble I've had when the user
	 * didn't choose anything (bad) -
	 * a do while where the condition is that the selected
	 * button is null.
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box.
	 * @param options an array containing the Strings for the options (as Objects)
	 * @param isPopup whether or not the dialog should be a popup or internal dialog. 
	 * @param isCancelPossible To allow cancel if desired. true is default
	 * @since 0.9.2 20.4.2020 11:32
	 * @return the String associated with the chosen radio button
	 */
	public static String executeRadioSelector(JPanel parent, String title, String message, Object[] options, boolean isPopup, boolean isCancelPossible) {
		/*
		 * Thanks whiskeyspider
		 * https://stackoverflow.com/questions/30265720/java-joptionpane-radio-buttons
		 */
		int optionCount = options.length;
		final JPanel panel = generatePanel(parent, LayoutManagerType.BOXY, BorderLayout.CENTER); // 10.4.2020 22:31 - the BoxLayout (BOXY) attempts to display options vertically
		/*
		 * Create a label to contain the message and then put it in the panel
		 */
		final JLabel label = new JLabel();
		label.setText(message);
		panel.add(label);
		ButtonGroup buttonGroup = new ButtonGroup();
		JRadioButton[] buttons = new JRadioButton[optionCount]; // This array is just convenient
		String option; // Added after I changed options to be Object[]
		for (int i = 0; i < optionCount; i++) {
			option = (String) options[i]; // Because of the change to Object[]
			//Create the radio buttons.
			buttons[i] = new JRadioButton(option);
			buttons[i].setActionCommand(option);
			// Group the radio buttons
			//buttonGroup.add(buttons[i]);
			// Add them to the panel
			//panel.add(buttons[i]);
			//I avoided the need to add an Item Listener, because this is a separate method and can return the selected option.		    
		}
		populateWithButtons(panel, buttonGroup, buttons);
		do {
			/*
			 * Added the do while 
			 * 0.9.0 15.4.2020 00:20
			 */
			if (isPopup) {
				// Added double if in 0.9.2 to allow to cancel
				if (isCancelPossible) {
					if (JOptionPane.showOptionDialog(parent, panel, title, JOptionPane.OK_CANCEL_OPTION, 
							JOptionPane.PLAIN_MESSAGE, null, null, 0) == JOptionPane.CANCEL_OPTION) {
						return null;
					}
				}
				else {
					JOptionPane.showMessageDialog(parent, panel, title, JOptionPane.PLAIN_MESSAGE);
				}
			} else {
				// Added double if in 0.9.2 to allow to cancel
				if (isCancelPossible) {
					if (JOptionPane.showInternalOptionDialog(parent, panel, title, JOptionPane.OK_CANCEL_OPTION, 
							JOptionPane.PLAIN_MESSAGE, null, null, 0) == JOptionPane.CANCEL_OPTION) {
						return null;
					}
				}
				else {
					JOptionPane.showInternalMessageDialog(parent, panel, title, JOptionPane.PLAIN_MESSAGE);
				}
			} 
		} while (buttonGroup.getSelection() == null);
		return buttonGroup.getSelection().getActionCommand();
	}

	/**
	 * Shows a message and radio buttons
	 * and accepts a choice from the user.
	 * @implNote 0.8.0 10.4.2020 21:45 - changed options from String[] 
	 * to Object[] to avoid a classCast Exception
	 * @apiNote Added the JPanel parameter in 0.9.0, 14.4.2020 22:20
	 * @apiNote separated to overloaded methods 0.9.0 14.4.2020 23:34
	 * @implNote 0.9.0 14.4.2020 22:22 - Changed the creation of JPanel panel
	 * to use the new generatePanel method, with parent as the parent,
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box.
	 * @param options an array containing the Strings for the options (as Objects)
	 * @param popup added in 0.9.0 14.4.2020 23:34 - To decide if 
	 * @since 0.8.0
	 * @return the String associated with the chosen radio button
	 */
	public static String executeRadioSelector(JPanel parent, String title, String message, Object[] options) {
		return executeRadioSelector(parent, title, message, options, true, true); // Defaults to a pop up
	}

	/**
	 * Show a yes and no question dialog with buttons
	 * for yes and no.
	 * @apiNote Added the JPanel parameter in 0.9.0, 15.4.2020 21:41
	 * @apiNote separated to overloaded methods 0.9.0 15.4.2020 21:41
	 * @implNote 0.9.0 15.4.2020 21:41 - Changed the creation of JPanel panel
	 * to use the new generatePanel method, with parent as the parent <br>
	 * @implNote Fixed to pass defaultOption to the JOptionPane
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box.
	 * @param defaultOption the int of the default option desired
	 * @param isPopup whether or not the dialog should be a popup or internal dialog. 
	 * @param includeCancelOrNot Added in 0.9.2 10:23 to allow canceling if desired.
	 * Should accept only JOptionPane.YES_NO_OPTION or JOptionPane.YES_NO_CANCEL_OPTION
	 * @since 0.9.0 (15.4.2020 21:42)
	 * @return the int of the choice
	 */
	public static int executeYesNoQuestion(JPanel parent, String title, String message, 
			int defaultOption, boolean isPopup, int includeCancelOrNot) {
		//
		if (isPopup) {
			return JOptionPane.showOptionDialog(parent, message, title,
					includeCancelOrNot, JOptionPane.QUESTION_MESSAGE, null, null, defaultOption);
		}
		else {
			return JOptionPane.showInternalOptionDialog(parent, message, title,
					includeCancelOrNot, JOptionPane.QUESTION_MESSAGE, null, null, defaultOption);
		}
	}

	/**
	 * Show a yes and no question dialog with buttons
	 * for yes and no.
	 * @apiNote Added the JPanel parameter in 0.9.0, 15.4.2020 21:41
	 * @apiNote separated to overloaded methods 0.9.0 15.4.2020 21:41
	 * @implNote 0.9.0 15.4.2020 21:41 - Changed the creation of JPanel panel
	 * to use the new generatePanel method, with parent as the parent,
	 * @param parent The panel in which the dialog will show. or null
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box.
	 * @param defaultOption the int of the default option desired
	 * @since 0.8.0 (10.4.2020 17:14)
	 * @return the int of the choice
	 */
	public static int executeYesNoQuestion(JPanel parent, String title, String message, int defaultOption) {
		//
		return executeYesNoQuestion(parent, title, message, defaultOption, true, JOptionPane.YES_NO_CANCEL_OPTION);
	}

	/**
	 * Show a plain message dialog
	 * @apiNote Decided to add it to allow to present a variable length 
	 * string array if necessary. <br>
	 * Also to migrate it from UserMaker and to make it more concise
	 * @apiNote separated to overloaded methods 0.9.1 (15.4.2020 23:34)
	 * @implNote added the parent JPanel 0.9.1 15.4.2020 23:37
	 * @param parent TODO
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box. variable length array
	 * to show many strings if necessary
	 * @since 0.9.1 (15.4.2020 23:34)
	 */
	public static void showPlainMessage(JPanel parent, String title, boolean isPopup, String... message) {
		if (isPopup) {
			JOptionPane.showMessageDialog(parent,
					message,
					title,
					JOptionPane.PLAIN_MESSAGE);
		}
		else {
			JOptionPane.showInternalMessageDialog(parent,
					message,
					title,
					JOptionPane.PLAIN_MESSAGE);
		}
	}

	/**
	 * Show a plain message dialog
	 * @apiNote Decided to add it to allow to present a variable length 
	 * string array if necessary. <br>
	 * Also to migrate it from UserMaker and to make it more concise<br>
	 * @apiNote separated to overloaded methods 0.9.1 (15.4.2020 23:34)
	 * @implNote added the parent JPanel 0.9.1 15.4.2020 23:36
	 * @param parent TODO
	 * @param title the title of the dialog box.
	 * @param message the message of the dialog box. variable length array
	 * to show many strings if necessary
	 * @since 0.8.0 (10.4.2020 22:38)
	 */
	public static void showPlainMessage(JPanel parent, String title, String... message) {
		showPlainMessage(null, title, true, message);
	}

	/**
	 * @since 0.9.0 (14.4.2020 ~23:20)
	 * @apiNote Another delegation to make adding buttons
	 * to a parent panel and a button group more concise.
	 * @param parent The parent panel where the buttons
	 * are presented.
	 * @param group the ButtonGroup overseeing the buttons.
	 * @param buttons the buttons themselves.
	 * @param separateButtons a boolean whether the buttons should have space 
	 * (if available) between them
	 * @implNote The buttons array is of the AbstractButtons
	 * abstract class, to allow all extending button types to
	 * be added.
	 * @apiNote separated to overloaded methods 0.9.0 14.4.2020 23:19
	 */
	public static void populateWithButtons(JPanel parent, ButtonGroup group, AbstractButton[] buttons, boolean separateButtons) {
		/*
		 * Before the loop, know if separation is desired and
		 * if so which kind is needed.
		 */
		int layoutCode = -1;
		if (separateButtons) {
			if  (parent.getLayout() instanceof BoxLayout) {
				layoutCode = ((BoxLayout) parent.getLayout()).getAxis();
			}
		}
		for (int i = 0; i < buttons.length; i++) {
			parent.add(buttons[i]);
			group.add(buttons[i]);
			// If separate buttons is desired and we still have more buttons, add Glue
			// WE WILL IMPEMENT FOR BOXLAYOUT FOR NOW
			if (i < buttons.length - 1) {
				if (layoutCode == BoxLayout.X_AXIS) {
					parent.add(Box.createHorizontalGlue());
				}
				else if (layoutCode == BoxLayout.Y_AXIS) {
					parent.add(Box.createVerticalGlue());
				}
			}
		}
	}

	/**
	 * @since 0.9.0 (14.4.2020 ~22:35)
	 * @apiNote Another delegation to make adding buttons
	 * to a parent panel and a button group more concise.
	 * @param parent The parent panel where the buttons
	 * are presented.
	 * @param group the ButtonGroup overseeing the buttons.
	 * @param buttons the buttons themselves.
	 * @implNote The buttons array is of the AbstractButtons
	 * abstract class, to allow all extending button types to
	 * be added.
	 * @apiNote separated to overloaded methods 0.9.0 14.4.2020 23:19
	 */
	public static void populateWithButtons(JPanel parent, ButtonGroup group, AbstractButton[] buttons) {
		populateWithButtons(parent, group, buttons, true);
	}

	/**
	 * Pretty straightforward:
	 * Generate an array (internally it is first a list)
	 * with buttonNumber buttons that have the text and actionCommand
	 * specified by relevant String in buttonStrings.
	 * @since 0.9.0 15.4.2020 22:30
	 * @param buttonNumber the number of buttons to be created
	 * @param buttonStrings the texts and action commands of the buttons
	 * @return and array of JButtons with all the buttons
	 */
	public static JButton[] generateJButtons (int buttonNumber, String[] buttonStrings) {
		JButton[] buttons = new JButton[buttonNumber];
		for (int i = 0; i < buttonNumber; i++) {
			buttons[i] = new JButton();			
			buttons[i].setText(buttonStrings[i]);
			buttons[i].setActionCommand(buttonStrings[i]);
		}
		return buttons;		
	}

	/**
	 * Creates a simple GUI consisting of a top JFrame
	 * and one or more JPanels, and returns the top frame. <br>
	 * The first JPanel will be set as the contentPane of the JFrame,
	 * and all other JPanels will be added to it. <br>
	 * Allows to specify the types of the layout managers,
	 * however, if any of the LayoutManagerType array is null,
	 * will default to BorderLayout for the contentPane panel,
	 * and BoxLayout for the others with X_AXIS. <br>
	 * Also allows to specify the layout constraints on all
	 * the children panels. If null will default to:
	 * South, North, Center, West, East, and back again.
	 * @since 0.9.0 (15.4.2020 12:05)
	 * @param title The title of the top frame.
	 * @param width The width of the top frame.
	 * @param height The height of the top frame.
	 * @param panelNumber The number of panels desired.
	 * First one is always set to be the frame's contentPane.
	 * Any number less than or equal to 1 is treated as 1.
	 * @param layoutManagers The array specifiying the layout managers.
	 * Accepts {@link LayoutManagerType#BORDER}, {@link LayoutManagerType#FLOW},
	 * {@link LayoutManagerType#BOXX}, {@link LayoutManagerType#BOXY} for now.
	 * If null, or somehow illegal (wrong size of array) will default to
	 * Behavior specified above.
	 * @param panelConstraints The array specifiying the layout constraints.
	 * If length is panelNumber - 1, it will apply as expected to the 
	 * children panel. If length is panelNumber THE FIRST ENTRY WILL BE IGNORED!
	 * If null of somehow illegal, will default to behavior specified above.
	 * @return the top frame.
	 */
	public static JFrame generateSimpleGUI(String title, int width, int height,
			int panelNumber, LayoutManagerType[] layoutManagers, String[] panelConstraints) {
		JFrame topFrame = generateFrame(title, width, height); // generates the top frame
		// if panelNumber less than 1 it is turned to 1
		if (panelNumber < 1) {
			panelNumber = 1;
		}
		JPanel[] panels = new JPanel[panelNumber]; // create an array of panels
		/*
		 * To avoid null pointer lykicheewi.burtefrahouse.exceptions, if the arrays are null, initialize arrays
		 * filled with nulls instead
		 */
		if (layoutManagers == null || layoutManagers.length != panelNumber) {
			layoutManagers = new LayoutManagerType[panelNumber];
		}
		if (panelConstraints == null 
				|| (panelConstraints.length != panelNumber && panelConstraints.length != panelNumber - 1)) {
			panelConstraints = new String[panelNumber];
		}
		// if panelConstraints' length is panelNumber - 1, prepend with null for index purposes
		if (panelConstraints.length == panelNumber - 1) {
			panelConstraints = ArrayTools.prepend(panelConstraints, null);
		}		
		// Iterate through the arrays and do the necessary things
		for (int i = 0; i < panelNumber; i++) {
			// The first panel is the contentPane and treated differently
			if (i == 0) {
				if (layoutManagers[i] == null) {
					panels[i] = generatePanel(topFrame, LayoutManagerType.BORDER); // default layout manager
				}
				else {
					panels[i] = generatePanel(topFrame, layoutManagers[i]);
				}
				topFrame.setContentPane(panels[i]);
			}
			else {
				if (panelConstraints[i] == null) {
					switch ((i - 1) % 5) { // switch case for the defaults
					case 0:
						panelConstraints[i] = BorderLayout.SOUTH;
						break;
					case 1:
						panelConstraints[i] = BorderLayout.NORTH;
						break;
					case 2:
						panelConstraints[i] = BorderLayout.CENTER;
						break;
					case 3:
						panelConstraints[i] = BorderLayout.WEST;
						break;
					case 4:
						panelConstraints[i] = BorderLayout.EAST;
						break;
					}
				}
				if (layoutManagers[i] == null) {
					panels[i] = generatePanel(panels[0], LayoutManagerType.BOXX, panelConstraints[i]); // default layout manager
				}
				else {
					panels[i] = generatePanel(panels[0], layoutManagers[i], panelConstraints[i]); // default layout manager
				}

			}
		}

		return topFrame;
	}

	/**
	 * Using a JFileChooser object, this method opens a dialog window which allows to choose only files,
	 * waits for the user to choose a file, and then returning it.
	 * @apiNote OVERLOADED 20.4.2020 14:21
	 * @return Returns the file as a File Object.
	 * @throws IOException IDE insisited upon adding new File(".").getCanonicalPath()
	 * @since 0.9.1 (15.4.2020 23:52)
	 */
	public static File chooseFileName(JPanel parent) throws IOException {
		return chooseFileName(parent, null);
	}

	/**
	 * Using a JFileChooser object, this method opens a dialog window which allows to choose only files,
	 * waits for the user to choose a file, and then returning it.
	 * @param defaultFile default file name
	 * @return Returns the file as a File Object.
	 * @throws IOException IDE insisited upon adding new File(".").getCanonicalPath()
	 * @since 0.9.2 (20.4.2020 14:19)
	 */
	public static File chooseFileName(JPanel parent, String defaultFile) throws IOException {
		/*
		 * A lot of this method is from Oracle's tutorials
		 */
		// Create a file chooser
		/*
		 * The JFileChooser allows us to open up a dialog window for choosing Files and/or
		 * directories. With this, we can choose the relevant directory that houses the data we need.
		 * As an added bonus I use new File(".").getCanonicalPath() to get the current directory
		 * to act as the starting directory. 
		 * Solution previously acquired from https://www.technicalkeeda.com/java-tutorials/get-current-file-path-in-java.
		 */
		final JFileChooser fc = new JFileChooser(new File(".").getCanonicalPath());
		if (defaultFile != null) {
			fc.setSelectedFile(new File(defaultFile));
		}
		// Makes the FileChooser be able to select only Files
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		/*
		 * In order to be able to allow for a user that accidentally 
		 * closed the dialog to be able to have a chance at choosing a directory, we
		 * are using another Class from the javax.swing: JOptionePane.
		 * We are using it to present a yes/no question whose input we use as a control
		 * variable in a while loop, allowing someone that repeatedly accidentally exits
		 * to get another chance.
		 */
		int userChoice = JOptionPane.YES_OPTION; // JOptionePane comes with Constant variables for its options. We're initialising to YES
		// YES continues the loop.
		while (userChoice == JOptionPane.YES_OPTION) {
			/*
			 * Now show the dialog window. The showDialog method
			 * returns an int that signifies whether the Choose Directory
			 * was chosen, or the dialog was exited another way. 
			 */
			int returnVal = fc.showDialog(parent, "Choose File");
			// In case the user chose a directory, return the File object representing it
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				return fc.getSelectedFile();
			}
			// Else, using the JOptionPane, allow the user another chance (YES), or get out of the while (NO)
			else {
				userChoice = JOptionPane.showOptionDialog(parent,
						"Open command cancelled. Would you like to open the dialog window again?", "Try Again?",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
			} 
		}
		return null; // If didn't choose a directory, then the method returns null
	}
	
	/**
	 * @since 0.9.2 (20.4.2020 11:59)
	 * @param parent
	 * @return YES or NO
	 */
	public static int confirmCancel (JPanel parent) {
		return JOptionPane.showOptionDialog(parent,
				"Sure you want to cancel?", "cancel?",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, JOptionPane.YES_OPTION);
	}

	/**
	 * @param args
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// 
		//TEST
		int frameTest = 2;
		try {
			UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (frameTest == 1) {

			SwingUtilities.invokeLater(() -> {
				JFrame frame = generateFrame("Hello", 800, 650);
				// Create a panel and add components to it.
				JPanel contentPane = generatePanel(frame, LayoutManagerType.BORDER);
				JPanel bottomButtonPanel = generatePanel(contentPane, LayoutManagerType.BOXX, BorderLayout.SOUTH);
				ButtonGroup bottomButtons = new ButtonGroup();
				JButton action1 = new JButton("This is a left button");
				JButton action2 = new JButton("This is a right button");
				JButton action3 = new JButton("This is a central button");
				populateWithButtons(bottomButtonPanel, bottomButtons, new JButton[] {action1, action3, action2});
				showFrame(frame, true);				

				action1.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						String mood = executeFreeTextDialog(contentPane, "Hi there", "What's up?", false);

					}
				});
				action2.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						String taste = executeRadioSelector(contentPane, "Taste", "What taste do you prefer?",
								new String[]{"Sweet", "Salty", "Sour", "Bitter", "Umami"}, false, true);

					}
				});



			});
		}
		else if (frameTest == 0) {

			JDialog dialog = generateDialog("What?", 800, 650);
			// Create a panel and add components to it.
			JPanel contentPane = new JPanel(new BorderLayout());
			dialog.setContentPane(contentPane);
			JPanel bottomButtonPanel = new JPanel();
			bottomButtonPanel.setLayout(new BoxLayout(bottomButtonPanel, BoxLayout.X_AXIS));
			ButtonGroup bottomButtons = new ButtonGroup();
			JButton action1 = new JButton("This is a left button");
			JButton action2 = new JButton("This is a right button");
			contentPane.add(bottomButtonPanel, BorderLayout.SOUTH);
			bottomButtonPanel.add(action1);
			bottomButtonPanel.add(action2);
			bottomButtons.add(action1);
			bottomButtons.add(action2);
			showDialog(dialog, true);
			String mood = executeFreeTextDialog(contentPane, "Hi there", "What's up?");
		}
		else if (frameTest == 2) {
			SwingUtilities.invokeLater(() -> {
				JFrame frame = generateSimpleGUI("Hello", 800, 650, 2,
						new LayoutManagerType[] {LayoutManagerType.BORDER, LayoutManagerType.BOXX},
						new String[] {BorderLayout.SOUTH});
				// Create a panel and add components to it.
				JPanel contentPane = (JPanel) frame.getContentPane();
				JPanel bottomButtonPanel = (JPanel) contentPane.getComponent(0);
				ButtonGroup bottomButtons = new ButtonGroup();
				JButton[] actions = generateJButtons(3,
						new String[] {"This is a left button", "This is a central button", "This is a right button"});
				populateWithButtons(bottomButtonPanel, bottomButtons, actions);
				showFrame(frame, true);

				actions[0].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						String mood = executeFreeTextDialog(contentPane, "Hi there", "What's up?", false);

					}
				});
				actions[2].addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						String taste = executeRadioSelector(contentPane, "Taste", "What taste do you prefer?",
								new String[]{"Sweet", "Salty", "Sour", "Bitter", "Umami"}, false, true);

					}
				});

			});
		}
	}

	/**
	 * @apiNote Added for support to MySQLConnector
	 * @since 0.9.2 (24.4.2020 11:57)
	 * @param parent
	 * @param title
	 * @param message
	 * @return
	 */
	public static String executePasswordDialog(JPanel parent, String title, String message, boolean isPopup) {
		/*
		 * Thank you Eng.Fouad in
		 * https://stackoverflow.com/questions/8881213/joptionpane-to-get-password
		 * for saving me the trouble of writing (most of) this method myself 
		 */
		JPanel panel = new JPanel();
		JLabel label = new JLabel(message);
		JPasswordField pass = new JPasswordField(20);
		panel.add(label);
		panel.add(pass);
		String[] options = new String[]{"OK", "Cancel"};
		int option = JOptionPane.NO_OPTION;
        if (isPopup) {
        	option = JOptionPane.showOptionDialog(parent, panel, title,
                    JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[1]);
        }
        else {
        	option = JOptionPane.showInternalOptionDialog(parent, panel, title,
                    JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, options, options[1]);
        }
		if(option == 0) // pressing OK button
		{
			return String.copyValueOf(pass.getPassword());
		}
		return null;
	}

	/**
	 * @apiNote Added for support to MySQLConnector
	 * @since 0.9.2 (24.4.2020 11:54)
	 * @param parent
	 * @param title
	 * @param message
	 * @return
	 */
	public static String executePasswordDialog(JPanel parent, String title, String message) {
		return executePasswordDialog(parent, title, message, true);
	}





}
