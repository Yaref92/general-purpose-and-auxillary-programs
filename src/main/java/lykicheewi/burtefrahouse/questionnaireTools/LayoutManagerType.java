/**
 * 
 */
package lykicheewi.burtefrahouse.questionnaireTools;

/** 
 * An Enumeration to assist in the generation of JPanels
 * in DialogKit. Helps with assigning one of
 * the LayoutManagers INCLUDING BoxLayout,
 * which needs the JPanel to be first initialized
 * and then assigned it.
 * @since 0.9.0 (14.4.2020 21:31)
 * @author Yaron Efrat
 */
public enum LayoutManagerType {
    /**
     * 
     */
	BORDER(1), // BorderLayout, receiving the value 1
    FLOW(2), // FlowLayout, receiving the value 2
    BOXX(3), // BoxLayout with the X_AXIS axis chosen, receiving the value 3
    BOXY(4), // BoxLayout with the Y_AXIS axis chosen, receiving the value 4
    /**
     * GRIDLAYOUT(1,0), receiving the value 5
     * @apiNote Added in the process of CHANGE202010021021
     * in order to assist with a scrollable JPanel of JPanels
     * @since 0.13.0 02.10.2020 11:18
     */
    GRIDX(5),
    /**
     * GRIDLAYOUT(0,1), receiving the value 6
     * @apiNote Added in the process of CHANGE202010021021
     * in order to assist with a scrollable JPanel of JPanels
     * @since 0.13.0 02.10.2020 11:18
     */
    GRIDY(6)
    ;
	
	private final int layoutCode;
	
	private LayoutManagerType(int code) {
		this.layoutCode = code;
	}

	/**
	 * @return the layoutCode
	 */
	public int getLayoutCode() {
		return layoutCode;
	}
}
