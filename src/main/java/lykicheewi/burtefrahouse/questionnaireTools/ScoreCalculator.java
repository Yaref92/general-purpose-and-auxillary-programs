/**
 * 
 */
package lykicheewi.burtefrahouse.questionnaireTools;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Element;
import org.xml.sax.SAXException;

import lykicheewi.burtefrahouse.fileTools.FileInputKit;
import lykicheewi.burtefrahouse.fileTools.XMLInputKit;
import lykicheewi.burtefrahouse.miscTools.XMLAuxTools.ExpandedElement;

/**
 * @author Yaron Efrat
 *
 */
public class ScoreCalculator {

	/*
	 * This map represents the scoring scheme 
	 * by associating each number with the scores 
	 * for each of its answers.
	 * This map has question numbers as keys,
	 * And another map of answer letter keys and score values for each value.
	 * It is static, because it should remain the same for the questionnaire, even when going through 
	 * several people's answers
	 * TODO MIGHT HAVE TO CHANGE IT TO A LIST OF HASHMAPS because of separation between mentors and mentees.
	 */
	static HashMap<Integer, HashMap<Character, Integer> > scoringMap;
	/*
	 * This map has question numbers as keys,
	 * And answer letter as values.
	 */
	HashMap<Integer, Character> answerMap;
	/*
	 * This map is obtained using the the previous two maps.
	 * It takes the answerMap, and then translates the Character into
	 * an Integer using the scoringMap
	 * UPDATE 0.5.0: Made it public
	 */
	public HashMap<Integer, Integer> achievedScores;
	/*
	 * 
	 */
	int totalScore;
	/*
	 * 
	 */


	/**
	 * Creating an instance of the scoreCalculator for a single user.
	 * The scoringMap is static and should only be created once.
	 * Should be accessible by all instances later.
	 * @param answerMap
	 */
	public ScoreCalculator(HashMap<Integer, Character> answerMap) {
		this.answerMap = answerMap;
		achievedScores = new HashMap<Integer, Integer>(); // Creates a new achievedScores map to be filled by the ScoreCalculator
		totalScore = 0; // Since it will be added to as a summing variable
	}




	/**
	 * If pathName is empty will use the default file name and otherwise the name
	 * specified by pathName 
	 * @param pathName - Can be either only the file name or the full path name. 
	 * FileInputKit can handle it with either option.
	 * @throws FileNotFoundException
	 */
	public ScoreCalculator(String pathName) throws FileNotFoundException {
		FileInputKit userAnswers;
		/*
		 * If the String is empty, use a default value.
		 */
		if (pathName.length() == 0) {			
			userAnswers = new FileInputKit("userAnswers.txt");
		}
		else {			
			userAnswers = new FileInputKit(pathName);
		}
		/*
		 * 
		 */
		generateAnswerMap(userAnswers);
		/*
		 * 
		 */
		achievedScores = new HashMap<Integer, Integer>(); // Creates a new achievedScores map to be filled by the ScoreCalculator
		totalScore = 0; // Since it will be added to as a summing variable
	}


	/**
	 * @since 0.10.0 (02.06.2020 ~10:18)
	 * @param userRow A ResultSet with the cursor
	 * pointing at the desired user
	 * @implNote IMPORTANT!! <br> 
	 * userRow is actually 
	 * the original ResultSet with all users of the type
	 * because extracting a specific row is not so trivial
	 * and creating additional queries is prone to bugs
	 * and also more convoluted. <br>
	 * BE WARNED: DO NOT ADJUST CURSOR POSITION 
	 * IN ScoreCalculator!
	 */
	public ScoreCalculator(ResultSet userRow, int numberOfQuestions) {
		/*
		 * A new method for extracting data
		 * from database ResultSet
		 */
		generateAnswerMap(userRow, numberOfQuestions);
		achievedScores = new HashMap<Integer, Integer>(); // Creates a new achievedScores map to be filled by the ScoreCalculator
		totalScore = 0; // Since it will be added to as a summing variable
	}




	/**
	 * This method uses the static scoringMap and the instance variable
	 * answerMap and uses their help to generate the achievedScores
	 * and the totalScore
	 */
	public void calculateScores () {
		/*
		 * Using the for (? : ?) we will be able to use the already existing answerMap
		 */
		for (Integer questionNumber : answerMap.keySet()) {
			Character chosenAnswer = answerMap.get(questionNumber); // First get the selected answer
			/*
			 * Had to update to include the handling of a default value,
			 * as expanded upon in FIX202009241423 in 
			 * LimeSurveyCommunicator.
			 * @since 0.12.0 24.09.2020 14:25
			 */
			Integer associatedScore; // Then get the relevant score from scoringMap
			if (chosenAnswer == 'z') {
				/*
				 * For now, it is a hardcoded value.
				 * eventually though, it will be part of 
				 * the scoring scheme.
				 */
				associatedScore = 0;
			}
			else {
				associatedScore = scoringMap.get(questionNumber).get(chosenAnswer);
			}
			achievedScores.put(questionNumber, associatedScore); // Then put the pair (questionNumber, associatedScore) in the achievedScores
			totalScore += associatedScore; // And also add the score to the totalScore
		}
	}




	/**
	 * Pretty similar to generateScoringMap, but simpler and obviously not static.
	 * @param userAnswers - The FileInputKit relating to the txt file 
	 * containing the user's answers
	 */
	private void generateAnswerMap(FileInputKit userAnswers) {
		answerMap = new HashMap<Integer, Character>();
		while (userAnswers.inputScanner.hasNext()) {
			/*
			 * First of all, we parse the String from readNext to Integer. readNext defaults to 
			 * flushing the newline separator.
			 */
			Integer currentQuestion = Integer.parseInt(userAnswers.readNext());
			/*
			 * We then readNext, expecting it to be a String of length one and even if not,
			 * then it takes the first character. AGAIN: readNext defaults to 
			 * flushing the newline separator.
			 */
			Character currentAnswer = Character.valueOf(userAnswers.readNext().charAt(0));
			/*
			 * Now we should check if the current question and current answer are legal,
			 * i.e. there is such a question number as currentQuestion and it
			 * does contain the currentAnswer as a possibility.
			 * If so put the question answer pair in the map
			 */
			if (scoringMap.containsKey(currentQuestion)) {
				if (scoringMap.get(currentQuestion).containsKey(currentAnswer)) {
					answerMap.put(currentQuestion,currentAnswer);
				}
			}			
		}
	}

	/**
	 * Pretty similar to generateScoringMap, but simpler and obviously not static.
	 * @since 1.2.0 (02.06.2020 10:54)
	 * @param userRow
	 * @param numberOfQuestions
	 */
	private void generateAnswerMap(ResultSet userRow, int numberOfQuestions) {
		answerMap = new HashMap<Integer, Character>();
		// Answer_to_question_
		String columnNameTemplate = "Answer_to_question_"; 
		// currentQuestion starts at 1 because counting in SQL starts at 1
		for (Integer currentQuestion = 1; currentQuestion <= numberOfQuestions; currentQuestion++) {
			Character currentAnswer = null;
			/*
			 * We use the ResultSet to extract the String,
			 * expecting it to be a String of length one and even if not,
			 * then it takes the first character. 
			 */
			try {
				currentAnswer = userRow.getString(columnNameTemplate + currentQuestion.toString()).charAt(0);
			} catch (SQLException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
			/*
			 * Now we should check if the current question and current answer are legal,
			 * i.e. there is such a question number as currentQuestion and it
			 * does contain the currentAnswer as a possibility.
			 * If so put the question answer pair in the map
			 */
			if (scoringMap.containsKey(currentQuestion)) {
				if (scoringMap.get(currentQuestion).containsKey(currentAnswer)) {
					answerMap.put(currentQuestion,currentAnswer);
				}
				/*
				 * I had a bug where some answers returned
				 * '-' instead of a letter and that
				 * ruined everything.
				 * For now I will make an else that puts
				 * 'a' in such cases.
				 * We'll figure it out later
				 * 1.5.1 09.09.2020 12:24
				 * TEMPFIX202009091224
				 * TODO Find a better solution
				 */
				else {
					answerMap.put(currentQuestion,'a');
				}
			}
		}

	}




	/**
	 * This method will use the FileInputKit of the scoringScheme
	 * to generate a scoreMap.
	 * It will do so by taking a question number as key,
	 * Take the rest until safe word as a token which breaks down 
	 * to another map of answer letter keys and score values.
	 * It continues until reaching the end of the file
	 * @param scoringScheme The FileInputKit relating to the txt file 
	 * containing the scoring scheme
	 */
	private static void generateScoringMap(FileInputKit scoringScheme) {

		scoringMap = new HashMap<Integer, HashMap<Character,Integer>>();
		while (scoringScheme.inputScanner.hasNext()) {
			/*
			 * First of all, we parse the String from readNext to Integer. readNext defaults to 
			 * flushing the newline separator.
			 * We then insert it as a key with a new HashMap as value
			 */
			Integer currentQuestion = Integer.parseInt(scoringScheme.readNext());
			scoringMap.put(currentQuestion, new HashMap<Character,Integer>());
			/*
			 * Next, we will tokenize the String until the safe word so we can put its constituent tokens 
			 * into the map in the value
			 */
			scoringScheme.tokenizeUntilSafeWord();
			/*
			 * Now, it goes through the tokenizer, 2 by 2, taking the "first char of the next String" as the key, and then the
			 * integer as the value until the tokenizer reaches the end
			 */
			while (scoringScheme.hasMoreTokens()) {
				Character currentAnswer = Character.valueOf(scoringScheme.getNextToken().charAt(0));
				scoringMap.get(currentQuestion).put(currentAnswer, Integer.parseInt(scoringScheme.getNextToken()));
			}
		}

	}

	/**
	 * This method will use the XMLInputKit of the scoringScheme
	 * to generate a scoreMap.
	 * It will do so by taking a question number as key,
	 * and the value as another map of answer letter keys 
	 * and score values.
	 * @since 0.11.0 (25.07.2020 11:57)
	 * @param scoringScheme The XMLInputKit relating to the xml file 
	 * containing the scoring scheme
	 */
	private static void generateScoringMapFromXML(XMLInputKit scoringScheme) {
		// Auto-generated method stub
		scoringMap = new HashMap<Integer, HashMap<Character,Integer>>();
		HashMap<Element, ArrayList<ExpandedElement>> scoringElementMap = scoringScheme.getElementMap();
		/*
		 * I take the collection of children (as ExpandedElement) of the root.
		 * Those are the questions.
		 * I iterate over them, get the question number and then iterate over
		 * the answers
		 */
		for (ExpandedElement question : scoringElementMap.get(scoringScheme.getRootElement())) {
			Integer currentQuestion = Integer.parseInt(question.getAttributeMap().get("number"));
			scoringMap.put(currentQuestion, new HashMap<Character,Integer>());
			/*
			 * I take the collection of children (as ExpandedElement) of the question.
			 * Those are the answers.
			 * I iterate over them to get the answer letter and the score
			 */
			for (ExpandedElement answer : scoringElementMap.get(question.getTheElement())) {
				Character currentAnswer = answer.getAttributeMap().get("letter").charAt(0);
				Integer currentScore = Integer.parseInt(answer.getData());
				scoringMap.get(currentQuestion).put(currentAnswer, currentScore);
			}

		}
	}




	/**
	 * @since 0.5.0 to help with MMMTPrototype
	 * @return - the size (number of key-value mappings) of scoringMap
	 */
	public static int getScoringMapSize() {
		// Auto-generated method stub
		return scoringMap.size();
	}




	/**
	 * Should actually be used, and not main.
	 * main is for testing
	 * @param pathName
	 * @param fromXML A boolean for whether we are using an XML file
	 * for the scoring scheme or not
	 * @apiNote Added the XML functionality on 0.11.0 25.07.2020 11:50
	 * @throws FileNotFoundException
	 */
	public static void initializeCalculator (String pathName, boolean fromXML) throws FileNotFoundException {
		/*
		 * The scoringScheme is expected to 
		 * have the question number, followed by the scoring rules for that question
		 * which are formatted as "answer_letter score"
		 * followed by the safe word "question_end"
		 */
		FileInputKit scoringScheme = null;
		/*
		 * If there are no command line arguments, then the default filenames
		 * "scoringScheme.txt" and "userAnswers.txt"
		 * are assumed and it is also assumed they are in the same directory as this
		 * file.
		 * Otherwise, will use the command line arguments.
		 * The safe word for scoringScheme is "question_end"
		 */
		if (pathName.length() == 0) {
			/*
			 * if added 0.11.0 25.07.2020 11:52
			 */
			if (!fromXML) {
				scoringScheme = new FileInputKit("scoringScheme.txt", "question_end");
			}
			else {
				try {
					//					scoringScheme = new XMLInputKit("scoringScheme.xml");
					/*
					 * Since I had to update it to work with the web front end, using any
					 * GUI element here will RAISE AN EXCEPTION
					 * so I had to change it (2.0.0 02.11.2020 10:49)
					 * to a hardcoded file name FOR NOW.
					 * Later the front end can handle this.
					 * FIX202011021039
					 */
					scoringScheme = new XMLInputKit("scoringScheme.xml", true, true);
				} catch (ParserConfigurationException e) {
					// Auto-generated catch block
					e.printStackTrace();
				} catch (SAXException e) {
					// Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		else {
			/*
			 * if added 0.11.0 25.07.2020 11:52
			 */
			if (!fromXML) {
				scoringScheme = new FileInputKit(pathName, "question_end");
			}
			else {
				try {
					//					scoringScheme = new XMLInputKit(pathName);
					/*
					 * Since I had to update it to work with the web front end, using any
					 * GUI element here will RAISE AN EXCEPTION
					 * so I had to change it (2.0.0 02.11.2020 11:05)
					 * to a hardcoded file name FOR NOW.
					 * Later the front end can handle this.
					 * FIX202011021039
					 */
					scoringScheme = new XMLInputKit(pathName, true, true);
				} catch (ParserConfigurationException e) {
					// Auto-generated catch block
					e.printStackTrace();
				} catch (SAXException e) {
					// Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		/*
		 * 
		 */
		/*
		 * if added 0.11.0 25.07.2020 11:52
		 */
		if (!fromXML) {
			generateScoringMap(scoringScheme);
		}
		else {
			generateScoringMapFromXML((XMLInputKit) scoringScheme);
		}

	}




	/**
	 * This method simply prints the achievedScores map in a 
	 * comfortable way and adds the totalScore after that.
	 * Since I have done it in such a way that it has a rising number for the questions
	 * that is how I will access it: using rising counter, instead
	 * of fighting with the HashMap over order, or going to 
	 * create LinkedHashMap for insertion order and whatnot
	 * Not worth the risk or effort and can always be changed
	 * <br> {@literal} Change if desirable
	 */
	public void printAchievedScores() {
		int i = 1;
		System.out.println("-------------------------------------------");
		System.out.println("The user's scores for each question are as follows:");
		while (achievedScores.containsKey(i)) {
			System.out.println("-------------------------------------------");
			System.out.println("The score received for Question number " + i + " is: " + achievedScores.get(i));
			i++;
		}
		System.out.println("-------------------------------------------");
		System.out.println("The total score for the questionnaire is: " + totalScore);
		System.out.println("-------------------------------------------");

	}




	/**
	 * This method simply prints the answer map in a 
	 * comfortable way.
	 * Since I have done it in such a way that it has a rising number for the questions
	 * that is how I will access it: using rising counter, instead
	 * of fighting with the HashMap over order, or going to 
	 * create LinkedHashMap for insertion order and whatnot
	 * Not worth the risk or effort and can always be changed
	 * <br> {@literal} Change if desirable
	 */
	public void printAnswerMap() {
		int i = 1;
		System.out.println("-------------------------------------------");
		System.out.println("The user's answers are as follows:");
		while (answerMap.containsKey(i)) {
			System.out.println("-------------------------------------------");
			System.out.println("Question number " + i + "'s chosen answer is: " + answerMap.get(i));
			i++;
		}
		System.out.println("-------------------------------------------");
	}




	/**
	 * This method simply prints the scoring map in a 
	 * comfortable way.
	 * Since I have done it in such a way that it has a rising number for the questions and 
	 * rising char for the answers that
	 * is how I will access it: using rising counter, instead
	 * of fighting with the HashMap over order, or going to 
	 * create LinkedHashMap for insertion order and whatnot
	 * Not worth the risk or effort and can always be changed
	 * <br> {@literal} Change if desirable
	 */
	public static void printScoringMap() {
		int i = 1;
		char c = 'a';
		while (scoringMap.containsKey(i)) {
			System.out.println("-------------------------------------------");
			System.out.println("Question number " + i + "'s scoring scheme:\n");
			while (scoringMap.get(i).containsKey(c)) {
				System.out.println("Score associated with answer " + c + " is: " + scoringMap.get(i).get(c));
				c++;
			}
			c = 'a';
			i++;
		}
		System.out.println("-------------------------------------------");
	}


	/**
	 * @param args
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException {
		/*
		 * The scoringScheme is expected to 
		 * have the question number, followed by the scoring rules for that question
		 * which are formatted as "answer_letter score"
		 * followed by the safe word "question_end"
		 */
		FileInputKit scoringScheme;		
		/*
		 * If there are no command line arguments, then the default filenames
		 * "scoringScheme.txt" and "userAnswers.txt"
		 * are assumed and it is also assumed they are in the same directory as this
		 * file.
		 * Otherwise, will use the command line arguments.
		 * The safe word for scoringScheme is "question_end"
		 * NOTTODO Make it more robust for cases where there are more or less than 2 command line arguments
		 */
		if (args.length == 0) {
			scoringScheme = new FileInputKit("scoringScheme.txt", "question_end");
		}
		else {
			scoringScheme = new FileInputKit(args[0], "question_end");
		}
		/*
		 * 
		 */
		generateScoringMap(scoringScheme);
		/*
		 * For testing I would like to see the results
		 */
		printScoringMap();

	}

	/**
	 *
	 * @apiNote Generated as hotfix for
	 * selectCandidates to be able to get
	 * match score. Might remove it
	 * and/or truly fix the problem 
	 * @since 2.0.0 (18.11.2020 10:24:10 AM)
	 * @param scoringMap the scoringMap to set
	 */
	public static void setScoringMap(HashMap<Integer, HashMap<Character, Integer>> scoringMap) {
		ScoreCalculator.scoringMap = scoringMap;
	}




	/**
	 * Calculates the highest difference that
	 * can exist for the question represented by
	 * questionNumber
	 * @apiNote Added as part of the attempt
	 * to have match scores
	 * ADD202011130958
	 * @since 0.16.0 (13.11.2020 10:41:17 AM)
	 * @param questionNumber
	 * @return
	 */
	public static int calculateQuestionMaxDiff(int questionNumber) {
		Integer[] questionScores = ScoreCalculator.scoringMap.get(questionNumber).values().toArray(new Integer[1]);
		int max = Integer.MIN_VALUE, min = Integer.MAX_VALUE;
		for (int i = 0; i < questionScores.length; i++) {
			if(questionScores[i] > max) {
				max = questionScores[i];
			}
			if(questionScores[i] < min) {
				min = questionScores[i];
			}
		}
		return max - min;
	}

}
