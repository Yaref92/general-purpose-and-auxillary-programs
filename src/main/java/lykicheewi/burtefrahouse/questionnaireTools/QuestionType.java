/**
 * 
 */
package lykicheewi.burtefrahouse.questionnaireTools;

/**
 * Created to accommodate a requirement in the MMMT
 * see CHANGE202009301121 in MMMTAdminGUI.
 * Generalizes the question types (inspired by
 * LimeSurvey's type) into the 4 broad categories
 * of List question (basically single choice
 * questions)
 * Array questions (questions with arrays of 1-5.
 * Excluding arrays of free text etc.)
 * Free text questions (including free text
 * arrays)
 * and Multiple choice questions
 * @since 0.13.0 (30.09.2020 11:26:34 AM)
 * @author Yaron Efrat
 */
public enum QuestionType {
	/**
	 * All list type questions, receiving the value "lis"
	 */
	LIST("lis"), 
	/**
	 * All array type questions, receiving the value "arr"
	 */
    ARRAY("arr"),
    /**
     * All free text type questions, receiving the value "txt"
     */
    FREETEXT("txt"),
    /**
     * All multiple choice type questions, receiving the value "mul"
     */
    MULTIPLECHOICE("mul")
    ;
	
	private final String typeString;
	
	private QuestionType(String code) {
		this.typeString = code;
	}

	/**
	 * @return the typeString
	 */
	public String getTypeString() {
		return typeString;
	}
}
