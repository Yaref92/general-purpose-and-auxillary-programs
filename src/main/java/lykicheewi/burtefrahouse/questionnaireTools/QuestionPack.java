/**
 * 
 */
package lykicheewi.burtefrahouse.questionnaireTools;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JPanel;

import lykicheewi.burtefrahouse.fileTools.FileInputKit;
import lykicheewi.burtefrahouse.miscTools.SerializeTools;

/** 
 * Inspired by the efforts to implement UserMaker
 * @since 0.8.0 (10.4.2020 19:28)
 * @implNote Have turned it into a Serializable object since
 * 0.10.0; 20.05.2020 22:06 (when I learned what serialization actually is)
 * @author Yaron Efrat
 */
public class QuestionPack implements Serializable {
    /**
	 * Auto generated serialVersionUID
	 */
	private static final long serialVersionUID = 8251277700725208741L;
	/**
	 * 
	 * @apiNote changed to private (03.08.2020 09:04)
	 */
	private ArrayList<String> titles; // A list containing the titles of the questions
	/**
	 * 
	 * @apiNote changed to private (03.08.2020 09:04)
	 */
    private ArrayList<String> messages; // A list containing the messages
    int questionCount;
    /**
     * 
     * @apiNote changed to private (03.08.2020 09:04)
     */
    private ArrayList<ArrayList<String>> possibleAnswers; // Each item in the list is the possible answers to the question with that ID
    /*
     * An input kit to receive all the questions.
     * Its file should follow the following format:
     * title
     * message
     * answer 1;
     * answer 2;
     * ...
     * answer n;
     * question_end
     */
    FileInputKit questionSource; // An input kit to
    /*
     * TODO Possibly also add an int for questionID to be pulled from 
     * Lime survey. It might even replace
     * the auto incrementation in the database.
     */
    
    /**
     * Added to accomodate a requirement in the MMMT
     * see CHANGE202009301121 in MMMTAdminGUI
     * @implNote Changed from String
     * to ArrayList<String> 12:35
     * @since 0.13.0 (30.09.2020 11:23)
     */
    private ArrayList<String> questionTypes;
    
    /**
	 * Added this to avoid making spelling mistakes anywhere in this class or elsewhere.
	 * this and the replacement of individual literal Strings will be given the name
	 * DEFSTRSWP202005242156
	 * @since 1.2.0 (24.05.2020 21:56)
	 */
	public static final String defaultQuestionPackFileName = "CurrentQuestionPack.ser";
    
	/**
	 * @param parent Added 0.9.1 15.4.2020 23:56 to allow
	 * to be incorporated into GUI apps
	 * @throws IOException 
	 * 
	 */
	public QuestionPack(JPanel parent) throws IOException {
		questionSource = new FileInputKit(parent, "question_end", "Question list.txt");
		initializeLists();
		questionCount = 0;
		processQuestions();
	}

	/**
	 * Added in case a null was handed to
	 * LimeSurveyCommunicator to load
	 * questions into. So that no
	 * NullPointerException occurs.
	 * We will just initialize the lists and
	 * that's it
	 * @since 0.12.0 (03.09.2020 16:10:39)
	 */
	public QuestionPack() {
		initializeLists();
	}

	/**
	 * @implNote Safer to have it private with a getter
	 * @since 0.11.0 (03.08.2020 09:01:44)
	 * @return the messages
	 */
	public ArrayList<String> getMessages() {
		return messages;
	}

	/**
	 * @implNote Safer to have it private with a getter
	 * @since 0.11.0 (03.08.2020 09:01:44)
	 * @return the possibleAnswers
	 */
	public ArrayList<ArrayList<String>> getPossibleAnswers() {
		return possibleAnswers;
	}

	/**
	 * @return the questionCount
	 */
	public int getQuestionCount() {
		return questionCount;
	}
	
	/**
	 * @implNote Safer to have it private with a getter
	 * @since 0.11.0 (03.08.2020 09:01:44)
	 * @return the titles
	 */
	public ArrayList<String> getTitles() {
		return titles;
	}

	/**
	 * A simple method to initialize the lists
	 */
	private void initializeLists() {
		titles = new ArrayList<String>();
		messages = new ArrayList<String>();
		possibleAnswers = new ArrayList<ArrayList<String>>();
		questionTypes = new ArrayList<String>(); // Added 0.13.0 30.09.2020 12:57 after questionTypes was added
	}

	public void processQuestions() {
		// We will also use this to keep track of the number of questions.
		/*
		 * You do a while loop so that it keeps running
		 * as long as input scanner has next
		 */
		while (questionSource.inputScanner.hasNext()) {
			titles.add(questionSource.readNextLine());
			messages.add(questionSource.readNextLine());
			/*
			 * Next, we will tokenize the String until the safe word so we can put its constituent tokens 
			 * into the List-inside-a-List that is possibleAnswers
			 */
			questionSource.tokenizeUntilSafeWord(";");
			/*
			 * Now, we initialize an internal answer list
			 */
			possibleAnswers.add(new ArrayList<String>());
			/*
			 * Now an internal while that runs as long as there are tokens
			 */
			while (questionSource.hasMoreTokens()) {
				possibleAnswers.get(questionCount).add(questionSource.getNextToken());
				/*
				 * It uses a questionCount that is one unit behind, but 
				 * arrays and arraylists start at 0 so that's fine
				 * We just put all tokens which are the relevant
				 * answer strings
				 */
			}
			/*
			 * Due to the way the FileInputKit's tokenization works,
			 * there are a lot of trailing whitespaces.
			 * I am going to add a method to FileInputKit
			 * called trimDelim to deal with trimming a delimiter
			 * from the beginning and end of a String in order to
			 * deal with it and use it here.
			 * Will for loop over possibleAnswers
			 */
			for (int i =0; i < possibleAnswers.get(questionCount).size(); i++) {
				possibleAnswers.get(questionCount).set(i, FileInputKit.trimDelim(possibleAnswers.get(questionCount).get(i), ' '));
			}
			/*
			 * Remove the last one if it is now empty after being trimmed
			 */
			if (possibleAnswers.get(questionCount).get(possibleAnswers.get(questionCount).size() - 1).isEmpty()) {
				possibleAnswers.get(questionCount).remove(possibleAnswers.get(questionCount).size() - 1);
			}
			questionCount++; // Incrementing the count
		}
	}

	/**
	 *
	 * @since 0.12.0 (03.09.2020 16:30:06)
	 * @param questionCount the questionCount to set
	 */
	public void setQuestionCount(int questionCount) {
		this.questionCount = questionCount;
	}

	/**
	 * Serializes the QuestionPack to be reused in subsequent
	 * occasions
	 * @since 0.10.0 (20.05.2020 22:09)
	 * @throws IOException
	 */
	public void serializeQuestionPack() throws IOException {
		SerializeTools.serializeToFile(this, defaultQuestionPackFileName); // DEFSTRSWP202005242156 - swapped from literal String to avoid spelling mistakes and to be more clear
	}
	
	/**
	 * Serializes the QuestionPack to be reused in subsequent
	 * occasions.<br>
	 * Was added because of the discovery of having separate
	 * questions for mentors and mentees.
	 * @since 0.11.0 (16.06.2020 21:41)
	 * @throws IOException
	 */
	public void serializeQuestionPack(String fileName) throws IOException {
		if (fileName.contains(".ser")) {
			SerializeTools.serializeToFile(this, fileName);
		}
		else {
			SerializeTools.serializeToFile(this, fileName + ".ser");
		}
	}

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TESTING GROUNDS
		QuestionPack testPack = new QuestionPack(null);
		System.out.println("QuestionPack before serialization:");
		System.out.println(testPack.toString());
		System.out.println("number of questions: " + testPack.getQuestionCount());
		System.out.println("1st question: " + testPack.messages.get(0));
		testPack.serializeQuestionPack();
		QuestionPack testPack2;
		testPack2 = (QuestionPack) SerializeTools.deserializeFromFile(defaultQuestionPackFileName); // DEFSTRSWP202005242156 - swapped from literal String to avoid spelling mistakes and to be more clear
		System.out.println("QuestionPack after deserialization:");
		System.out.println(testPack2.toString());
		System.out.println("number of questions: " + testPack2.getQuestionCount());
		System.out.println("1st question: " + testPack2.messages.get(0));

	}

	/**
	 *
	 * @since 0.13.0 (30.09.2020 11:24:07 AM)
	 * @return the questionType
	 */
	public ArrayList<String> getQuestionTypes() {
		return questionTypes;
	}

	/**
	 *
	 * @since 0.13.0 (30.09.2020 11:24:07 AM)
	 * @param questionTypes the questionType to set
	 */
	public void setQuestionTypes(ArrayList<String> questionTypes) {
		this.questionTypes = questionTypes;
	}
}
