/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import lykicheewi.burtefrahouse.miscTools.Converter;

/**
 *
 * @since 0.13.0 (08.10.2020 9:43:28 AM)
 * @author Yaron Efrat
 */
public class ConverterTest {

	/**
	 *
	 * @since 0.13.0 (08.10.2020 9:43:28 AM)
	 * @param args
	 */
	public static void main(String[] args) {
		Converter<String, Boolean> test1 = new Converter<String, Boolean>(){};
		System.out.print(test1.getTypeN().getSimpleName());
		Converter<Integer, String> test2 = new Converter<Integer, String>(){};
		String test3 = test2.convert(21);
		System.out.print(test3 + test1.convert(""));
	}

}
