/**
 * 
 */
package lykicheewi.burtefrahouse.miscTools;

import lykicheewi.burtefrahouse.miscTools.ArrayTools;

/**
 *
 * @since ?.?.? (08.10.2020 1:19:26 PM)
 * @author Yaron Efrat
 */
public class ArrayToolsTest {

	/**
	 *
	 * @since ?.?.? (08.10.2020 1:19:26 PM)
	 * @param args
	 */
	public static void main(String[] args) {
		String[] test1 = new String[4];
		ArrayTools.fillWithAscendingNumbers(test1, 5, String.class);
		ArrayTools.printArrayElements(test1);

	}

}
