package lykicheewi.burtefrahouse.questionnaireTools;

import java.io.FileNotFoundException;

import lykicheewi.burtefrahouse.questionnaireTools.ScoreCalculator;

public class scoringMapStaticTest {

	public scoringMapStaticTest() {
		// TODO Auto-generated constructor stub
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		ScoreCalculator.initializeCalculator("", false);
		ScoreCalculator test = new ScoreCalculator("");
		ScoreCalculator.printScoringMap();
		test.printAnswerMap();	
		test.calculateScores();
		test.printAchievedScores();
	}

}
