/**
 * 
 */
package lykicheewi.burtefrahouse.databaseTools;

import java.sql.SQLException;

import lykicheewi.burtefrahouse.databaseTools.DatabaseActions;
import lykicheewi.burtefrahouse.databaseTools.MySQLConnector;
import lykicheewi.burtefrahouse.databaseTools.SQLUser;

/** 
 * @since 0.10.0 (27.4.2020 ~10:30)
 * @author Yaron Efrat
 */
public class GeneralTest {

	/**
	 * 
	 */
	public GeneralTest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		//SQLUser currentUser = new SQLUser();
		SQLUser currentUser = new SQLUser("root", "fE^4lvER0hj094", true);
		MySQLConnector connector = new MySQLConnector(currentUser);
		System.out.println("The answer to whether " + currentUser.getUserName() + " exists is " +
		                 currentUser.isAnExistingUser(connector.getConn()));
		SQLUser fakeUser = new SQLUser("moot", "qua", true);
		System.out.println("The answer to whether " + fakeUser.getUserName() + " exists is " +
		                 fakeUser.isAnExistingUser(connector.getConn()));
		System.out.println("Attempting to connect with config file");
		MySQLConnector connector2 = new MySQLConnector();
		System.out.println("The column 'idnew_table' exists in this table? " + DatabaseActions.isExistingColumn(connector2.getConn(), "new_table", "idnew_table"));
		System.out.println("The column 'fake_column' exists in this table? " + DatabaseActions.isExistingColumn(connector2.getConn(), "new_table", "fake_column"));
	}

}
